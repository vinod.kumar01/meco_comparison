# -*- coding: utf-8 -*-
"""
Created on Sun Feb 21 10:56:06 2021

@author: Vinod
"""
import os
import numpy as np
import netCDF4
from datetime import datetime, timedelta
from tensorflow import keras
from matplotlib.colors import LinearSegmentedColormap
from scipy.interpolate import griddata
from ERA5.ERA5_tools import era2dict
from plot_tools import plt, map_prop, ccrs
from ML_tools import arrange_emis


def man_cm(vals, c_names):
    norm = plt.Normalize(min(vals), max(vals))
    tuples = list(zip(map(norm, vals), c_names))
    new_cm = LinearSegmentedColormap.from_list("", tuples)
    return new_cm


llon, ulon, llat, ulat = 0.6, 18.4, 43.6, 55.4
prop_dict = {'extent': [llon, ulon, llat, ulat], 'name': 'MECO_DE',
             'border_res': '10m'}
projection = ccrs.PlateCarree()
map2plot = map_prop(**prop_dict)

vals = [5e4, 1e5, 1.2e5, 1.5e5, 1.8e5, 2e5, 4e5, 5e5, 7e5]
c_names = ["azure", "deepskyblue", "aquamarine", "greenyellow", "darkorange",
           "orangered", "red", "darkred", "purple"]
new_cm = man_cm(vals, c_names)

# %% Tropomi data
month = 5
dirname_sim = r'M:\nobackup\vinod\model_work\MECO\sorbit_subsample\2018_05\UBA_fl\qa_75'
filename = 'meanVCD05_merge.nc'
with netCDF4.Dataset(os.path.join(dirname_sim, filename)) as f:
    tropcol_sat = f.variables['NO2_tropcol_sat'][:]
    lons = f.variables['lon'][:]
    lats = f.variables['lat'][:]
    timeax = f.variables['time'][:]
    timeax = np.array(timeax, dtype='f8')
    time_dsp = [datetime.strptime(f.variables['time'].units[11:30],
                                  '%Y-%m-%d %H:%M:%S') + timedelta(days=i)
                for i in timeax]

mean_tropomi = np.nanmean(tropcol_sat, axis=0)
with netCDF4.Dataset(r'M:\nobackup\vinod\model_work\MECO\sorbit_subsample\UBA_fl_05_sorbit_s5a.nc') as sim_c:
    hsurf = sim_c.variables['HSURF_ave'][:]

# %% Met data from ERA5 data
era_file_lvl = r'M:\nobackup\vinod\model_work\MECO\ERA5\2018_05_12_Meco_de_lev.nc'
params_lvl = {'wind_u': 'u',
              'wind_v': 'v',
              'sfc_temp': 't'}
era_data_lvl = era2dict(era_file_lvl, params_lvl)

era_file_sfc = r'M:\nobackup\vinod\model_work\MECO\ERA5\2018_05_12_Meco_de_sfc.nc'
params_sfc = {'sfc_press': 'sp',
              'pblh': 'blh'}
era_data_sfc = era2dict(era_file_sfc, params_sfc)
era_llat, era_llon = np.meshgrid(era_data_sfc['lon']['data'],
                                 era_data_sfc['lat']['data'])
date_display_era = datetime.strptime(era_data_sfc['time']['unit'][12:31],
                                     '%Y-%m-%d %H:%M:%S')
date_time_era = [date_display_era+timedelta(hours=int(dt))
                 for dt in era_data_sfc['time']['data']]
keep = np.asarray([i.month for i in date_time_era]) == month
keep &= np.asarray([i.hour for i in date_time_era]) == 12
met_regrid = {'flux_u_sfc': [], 'flux_v_sfc': [],
              'flux_u_500': [], 'flux_v_500': [],
              'pblh': [], 'ps_mean': [], 'sfc_temp': []}
era_prs_mean = np.nanmean(era_data_sfc['sfc_press']['data'][keep, :], 0)
era_pblh_mean = np.nanmean(era_data_sfc['pblh']['data'][keep, :], 0)
era_temp_mean = np.nanmean(era_data_lvl['sfc_temp']['data'][keep, -1, :], 0)
era_u_sfc = np.nanmean(era_data_lvl['wind_u']['data'][keep, -1, :], 0)
era_v_sfc = np.nanmean(era_data_lvl['wind_v']['data'][keep, -1, :], 0)
era_u_500 = np.nanmean(era_data_lvl['wind_u']['data'][keep, -3, :], 0)
era_v_500 = np.nanmean(era_data_lvl['wind_v']['data'][keep, -3, :], 0)


met_regrid['sfc_temp'] = griddata((era_llat.ravel(), era_llon.ravel()),
                                  era_temp_mean.ravel(), (lons, lats)) - 273.15
met_regrid['pblh'] = griddata((era_llat.ravel(), era_llon.ravel()),
                              era_pblh_mean.ravel(), (lons, lats))*1e-3
met_regrid['ps_mean'] = griddata((era_llat.ravel(), era_llon.ravel()),
                                 era_prs_mean.ravel(), (lons, lats))*1e-5
met_regrid['flux_u_sfc'] = griddata((era_llat.ravel(), era_llon.ravel()),
                                    era_u_sfc.ravel(), (lons, lats))*mean_tropomi
met_regrid['flux_v_sfc'] = griddata((era_llat.ravel(), era_llon.ravel()),
                                    era_v_sfc.ravel(), (lons, lats))*mean_tropomi
met_regrid['flux_u_500'] = griddata((era_llat.ravel(), era_llon.ravel()),
                                    era_u_500.ravel(), (lons, lats))*mean_tropomi
met_regrid['flux_v_500'] = griddata((era_llat.ravel(), era_llon.ravel()),
                                    era_v_500.ravel(), (lons, lats))*mean_tropomi

# %% restructure data
dataset2d = []
for i in range(2, mean_tropomi.shape[-2]-2):
    print(i)
    for j in range(2, mean_tropomi.shape[-1]-2):
        data_row = []
        # data_row.extend(vcd_mean[i, j].ravel())
        # data_row.extend(flux_u_sfc[i-2: i+3, j-2: j+3].ravel())
        # data_row.extend(flux_v_sfc[i-2: i+3, j-2: j+3].ravel())
        # data_row.extend(flux_u_500[i-2: i+3, j-2: j+3].ravel())
        # data_row.extend(flux_v_500[i-2: i+3, j-2: j+3].ravel())
        data_row.extend(mean_tropomi[i-2: i+3, j-2: j+3].ravel())
        data_row.extend(met_regrid['flux_u_sfc'][i, j].ravel())
        data_row.extend(met_regrid['flux_v_sfc'][i, j].ravel())
        data_row.extend(met_regrid['flux_u_500'][i, j].ravel())
        data_row.extend(met_regrid['flux_v_500'][i, j].ravel())
        data_row.extend([met_regrid['sfc_temp'][i, j]])
        data_row.extend([met_regrid['ps_mean'][i, j]])
        data_row.extend(hsurf[i-2: i+3, j-2: j+3].ravel())
        data_row.extend([met_regrid['pblh'][i, j]])
        dataset2d.append(data_row)
dataset2d = np.asarray(dataset2d)

# %% load DNN model
dnn_model = keras.models.load_model('UBA_05_06_02_model')
# %%make predictions
test_predictions = dnn_model.predict(dataset2d).flatten()

# %% add lat lon information to emissions
test_predictions_grid = test_predictions.reshape(176, 156)
# Plot predicted emissions
datadict = {'lat': lats[2:-2, 2:-2], 'lon': lons[2:-2, 2:-2],
            'vmin': 0, 'vmax': 1,
            'text': 'NOx emission flux (${\mu}g ~m^{-2} ~s^{-1}$)',
            'label': '', 'plotable': test_predictions_grid}
fig, ax = plt.subplots(subplot_kw=dict(projection=ccrs.PlateCarree()))
cs = map2plot.plot_map(datadict, ax, cmap=new_cm,
                       mode='pcolormesh',
                       projection=projection, alpha=1)
fig.canvas.draw()
plt.tight_layout()
fig.subplots_adjust(right=0.86, wspace=0.02)
cbar_ax = fig.add_axes([0.88, 0.2, 0.02, 0.6])
cbar = fig.colorbar(cs, cax=cbar_ax, extend='max', shrink=0.8)
cbar.set_label(datadict['text'], fontsize=10)
# %% Scatterplot wrt VCD/tau
# steday state emissiosn = VCD/tau, tau=4h
ss_emis = mean_tropomi*1e16*100*100/(4*3600)   # molecules/m2/s
ss_emis /= 6.0232e23   # moles/m2/s
ss_emis *= 46*1e6    # mu g/m2/s
ss_emis = ss_emis[2:-2, 2:-2].ravel()

fig, ax = plt.subplots()
hbin = ax.hexbin(ss_emis, test_predictions, mincnt=1, cmap='jet',
          gridsize=1000, vmin=1, vmax=20)
ax.set_xlabel('VCD/tau ($\mu$g m$^{-2}$ s$^{-1}$)')
ax.set_ylabel('Predicted emission ($\mu$g m$^{-2}$ s$^{-1}$)')
cb = plt.colorbar(hbin, ax=ax, orientation='vertical', shrink=0.9,
                  extend='max')
lims = [0, 1]
ax.set_xlim(lims)
ax.set_ylim(lims)
ax.grid(alpha=0.3)
ax.plot(lims, lims, alpha=0.5, c='k')
cb.set_label('Frequency')
# %% Scatterplot wrt UBA emissions used for training
# steday state emissiosn = VCD/tau, tau=4h
dirname = r'M:\nobackup\vinod\model_work\MECO\sorbit_subsample'
emis_file = 'UBA_TNO_2018_syn_sfc_remapcon.nc'
month = 5
flux_val = arrange_emis(os.path.join(dirname, 'emissions', emis_file),
                        month)

fig, ax = plt.subplots()
hbin = ax.hexbin(flux_val, test_predictions, mincnt=1, cmap='jet',
          gridsize=1000, vmin=1, vmax=20)
ax.set_xlabel('UBA emissions ($\mu$g m$^{-2}$ s$^{-1}$)')
ax.set_ylabel('Predicted emission ($\mu$g m$^{-2}$ s$^{-1}$)')
cb = plt.colorbar(hbin, ax=ax, orientation='vertical', shrink=0.9,
                  extend='max')
lims = [0, 1]
ax.set_xlim(lims)
ax.set_ylim(lims)
ax.grid(alpha=0.3)
ax.plot(lims, lims, alpha=0.5, c='k')
cb.set_label('Frequency')