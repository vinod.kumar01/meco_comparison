import netCDF4
import matplotlib.pyplot as plt
from plot_tools import map_prop
import cartopy.crs as ccrs
import numpy as np
import os

dirname = r'M:\nobackup\vinod\model_work\MECO\domain'
filename = 'hsurf_02'
f1 = netCDF4.Dataset(os.path.join(dirname, filename + ".nc"), 'r')
lon = f1.variables['philon_2d_ave'][:]
lat = f1.variables['philat_2d_ave'][:]
param = f1.variables['HSURF_ave']
V0 = param[:]
llat, ulat = np.floor(np.min(lat)), np.ceil(np.max(lat))
llon, ulon = np.floor(np.min(lon)), np.ceil(np.max(lon))
prop_dict = {
             # 'extent': [llon, ulon, llat, ulat],
             'extent': [0, 19, 43, 56],
             'name': filename,
             'border_res': '10m'}
map2plot = map_prop(**prop_dict)
projection = ccrs.PlateCarree()  #ccrs.Orthographic(8, 40)  # ccrs.PlateCarree()
fig, ax = plt.subplots(subplot_kw=dict(projection=projection))
datadict = {'lat': lat, 'lon': lon, 'plotable': V0,
            'vmin': -14, 'vmax': 3500, 'label': '',
            'text': 'Surface altitude (m)'}
scat = map2plot.plot_map(datadict, ax, cmap='terrain', projection=projection,
                         alpha=0.2)
f1.close()
'''
second layer
'''
filename2 = 'hsurf_03'
f2 = netCDF4.Dataset(os.path.join(dirname, filename2 + ".nc"), 'r')
lon2 = f2.variables['philon_2d_ave'][:]
lat2 = f2.variables['philat_2d_ave'][:]
V2 = f2.variables['HSURF_ave'][:]
datadict = {'lat': lat2, 'lon': lon2, 'plotable': V2,
            'vmin': -14, 'vmax': 3500, 'label': '',
            'text': 'Surface altitude (m)'}
scat = map2plot.plot_map(datadict, ax, cmap='terrain', projection=projection,
                         alpha=0.5)
f2.close()

'''
Third layer
'''
filename3 = 'hsurf_04'
f3 = netCDF4.Dataset(os.path.join(dirname, filename3 + ".nc"), 'r')
lon3 = f3.variables['philon_2d_ave'][:]
lat3 = f3.variables['philat_2d_ave'][:]
V3 = f3.variables['HSURF_ave'][:]
datadict = {'lat': lat3, 'lon': lon3, 'plotable': V3,
            'vmin': -14, 'vmax': 3500, 'label': '',
            'text': 'Surface altitude (m)'}
scat = map2plot.plot_map(datadict, ax, cmap='terrain', projection=projection,
                         alpha=1)
f3.close()
df = [{'lon': 8.2283, 'lat': 49.9909, 'site': 'MPIC'}]

for point in df:
    lo, la = point['lon'], point['lat']
    plt.annotate(point['site'], xy=(lo, la),  xycoords='data', size=14,
                 xytext=(lo, la), textcoords='data', color='white',
                 weight='bold',
                 horizontalalignment='left', verticalalignment='bottom',
                 arrowprops=dict(facecolor='black', shrink=0.05))
    plt.plot(lo, la, 'ro')
cb = fig.colorbar(scat, ax=ax, orientation='horizontal',
                  extend='max', fraction=0.03, shrink=0.5, pad=0.05)
cb.set_label(datadict['text'], fontsize=14)
angles = {'T1': 321, 'T2': 51, 'T3': 141, 'T4': 231}
length = 1.5   # 0.8
x0, y0 = df[0]['lon'], df[0]['lat']
for telescope, angle in angles.items():
    endx = length * np.sin(np.radians(angle))
    endy = length * np.cos(np.radians(angle))
    ax.plot([x0, x0+endx], [y0, y0+endy], c='k', linewidth=3)
    ax.annotate(telescope, xy=(x0+endx, y0+endy),  xycoords='data',
                xytext=(x0+endx, y0+endy), textcoords='data', color='white',
                horizontalalignment='left', verticalalignment='top', size=14,
                weight='bold',
                arrowprops=dict(facecolor='white', shrink=0.05))
plt.savefig(r'D:\postdoc\COSMO-CCLM\Manuscript_MECO_DOAS_eval\figures\fig1_right.png', dpi=300, format='png')
