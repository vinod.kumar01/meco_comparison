# -*- coding: utf-8 -*-
"""
Created on Sat Oct  3 18:02:38 2020

@author: Vinod
"""
# %% functions and imports
from os import path
import numpy as np
from geopy.distance import distance
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
from mod_colormap import add_white
from tools import tracer_prop
from plot_tools import plot_layer
import filetools as ft
from DOAS_compare.load_bamf import bAMF_mean


def plot_bAMF_2d(tel='T1', elev=30, ax='ax'):
    if np.min(bamf_now.T) < 0:
        replace_frac = -np.min(bamf_now) / (np.max(bamf_now) -
                                            np.min(bamf_now))
    else:
        replace_frac = 0
    mod_colormap = add_white('Reds', replace_frac=replace_frac,
                             start=None, transparent=False)
    scat = ax.pcolormesh(bAMF_day['displacement'][tel][45:70],
                         bAMF_day['h_grid'][:33], bamf_now[:25, :].T,
                         cmap=mod_colormap)
    ax.scatter(0, 0.05, s=60, marker='x', c='k')
    # ax.set_xlabel('Distance in viewing direction (Km)', size=14)
    # ax.set_ylabel('Altitude (Km)', size=14)
    dt_text = date_time[dt].strftime('%d-%m-%Y %H:%M')
    ax.text(0.01, 0.8, '{}\nelev = {}$^\circ$\n{}'.format(tel, elev, dt_text),
            size=12, color='k', transform=ax.transAxes)
    ax.minorticks_on()
    return(scat)


def plot_profile_2d(tel='T1', vmin=1.e-8, vmax=2.e11, ax='ax'):
    scat = ax.pcolormesh(bAMF_day['displacement'][tel][45:70],
                         bAMF_day['h_grid'][:33],
                         sim_conc[tel]['conc']['data'][dt, :25, :].T,
                         vmin=vmin, vmax=vmax, cmap='jet')
    ax.scatter(0, 0.05, s=60, marker='x', c='white')
    ax.set_xlabel('Distance in viewing direction (Km)', size=14)
    ax.set_ylabel('Altitude (Km)', size=14)
    ax.text(0.05, 0.9, tel, size=14, color='white', transform=ax.transAxes)
    ax.minorticks_on()
    return(scat)


def plot_profile_3d(tel='T1', unit='Distance', vmin=1.e-8, vmax=2.e11):
    if unit == 'Distance':
        fig, ax, cbar = plot_layer(lat_distance[tel][45:70],
                                   lon_distance[tel][45:70],
                                   bAMF_day['h_grid'][:33],
                                   sim_conc[tel]['conc']['data'][dt, :25, :].T,
                                   vmin=vmin, vmax=vmax)
        ax.scatter(0, 0, s=60, marker='x', c='k')
        ax.set_xlabel('Distance along Longitude (Km)')
        ax.set_ylabel('Distance along Latitude (Km)')
    else:
        fig, ax, cbar = plot_layer(bAMF_day['coords'][tel][45:70, 0],
                                   bAMF_day['coords'][tel][45:70, 1],
                                   bAMF_day['h_grid'][:33],
                                   sim_conc[tel]['conc']['data'][dt, :25, :].T,
                                   vmin=vmin, vmax=vmax)
        ax.scatter(8.2283, 49.9909, s=60, marker='x', c='k')
        ax.set_xlabel('Longitude')
        ax.set_ylabel('Latitude')

    ax.set_title('Simulated NO$_2$ for ' +
                 datetime.strftime(date_time[dt], '%d-%m-%Y %Hh UTC'))

    ax.set_zlabel('Altitude (Km)')
    cbar.set_label('NO$_2$ conc. (molecules cm $^{-3}$)')
    if tel in ["T1", "T3"]:
        ax.view_init(azim=-110, elev=30)
    else:
        ax.view_init(azim=-50, elev=30)
    plt.tight_layout()


no2 = {'name': 'NO2', 'label': 'NO$_2$', 'vcd_unit': 'molecules cm$^{-2}$',
       'conc_unit': 'molecules cm$^{-3}$',
       'dscd_lim': {30: [0, 3e16], 3: [0, 2.2e17]}}
hcho = {'name': 'HCHO', 'label': 'HCHO', 'vcd_unit': 'molecules cm$^{-2}$',
        'conc_unit': 'molecules cm$^{-3}$',
        'dscd_lim': {30: [-2e16, 8e16], 3: [-1e16, 1.e17]}}
# %% load data
tracer = tracer_prop(**no2)
dirname_meas = r'M:\nobackup\vinod\model_work\MECO\DOAS_COMP\4azim_doas'
dirname_sim = r'M:\nobackup\vinod\model_work\MECO\DOAS_COMP'
exp = 'UBA_di_2km'  # T42, T106, T42_diurnal, T106_7km, T106_rot
mpic = (49.9909, 8.2283)
sim_conc = ft.hdf2dict(path.join(dirname_sim,
                                 'sim_conc_{}_{}.hdf'.format(exp, tracer.name)))
dtlist = ['180509']
# bAMF_day = bamf_daylistloader(dtlist=dtlist)
bAMF_day = bAMF_mean(dtlist=dtlist, year=2018)
# %% processing and plotting
date_time = [datetime.strptime(sim_conc['T1']['date_time']['unit'],
                               '%Y-%m-%d %H:%M:%S') + timedelta(days=i)
             for i in sim_conc['T1']['date_time']['data']]
lon_distance, lat_distance = {}, {}
for tel in ['T1', 'T2', 'T3', 'T4']:
    lon_distance[tel] = np.array([distance(mpic, [mpic[0], i]).kilometers
                                 if i > mpic[1] else
                                 -distance(mpic, [mpic[0], i]).kilometers
                                 for i in bAMF_day['coords'][tel][:, 1]])
    lat_distance[tel] = np.array([distance(mpic, [i, mpic[1]]).kilometers
                                 if i > mpic[0] else
                                 -distance(mpic, [i, mpic[1]]).kilometers
                                 for i in bAMF_day['coords'][tel][:, 0]])
dt = 138
# concentration profiles
fig, ax = plt.subplots(2, 2, figsize=[10, 8], sharey=True)
#a = plt.suptitle('Simulated NO$_2$ for ' +
#             datetime.strftime(date_time[dt], '%d-%m-%Y %Hh UTC'))
for i, tel in enumerate(['T1', 'T2', 'T4', 'T3']):
    row = int(i/2)
    col = i % 2
    scat = plot_profile_2d(tel=tel, vmin=1.e-8, vmax=2.e11, ax=ax[row, col])
for i in range(2):
    ax[i, 1].set_ylabel('')
for i in range(2):
    ax[0, i].set_xlabel('')
plt.tight_layout()
fig.subplots_adjust(right=0.88, wspace=0.02)
cbar_ax = fig.add_axes([0.90, 0.15, 0.02, 0.7])
cbar = fig.colorbar(scat, cax=cbar_ax, extend='max')
cbar.set_label('NO$_2$ conc. (molecules cm $^{-3}$)', fontsize=12)
savestr = datetime.strftime(date_time[dt], '%Y%m%d_%H')
# plt.savefig(path.join(dirname_sim, 'plots',
#                       'simVCD_{}_xy.png'.format(savestr)), format='png', dpi=300)
# bAMF
for ei in [3, 5, 15, 30]:
    fig, ax = plt.subplots(2, 2, figsize=[10, 8], sharey=True)
    for i, tel in enumerate(['T1', 'T2', 'T4', 'T3']):
        row = int(i/2)
        col = i % 2
        try:
            bamf_now = bAMF_day['bAMF'][date_time[dt].strftime('%Y%m%d_%H%M')][tel][ei][45:125, :33]
        except IndexError:
            bamf_now = np.full([80, 33], np.nan)
        scat = plot_bAMF_2d(tel=tel, elev=ei, ax=ax[row, col])
    for i in range(2):
        ax[i, 1].set_ylabel('')
    for i in range(2):
        ax[0, i].set_xlabel('')
    plt.tight_layout()
    fig.subplots_adjust(right=0.88, wspace=0.02)
    cbar_ax = fig.add_axes([0.90, 0.15, 0.02, 0.7])
    cbar = fig.colorbar(scat, cax=cbar_ax, extend='max')
    cbar.set_label('differential box airmass factors', fontsize=12)
    # plt.savefig(path.join(dirname_sim, 'plots',
    #                       'bamf_{}_{}_2d.png'.format(ei, savestr)),
    #             format='png', dpi=300)
    plt.close()
fig, ax = plt.subplots(2, 3, figsize=[12, 7], sharey='row', sharex='col')
tel = 'T1'
for i, ei in enumerate([30, 15, 10, 8, 5, 3]):
    row = int(i/3)
    col = i % 3
    try:
        bamf_now = bAMF_day['bAMF'][date_time[dt].strftime('%Y%m%d_%H%M')][tel][ei][45:125, :33]
    except IndexError:
        bamf_now = np.full([80, 33], np.nan)
    scat = plot_bAMF_2d(tel=tel, elev=ei, ax=ax[row, col])
    cbar_ax = ax[row, col].inset_axes((0.88, 0.3, .02, .6))
    cbar = fig.colorbar(scat, cax=cbar_ax, extend='max')
    cbar.set_label('differential\nbox airmass factors', fontsize=12)
    cbar_ax.yaxis.set_label_position('left')
    cbar_ax.minorticks_on()
for i in range(2):
    ax[i, 1].set_ylabel('')
    ax[i, 2].set_ylabel('')
for i in range(3):
    ax[0, i].set_xlabel('')
fig.add_subplot(111, frameon=False)
# hide tick and tick label of the big axis
plt.tick_params(labelcolor='none', which='both', top=False, bottom=False, left=False, right=False)
plt.xlabel('Distance in viewing direction (Km)', size=14)
plt.ylabel('Altitude (Km)', size=14)
plt.tight_layout()
plt.savefig(path.join(dirname_sim, 'plots',
                      'bamf_{}_{}_2d.png'.format(tel, savestr)),
            format='png', dpi=300)

# %% plot of VCDs in all the 4 viewing directions
sim_conc_merge = sim_conc['T1']['conc']['data'][dt, 5:25, :][::-1, :].copy()
sim_conc_merge = np.append(sim_conc_merge, sim_conc['T3']['conc']['data'][dt, 5:25, :], axis=0)
sim_conc_merge[-1, :] = np.nan
sim_conc_merge = np.append(sim_conc_merge, sim_conc['T2']['conc']['data'][dt, 5:25, :][::-1, :], axis=0)
sim_conc_merge = np.append(sim_conc_merge, sim_conc['T4']['conc']['data'][dt, 5:25, :], axis=0)
sim_conc_merge *= 1e-11
#lats = bAMF_day['coords']['T1'][50:70, 0]
#lats = np.append(lats, bAMF_day['coords']['T3'][50:70, 0], axis=0)
#lats = np.append(lats, bAMF_day['coords']['T2'][50:70, 0], axis=0)
#lats = np.append(lats, bAMF_day['coords']['T4'][50:70, 0], axis=0)
#lons = bAMF_day['coords']['T1'][50:70, 1]
#lons = np.append(lons, bAMF_day['coords']['T3'][50:70, 1], axis=0)
#lons = np.append(lons, bAMF_day['coords']['T2'][50:70, 1], axis=0)
#lons = np.append(lons, bAMF_day['coords']['T4'][50:70, 1], axis=0)
lats = lat_distance['T1'][50:70][::-1]
lats = np.append(lats, lat_distance['T3'][50:70], axis=0)
lats = np.append(lats, lat_distance['T2'][50:70][::-1], axis=0)
lats = np.append(lats, lat_distance['T4'][50:70], axis=0)
lons = lon_distance['T1'][50:70][::-1]
lons = np.append(lons, lon_distance['T3'][50:70], axis=0)
lons = np.append(lons, lon_distance['T2'][50:70][::-1], axis=0)
lons = np.append(lons, lon_distance['T4'][50:70], axis=0)
#
fig, ax, cbar = plot_layer(lats, lons, bAMF_day['h_grid'][:33],
                            sim_conc_merge.T, vmin=1e-3, vmax=1.5)
# ax.view_init(azim = -70, elev = 70)  # default values (-60, 30) in degrees
ax.view_init(azim=-85, elev=70)
#a = ax.scatter(8.2283, 49.9909, s=200, marker='o', c='k')
a = ax.scatter(0, 0, s=200, marker='o', c='k')

#ax.set_title('Simulated NO$_2$ for ' +
#             datetime.strftime(date_time[dt], '%d-%m-%Y %Hh UTC'))
##ax.set_xlabel('Longitude')
##ax.set_ylabel('Latitude', labelpad=10)
ax.set_zlabel('Altitude (Km)')
ax.set_xlabel('Distance along longitude (Km)')
ax.set_ylabel('Distance along latitude (Km)', labelpad=10)
cbar.set_label('NO$_2$ conc. (10$^{11}$ molecules cm $^{-3}$)')
plt.tight_layout()
plt.savefig(path.join(dirname_sim, 'simVCD_all' + savestr+'_latlon.png'),
            format='png', dpi=300)
