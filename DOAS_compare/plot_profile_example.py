# -*- coding: utf-8 -*-
"""
Created on Mon Jan 18 14:22:20 2021

@author: Vinod
"""

import matplotlib.pyplot as plt
import numpy as np


def calc_profile(c, h, s, z, **kwargs):
    elev_profile = kwargs.get('elev_profile', 'box')
    if s == 1:  # Box profile
        if z <= h:
            p = c/h
        else:
            p = 0
    elif s < 1:
        p = s*c/h
        if z > h:
            p *= np.exp(-((z-h)/h)*(s/(1-s)))
    elif s > 1:
        if elev_profile == 'box':
            h1 = (s-1)/h
            h2 = (2-s)/h
            if z < h1:
                p = 0
            elif z > h:
                p = 0
            elif (h1 < z) & (z <= h):
                p = c/h2
            else:
                print('P undefined for z={} and s={}'.format(z, s))
        else:
            if z <= h:
                c0 = c-(s-1)
                c1 = c+(s-1)
                p = c0 + (c1-c0)*z/h
            else:
                p = 0
    return p


h = 1
S = {'Exp. decrease': 0.8, 'Box': 1, 'Elevated box': 1.2,
     'Linear increase': 1.2}
z_grid = np.arange(0, 3, 0.01)
c = 1
profile = {}
for s_name, s in S.items():
    key_now = 's = {} (\n{})'.format(s, s_name)
    if s_name == 'Linear increase':
        elev_profile = 'linear'
    else:
        elev_profile = 'box'
    profile[s_name] = [calc_profile(c, h, s, z, elev_profile=elev_profile)
                       for z in z_grid]

# %% plotting
fig, ax = plt.subplots()
for s_now, p_now in profile.items():
    ax.plot(p_now, z_grid, label='s = {} \n{}'.format(S[s_now], s_now))
    ax.fill_between(p_now, z_grid, np.zeros(len(z_grid)), z_grid, alpha=0.2)
ax.legend(prop={'size': 12})
ax.set_ylim(0, 2.5)
ax.set_xlim(0, 1.3)
bbox_props = dict(boxstyle="round", fc="w", ec="0.5", alpha=0.9)
ax.tick_params(axis='both', which='major', labelsize=12)
ax.annotate('AOD = 1.0\n h = 1.0', xy=(0.3, 0.9),
            xycoords='axes fraction', size=12, bbox=bbox_props)
ax.set_xlabel('Aerosol extinction (km $^{-1}$)', size=12)
ax.set_ylabel('Altitude (km)', size=12)
ax.minorticks_on()
plt.tight_layout()
# plt.savefig(r'D:\postdoc\COSMO-CCLM\Manuscript_MECO_DOAS_eval\figures\pimax_schematic.png',
#             format='png', dpi=300)
