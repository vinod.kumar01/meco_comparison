# -*- coding: utf-8 -*-
"""
Created on Mon May  4 22:00:15 2020

@author: Vinod
"""
import matplotlib.pyplot as plt
from os import path
import pandas as pd
import numpy as np
from glob import glob
from datetime import datetime, timedelta, date
import matplotlib.dates as mdates
import filetools as ft
from tools import doy_2_datetime, tracer_prop, lin_fit, load_cloud, wind
from plot_tools import plot_diurnal, plot_scatter_ax
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)


no2 = {'name': 'NO2', 'label': 'NO$_2$', 'vcd_unit': 'molecules cm$^{-2}$',
       'conc_unit': 'molecules cm$^{-3}$',
       'dscd_lim': {30: [0, 3e16], 3: [0, 2.2e17], 2: [0, 2.7e17]}}
hcho = {'name': 'HCHO', 'label': 'HCHO', 'vcd_unit': 'molecules cm$^{-2}$',
        'conc_unit': 'molecules cm$^{-3}$',
        'dscd_lim': {30: [-2e16, 8e16], 3: [-1e16, 1.e17]}}
tracer = tracer_prop(**no2)
dirname_meas = r'M:\nobackup\vinod\model_work\MECO\DOAS_COMP\4azim_doas'
dirname_sim = r'M:\nobackup\vinod\model_work\MECO\DOAS_COMP'
exp = 'UBA_di_2km'  # T42, T106, T42_diurnal, T106_7km, T106_rot
sim_dscd = ft.hdf2dict(path.join(dirname_sim,
                                 'sim_dSCD_{}_{}.hdf'.format(exp, tracer.name)))
bbox_props = {'boxstyle': "round", 'fc': "y", 'ec': "0.5", 'alpha': 0.8}
# %% Measured met data
start_date = date(2018, 5, 4)
end_date = date(2018, 5, 23)
met_file = r'M:\home\jremmers\Projects\MAD_CAT2013\Datasets\Wetter_Uni\wetterdaten_mpic\2018station1.txt'
wdf = pd.read_csv(met_file, header=None, delim_whitespace=True,
                 names=['Date_time', 'c1', 'c2', 'c3', 'c4', 'c5', 'c6', 'c7',
                        'wd', 'ws', 'ws_max'])
wdf['Date_time'] = pd.to_datetime(wdf['Date_time'], format='%y%m%d%H%M%S')
wdf = wdf.set_index('Date_time')
winds = wind(ws=wdf['ws'], wd=wdf['wd'])
wdf['wind_u'], wdf['wind_v'] = winds.sep_wind()
idx = (wdf.index.date >= start_date) & (wdf.index.date <= end_date)
idx &= (~wdf['wind_u'].isnull()) & (~wdf['wind_v'].isnull())
wdf_sel = wdf[(wdf.index.date >= start_date) & (wdf.index.date <= end_date)]
wdf_sel = wdf_sel.resample('H').mean()
winds = wind(u=wdf_sel['wind_u'], v=wdf_sel['wind_v'])
wdf_sel['ws_mean'], wdf_sel['wd_mean'] = winds.merge_uv()
wdf = wdf_sel[(wdf_sel.index.hour >= 5) & (wdf_sel.index.hour <= 21)][3::3]
# %% doas data
fig2, ax2 = plot_scatter_ax(histx=True, histy=True)
fit_data_cf = {'sim': np.array([]), 'meas': np.array([])}
fit_data = {'sim': np.array([]), 'meas': np.array([]),
            'date_time': np.array([], dtype='datetime64'),
            'tel': np.array([]), 'elev': np.array([]),
            'ot_cloud': np.array([]), 'ctype': np.array([])}
comp_idv = {}
summary_tab = []
summary_tab_cf = []
for tel, tel_color in {'T1': 'g', 'T2': 'b', 'T3': 'r', 'T4': 'k'}.items():
    comp_idv[tel] = {}
    f_names = glob(path.join(dirname_meas, tracer.name,
                             '{}_{}_201805*.asc'.format(tracer.name, tel)))
    f_names.sort(key=path.basename)
    col_names = pd.read_csv(f_names[0], dtype=None, delimiter='\t',
                            header=45).columns
    df = pd.concat(pd.DataFrame(np.loadtxt(f, comments='*')) for f in f_names)
    df.columns = col_names
    df['date_time'] = df.apply(lambda x: doy_2_datetime(x['DOY']+1, 2018),
                               axis=1)
#    df = df[df['date_time']<pd.to_datetime('2018-05-13')]
    df = df.set_index('date_time')
    df = df[df['RMS'] < 1e-3]
    # DOY start with 0.0 for January 1st, 0:00 UTC
    cloud_dat = load_cloud(tel)
    fig, ax = plt.subplots(figsize=[15, 6])
    # for elev in [30, 15, 10, 8, 5, 3, 2]:
    for elev, color in {3: 'k'}.items():
        comp_idv[tel][elev] = {'meas': [], 'sim': []}
        df_sel_mean = df[df['Elev_angle'] == elev].resample('H').mean()
        df_sel_std = df[df['Elev_angle'] == elev].resample('H').std()
        data = sim_dscd[tel][str(elev)]
        date_time = [datetime.strptime(data['date_time']['unit'],
                                       '%Y-%m-%d %H:%M:%S') + timedelta(days=i)
                     for i in data['date_time']['data']]
        df_sim = pd.DataFrame(data={'date_time': date_time,
                                    'dSCD': data['dSCD']['data'],
                                    'dAMF': data['dAMF']['data']})
        df_sim = df_sim.set_index('date_time')
        df_sim = df_sim[(df_sim.index.date >= start_date) &
                        (df_sim.index.date < end_date)]
        df_sim = df_sim.reindex(index=df_sel_mean.index)
        meas_col = '{}_DSCD'.format(tracer.name.upper())
        mask = df_sel_mean[meas_col].isnull() | df_sim['dSCD'].isnull()
        cloud_reidx = cloud_dat.reindex(df_sim.index)
        mask_otcloud = (mask) | (cloud_reidx['Thick clouds_o4'] == 1)
        mask_ccloud = (mask_otcloud) | (cloud_reidx['cloud_type'] > 4)
        mask_bcloud = (mask_ccloud) | (cloud_reidx['cloud_type'] == 4)
        ax.plot(df_sim.index, df_sim['dSCD'], 'b-d',
                label='CM02 ({}$^\circ$ elev.)'.format(elev))
        eb = ax.errorbar(df_sel_mean.index,
                         df_sel_mean[meas_col],
                         df_sel_std[meas_col],
                         c='r', fmt='-+', capsize=4,
                         label='MAX-DOAS ({}$^\circ$ elev.)'.format(elev))
        [bar.set_alpha(0.5) for bar in eb[2]]
#        ax.plot('date_time', '{}_DSCD'.format(tracer.name.upper()),
#                '-+', c='r', data=df[df['Elev_angle'] == elev],
#                label='Meas. elev={}$^\circ$'.format(elev))
        ax.set_ylabel('{} dSCD ({})'.format(tracer.label, tracer.vcd_unit),
                      size=16)
        ax.xaxis.set_major_formatter(mdates.DateFormatter("%d-%b"))
        ax.tick_params(axis='both', which='major', labelsize=16)
        ax.minorticks_on()
        ax2['scat'].scatter(df_sel_mean[meas_col][~mask_bcloud],
                            df_sim['dSCD'][~mask_bcloud], s=4, alpha=0.7,
                            c=tel_color, label=tel if elev == 3 else None)
        ax2['scat'].scatter(df_sel_mean[meas_col][mask_bcloud],
                            df_sim['dSCD'][mask_bcloud], edgecolors=tel_color,
                            facecolors="none", s=4, alpha=0.7, label=None)
#        # plot dAMFs
#        ax3 = ax.twinx()
#        ax3.plot(df_sim.index, df_sim['dAMF'], '--', c='k',
#                 label='dAMF elev={}$^\circ$'.format(elev))
#        ax3.set_ylabel('NO$_2$ differential airmass factors ')
#        ax3.set_ylim(0.5, 2.6)
#        ax3.legend(loc='upper left')
#        ax3.minorticks_on()
        comp_idv[tel][elev] = {'meas': df_sel_mean[meas_col].values,
                               'sim': df_sim['dSCD'].values}
        fit_data['meas'] = np.append(fit_data['meas'],
                                     df_sel_mean[meas_col].values)
        fit_data['elev'] = np.append(fit_data['elev'],
                                     np.ones(len(df_sel_mean))*elev)
        fit_data['tel'] = np.append(fit_data['tel'],
                                    np.ones(len(df_sel_mean))*int(tel[1]))
        fit_data['sim'] = np.append(fit_data['sim'], df_sim['dSCD'].values)
        fit_data['ctype'] = np.append(fit_data['ctype'], cloud_reidx['cloud_type'])
        fit_data['ot_cloud'] = np.append(fit_data['ot_cloud'], cloud_reidx['Thick clouds_o4'])
        fit_data['date_time'] = np.append(fit_data['date_time'], df_sim.index.values)
        # append data to dictionory of r, RMS and bias pf telescope and elev
        fit_data_cf['meas'] = np.append(fit_data_cf['meas'],
                                        df_sel_mean[meas_col][~mask_ccloud].values)
        fit_data_cf['sim'] = np.append(fit_data_cf['sim'],
                                       df_sim['dSCD'][~mask_ccloud].values)
        fit_stats = lin_fit(df_sel_mean[meas_col][~mask],
                            df_sim['dSCD'][~mask])
        fit_stats_cf = lin_fit(df_sel_mean[meas_col][~mask_bcloud],
                               df_sim['dSCD'][~mask_bcloud])
        fit_param = fit_stats.stats_calc(frac_range=[0.5, 2])
        fit_param_cf = fit_stats_cf.stats_calc(frac_range=[0.5, 2])
        rel_bias = 100*fit_param['bias']/fit_param['x_mean']
        rel_rmse = 100*fit_param['rmse']/fit_param['x_mean']
        rel_bias_cf = 100*fit_param_cf['bias']/fit_param_cf['x_mean']
        rel_rmse_cf = 100*fit_param_cf['rmse']/fit_param_cf['x_mean']
        summary_tab.append([tel, elev, fit_param['x_mean'],
                            fit_param['rmse'], rel_rmse,
                            fit_param['bias'], rel_bias, fit_param['r']])
        summary_tab_cf.append([tel, elev, fit_param_cf['x_mean'],
                               fit_param_cf['rmse'], rel_rmse_cf,
                               fit_param_cf['bias'], rel_bias_cf,
                               fit_param_cf['r']])
        # #plot diurnal profiles
        # fig3, ax3 = plt.subplots(figsize=[9, 4])
        # plot_diurnal(df_sel_mean[meas_col], df_sel_mean.index,
        #               ax=ax3, color='r', label='MAX-DOAS')
        # plot_diurnal(df_sim['dSCD'], df_sim.index,
        #               ax=ax3, color='b', label='CM02')
        # ax3.set_ylim(0, 2.5e17)
        # ax3.grid(alpha=0.3)
        # ax3.set_ylabel(tracer.label + ' dSCD (molecules cm$^{-2}$)', size=12)
        # ax3.set_xlabel('Time (UTC)', size=12)
        # ax3.annotate('{}\n{}$^\circ$ elev.'.format(tel, elev), xy=[0.8, 0.8],
        #               size=16, xycoords='axes fraction',  bbox=bbox_props)
        # fig3.tight_layout()
        # fig3.savefig(path.join(dirname_meas, tracer.name, 'plots',
        #                         'diurnal_{}_{}_{}_mean.png'.format(tel,
        #                                                           elev, exp)),
        #               format='png', dpi=300)
    cloud_ax = ax.twinx()
    cloud_ax.set_yticks([])
    cloud_ax.set_ylim(tracer.dscd_lim[elev])
    cloud_ax.xaxis.set_major_formatter(mdates.DateFormatter("%d-%b"))
    for cloud_type, cloud_val in {'Broken clouds': 4,
                                  'Cont. clouds': 5}.items():
        cloud_sel = cloud_dat[cloud_dat['cloud_type'] == cloud_val]
        cloud_ax.bar(cloud_sel.index, tracer.dscd_lim[2][1], color='grey',
                     alpha=0.3*(cloud_val-3), width=0.03, label=cloud_type)
    cloud_sel = cloud_dat[cloud_dat['Thick clouds_o4'] == 1]
    cloud_ax.bar(cloud_sel.index, tracer.dscd_lim[2][1], color='r',
                 alpha=0.4, width=0.03, label='Optically\nthick clouds')
    ax2['scat'].legend(loc='upper left')
    ax.set_xlim(start_date, end_date)
    ax.set_ylim(tracer.dscd_lim[elev])
    if tel == 'T2':
        ax.legend(loc='upper right', ncol=2, fontsize=18,
                  fancybox=True, framealpha=0.7)
    if tel == 'T4':
        cloud_ax.legend(loc='upper right', ncol=3, fontsize=18,
                        fancybox=True, framealpha=0.7)
    if tel == 'T1':
        ax.barbs(wdf.index, 0.9*tracer.dscd_lim[elev][1], wdf['wind_u'],
                 wdf['wind_v'], color='m',
                 barb_increments={'half': 1, 'full': 2, 'flag': 5})
    ax.grid(axis='y', alpha=0.4)
    fig.tight_layout()
    ax.annotate('('+chr(96+int(tel[1]))+') '+tel, xy=[0.01, 0.8], size=18,
                xycoords='axes fraction',  bbox=bbox_props)
    # fig.savefig(path.join(dirname_meas, tracer.name, 'plots',
    #             '{}_dSCD_{}_{}_mean.png'.format(tel, elev, exp)),
    #             format='png', dpi=300)
# np.savetxt(path.join(dirname_meas, 'summary_{}_{}.csv'.format(tracer.name, exp)),
#             np.array(summary_tab, dtype='object'), delimiter=",",
#             header='Telescope, elev, Mean, RMSE, rel_RMSE, BIAS, rel_BIAS, R',
#             fmt='%s,%i,%1.2E,%1.2E,%1.2f,%1.2E,%1.2f,%1.2f')
# np.savetxt(path.join(dirname_meas, 'summary_{}_{}_cf.csv'.format(tracer.name, exp)),
#             np.array(summary_tab_cf, dtype='object'), delimiter=",",
#             header='Telescope, elev, Mean, RMSE, rel_RMSE, BIAS, rel_BIAS, R',
#             fmt='%s,%i,%1.2E,%1.2E,%1.2f,%1.2E,%1.2f,%1.2f')
# =============================================================================
# Create scatter plot considering all the data including
# all telescopes and elevation angles
# =============================================================================
ax2['scat'].set_ylim(tracer.dscd_lim[elev])
ax2['scat'].set_xlim(tracer.dscd_lim[elev])
mask = np.isnan(fit_data['meas']) | np.isnan(fit_data['sim'])
for elev in [30, 15, 10, 8, 5, 3, 2]:
    idx = (fit_data['elev'] == elev) & (~mask)
    count, binEdges = np.histogram(fit_data['meas'][idx], bins=20)
    bincenters = 0.5*(binEdges[1:]+binEdges[:-1])
    ax2['histx'].plot(bincenters, count, label=str(elev)+'$^\circ$')
    count, binEdges = np.histogram(fit_data['sim'][idx], bins=20)
    bincenters = 0.5*(binEdges[1:]+binEdges[:-1])
    ax2['histy'].plot(count, bincenters)
    ax2['histy'].set_xlim(200, 0)
    ax2['histx'].set_ylim(0, 200)
fit_stats = lin_fit(fit_data['meas'][~mask], fit_data['sim'][~mask])
fit_param = fit_stats.stats_calc(method="via_origin", frac_range=[0.5, 2])
fit_stats.add_fit_lines(ax=ax2['scat'], frac_range=[0.5, 2], line_color='grey',
                        add_fill_area=True, add_regression=False,
                        fit_label='lin. fit (all)')
fit_stats_cf = lin_fit(fit_data_cf['meas'], fit_data_cf['sim'])
fit_param_cf = fit_stats_cf.stats_calc(method="via_origin",
                                       frac_range=[0.5, 2])
fit_stats_cf.add_fit_lines(ax=ax2['scat'], frac_range=[0.5, 2], line_color='k',
                           add_fill_area=False, add_regression=False,
                           fit_label='lin. fit (c.f.)')

ax2['scat'].set_xlabel('MAX-DOAS {} dSCD ({})'.format(tracer.label, tracer.vcd_unit))
ax2['scat'].set_ylabel('Simulated {} dSCD ({})'.format(tracer.label, tracer.vcd_unit))
ax2['scat'].legend(loc='upper left')
ax2['histx'].legend(loc=[0.74, 0.12], ncol=2, labelspacing=0.2, columnspacing=0.2)
# ax2.text(0.4, 0.95, 'elev={}$^\circ$'.format(elev), transform=ax2.transAxes)
bbox_props = dict(boxstyle="round", fc="w", ec="0.5", alpha=0.9)
# ax2.annotate('{}$^\circ$elev, {}'.format(elev, exp), xy=(0.66, 0.93),
#             xycoords='axes fraction', size=12, bbox=bbox_props)
ax2['scat'].annotate('{}'.format(exp[:6]), xy=(0.8, 0.93),
                     xycoords='axes fraction', size=12, bbox=bbox_props)
fit_stats.add_fit_param(pos_x=0.34, pos_y=0.9, ax=ax2['scat'], color='k',
                        annotation="All measurements\n",
                        showr=True, showfrac=True, show_fit_eqn=False)
fit_stats_cf.add_fit_param(pos_x=0.34, pos_y=0.78, ax=ax2['scat'], color='k',
                           annotation="Cloud free measurements\n",
                           showr=True, showfrac=True, show_fit_eqn=False)
# fig2.savefig(path.join(dirname_meas, tracer.name, 'plots',
#             '{}_scatter_{}_{}.png'.format(tel, elev, exp)),
#             format='png', dpi=300)

# %% Frequency distribution
# fig, ax = plt.subplots(3, 2, figsize=[12, 8], sharey=True)
# col = 1
# for i, elev in enumerate([30, 15, 10,  8, 5, 3]):
#     row = int(i/2)
#     col = 1-col
#     idx = fit_data['elev'] == elev
#     mask = np.isnan(fit_data['meas'][idx]) | np.isnan(fit_data['sim'][idx])
#     count_meas, bins_meas = np.histogram(fit_data['meas'][idx][~mask], 20)
#     count_UBA_fl, bins_UBA_fl = np.histogram(fit_data_merge['UBA_fl'][idx][~mask], bins_meas)
#     count_TNO_fl,bins_TNO_fl = np.histogram(fit_data_merge['TNO_fl'][idx][~mask], bins_meas)
#     count_UBA_di,bins_UBA_di = np.histogram(fit_data_merge['UBA_di'][idx][~mask], bins_meas)
#     ax[row, col].plot(bins_meas[1:], count_meas, 'r-+', label='MAX-DOAS')
#     ax[row, col].plot(bins_TNO_fl[1:], count_TNO_fl, 'b-+', label='TNO_fl')
#     ax[row, col].plot(bins_UBA_fl[1:], count_UBA_fl, 'g-+', label='UBA_fl')
#     ax[row, col].plot(bins_UBA_di[1:], count_UBA_di, 'k-+', label='UBA_di')
#     ax[row, col].annotate('{}$^\circ$ elev.'.format(elev), xy=[0.4, 0.85],
#                           size=16, xycoords='axes fraction',  bbox=bbox_props)
    # if col == 0:
    #     ax[row, col].set_ylabel('Frequency', size=14)
    # if row == 2:
    #     ax[row, col].set_xlabel('{} dSCD ({})'.format(tracer.label,
    #                                                   tracer.vcd_unit), size=14)
#     ax[row, col].grid(alpha=0.3)
# ax[0, 0].legend(loc='upper right', prop={'size': 14})
# plt.tight_layout(pad=0.2)
# plt.savefig(path.join(dirname_sim, 'histogram_dSCD.png'),
#             format='png', dpi=300)
# %% diurnal profiles of data corresponding to all the telescopes
mask_cloud = False
fig3, ax3 = plt.subplots(figsize=[9, 4])
elev = 3
idx = (fit_data['elev'] == elev)
if mask_cloud:
    idx &= ((fit_data['ctype'] < 4) & (fit_data['ot_cloud'] == 0))
mask = np.isnan(fit_data['meas'][idx]) | np.isnan(fit_data['sim'][idx])

plot_diurnal(fit_data['meas'][idx][~mask], fit_data['date_time'][idx][~mask],
              ax=ax3, color='r', label='MAX-DOAS')
plot_diurnal(fit_data['sim'][idx][~mask], fit_data['date_time'][idx][~mask],
              ax=ax3, color='b', label='CM02')
ax3.set_ylim(0, 2.5e17)
ax3.grid(alpha=0.3)
ax3.set_ylabel(tracer.label + ' dSCD (molecules cm$^{-2}$)', size=14)
ax3.set_xlabel('Time (UTC)', size=14)
ax3.annotate('{}\n{}$^\circ$ elev.'.format(exp[:6], elev), xy=[0.8, 0.8],
              size=16, xycoords='axes fraction',  bbox=bbox_props)
ax3.tick_params(axis='both', which='major', labelsize=14)
ax3.legend(loc='upper left', prop={'size':14})
ax3.set_xlim(18641, 18642)
fig3.tight_layout()
fig3.savefig(path.join(dirname_meas, tracer.name, 'plots',
                        'diurnal_all_{}_{}_mean.png'.format(elev, exp)),
              format='png', dpi=300)
# %% box whiskers flot for data distribution for different telescopes
# fig, ax = plt.subplots(2, 2, sharex=True, sharey=True, figsize=[14, 8])
# for t, tel in enumerate(['T1', 'T2', 'T4', 'T3']):
#     row = int(t/2)
#     col = t % 2
#     box_data = {'MAX-DOAS': [], 'TNO_fl': [], 'UBA_fl': [], 'UBA_di': []}
#     ax[row, col].grid(alpha=0.3)
#     ax[row, col].annotate('('+chr(96+int(tel[1]))+') '+tel, xy=(0.5, 0.9),
#                           xycoords='axes fraction', size=16, bbox=bbox_props)
#     for elev in [2, 3, 5, 8, 10, 15, 30]:
#         idx = (fit_data['elev'] == elev) & (fit_data['tel'] == int(tel[1]))
#         mask = np.isnan(fit_data['meas'][idx]) | np.isnan(fit_data['sim'][idx])
#         box_data['MAX-DOAS'].append(fit_data['meas'][idx][~mask])
#         box_data['TNO_fl'].append(fit_data_merge['TNO_fl'][idx][~mask])
#         box_data['UBA_fl'].append(fit_data_merge['UBA_fl'][idx][~mask])
#         box_data['UBA_di'].append(fit_data_merge['UBA_di'][idx][~mask])

#     pos = -0.2
#     for keys, vals in {'MAX-DOAS': 'r', 'TNO_fl': 'b',
#                         'UBA_fl': 'g', 'UBA_di': 'k'}.items():
#         c = vals
#         if (row == 0) & (row == 0):
            # ax[row, col].scatter(np.nan, np.nan, facecolor='none',
            #                       edgecolor=c, label=keys)
#         bw = {'boxprops': {'color': c}, 'whiskerprops': {'color': c},
#               'capprops': {'color': c}, 'medianprops': {'color': c},
#               'meanprops': {'marker': 'o', 'markeredgecolor': c,
#                             'markerfacecolor': 'none', 'markersize': 4}}
#         bp = ax[row, col].boxplot(box_data[keys], positions=np.arange(7)+pos,
#                                   whis=[5, 95], widths=0.15, showfliers=False,
#                                   showmeans=True, boxprops=bw['boxprops'],
#                                   medianprops=bw['medianprops'],
#                                   capprops=bw['capprops'],
#                                   meanprops=bw['meanprops'],
#                                   whiskerprops=bw['whiskerprops'])
#         pos += 0.15
#     if col == 0:
#         ax[row, col].set_ylabel('{} dSCD ({})'.format(tracer.label,
#                                                       tracer.vcd_unit), size=14)
#     if row == 1:
#         ax[row, col].set_xlabel('Elevation angle ($^\circ$)', size=14)
# ax[0, 0].set_xticks(np.arange(7))
# ax[0, 0].set_xticklabels([str(i) for i in [2, 3, 5, 8, 10, 15, 30]])
# plt.tight_layout()
# ax[0, 0].legend(loc='upper right', prop={'size': 14})
# ax[0, 0].set_ylim(0,2e17)
# plt.savefig(path.join(dirname_sim, 'bw_dSCD_all.png'),
#             format='png', dpi=300)

# fig, ax = plt.subplots(figsize=[6, 4])
# elev = 3
# box_data = {'MAX-DOAS': [], 'TNO_fl': [], 'UBA_fl': [], 'UBA_di': []}
# ax.grid(alpha=0.3)
# for dow in range(7):
#     idx = (fit_data['elev'] == elev)
#     idx &= (pd.DatetimeIndex(fit_data['date_time']).dayofweek == dow)
#     mask = np.isnan(fit_data['meas'][idx]) | np.isnan(fit_data['sim'][idx])
#     box_data['MAX-DOAS'].append(fit_data['meas'][idx][~mask])
#     box_data['TNO_fl'].append(fit_data_merge['TNO_fl'][idx][~mask])
#     box_data['UBA_fl'].append(fit_data_merge['UBA_fl'][idx][~mask])
#     box_data['UBA_di'].append(fit_data_merge['UBA_di'][idx][~mask])
# pos = -0.2
# for keys, vals in {'MAX-DOAS': 'r', 'TNO_fl': 'b',
#                     'UBA_fl': 'g', 'UBA_di': 'k'}.items():
#     c = vals
#     ax.scatter(np.nan, np.nan, facecolor='none',
#                               edgecolor=c, label=keys)
#     bw = {'boxprops': {'color': c}, 'whiskerprops': {'color': c},
#           'capprops': {'color': c}, 'medianprops': {'color': c},
#           'meanprops': {'marker': 'o', 'markeredgecolor': c,
#                         'markerfacecolor': 'none', 'markersize': 4}}
#     bp = ax.boxplot(box_data[keys], positions=np.arange(7)+pos,
#                     whis=[5, 95], widths=0.15, showfliers=False,
#                     showmeans=True, boxprops=bw['boxprops'],
#                     medianprops=bw['medianprops'], capprops=bw['capprops'],
#                     meanprops=bw['meanprops'],
#                     whiskerprops=bw['whiskerprops'])
#     pos += 0.15
# ax.set_xticks(np.arange(7))
# ax.set_xticklabels(['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'])
# ax.tick_params(axis='both', which='major', labelsize=14)
# ax.set_ylabel('{} dSCD ({})'.format(tracer.label, tracer.vcd_unit), size=14)
# plt.tight_layout()
# ax.legend(loc='upper right', prop={'size': 14})
# plt.savefig(path.join(dirname_sim, 'bw_dSCD_weekday.png'),
#             format='png', dpi=300)

# %% plot sensitivity distance
# for tel in ['T1', 'T2', 'T3', 'T4']:
#     fig, ax = plt.subplots(figsize=[12, 6])
#     for elev, color in {3: 'r', 10: 'g', 15: 'b', 30: 'k'}.items():
#         data = sim_dscd[tel][str(elev)]
#         date_time = [datetime.strptime(data['date_time']['unit'],
#                                       '%Y-%m-%d %H:%M:%S') + timedelta(days=i)
#                     for i in data['date_time']['data']]
#         df_sim = pd.DataFrame(data={'date_time': date_time,
#                               'sensitivity': data['sensitivity']['data']})
#         df_sim = df_sim.set_index('date_time')
#         df_sim = df_sim.resample('H').mean()
#         ax.plot(df_sim.index, df_sim['sensitivity'], '-x', c=color,
#                 label='Model elev={}$^\circ$'.format(elev))
#     ax.set_ylabel('Sensitivity distance (Km)', size=12)
#     ax.set_xlabel('Date and Time (UTC)', size=12)
#     ax.set_xlim([date(2018, 5, 4), date(2018, 5, 23)])
#     ax.set_ylim([0, 20])
#     ax.legend(loc='upper right')
#     ax.grid(axis='y', alpha=0.4)
#     ax.text(0.01, 0.85, tel, transform=ax.transAxes)
#     plt.tight_layout()
    # plt.savefig(path.join(dirname_sim, tel+'_sensitivity_mean.png'),
    #             format='png', dpi=300)
fig, ax = plt.subplots()
pos = -0.2
HSD_data = {tel: [] for tel in ['T1', 'T2', 'T4', 'T3']}
for tel, vals in {'T1': 'r', 'T2': 'b',
                    'T3': 'g', 'T4': 'k'}.items():
    for elev in [2, 3, 5, 8, 10, 15, 30]:
        data_now = sim_dscd[tel][str(elev)]['sensitivity']['data']
        HSD_data[tel].append(data_now[~np.isnan(data_now)])

    c = vals
    bw = {'boxprops': {'color': c}, 'whiskerprops': {'color': c},
          'capprops': {'color': c}, 'medianprops': {'color': c},
          'meanprops': {'marker': 'o', 'markeredgecolor': c,
                        'markerfacecolor': 'none', 'markersize': 4}}
    ax.scatter(np.nan, np.nan, facecolor='none', edgecolor=c, label=tel)
    bp = ax.boxplot(HSD_data[tel], positions=np.arange(7)+pos,
                              whis=[5, 95], widths=0.15, showfliers=False,
                              showmeans=True, boxprops=bw['boxprops'],
                              medianprops=bw['medianprops'],
                              capprops=bw['capprops'],
                              meanprops=bw['meanprops'],
                              whiskerprops=bw['whiskerprops'])
    pos += 0.15
ax.set_xticks(np.arange(7))
ax.set_xticklabels([str(i) for i in [2, 3, 5, 8, 10, 15, 30]])
ax.legend(loc='upper right', prop={'size': 14}, ncols=2)
ax.set_ylim(0, 14)
ax.set_ylabel('Horizontal sensitivity distance (Km)', size=14)
ax.set_xlabel('Elevation angle ($^\circ$)', size=14)
ax.tick_params(axis='both', which='major', labelsize=14)
ax.grid(alpha=0.3)
plt.tight_layout()
plt.savefig(path.join(dirname_sim, 'HSD.png'),
            format='png', dpi=300)