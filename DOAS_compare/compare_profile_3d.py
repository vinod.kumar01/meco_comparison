# -*- coding: utf-8 -*-
"""
Created on Thu Oct 31 17:35:49 2019

@author: Vinod
"""

# %% imports
import numpy as np
import netCDF4
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
from os import path
import h5py
from scipy.interpolate import RegularGridInterpolator
from scipy.ndimage import rotate
from geopy.distance import distance
import filetools as ft
from tools import tracer_prop, matlab2datetime
import matplotlib.gridspec as gridspec


def rotateImage(img, angle, pivot):
    padX = [img.shape[1] - pivot[0], pivot[0]]
    padY = [img.shape[0] - pivot[1], pivot[1]]
    imgP = np.pad(img, [padY, padX], 'constant')
    imgR = rotate(imgP, angle, reshape=False)
    return imgR[padY[0]: -padY[1], padX[0]: -padX[1]]


save_interpolated_model = True
rotate_plume = True
# %% load doas profiles
dirname = r'M:\nobackup\vinod\model_work\MECO\DOAS_COMP'
meas_path = r'M:\home\jremmers\Projects\Vinod\results\3D_inversion_v2\no2_180507'
mat_file = path.join(meas_path, 'no2_180507.mat')
f = h5py.File(mat_file, 'r')
meas_ref = f.get('inversion/meas')[:]
e_res_ref = f.get('inversion/e_res')[:]
profile_meas = {"T1": [], "T2": [], "T3": [], "T4": []}
inv_coords = {"T1": [], "T2": [], "T3": [], "T4": []}
date_time_meas, res_min = [], []
inv_hgrid = np.array(f[e_res_ref[15][0]]['mean/profile3d/grid_altitude1'])
for tel in ["T1", "T2", "T3", "T4"]:
    inv_coords[tel] = np.genfromtxt(path.join(dirname,
                                              'Gas_inversion_lat_lon_'+tel+'.txt'))
    for i in range(len(e_res_ref)):
        profile_now = np.array(f[e_res_ref[i][0]]['mean/profile3d/'+tel])
        if profile_now.shape[1] < 17:
            nan_array = np.full([250, 17-profile_now.shape[1]], np.nan)
            profile_now = np.append(profile_now, nan_array, axis=1)
        datenum_now = np.array(f[meas_ref[i][0]]['datenum'])[0, 0]
        profile_meas[tel].append(profile_now[:, :17])
        if tel == 'T1':
            date_time_meas.append(matlab2datetime(datenum_now))
            res_min_now = np.array(f[e_res_ref[i][0]]['resMin'])[0][0]
            res_min.append(res_min_now)
    profile_meas[tel] = np.array(profile_meas[tel])
f.close()

# calculate mean profile
mean_meas_profile = {'date_time': [],
                     'profile': {"T1": [], "T2": [], "T3": [], "T4": []}}
hourlist = set([i.hour for i in date_time_meas])
for hour in hourlist:
    hhmm = str(hour).zfill(2)+'00'
    keep = np.array([i.hour for i in date_time_meas]) == hour
    timenow = np.array(date_time_meas)[keep][0].replace(minute=0, second=0,
                                                        microsecond=0)
    mean_meas_profile['date_time'].append(timenow)
    keep &= np.array(np.array(res_min) < 8.e14)
    # residuum criteria
    for tel in ["T1", "T2", "T3", "T4"]:
        profile_sel = profile_meas[tel][keep, :]
        mean_profile_now = np.nanmean(profile_sel, 0)
        mean_meas_profile['profile'][tel].append(mean_profile_now)
for tel in ["T1", "T2", "T3", "T4"]:
    mean_meas_profile['profile'][tel] = np.asarray(mean_meas_profile['profile'][tel])

# %% load model data and interpolate along MAX-DOAS retrieved profiles
exp = 'T106'
no2 = {'name': 'NO2', 'label': 'NO$_2$', 'vcd_unit': 'molecules cm$^{-2}$',
       'conc_unit': 'molecules cm$^{-3}$'}
hcho = {'name': 'HCHO', 'label': 'HCHO', 'vcd_unit': 'molecules cm$^{-2}$',
        'conc_unit': 'molecules cm$^{-3}$'}
tracer = tracer_prop(**no2)
if save_interpolated_model:
    filename_sim = 'SAT_CHEM18_5_no2_1hr_'+exp+'_regrid_small.nc'
    f_sim = netCDF4.Dataset(path.join(dirname, filename_sim), 'r')
    lat = f_sim.variables['lat'][:]
    lon = f_sim.variables['lon'][:]
    aps = f_sim.variables['aps'][:]
    tm1 = f_sim.variables['tm1_ave'][:]
    timeax = f_sim.variables['time'][:]
    vlevs = len(f_sim.variables['dhybm'][:])
    try:
        time_unit = f_sim.variables['time'].units[11:30]
        time_dsp = [datetime.strptime(time_unit,
                                      '%Y-%m-%d %H:%M:%S') + timedelta(days=i)
                    for i in timeax]
    except ValueError:
        time_unit = f_sim.variables['time'].units[10:30]
        time_dsp = [datetime.strptime(time_unit,
                                      '%Y-%m-%d %H:%M:%S') + timedelta(days=i)
                    for i in timeax]
    prs_m = np.array([f_sim.variables['dhyam'][i] +
                      (aps[:]*f_sim.variables['dhybm'][i])
                      for i in range(0, vlevs)])
    prs_m = np.transpose(prs_m, (1, 0, 2, 3))
    prs_m[prs_m < 0] = np.nan
    h_interface = f_sim.variables['hhl'][:]
    # height of interface  above ground level
    h_agl = h_interface - h_interface[vlevs, :]
    # height above Mainz
    h_m_agl = (h_agl[1:, 50, 50] + h_agl[:-1, 50, 50])/2  # layer midpoint
    l4km = 26 if vlevs == 50 else 19
    h_m_agl = h_m_agl[l4km:]  # select upto 4 km
    # =============================================================================
    # mid layer height above ground level at Mainz
    # this does not work because meas altitude starts from 0
    # whereas mid layer model altitude starts from ~10
    # =============================================================================
    # h_m_agl = (h_m_agl[1:]+h_m_agl[:-1])/2
    # height in ascending order
    h_m_agl = h_m_agl[::-1]
    tracer_mr = f_sim.variables[tracer.name+'_ave'][:]
    tracer_conc = tracer_mr*6.023E23*prs_m*1E-6/8.314/tm1
    # select data until 3km
    tracer_conc_sel = tracer_conc[:, l4km:, :].filled(np.nan)
    # flip data vertically for regular grid interpolation
    tracer_conc_sel = tracer_conc_sel[:, ::-1, :, :]
    if rotate_plume:
        # rotate 10 degree around frankfyrt
        pivot = [74, 55]  # index for location of Frankfurt
        tracer_conc_sel_rot = tracer_conc_sel.copy()
        for t in range(tracer_conc_sel.shape[0]):
            for l in range(tracer_conc_sel.shape[1]):
                temp_arr = tracer_conc_sel[t, l, :, :].copy()
                temp_arr[np.isnan(temp_arr)] = 0
                temp_arr_rot = rotateImage(temp_arr, 10, pivot)
                temp_arr_rot[temp_arr_rot < np.nanmin(temp_arr)] = np.nan
                tracer_conc_sel_rot[t, l, :, :] = temp_arr_rot
        tracer_conc_sel = tracer_conc_sel_rot
        exp = exp+'_rot'

    mpic = (49.9909, 8.2283)
    displacement = {}
    for tel in ["T1", "T2", "T3", "T4"]:
        displacement[tel] = [distance(mpic, i).kilometers
                             for i in inv_coords[tel]]
        displacement[tel] = np.array(displacement[tel])
    sim_conc = {'time': [], 'altitude': inv_hgrid[:200],
                'displacement': displacement,
                'conc': {'T1': [], 'T2': [], 'T3': [], 'T4': []}}
    for timestep in range(len(timeax)):
        model_dt = time_dsp[timestep]
        # only consider daytime values
        if (model_dt.hour < 4) | (model_dt.hour > 19):
            continue
        sim_conc['time'].append(timeax[timestep])
        # convert sim grid to km
        fn_tracer = RegularGridInterpolator((lon, lat, h_m_agl/1000),
                                            tracer_conc_sel[timestep, :].T)
        for tel in ["T1", "T2", "T3", "T4"]:
            sim_conc_now = []
            # index 50 is location of MAX-DOAS
            for x in range(len(inv_coords[tel])):
                # interpolate model concentartion along line of telescope
                tempval = [fn_tracer((inv_coords[tel][x][1],
                                      inv_coords[tel][x][0], h_now))
                           for h_now in inv_hgrid[:200]]
                sim_conc_now.append(tempval)
            sim_conc['conc'][tel].append(sim_conc_now)
    for tel in ["T1", "T2", "T3", "T4"]:
        sim_conc['conc'][tel] = np.array(sim_conc['conc'][tel])[:, :, :, 0]

    savename = 'sim_conc_no2_{}_{}.hdf'.format(exp, tracer.name)
    ft.dict2hdf5(path.join(dirname, savename), sim_conc)
    f = h5py.File(path.join(dirname, savename), 'r+')
    f['time'].attrs['unit'] = f_sim.variables['time'].units
    f['displacement'].attrs['unit'] = "Distance from telescope, +ve along T1, T2"
    f['altitude'].attrs['unit'] = "Kilometers from ground level"
    f['conc'].attrs['unit'] = "Molecules cm -3"
    f.close()
    f_sim.close()
# %% load model simulated tracer inteprolated along MAX-DOAS inversion grids
savename = 'sim_conc_no2_{}_{}.hdf'.format(exp, tracer.name)
sim_data = ft.hdf2dict(path.join(dirname, savename))
with netCDF4.Dataset(path.join(dirname, savename)) as f:
    timeunit = f.variables['time'].unit
try:
    sim_dt = [datetime.strptime(timeunit[11:],
                                '%Y-%m-%d %H:%M:%S') + timedelta(days=i)
              for i in sim_data['time']]
except ValueError:
    sim_dt = [datetime.strptime(timeunit[10:],
                            '%Y-%m-%d %H:%M:%S') + timedelta(days=i)
          for i in sim_data['time']]
# %% plotting
savepath = r'M:\nobackup\vinod\model_work\MECO\DOAS_COMP\4azim_doas\NO2\plots\7km'

for dt in range(len(sim_dt)):
    meas_idx = np.array(mean_meas_profile['date_time']) == sim_dt[dt]
    if np.sum(meas_idx) != 1:
        continue
    # plot vertical profile over MPIC around tropomi overpass
    if dt == 39:
        profile_mpic_meas = np.array([mean_meas_profile['profile'][tel][meas_idx][0, :200, 0]
                                     for tel in ["T1", "T2", "T3", "T4"]])
        profile_mpic_meas = np.sum(profile_mpic_meas, 0)
        profile_mpic_sim = sim_data['conc']['T1'][dt, 0, :]
        fig,ax = plt.subplots()
        ax.plot(profile_mpic_meas, sim_data['altitude'], '-+', label='MAX-DOAS')
        ax.plot(profile_mpic_sim, sim_data['altitude'], '-+', label='MECO(n)')
        ax.legend()
        ax.set_ylabel('Altitude')
        ax.set_ylabel('Altitude (Km)')
        ax.set_xlabel('NO$_2$ concentration (molecules cm$^{-3}$)')
    fig = plt.figure(figsize=(12, 8))
    outer = gridspec.GridSpec(2, 2, wspace=0.2, hspace=0.2)
    for i, tel in enumerate(["T1", "T2", "T3", "T4"]):
        inner = gridspec.GridSpecFromSubplotSpec(2, 1,
                        subplot_spec=outer[i], wspace=0.1, hspace=0.1)
        for j in range(2):
            ax0 = plt.Subplot(fig, inner[0])
            pm1 = ax0.pcolormesh(sim_data['displacement'][tel][:17],
                                 sim_data['altitude'],
                                 sim_data['conc'][tel][dt, :17, :].T,
                                 cmap='jet', vmin=1e9, vmax=2.5e11)
            ax0.set_ylim(0, 2)
            ax0.set_ylabel('Altitude (Km)')
            ax1 = plt.Subplot(fig, inner[1])
            ax1.pcolormesh(sim_data['displacement'][tel][:17],
                           sim_data['altitude'],
                           mean_meas_profile['profile'][tel][meas_idx][0, :200, :],
                           cmap='jet', vmin=1e9, vmax=2.5e11)
            ax0.set_ylim(0, 2)
            ax1.set_ylim(0, 2)
            ax0.set_ylabel('Altitude (Km)')
            ax1.set_ylabel('Altitude (Km)')
            ax1.set_xlabel('Distance from telescope (Km)')
            ax0.set_xticks([])
            ax0.text(0.7, 0.8, tel+'\n'+sim_dt[dt].strftime('%d-%m-%y %H:%M'),
                     color='white', transform=ax0.transAxes)
            ax0.text(0.1, 0.8, 'MECO(n)',
                     color='white', transform=ax0.transAxes)
            ax1.text(0.1, 0.8, 'MAX-DOAS',
                     color='white', transform=ax1.transAxes)
            fig.add_subplot(ax0)
            fig.add_subplot(ax1)
    cbar_ax = fig.add_axes([0.92, 0.15, 0.015, 0.6])
    cbar = fig.colorbar(pm1, cax=cbar_ax, extend="max")
    cbar.set_label(tracer.label + ' concentration (molecules cm$^{-3}$)',
                   labelpad=2)
    savestr = 'profile3d_comp_{}.png'.format(sim_dt[dt].strftime('%d_%m_%H'))
    fig.savefig(path.join(savepath, savestr), dpi=200, format='png')
    plt.close()
