# -*- coding: utf-8 -*-
"""
Created on Thu Oct 31 22:14:09 2019
# =============================================================================
# Steps:
# 1. Interpolate the model data to DOAS bAMF 3D coordinates
# lat, lon and height grid.
# 2a. First way:
# 	1. calculate VCD above measurement location
# 	2. Calculate AMF using bAMF and NO2 profile above measurement location
#     (does not account for heteogeinity)
# 	3. Calcualte SCD as VCD*AMF
# 2b. Second way:
# 	1. Calculate box VCD in each grid.
# 	2. Mulitply with corresponding box AMF in each grid to get box SCD
# 	3. Calcualte SCD as sum of individual box SCD.
# =============================================================================
@author: Vinod
"""

# %% imports
import numpy as np
import netCDF4
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
from os import path
from scipy.interpolate import RegularGridInterpolator, interp1d
from scipy.ndimage import rotate
from geopy.distance import distance
from DOAS_compare.load_bamf import bAMF_mean
import filetools as ft
from tools import tracer_prop


def idx_neartime(model_dt, bamf_dtlist):
    # bAMF are available only for some days
    # for trial i use bAMF for 6th May
    if not model_dt.date() in set([i.date() for i in bamf_dtlist]):
        model_dt = model_dt.replace(day=6)
    timediff = [abs((model_dt-i).total_seconds()) for i in bamf_dtlist]
    idx = np.argmin(timediff)
    near_dt = bamf_dtlist[idx]
    return(near_dt.strftime('%Y%m%d_%H%M'))


def rotateImage(img, angle, pivot):
    padX = [img.shape[1] - pivot[0], pivot[0]]
    padY = [img.shape[0] - pivot[1], pivot[1]]
    imgP = np.pad(img, [padY, padX], 'constant')
    imgR = rotate(imgP, angle, reshape=False)
    return imgR[padY[0]: -padY[1], padX[0]: -padX[1]]


# %% load model data
exp = 'di_2km'  # T42, T106, T42_diurnal, T106_7km
emis = 'UBA'
rotate_plume = False
dirname_sim = r'M:\nobackup\vinod\model_work\MECO\DOAS_COMP'
filename_sim = 'SAT_CHEM18_5_no2_1hr_'+exp+'_regrid.nc'
f_sim = netCDF4.Dataset(path.join(dirname_sim, filename_sim), 'r')
no2 = {'name': 'NO2', 'label': 'NO$_2$', 'vcd_unit': 'molecules cm$^{-2}$',
       'conc_unit': 'molecules cm$^{-3}$'}
hcho = {'name': 'HCHO', 'label': 'HCHO', 'vcd_unit': 'molecules cm$^{-2}$',
        'conc_unit': 'molecules cm$^{-3}$'}
tracer = tracer_prop(**no2)
lat = f_sim.variables['lat'][:]
lon = f_sim.variables['lon'][:]
aps = f_sim.variables['aps'][:]
tm1 = f_sim.variables['tm1_ave'][:]
timeax = f_sim.variables['time'][:]
try:
    time_unit = f_sim.variables['time'].units[11:30]
    time_dsp = [datetime.strptime(time_unit,
                                  '%Y-%m-%d %H:%M:%S') + timedelta(days=i)
                for i in timeax]
except ValueError:
    time_unit = f_sim.variables['time'].units[10:30]
    time_dsp = [datetime.strptime(time_unit,
                                  '%Y-%m-%d %H:%M:%S') + timedelta(days=i)
                for i in timeax]
prs_m = np.array([f_sim.variables['dhyam'][i] +
                  (aps[:]*f_sim.variables['dhybm'][i])
                  for i in range(0, tm1.shape[1])])
prs_m = np.transpose(prs_m, (1, 0, 2, 3))
prs_m[prs_m < 0] = np.nan
h_interface = f_sim.variables['hhl'][:]
# height of interface  above ground level
h_agl = h_interface - h_interface[tm1.shape[1], :]
# height above Mainz
l4km = 27 if tm1.shape[1] == 50 else 19
h_m_agl = h_agl[l4km:, 50, 50]
# =============================================================================
# mid layer height above ground level at Mainz
# this does not work because meas altitude starts from 0
# whereas mid layer model altitude starts from ~10
# =============================================================================
# h_m_agl = (h_m_agl[1:]+h_m_agl[:-1])/2
# height in ascending order
h_m_agl = h_m_agl[::-1]
tracer_mr = f_sim.variables[tracer.name+'_ave'][:]
tracer_conc = tracer_mr*6.023E23*prs_m*1E-6/8.314/tm1
# select data until 3km
tracer_conc_sel = tracer_conc[:, l4km-1:, :].filled(np.nan)
# flip data vertically for regular grid interpolation
tracer_conc_sel = tracer_conc_sel[:, ::-1, :, :]
if rotate_plume:
    # rotate 10 degree around frankfyrt
    pivot = [74, 55]  # index for location of Frankfurt
    tracer_conc_sel_rot = tracer_conc_sel.copy()
    for t in range(tracer_conc_sel.shape[0]):
        for ll in range(tracer_conc_sel.shape[1]):
            temp_arr = tracer_conc_sel[t, ll, :, :].copy()
            temp_arr[np.isnan(temp_arr)] = 0
            temp_arr_rot = rotateImage(temp_arr, 10, pivot)
            temp_arr_rot[temp_arr_rot < np.nanmin(temp_arr)] = np.nan
            tracer_conc_sel_rot[t, ll, :, :] = temp_arr_rot
    tracer_conc_sel = tracer_conc_sel_rot
f_sim.close()
# %% load box AMFS
dtlist = ['180506', '180507', '180508', '180509', '180510', '180511',
          '180512', '180513', '180515', '180516', '180517', '180518',
          '180519', '180520', '180521', '180522']
bAMF_day = bAMF_mean(dtlist=dtlist, year=2018)
# get the nearest time for which bAMF are available
meas_hgrid = bAMF_day['h_grid']
meas_hgrid *= 1000
# grid width in cm
dh_grid = 100*(meas_hgrid[1:]-meas_hgrid[:-1])
bamf_dtlist = bAMF_day['bAMF'].keys()
bamf_dtlist = [datetime.strptime(i, '%Y%m%d_%H%M') for i in bamf_dtlist]
sim_conc = {}
sim_conc = {}
for tel in ["T1", "T2", "T3", "T4"]:
    sim_conc[tel] = {'date_time': {'unit': time_unit,
                                   'data': []},
                     'conc': {'unit': 'molecueles cm-3',
                              'data': []},
                     'displacement': {'unit': 'km',
                                      'data':bAMF_day['displacement'][tel][45:125]},
                     'h_grid': {'unit': 'km',
                                'data': bAMF_day['h_grid']}}
sim_bvcd = {}
sim_vcd = {}
sim_dscd = {}
lon_distance = {}
lat_distance = {}
for timestep in range(len(timeax)):
    model_dt = time_dsp[timestep]
    # only consider daytime values
    if (model_dt.hour < 4) | (model_dt.hour > 19):
        continue
    fn_tracer = RegularGridInterpolator((lon, lat, h_m_agl),
                                        tracer_conc_sel[timestep, :].T)
    # # find nearest time for which bAMF is calculated
    # dt = idx_neartime(model_dt, bamf_dtlist)
    dt = model_dt.strftime('%Y%m%d_%H%M')
    if not sim_dscd:
        for tel in ["T1", "T2", "T3", "T4"]:
            sim_dscd[tel] = {}
    for tel in ["T1", "T2", "T3", "T4"]:
        tempgrid = []
        # index 50 is location of MAX-DOAS
        for x in np.arange(45, 125, 1):
            # interpolate model concentartion along line of telescope
            tempval = [fn_tracer((bAMF_day['coords'][tel][x][1],
                                  bAMF_day['coords'][tel][x][0], h_now))
                       for h_now in meas_hgrid[:33]]
            tempgrid.append(tempval)
        sim_conc[tel]['conc']['data'].append(np.asarray(tempgrid))
        sim_conc[tel]['date_time']['data'].append(timeax[timestep])
        mpic = (49.9909, 8.2283)
        lon_distance[tel] = np.array([distance(mpic, [mpic[0], i]).kilometers
                                     if i > mpic[1] else
                                     -distance(mpic, [mpic[0], i]).kilometers
                                     for i in bAMF_day['coords'][tel][:, 1]])
        lat_distance[tel] = np.array([distance(mpic, [i, mpic[1]]).kilometers
                                     if i > mpic[0] else
                                     -distance(mpic, [i, mpic[1]]).kilometers
                                     for i in bAMF_day['coords'][tel][:, 0]])
        # calculate box vcd in each grid
        sim_bvcd[tel] = tempgrid*dh_grid[:33]
        # calculate the VCD above the position of instrument
        sim_vcd[tel] = np.sum(sim_bvcd[tel][5, :])
    #    sim_vcd = np.nansum(sim_bvcd[tel], 1)
        if not sim_dscd[tel]:
            for ei in [3, 5, 10, 15, 30]:
                sim_dscd[tel][ei] = {'dSCD': {'unit': 'molecueles cm-2',
                                              'data': []},
                                     'dAMF': {'unit': '1',
                                              'data': []},
                                     'sensitivity': {'unit': 'Km',
                                                     'data': []},
                                     'date_time': {'unit': time_unit,
                                                   'data': []}}
        for ei in [3, 5, 10, 15, 30]:
            try:
                bamf_now = bAMF_day['bAMF'][dt][tel][ei][45:125, :33]
            except (KeyError, IndexError) as e:
                bamf_now = np.full([80, 33], np.nan)
                # happens at high SZA when we have data for one telescope
                # but not for other
            bamf_now[bamf_now < 0] = 0
            dSCD_now = sim_bvcd[tel]*bamf_now
            '''
            horizontal representativeness
            I calculate horizontal representativeness as the distance which
            accounts for 90% of the SCD
            '''
            vertsum = np.sum(dSCD_now[5:, :], 1)
            cum_dSCD = np.cumsum(vertsum)
            dist_now = abs(bAMF_day['displacement'][tel][50:125])
            fn_cumsum = interp1d(cum_dSCD, dist_now)
            # I only sum in in the foreward viewing direction. Skip 5 in back.
            dSCD_now = np.sum(dSCD_now[5:, :])
            hor_sensitivity_nw = fn_cumsum(0.9*dSCD_now)
            dAMF_now = dSCD_now / sim_vcd[tel]
            sim_dscd[tel][ei]['dSCD']['data'].append(dSCD_now)
            sim_dscd[tel][ei]['dAMF']['data'].append(dAMF_now)
            sim_dscd[tel][ei]['sensitivity']['data'].append(hor_sensitivity_nw)
            sim_dscd[tel][ei]['date_time']['data'].append(timeax[timestep])
sim_conc[tel]['conc']['data']= np.array(sim_conc[tel]['conc']['data'])
ft.dict2hdf5(path.join(dirname_sim,
                       'sim_dSCD_{}_{}_{}.hdf'.format(emis, exp, tracer.name)),
             sim_dscd)
ft.dict2hdf5(path.join(dirname_sim,
                       'sim_conc_{}_{}_{}.hdf'.format(emis, exp, tracer.name)),
             sim_conc)
# %% plotting
for tel in ["T1", "T2", "T3", "T4"]:
    fig, ax = plt.subplots()
    for elev in [3, 10, 15, 30]:
        data = sim_dscd[tel][elev]
        date_time = [datetime.strptime(data['date_time']['unit'],
                                       '%Y-%m-%d %H:%M:%S') + timedelta(days=i)
                     for i in data['date_time']['data']]
        ax.scatter(date_time, data['dSCD']['data'],
                   label='elev={}$^\circ$'.format(elev), s=4)
        ax.set_ylabel('NO$_2$ dSCD (molecules cm$^{-2}$)')
#        ax.text(0.1, 0.9, '{}\n elev={}$^\circ$'.format(tel, elev),
#                transform=ax.transAxes)
    ax.legend()
    plt.tight_layout()
