# -*- coding: utf-8 -*-
"""
Created on Wed Oct 30 09:12:42 2019

@author: Vinod
"""

import numpy as np
import os
import h5py
from date_converter import doy_2_datetime
dirname = r'M:\home\jremmers\Projects\Vinod\results\inversion_results\no2\T1_no2_180506'
filename = 'T1_no2_180506.mat'
f = h5py.File(os.path.join(dirname, filename),'r')
# get reference
c_mean_ref = f.get('inversion/c_mean')[:]
date_time_ref = f.get('input/meas/fracDay')[:]

data = np.zeros([c_mean_ref.shape[1]])
date_time = np.zeros([c_mean_ref.shape[1]])
for i in range(c_mean_ref.shape[1]):
    data[i]  = np.array(f[c_mean_ref[0][i]])[0][0]
    date_time[i]  = np.array(f[date_time_ref[i][0]])[0][0]
    
date_time = np.array([doy_2_datetime(i, 2018) for i in date_time])  

