# -*- coding: utf-8 -*-
"""
Created on Thu Jan 17 12:36:28 2019

@author: Vinod
"""

import netCDF4
import os
import glob
import numpy as np
import itertools
from datetime import datetime
import pandas as pd
dirname = r"M:\nobackup\vinod\omi_raw\level3\Mohali"
fnames = glob.glob(os.path.join(dirname, 'OMI-Aura_L3-OMNO2d_2015m*.he5.nc'))
date_time = np.empty(len(fnames), dtype='object')
no2_vcd = np.empty(len(fnames), dtype='float')
counter = 0
for file in fnames:
    filename = os.path.basename(file)
    f = netCDF4.Dataset(file,'r')
    LON, LAT = f.variables['lon'][:], f.variables['lat'][:]
    trop_no2_cs = f.variables['ColumnAmountNO2TropCloudScreened'][:]
    trop_no2_raw = f.variables['ColumnAmountNO2Trop'][:]
    
    
    lat_st, lat_end, lon_st, lon_end = 30.4, 31, 76.4, 77  # Mohali
    cond_lat = (LAT > lat_st) & (LAT < lat_end)
    cond_lon = (LON > lon_st) & (LON < lon_end)
    idx_select_lon = np.where(cond_lon)
    idx_select_lat = np.where(cond_lat)
    if len(idx_select_lat) == 0 & len(idx_select_lat) == 0:
        vcd_ary = np.nan
    else:
        vcd_ary = np.ma.array([trop_no2_cs[j,i]
        for j, i in itertools.product(idx_select_lat[0],idx_select_lon[0])])
    no2_vcd[counter] = np.ma.mean(vcd_ary)
    date_time[counter] = datetime.strptime(filename[19:28] + "13:30",
             '%Ym%m%d%H:%M')
    f.close()
    counter+=1
df = pd.DataFrame(np.column_stack((date_time, no2_vcd)),
                   columns=['Date_time', 'no2_vcd'])
df1 = df.sort_values(by=['Date_time'])
df1.to_csv(os.path.join(dirname, "OMI_NO2_2015.csv"), index=False)

    
    
