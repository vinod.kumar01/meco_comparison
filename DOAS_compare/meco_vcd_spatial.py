# -*- coding: utf-8 -*-
"""
Created on Thu Oct 31 09:22:43 2019

@author: Vinod
"""

# %% imports
import netCDF4
import os
import numpy as np
import cartopy.crs as ccrs
from plot_tools import map_prop
import matplotlib.pyplot as plt
from datetime import datetime, timedelta
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
from mod_colormap import add_white
from scipy.ndimage import rotate


def rot(phi):
    phi = np.deg2rad(phi)
    return np.array([[np.cos(phi), -np.sin(phi)], [np.sin(phi), np.cos(phi)]])


def draw_rectangle(x, y, w, h, angle, ax):
    a = np.array((-w / 2, 0))
    b = np.array((w / 2, 0))
    c = np.array((w / 2, h))
    d = np.array((-w / 2, h))
    if angle != 0:
        a = np.matmul(rot(angle), a)
        b = np.matmul(rot(angle), b)
        c = np.matmul(rot(angle), c)
        d = np.matmul(rot(angle), d)
    a += [x, y]
    b += [x, y]
    c += [x, y]
    d += [x, y]
    ax.plot([a[1], b[1]], [a[0], b[0]], 'r-', alpha=0.8)
    ax.plot([b[1], c[1]], [b[0], c[0]], 'r-', alpha=0.8)
    ax.plot([c[1], d[1]], [c[0], d[0]], 'r-', alpha=0.8)
    ax.plot([d[1], a[1]], [d[0], a[0]], 'r-', alpha=0.8)
    return a, b, c, d


def rotateImage(img, angle, pivot):
    padX = [img.shape[1] - pivot[0], pivot[0]]
    padY = [img.shape[0] - pivot[1], pivot[1]]
    imgP = np.pad(img, [padY, padX], 'constant')
    imgR = rotate(imgP, angle, reshape=False)
    return imgR[padY[0]: -padY[1], padX[0]: -padX[1]]


new_cm = add_white("rainbow", shrink_colormap=False, replace_frac=0.1,
                   start="min")
# %% model data
dirname_sim = r'M:\nobackup\vinod\model_work\MECO\DOAS_COMP'
filename_sim = 'UBA_di_18_5_1day.nc'
# filename_sim = 'SAT_CHEM18_5_no2_1hr_T106_regrid_small.nc'
f_sim = netCDF4.Dataset(os.path.join(dirname_sim, filename_sim), 'r')
tracer = 'NO2'
if 'sorbit' in filename_sim:
    lat = f_sim.variables['geolat'][:]
    lon = f_sim.variables['geolon'][:]
    dhyam = f_sim.variables['dhyam'][:]
    dhybm = f_sim.variables['dhybm'][:]
    wind_u = f_sim.variables['COSMO_um1'][:, -1, :, :]
    wind_v = f_sim.variables['COSMO_vm1'][:, -1, :, :]
    aps = f_sim.variables['ps'][:]
    tm1 = f_sim.variables['COSMO_tm1'][:]
    timeax = f_sim.variables['time'][:]
    tracer_mr = f_sim.variables['tracer_gp_'+tracer][:]
    h_interface = np.nanmean(f_sim.variables['COSMO_ORI_HHL'][:], 0)
    time_dsp = [datetime.strptime(f_sim.variables['time'].units[10:30],
                                  '%Y-%m-%d %H:%M:%S') + timedelta(days=i-1)
                for i in timeax]
else:
    timeax = f_sim.variables['time'][:]
    tm1 = f_sim.variables['tm1_ave'][:]
    tracer_mr = f_sim.variables[tracer+'_ave'][:]
    h_interface = f_sim.variables['hhl'][:]
    lat = f_sim.variables['lat'][:]
    lon = f_sim.variables['lon'][:]
    aps = f_sim.variables['aps'][:]
    try:
        wind_u = f_sim.variables['um1_ave'][:, -1, :, :]
        wind_v = f_sim.variables['vm1_ave'][:, -1, :, :]
    except:
        pass
    try:
        time_dsp = [datetime.strptime(f_sim.variables['time'].units[10:30],
                                      '%Y-%m-%d %H:%M:%S') + timedelta(days=i)
                    for i in timeax]
    except ValueError:
        time_dsp = [datetime.strptime(f_sim.variables['time'].units[11:30],
                                      '%Y-%m-%d %H:%M:%S') + timedelta(days=i)
                    for i in timeax]
    try:
        dhyam = f_sim.variables['dhyam'][:]
        dhybm = f_sim.variables['dhybm'][:]
    except KeyError:
        dhyam = f_sim.variables['dhyam_ave'][:]
        dhybm = f_sim.variables['dhybm_ave'][:]

l4km = 29 if tm1.shape[1] == 50 else 21
prs_m = np.array([dhyam[i] + (aps[:]*dhybm[i])
                  for i in range(0, len(dhyam))])
prs_m = np.transpose(prs_m, (1, 0, 2, 3))
prs_m[prs_m < 0] = np.nan

tracer_conc = tracer_mr*6.023E23*prs_m*1E-6/8.314/tm1
'''
calculation of box height
'''
h = np.ma.masked_array([h_interface[i, :] - h_interface[i+1, :]
                        for i in range(0, len(dhyam))])
tracer_sim_vcd_box = np.ma.masked_array([tracer_conc[i, :]*h*100
                                         for i in range(tracer_conc.shape[0])])
tracer_sim_geovcd = np.nansum(tracer_sim_vcd_box[:, l4km:len(dhyam), :, :], 1)

# %% plotting
df = [{'lon': 8.2283, 'lat': 49.9909, 'site': 'MPIC'},
      {'lon': 8.68, 'lat': 50.11, 'site': 'Frankfurt'}]
#for timestep in np.arange(12, len(time_dsp), 24):
for timestep in np.arange(len(time_dsp)):
    v0 = tracer_sim_geovcd[timestep, :]
    timetext = datetime.strftime(time_dsp[timestep], '%d.%m.%Y %H UTC')
#    llon, ulon, llat, ulat = 5.8, 11.2, 48.2, 52.1
    llon, ulon, llat, ulat = 7.5, 9.5, 49.0, 50.5
    prop_dict = {'extent': [llon, ulon, llat, ulat],
                 'name': ' ',
                 'border_res': '10m',
                 'bkg': None}
    datadict = {'lat': lat, 'lon': lon, 'plotable': v0,
                'vmin': 0, 'vmax': 8.0E15, 'label': '',
                'text': 'NO$_{2}$ VCD molecules cm$^{-2}$ (upto 4km)'}
    map2plot = map_prop(**prop_dict)
    projection = ccrs.PlateCarree()
    fig, ax = plt.subplots(subplot_kw=dict(projection=projection))
    plt.title('NO$_{2}$ VCD for ' + timetext)
    try:
        scat = map2plot.plot_map(datadict, ax, cmap=new_cm,
                                 projection=projection, alpha=0.8)

        # add wind vectors
        xx = np.arange(0, lon.shape[0], 4)
        yy = np.arange(0, lat.shape[1], 4)
        points = np.meshgrid(xx, yy)
        qv = ax.barbs(lon[points], lat[points],
                      wind_u[timestep, :][points], wind_v[timestep, :][points])
    except:
        llons, llats = np.meshgrid(lon, lat)
        datadict['lat'], datadict['lon'] = llats, llons
        scat = ax.pcolor(llons, llats, v0, cmap=new_cm,
                         vmin=0, vmax=8.0E15, transform=ccrs.PlateCarree())

    for point in df:
        lo, la = point['lon'], point['lat']
        plt.annotate(point['site'], xy=(lo, la),  xycoords='data',
                     xytext=(lo, la), textcoords='data', color='k',
                     horizontalalignment='left', verticalalignment='bottom',
                     arrowprops=dict(facecolor='black', shrink=0.05))
        plt.plot(lo, la, 'ko')
    
# %% plot angle of DOAS instrument
    x0, y0 = df[0]['lon'], df[0]['lat']
    length =  0.6
    fac = {}
    angles = {'T1': 321, 'T2': 51, 'T3': 141, 'T4': 231}
    for telescope,angle in angles.items():
        temp_fac = np.zeros(lat.shape)
        endx = length * np.sin(np.radians(angle))
        endy = length * np.cos(np.radians(angle))
        ax.plot([x0, x0+endx], [y0, y0+endy], c='k')
        ax.annotate(telescope, xy=(x0+endx, y0+endy),  xycoords='data',
                 xytext=(x0+endx, y0+endy), textcoords='data', color='k',
                 horizontalalignment='right', verticalalignment='bottom',
                 arrowprops=dict(facecolor='black', shrink=0.05))
        A,B,C,D = draw_rectangle(49.9909, 8.2283, 0.04, 0.05, angle, ax)
    # =============================================================================
    # at 50 degree north, 1 degree is ~71 km
    # =============================================================================
        polygon = Polygon([A, B, C, D])
        if not polygon.is_valid:
            polygon = Polygon([A, C, B, D])
        for i in range(180):
            for j in range(160):
                M = [lat[i, j], lon[i, j]]
                if polygon.contains(Point(M)):
                    temp_fac[i,j] = 1
        fac[telescope] = temp_fac
    plt.tight_layout()
    plt.savefig(os.path.join(dirname_sim, 'Model_maps', 'map_rect_'+datetime.strftime(time_dsp[timestep],
                                                                  '%Y%m%d_%H')+'.png'),
        format='png', dpi=300)
    plt.close()
#'''
#plot factors 
#A matrix of 0 and 1 which corresponding to pixels which are considered in a
#for calcuating VCD along given direction within assumed path length
#request = cimgt.GoogleTiles()
fig, ax=plt.subplots(subplot_kw=dict(projection=ccrs.PlateCarree()))
ax.set_extent([7, 9.4, 48.9, 51.2])
gl = ax.gridlines(crs=ccrs.PlateCarree(), draw_labels=True,
                    linewidth=1, color='gray', alpha=0.5, linestyle='--')
gl.xlabels_top = False
gl.ylabels_right = False
#ax.add_image(request, 8, interpolation='spline36')
#llons, llats = np.meshgrid(lon, lat)
scat = ax.scatter(lon, lat, c=np.ma.masked_where(v0*fac['T1']==0,v0*fac['T1']), cmap='jet',
             vmin=0, vmax=1.5E16, transform=ccrs.PlateCarree())
for point in df:
    lo, la = point['lon'], point['lat']
    plt.annotate(point['site'], xy=(lo, la),  xycoords='data',
                 xytext=(lo, la), textcoords='data', color='k',
                 horizontalalignment='left', verticalalignment='bottom',
                 arrowprops=dict(facecolor='black', shrink=0.05))
    plt.plot(lo, la, 'ko')
scat = ax.pcolormesh(llons, llats, np.ma.masked_where(v0*fac['T2']==0,v0*fac['T2']), cmap='jet',
             vmin=0, vmax=1.5E16, transform=ccrs.PlateCarree(), label='T2')
np.savetxt(os.path.join(dirname_sim, 'factor_T2_5_meco.txt'),fac['T1'])
np.savetxt(os.path.join(dirname_sim, 'factor_T3_5_meco.txt'),fac['T2'])
np.savetxt(os.path.join(dirname_sim, 'factor_T4_5_meco.txt'),fac['T3'])
np.savetxt(os.path.join(dirname_sim, 'factor_T1_5_meco.txt'),fac['T4'])
#'''