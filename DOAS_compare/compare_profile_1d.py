# -*- coding: utf-8 -*-
"""
Created on Thu Oct 31 17:35:49 2019

@author: Vinod
"""

# %% imports
import pandas as pd
import numpy as np
import netCDF4
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
from matplotlib.dates import DateFormatter
import os
from tools import doy_2_datetime

# %% load model data
dirname_sim = r'M:\nobackup\vinod\model_work\MECO\DOAS_COMP'
filename_sim = 'SAT_CHEM18_5_no2_1hr_T106_regrid_small.nc'
f_sim = netCDF4.Dataset(os.path.join(dirname_sim, filename_sim), 'r')
tracer = 'NO2'
lat = f_sim.variables['lat'][:]
lon = f_sim.variables['lon'][:]
aps = f_sim.variables['aps'][:]
tm1 = f_sim.variables['tm1_ave'][:]
timeax = f_sim.variables['time'][:]
time_dsp = [datetime.strptime(f_sim.variables['time'].units[11:30],
                              '%Y-%m-%d %H:%M:%S') + timedelta(days=i)
            for i in timeax]
prs_m = np.array([f_sim.variables['dhyam'][i] +
                  (aps[:]*f_sim.variables['dhybm'][i])
                  for i in range(0, 50)])
prs_m = np.transpose(prs_m, (1, 0, 2, 3))
prs_m[prs_m < 0] = np.nan
tracer_mr = f_sim.variables[tracer+'_ave'][:]
tracer_conc = tracer_mr*6.023E23*prs_m*1E-6/8.314/tm1

# load factors for viewing directions
fac = {}
tracer_prof_sim = {}
for tel in ["T1", "T2", "T3", "T4"]:
    fac[tel] = np.genfromtxt(os.path.join(dirname_sim,
                             'factor_{}_5.txt'.format(tel)))
    tracer_prof_sim[tel] = tracer_conc*fac[tel]
    tracer_prof_sim[tel] = np.ma.masked_where(tracer_prof_sim[tel] == 0,
                                              tracer_prof_sim[tel])
    tracer_prof_sim[tel] = np.ma.mean(tracer_prof_sim[tel], 2)
    tracer_prof_sim[tel] = np.ma.mean(tracer_prof_sim[tel], 2)

'''
calculation of box height
'''
h_interface = f_sim.variables['hhl'][:]
h1d = h_interface*fac['T4']
h1d = np.ma.masked_where(h1d == 0, h1d)
h1d = np.ma.mean(h1d, 1)
h1d = np.ma.mean(h1d, 1)
hagl = h1d - h1d[-1]

# %% load doas profiles
meas_path = r'M:\nobackup\vinod\model_work\MECO\DOAS_COMP\inversion_results'
for tel in ["T1", "T2", "T3", "T4"]:
    meas_date_time = np.genfromtxt(os.path.join(meas_path, 'frac_day_' +
                                                tel+'.txt'))
    meas_date_time = np.array([doy_2_datetime(i, 2018)
                              for i in meas_date_time])
    idx_tropomi_overpass = np.array([i.hour for i in meas_date_time]) == 11
#    data between 13 and 14 local time in CEST (11 and 12 UTC)
    meas_profile = np.genfromtxt(os.path.join(meas_path, 'no2_pro_' +
                                              tel+'.txt'), delimiter=',')
    meas_grid = np.genfromtxt(os.path.join(meas_path, 'profile_grid.txt'),
                              delimiter=',')
    meas_grid = meas_grid
    # %% plotting
    fig, ax = plt.subplots(2, 1, sharex=True, figsize=[10, 5])
    pcm = ax[0].pcolormesh(meas_date_time, meas_grid, meas_profile.T,
                           vmin=1E10, vmax=4E11, cmap="Reds",
                           label='DOAS '+tel)  # YlOrRd
    ax[0].set_ylabel('Height (km)')
    ax[0].set_ylim(0, 4)
    ax[0].text(0.9, 0.9, 'DOAS ' + tel, transform=ax[0].transAxes,
               bbox=dict(facecolor='white', edgecolor='black'))
    ax[0].minorticks_on()
    pcs = ax[1].pcolormesh(time_dsp, hagl[26:52]/1000,
                           tracer_prof_sim[tel][:,25:51].T,
                           vmin=1E10, vmax = 4E11, cmap="Reds")
    ax[1].set_ylabel('Height (km)')
    ax[1].set_xlabel('Date and time (UTC)')
    ax[1].set_xlim([pd.to_datetime('05-06-2018 05:00:00'),
                   pd.to_datetime('05-08-2018 19:00:00')])
    ax[1].set_ylim(0, 4)
    ax[1].xaxis.set_major_formatter(DateFormatter('%d-%m %H'))
    ax[1].text(0.9, 0.9, 'MECO(n)', transform=ax[1].transAxes,
               bbox=dict(facecolor='white', edgecolor='black'))
    ax[1].minorticks_on()
    plt.tight_layout()
    fig.subplots_adjust(right=0.85)
    cbar_ax = fig.add_axes([0.89, 0.15, 0.02, 0.7])
    cbar = fig.colorbar(pcs, cax=cbar_ax, extend="both")
    cbar.set_label(tracer + ' concentration (mcl cm$^{-3}$)', labelpad=2)
    plt.savefig(os.path.join(meas_path, 'profile_' + tel+'_3d_5.png'),
                format='png', dpi=300)
# %% mean vertical profiles
meas_profile_overpass = []
for tel in ["T1", "T2", "T3", "T4"]:
    meas_date_time = np.genfromtxt(os.path.join(meas_path, 'frac_day_' +
                                                tel+'.txt'))
    meas_date_time = np.array([doy_2_datetime(i, 2018)
                              for i in meas_date_time])
    idx_tropomi_overpass = np.array([i.hour for i in meas_date_time]) == 11
#    data between 13 and 14 local time in CEST (11 and 12 UTC)
    meas_profile = np.genfromtxt(os.path.join(meas_path, 'no2_pro_' +
                                              tel+'.txt'), delimiter=',')
    meas_profile_overpass.append(np.nanmean(meas_profile[idx_tropomi_overpass, :], 0))
