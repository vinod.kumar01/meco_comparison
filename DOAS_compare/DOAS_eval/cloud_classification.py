# -*- coding: utf-8 -*-
"""
Created on Thu Aug  8 09:47:00 2019

@author: Vinod
Script for cloud classification
according to T. Wagner et.al., https://www.atmos-meas-tech.net/9/4803/2016/
and
T. Wagner et.al., https://www.atmos-meas-tech.net/7/1289/2014/ for optional
thick cloud identification
"""

import numpy as np
import os
import tools
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from scipy import exp
# from collections import defaultdict


def fitFunc(x, A0, A, mu, sigma):
    return A0 + A*exp(-((x-mu)**2/(2.*sigma**2)))


# %%data preparation
Inp = {'sfc_height': 150, 'ignore_elev': [-0.5, 0.0, 0.99], 'seq': None,
       'Thick_cloud_method': 'O4', 'O4_dSCD': 'O4_fix.SlCol(o4)',
       'O4_dSCD_err': 'O4_fix.SlErr(o4)'}

dirname = r'D:\wat\minimax\Mainz_4az\results\T4'
savedir = dirname
f_count = 0
for period in ["20180327_20180513", "20180514_20180914"]:
    filename_in = period + '.ASC'
    qdoas_file = os.path.join(dirname, filename_in)
    if f_count == 0:
        data = tools.qdoas_loader(qdoas_file, skip_rows=1)
    else:
        data = data.append(tools.qdoas_loader(qdoas_file, skip_rows=1))
    f_count += 1

rel_col = ['Date_time', 'Name', 'Fractional day', 'SZA', 'Solar Azimuth Angle',
           Inp['O4_dSCD'], Inp['O4_dSCD_err'],
           'Fluxes 330', 'Fluxes 360', 'Fluxes 390', 'Scans',	'Tint']
if Inp['seq'] is None:
    Inp['seq'] = len(data['Name'].unique()) - len(Inp['ignore_elev'])
else:
    input('Number of elevation angles are {}. Are you sure?\
                \nIf yes, press Enter to continue'.format(Inp['seq']))
data = data[rel_col]
# Calculation of radiance : Might be analysis software dependent QDOAS/WINDOAS
data['Radiance 360'] = 1000*data['Fluxes 360']/(data['Scans']*data['Scans']*data['Tint'])
CI = data['Fluxes 330']/data['Fluxes 390']
VCD_O4 = tools.O4_VCD_sfc(298, Inp['sfc_height'])
dAMF_O4 = data[Inp['O4_dSCD']]/VCD_O4
dAMF_O4_err = data[Inp['O4_dSCD_err']]/VCD_O4
data['CI'] = CI
data['VCD_O4'] = VCD_O4
data['dAMF_O4'] = dAMF_O4
data['dAMF_O4_err'] = dAMF_O4_err
data = data[~np.isin(data['Name'], Inp['ignore_elev'])].reset_index(drop=True)
data90 = data[data['Name'] == 90].reset_index(drop=True)
idx_90 = np.array(np.where(data['Name'] == 90))[0, :]

# %% generate thresholds
s_norm = data90['SZA']/90  # SZA normalized
# CI Clear-sky reference
poly_CI_CSR = np.poly1d([8.399, -24.253, 29.143, -21.056, 7.673, -0.197, 0.964])
CI_CSR = poly_CI_CSR(s_norm)

# CI Threshold value
poly_CI_TH = np.poly1d([-0.654, 0.367, 2.647, -6.006, 3.576, -0.094, 0.779])
CI_TH = poly_CI_TH(s_norm)

# CI Minimum value
poly_CI_MIN = np.poly1d([-5.261, 8.045, 0.621, -6.588, 3.029, 0.09, 0.66])
CI_MIN = poly_CI_MIN(s_norm)

# CI TSI
poly_TSI_TH = np.poly1d([13.66, -32.298, 28.522, -14.468, 4.644, -0.288, 0.304])
TSI_TH = poly_TSI_TH(s_norm)*0.06

# O4 AMF zenith +0.85 for threthold
poly_O4_TH = np.poly1d([-81.975, 197.773, -172.649, 64.482, -7.832, 0.964, 1.265])
O4_TH = poly_O4_TH(s_norm)

'''
I do not use 0.85 here as it is only required for threshold for thick cloud
identification. For O4 normalization, it is used as it is

'''
# %%CI normalization

CI_Norm = data90['CI']/CI_MIN
idx_60 = data90['SZA'] < 60
n_bins = np.arange(0, 3.01, 0.01)
count, bins = np.histogram(CI_Norm[data90['SZA'] < 60], n_bins)
count = count/np.sum(count)
p0 = [0, np.max(count), n_bins[np.argmax(count)], 0.2]
fitParams, fitCovariances = curve_fit(fitFunc, bins[:-1], count, p0)
fit_line = fitFunc(bins[:-1], fitParams[0], fitParams[1],
                   fitParams[2], fitParams[3])
calibration_CI_scale = 1/fitParams[2]
# calibration_CI_scale = 1/1.627
print('CI scaled calibration value is ' + str(calibration_CI_scale))

fig, ax = plt.subplots()
ax.scatter(bins[:-1], count, s=2, label='CI distribution')
ax.plot(bins[:-1], fit_line, '-', color='r', label='Gauss fit')
ax.legend(loc='upper right')
ax.set_xlabel('Normalized CI')
ax.set_ylabel('Relative frequency')
ax.grid(alpha=0.4)
plt.show()

# %% generate CI quantity for cloud classification
CI_scaled = data90['CI'] * calibration_CI_scale
daystr = data90['Date_time'].apply(str)
daystr = daystr.str[0:11]
days = list(set(daystr))
days.sort()

TSI_scaled = CI_scaled.copy()
TSI_scaled[:] = 0
#
for day in days:
    idx_day = daystr.index[daystr == day]
    if len(idx_day) > 2:
        TSI_scaled[idx_day[0]] = 0
        TSI_scaled[idx_day[-1]] = 0
#        TSI_scaled[idx_day[1]:idx_day[-1]] = abs((CI_scaled[idx_day[:-2]] +
#                 CI_scaled[idx_day[2:]])/2-CI_scaled[idx_day[1:-1]])
        TSI_scaled[idx_day[1:-1]] = abs((CI_scaled[idx_day[:-2]].values +
                 CI_scaled[idx_day[2:]].values)/2-CI_scaled[idx_day[1:-1]].values)

spread_CI = CI_scaled.copy()
spread_CI[:] = 0
for i_sequence, day in enumerate(daystr):
    if i_sequence==0:
        spread_CI[i_sequence]=(max(data['CI'][0:idx_90[i_sequence]+1]) -
                  min(data['CI'][0:idx_90[i_sequence]+1]))*calibration_CI_scale
    else:
        if idx_90[i_sequence]-idx_90[i_sequence-1] < Inp['seq']:
            spread_CI[i_sequence] = 0  #incomplete sequence having zenith
        elif idx_90[i_sequence]-idx_90[i_sequence-1] > Inp['seq']:
            spread_CI[i_sequence]=(max(data['CI'][idx_90[i_sequence]-(Inp['seq']-1):idx_90[i_sequence]+1]) -
                  min(data['CI'][idx_90[i_sequence]-(Inp['seq']-1):idx_90[i_sequence]+1]))*calibration_CI_scale
                     #incomplete sequence having zenith
        else:
            spread_CI[i_sequence]=(max(data['CI'][idx_90[i_sequence-1]+1:idx_90[i_sequence]+1]) -
                  min(data['CI'][idx_90[i_sequence-1]+1:idx_90[i_sequence]+1]))*calibration_CI_scale

# %%primary cloud classification
# primary classification: clear sky low aerosols 1, clear sky high
# aerosols 2,cloud holes 3, broken clouds 4, continuous clouds 5
ctype = np.zeros((len(idx_90), 3))
idx1 = (CI_scaled >= CI_TH) & (TSI_scaled < TSI_TH)
ctype[idx1, 0] = 1
idx2 = (CI_scaled < CI_TH) & (TSI_scaled < TSI_TH) & (spread_CI >= 0.14)
ctype[idx2, 0] = 2
idx3 = (CI_scaled >= CI_TH) & (TSI_scaled >= TSI_TH)
ctype[idx3, 0] = 3
idx4 = (CI_scaled < CI_TH) & (TSI_scaled >= TSI_TH)
ctype[idx4, 0] = 4
idx5 = (CI_scaled < CI_TH) & (TSI_scaled < TSI_TH) & (spread_CI < 0.14)
ctype[idx5, 0] = 5

# %% fog and thick cloud classification
# O4 damf spread calculation
spread_O4 = np.zeros(len(CI_scaled))
for i_sequence, day in enumerate(daystr):
    if i_sequence == 0:
        spread_O4[i_sequence] = (max(data['dAMF_O4'][0:idx_90[i_sequence]+1]) -
                                 min(data['dAMF_O4'][0:idx_90[i_sequence]+1]))
    else:
        if idx_90[i_sequence]-idx_90[i_sequence-1] < Inp['seq']:
            spread_O4[i_sequence] = np.nan
        elif idx_90[i_sequence]-idx_90[i_sequence-1] > Inp['seq']:
            spread_O4[i_sequence]=(max(data['dAMF_O4'][idx_90[i_sequence]-(Inp['seq']-1):idx_90[i_sequence]+1]) -
                  min(data['dAMF_O4'][idx_90[i_sequence]-(Inp['seq']-1):idx_90[i_sequence]+1]))
        else:    
            spread_O4[i_sequence]=(max(data['dAMF_O4'][idx_90[i_sequence-1]+1:idx_90[i_sequence]+1]) -
                  min(data['dAMF_O4'][idx_90[i_sequence-1]+1:idx_90[i_sequence]+1]))
# fog
idx6 = (CI_scaled < CI_TH) & (spread_O4 < 0.37)
ctype[idx6, 1] = 1
# O4 calibration
if Inp['Thick_cloud_method'] == 'O4':
    idx_o4_calib = (ctype[:,0]<=3) & (data90['SZA'] > 30) & (data90['SZA'] < 50)
    data_o4_norm = data90['dAMF_O4'] - O4_TH
    data_o4_norm = data_o4_norm[idx_o4_calib]
    n_bins = np.arange(-3, 3.01, 0.01)
    count, bins = np.histogram(data_o4_norm, n_bins)
    count = count/np.sum(count)
    p0 = [0, np.max(count), n_bins[np.argmax(count)], 0.1]
    fitParams, fitCovariances = curve_fit(fitFunc, bins[:-1], count, p0)
    fit_line = fitFunc(bins[:-1], fitParams[0], fitParams[1], fitParams[2], fitParams[3])
    calibration_O4_FRS = np.negative(fitParams[2])
    # use calibration O4 FRS by manually looking at the graph
    # calibration_O4_FRS = 1.6
    print('O4 FRS AMF is ' + str(calibration_O4_FRS))
    
    fig,ax = plt.subplots()
    ax.scatter(bins[:-1], count, s=2, label='O4 AMF distribution')
    ax.plot(bins[:-1], fit_line, '-', color='r', label='Gauss fit')
    ax.legend(loc='upper right')
    ax.set_xlabel('Difference of measured O$_{4}$ DAMF and O$_{4}$ Threshold')
    ax.set_ylabel('Relative frequency')
    ax.grid(alpha=0.4)
    plt.show()
    
    O4_AMF = data90['dAMF_O4'] + calibration_O4_FRS

# Thick clouds
idx7 = (ctype[:, 0] >= 4) & (O4_AMF > O4_TH + 0.85)
ctype[idx7, 2] = 1
# %% optional radiance @ 360 threshold (Mohali specific)
if Inp['Thick_cloud_method'] == 'Radiance':
    ctype[:, 2] = 0
    #poly_RAD_TH = np.poly1d([-9.63825749E+09, 3.57488464E+10, -5.27091245E+10,
    #                        3.96026018E+10, -1.60495508E+10, 3.04217835E+09])
    rad_calib_param = 1.48E-08
    calib_radiance =  data90['Radiance 360']*rad_calib_param
    #poly_RAD_TH = np.poly1d([-1.11750E-02 , 3.51173E+00, -4.37084E+02, 
    #                         2.69092E+04, -9.12360E+05, 1.83665E+07])
    #for not calibrated
    poly_RAD_TH = np.poly1d([-1.523473E-10, 4.900129E-08, -6.218891E-06,
                             3.887413E-04, -1.334278E-02, 2.708933E-01])
        # for calibrated
    
    RAD_TH = poly_RAD_TH(data90['SZA'])
    idx8 = calib_radiance < 0.94*RAD_TH
    ctype[idx8, 2] = 1
## trend over long time series
#numdays = (data90['datetime']-pd.to_datetime('20121203', format='%Y%m%d')).dt.days
#numdays += data90['Fractional day']%1
#trend = np.polyfit(numdays[(data90['SZA']>45) & (data90['SZA']<50)],
#                           data90[(data90['SZA']>45) & (data90['SZA']<50)]['Fluxes 360'], 1)
# %% save output
data_cloud_classified = np.c_[data90[['Date_time',
                                      'Fractional day', 'SZA']].values,ctype[:, 0:3]]
np.savetxt(os.path.join(savedir, 'cloud_classified_Mainz_T4.csv'),
           data_cloud_classified, delimiter=',',
           fmt=('%s','%.3f', '%.2f', '%i', '%i', '%i'),
           header='Date_time, Fractional_Day, SZA, cloud_type, fog, Thick clouds_o4')
# calculate frequency
Freq = [len(ctype[:,0][ctype[:,0]==i])/len(ctype[:,0]) for i in [1,2,3,4,5]]
Freq.append(len(ctype[:,1][ctype[:,1]==1])/len(ctype[:,0]))
Freq.append(len(ctype[:,2][ctype[:,2]==1])/len(ctype[:,0]))
print('Percentage of \n clear sky low aerosols : ' + str(Freq[0]*100)
        + '\n clear sky high aerosol : ' + str(Freq[1]*100)
        + '\n cloud holes : ' + str(Freq[2]*100)
        + '\n broken clouds : ' + str(Freq[3]*100)
        + '\n continuous clouds : ' + str(Freq[4]*100)
        + '\n fog : ' + str(Freq[5]*100)
        + '\n thick clouds : ' + str(Freq[6]*100))