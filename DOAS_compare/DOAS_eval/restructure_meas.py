# -*- coding: utf-8 -*-
"""
Created on Tue Oct 27 13:59:22 2020
Copy measured spetra of 4Azimuth instrument and restructure to analyse
@author: Vinod
"""

import os
from datetime import date, timedelta
from shutil import copyfile

src_dir = r'M:\nobackup\jremmers\mfc'
dest_dir = r'D:\wat\minimax\Mainz_4az\spectra'
start_date = date(2018, 3, 30)
end_date = date(2018, 9, 14)
date_range = [start_date]
num_days = 1 + (end_date - start_date).days
date_range = [start_date + timedelta(days=x) for x in range(num_days)]

for d in date_range:
    print(d.strftime('%Y%m%d'))
    for tel in ['T1', 'T2', 'T3', 'T4']:
        src_dir_now = os.path.join(src_dir, d.strftime('%Y%m%d'), tel)
        if os.path.isdir(src_dir_now):
            dest_dir_now = os.path.join(dest_dir, tel, d.strftime('%Y%m%d'))
            os.makedirs(dest_dir_now, exist_ok=True)
            for root, dirs, files in os.walk(src_dir_now, topdown=False):
                for file in files:
                    copyfile(os.path.join(root, file),
                             os.path.join(dest_dir_now, file))
        else:
            print('No measurements for ' + d.strftime('%Y%m%d'))
