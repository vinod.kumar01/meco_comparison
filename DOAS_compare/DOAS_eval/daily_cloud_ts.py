# -*- coding: utf-8 -*-
"""
Created on Wed Oct 28 11:35:23 2020

@author: Vinod
"""

# %% data import
import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.dates as mdates
from tools import datetime2time

inp_dir = r'D:\wat\minimax\Mainz_4az\results'
tel = 'T1'
file_name = 'cloud_classified_Mainz_' + tel + '.csv'
df = pd.read_csv(os.path.join(inp_dir, tel, file_name), delimiter=',',
                 escapechar='#')
df.columns = df.columns.str.strip()
df['Date_time'] = pd.to_datetime(df['Date_time'].apply(str),
                                 format='%Y-%m-%d %H:%M:%S')
df['date'] = df['Date_time'].dt.date
df['time'] = df.apply(lambda x: datetime2time(x['Date_time']), axis=1)
# %% plotting
cmap = mpl.colors.ListedColormap(['b', 'goldenrod', 'plum', 'teal', 'k'])
norm = mpl.colors.BoundaryNorm(np.arange(1, 6), cmap.N)
width = 60
height = 30
verts = list(zip([-width, width, width, -width],
                 [-height, -height, height, height]))
fig, ax = plt.subplots(figsize=[8, 5])
scat = ax.scatter(df['date'], df['time'], marker=verts, s=90,
                  c=df['cloud_type'], cmap=cmap, label=None, vmin=1, vmax=5)
if len(df['cloud_type'].unique()) < 5:
    ax2 = ax.twinx()
    scat = ax2.scatter(np.zeros(5), np.zeros(5), marker=verts,
                       s=90, c=1+np.arange(5), cmap=cmap, label=None)
    ax2.get_yaxis().set_visible(False)
scat2 = ax.scatter(df[df['Thick clouds_o4'] == 1]['date'],
                   df[df['Thick clouds_o4'] == 1]['time'], marker=verts,
                   s=90, c='r', label='Thick\nclouds')
scat3 = ax.scatter(df[df['fog'] == 1]['date'],
                   df[df['fog'] == 1]['time'], marker=verts,
                   s=90, c='grey', label='fog')
ax.set_ylim([pd.to_datetime('1900-01-01 03:00:00'),
            pd.to_datetime('1900-01-01 19:45:00')])
ax.set_xlim([pd.to_datetime('2018-04-30 13:00:00'),
             pd.to_datetime('2018-05-31 14:00:00')])
ax.yaxis.set_major_formatter(mdates.DateFormatter('%H'))
ax.xaxis.set_major_formatter(mdates.DateFormatter('%d-%m'))
ax.set_ylabel('Time (UTC)', size=14)
props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
ax.text(0.02, 0.95, tel, transform=ax.transAxes, bbox=props)
cb = plt.colorbar(scat)
ticks = [1.6, 2.4, 3.2, 4.0, 4.8]
cb.set_ticks(ticks)
labels = ['Clear sky \nlow aerosol', 'Clear sky\nhigh\naerosol',
          'Cloud\nholes', 'Broken\nclouds', 'Continuous\nclouds']
cb.set_ticklabels(labels)
plt.tight_layout()
ax.legend(loc=[1.11, -0.063])
ax.grid(alpha=0.3)
plt.savefig(os.path.join(inp_dir, file_name.replace('csv', 'png')),
            format='png', dpi=300)
