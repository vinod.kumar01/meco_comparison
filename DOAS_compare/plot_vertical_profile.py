# -*- coding: utf-8 -*-
"""
Created on Wed Sep  9 11:58:13 2020

@author: Vinod
"""

import netCDF4
import os
import numpy as np
import cartopy.crs as ccrs
from plot_tools import map_prop
import matplotlib.pyplot as plt
from datetime import datetime, timedelta

# %% load data
tracer = 'NO2'
dirname_sim = r'M:\nobackup\vinod\model_work\MECO\DOAS_COMP'
filename_sim = 'UBA_di_18_5_1day.nc'
f_sim = netCDF4.Dataset(os.path.join(dirname_sim, filename_sim), 'r')
timeax = f_sim.variables['time'][:]
tm1 = f_sim.variables['tm1_ave'][:]
dhyam = f_sim.variables['dhyam'][:]
dhybm = f_sim.variables['dhybm'][:]
tracer_mr = f_sim.variables[tracer+'_ave'][:]
h_interface = f_sim.variables['hhl'][:]
lat = f_sim.variables['lat'][:]
lon = f_sim.variables['lon'][:]
aps = f_sim.variables['aps'][:]
try:
    time_dsp = [datetime.strptime(f_sim.variables['time'].units[10:30],
                                  '%Y-%m-%d %H:%M:%S') + timedelta(days=i)
                for i in timeax]
except ValueError:
    time_dsp = [datetime.strptime(f_sim.variables['time'].units[11:30],
                                  '%Y-%m-%d %H:%M:%S') + timedelta(days=i)
                for i in timeax]
h = np.ma.masked_array([h_interface[i, :] - h_interface[i+1, :]
                        for i in range(0, len(dhyam))])
prs_m = np.array([dhyam[i] + (aps[:]*dhybm[i])
                  for i in range(0, len(dhyam))])
prs_m = np.transpose(prs_m, (1, 0, 2, 3))
tracer_conc = tracer_mr*6.023E23*prs_m*1E-6/8.314/tm1