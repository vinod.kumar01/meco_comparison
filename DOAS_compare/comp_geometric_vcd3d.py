# -*- coding: utf-8 -*-
"""
Created on Thu Oct 31 22:14:09 2019

@author: Vinod
"""

# %% imports
from os import path
import pandas as pd
import numpy as np
from datetime import datetime, timedelta, date
import glob
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from plot_tools import plot_scatter_ax
from tools import doy_2_datetime, lin_fit, tracer_prop, load_cloud, wind
import filetools as ft

no2 = {'name': 'NO2', 'label': 'NO$_2$', 'vcd_unit': 'molecules cm$^{-2}$',
       'conc_unit': 'molecules cm$^{-3}$', 'vcd_lim': [0, 3e16]}
hcho = {'name': 'HCHO', 'label': 'HCHO', 'vcd_unit': 'molecules cm$^{-2}$',
        'conc_unit': 'molecules cm$^{-3}$', 'vcd_lim': [-2e16, 8e16]}
tracer = tracer_prop(**no2)
mask_dscd = True
add_cloud_mask = False
# Flag to mask all the geomteric vcds where calcualted dscd is not available.
# or in other words, O4 profile inversion did nto work.
# %% Measured met data
start_date = date(2018, 5, 4)
end_date = date(2018, 5, 23)
met_file = r'M:\home\jremmers\Projects\MAD_CAT2013\Datasets\Wetter_Uni\wetterdaten_mpic\2018station1.txt'
wdf = pd.read_csv(met_file, header=None, delim_whitespace=True,
                 names=['Date_time', 'c1', 'c2', 'c3', 'c4', 'c5', 'c6', 'c7',
                        'wd', 'ws', 'ws_max'])
wdf['Date_time'] = pd.to_datetime(wdf['Date_time'], format='%y%m%d%H%M%S')
wdf = wdf.set_index('Date_time')
winds = wind(ws=wdf['ws'], wd=wdf['wd'])
wdf['wind_u'], wdf['wind_v'] = winds.sep_wind()
idx = (wdf.index.date >= start_date) & (wdf.index.date <= end_date)
idx &= (~wdf['wind_u'].isnull()) & (~wdf['wind_v'].isnull())
wdf_sel = wdf[(wdf.index.date >= start_date) & (wdf.index.date <= end_date)]
wdf_sel = wdf_sel.resample('H').mean()
winds = wind(u=wdf_sel['wind_u'], v=wdf_sel['wind_v'])
wdf_sel['ws_mean'], wdf_sel['wd_mean'] = winds.merge_uv()
wdf = wdf_sel[(wdf_sel.index.hour >= 5) & (wdf_sel.index.hour <= 21)][3::3]
# %% load model data
dirname_sim = r'M:\nobackup\vinod\model_work\MECO\DOAS_COMP'
exp = 'UBA_di_2km'  # T42, T106, T42_diurnal, T106_7km, T106_rot
sim_conc = ft.hdf2dict(path.join(dirname_sim,
                                 'sim_conc_{}_{}.hdf'.format(exp, tracer.name)))
# =============================================================================
# I use sim_dSCD to conly consider the geometric for which sim dSCDS
# are available to make the comparison fair
# =============================================================================
sim_dscd = ft.hdf2dict(path.join(dirname_sim,
                                 'sim_dSCD_{}_{}.hdf'.format(exp, tracer.name)))
date_time = [datetime.strptime(sim_conc['T1']['date_time']['unit'],
                               '%Y-%m-%d %H:%M:%S') + timedelta(days=i)
             for i in sim_conc['T1']['date_time']['data']]
meas_hgrid = sim_conc['T1']['h_grid']['data']  # m
grid_width = (meas_hgrid[1:]-meas_hgrid[:-1])*100  # cm
sim_vcd = {}
for tel in ["T1", "T2", "T3", "T4"]:
    sim_vcd[tel] = np.dot(sim_conc[tel]['conc']['data'], grid_width[:33])
fig2, ax2 = plot_scatter_ax(histx=True, histy=True)
fit_data = {'sim': np.array([]), 'meas': np.array([])}
fit_stats_idv = {}
for tel, tel_color in {'T1': 'g', 'T2': 'b', 'T3': 'r', 'T4': 'k'}.items():
    cloud_dat = load_cloud(tel)
    df_sim = pd.DataFrame(data={'date_time': date_time,
                                'vcd_3': np.mean(sim_vcd[tel][:, 5:8], 1),
                                'vcd_6': np.mean(sim_vcd[tel][:, 5:11], 1),
                                'dscd': sim_dscd[tel]['30']['dSCD']['data']})
    df_sim = df_sim.set_index('date_time')
    idx_high_sza = df_sim.index.hour.isin([4, 19])
    df_sim[idx_high_sza] = np.nan
    # MAX-DOAS is located at grid 5 of conc grid
    # %% DOAS data
    geo_angle = 30
    if tracer.name == 'NO2':
        # dirname_meas = r'M:\home\jremmers\Projects\MAD_CAT2013\results\spectral_results\vis_171118_180313'
        dirname_meas = r'M:\nobackup\vinod\model_work\MECO\DOAS_COMP\4azim_doas'
        f_names = glob.glob(path.join(dirname_meas, tracer.name, 'no2_' + tel+'_201805*.asc'))
    elif tracer.name == 'HCHO':
        dirname_meas = r'M:\home\jremmers\Projects\MAD_CAT2013\results\spectral_results\HCHO_130618_170531'
        f_names = glob.glob(path.join(dirname_meas,'HCHO_'+tel+'_2018050*.asc'))
    f_names.sort(key=path.basename)
    col_names = pd.read_csv(f_names[0], dtype=None,
                            delimiter='\t', header=45).columns
    df = pd.concat(pd.DataFrame(np.loadtxt(f, comments='*')) for f in f_names)
    df.columns = col_names
    df['date_time'] = df.apply(lambda x: doy_2_datetime(x['DOY']+1, 2018),
                               axis=1)
    # DOY start with 0.0 for January 1st, 0:00 UTC
    df_geo = df[df['Elev_angle'] == geo_angle]
    df_geo['geo_vcd'] = df_geo[tracer.name+'_DSCD']
    df_geo['geo_vcd'] /= ((1/np.sin(geo_angle*np.pi/180)-1))
    df_geo = df_geo.set_index('date_time')
    df_geo = df_geo[(df_geo.index.date >= start_date) &
                    (df_geo.index.date < end_date)]
    df_geo_mean = df_geo.resample('H').mean()
    # get inversion VCD
    inv_path = r'M:\nobackup\vinod\model_work\MECO\DOAS_COMP\inversion_results'
    inv_vcd = np.loadtxt(path.join(inv_path, 'vcd_'+tel+'.txt'))
    inv_frac_day = np.loadtxt(path.join(inv_path, 'frac_day_'+tel+'.txt'))
    df_inv = pd.DataFrame(data={'frac_day': inv_frac_day, 'vcd': inv_vcd})
    df_inv['date_time'] = df_inv.apply(lambda x: doy_2_datetime(x['frac_day'],
                                       2018), axis=1)
    df_inv = df_inv.set_index('date_time')
    # %% plotting
    fig, ax = plt.subplots(figsize=[15, 6])
    eb = ax.errorbar(df_geo_mean.index,
                     df_geo_mean['geo_vcd'],
                     df_geo.resample('H').std()['geo_vcd'],
                     c='r', fmt='-+', capsize=4, label='MAX-DOAS\n(Geometric)')
    [bar.set_alpha(0.5) for bar in eb[2]]
    eb = ax.errorbar(df_inv.resample('H').mean().index,
                     df_inv.resample('H').mean()['vcd'],
                     df_inv.resample('H').std()['vcd'],
                     c='k', fmt='-x', capsize=4, label='PiMAX-3D')
    [bar.set_alpha(0.5) for bar in eb[2]]
    ax.plot(df_sim.index, df_sim['vcd_3'], 'b-d', label='CM02')
    # ax.plot(df_sim.index, df_sim['vcd_6'], 'g-.o', label='MECO(n) 6km')
    bbox_props = {'boxstyle': "round", 'fc': "y", 'ec': "0.5", 'alpha': 0.8}
    ax.annotate('('+chr(96+int(tel[1]))+') '+tel, xy=[0.01, 0.94], size=18,
                xycoords='axes fraction',  bbox=bbox_props)
    ax.set_ylim(tracer.vcd_lim)
    ax.set_xlim(start_date, end_date)
    ax.set_ylabel(" NO$_2$ VCD (molecules cm$^{-2}$)", fontsize=16)
    ax.tick_params(axis='both', which='major', labelsize=16)
    ax.grid(axis='y', alpha=0.4)
    plt.minorticks_on()
    cloud_ax = ax.twinx()
    cloud_ax.set_yticks([])
    cloud_ax.set_ylim(tracer.vcd_lim[1])
    cloud_ax.xaxis.set_major_formatter(mdates.DateFormatter("%d-%b"))
    for cloud_type, cloud_val in {'Broken clouds': 4,
                                  'Cont. clouds': 5}.items():
        cloud_sel = cloud_dat[cloud_dat['cloud_type'] == cloud_val]
        cloud_ax.bar(cloud_sel.index, tracer.vcd_lim[1], color='grey',
                     alpha=0.3*(cloud_val-3), width=0.03, label=cloud_type)
    cloud_sel = cloud_dat[cloud_dat['Thick clouds_o4'] == 1]
    cloud_ax.bar(cloud_sel.index, tracer.vcd_lim[1], color='r',
           alpha=0.4, width=0.03, label='Optically\nthick clouds')
    if tel == 'T2':
        ax.legend(loc='upper right', ncol=3, fontsize=18,
                  fancybox=True, framealpha=0.7)
    if tel == 'T4':
        cloud_ax.legend(loc='upper right', ncol=3, fontsize=18,
                        fancybox=True, framealpha=0.7)
    if tel == 'T1':
        ax.barbs(wdf.index, 2.5e16, wdf['wind_u'], wdf['wind_v'], color='m',
                 barb_increments={'half': 1, 'full': 2, 'flag': 5})
    plt.tight_layout()
    cloud_rel = cloud_dat.reindex(df_geo_mean.index)
    mask_cloud = (cloud_rel['cloud_type'].isin([4, 5]))
    mask_cloud |= (cloud_rel['Thick clouds_o4'] == 1)
    if mask_dscd:
        dscd_mask = df_sim.reindex(df_geo_mean.index)['dscd'].isnull()
    else:
        dscd_mask = df_sim.reindex(df_geo_mean.index)['vcd_3'].isnull()
        
    fig.savefig(path.join(dirname_meas, tracer.name,
                          tel+'_30_ts_comp3d_5_10.png'),
                format='png', dpi=300)
    ax2['scat'].scatter(df_geo_mean['geo_vcd'][~dscd_mask],
                        df_sim.reindex(df_geo_mean.index)['vcd_3'][~dscd_mask],
                        c=tel_color, s=4, label=tel, alpha=1)
    if add_cloud_mask:
        dscd_mask |= mask_cloud
    # Daily means
    # ax2['scat'].scatter(df_geo_mean['geo_vcd'][~dscd_mask].resample('D').mean(),
    #                     df_sim.reindex(df_geo_mean.index)['vcd_3'][~dscd_mask].resample('D').mean(),
    #                     c=tel_color, s=32, label=None, marker='d')
    # temporary fit for one telecope
    tf = lin_fit(df_geo_mean['geo_vcd'][~dscd_mask].values,
                 df_sim.reindex(df_geo_mean.index)['vcd_3'][~dscd_mask].values)
    fit_stats_idv[tel] = tf.stats_calc(frac_range=[0.5, 2])
    fit_stats_idv[tel]['bias'] /= fit_stats_idv[tel]['x_mean']
    fit_stats_idv[tel]['rmse'] /= fit_stats_idv[tel]['x_mean']
    fit_data['meas'] = np.append(fit_data['meas'],
                                 df_geo_mean['geo_vcd'][~dscd_mask].values)
    fit_data['sim'] = np.append(fit_data['sim'],
                                df_sim.reindex(df_geo_mean.index)['vcd_3'][~dscd_mask].values)
ax2['scat'].set_ylim([0, 3E16])
ax2['scat'].set_xlim([0, 3E16])
mask = np.isnan(fit_data['meas']) | np.isnan(fit_data['sim'])
fit_stats = lin_fit(fit_data['meas'][~mask], fit_data['sim'][~mask])
fit_param = fit_stats.stats_calc(frac_range=[0.5, 2])
fit_stats.add_fit_lines(ax=ax2['scat'], frac_range=[0.5, 2],
                        add_regression=False)
ax2['scat'].set_xlabel('MAX-DOAS geometric NO$_2$ VCD (molecules cm$^{-2}$)')
ax2['scat'].set_ylabel('Simulated NO$_2$ VCD (molecules cm$^{-2}$)')
ax2['scat'].legend(loc='upper left')
bbox_props = dict(boxstyle="round", fc="w", ec="0.5", alpha=0.9)
ax2['scat'].annotate('{}'.format(exp[:6]), xy=(0.7, 0.9),
                     xycoords='axes fraction', size=12, bbox=bbox_props)
fit_stats.add_fit_param(pos_x=0.36, pos_y=0.88, ax=ax2['scat'], color='k',
                        showr=True, showfrac=True, show_fit_eqn=False)
ax2['histx'].hist(fit_data['meas'][~mask], bins=20)
ax2['histy'].hist(fit_data['sim'][~mask], bins=20, orientation='horizontal')
ax2['histy'].set_xlim(130, 0)
fig2.savefig(path.join(dirname_meas, tracer.name,
                        'geovcd_30_scatter_{}.png'.format(exp)),
              format='png', dpi=300)
for tel in ['T1', 'T2', 'T3', 'T4']:
    print(tel+':{:.2f}, {:.2f}, {:.2f}'.format(fit_stats_idv[tel]['rmse']*100,
                                               fit_stats_idv[tel]['bias']*100,
                                               fit_stats_idv[tel]['r']))
