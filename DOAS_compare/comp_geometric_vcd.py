# -*- coding: utf-8 -*-
"""
Created on Mon Oct 15 14:17:45 2018

@author: Vinod
"""
# %% imports
import netCDF4
import os
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from datetime import datetime, timedelta, date
import glob
from scipy.interpolate import interp1d
#import scipy.io as sio

# %% model data
dirname_sim = r'M:\nobackup\vinod\model_work\MECO\DOAS_COMP'
filename_sim = 'SCOUT___201805_MNZ_new.nc'
# SCOUT___201504_MNZ.nc  # scout_mnz_2015_04_03.nc
f_sim = netCDF4.Dataset(os.path.join(dirname_sim, filename_sim), 'r')
tracer = 'NO2'
telescope = 'T2'
geo_angle = 15
timeax = f_sim.variables['time'][:]
time_dsp = [datetime.strptime(f_sim.variables['time'].units[10:30],
                              '%Y-%m-%d %H:%M:%S') + timedelta(days=i)
            for i in timeax]
try:
    aps = f_sim.variables['COSMO_ps_ave'][:]
    tracer_sim_conc = f_sim.variables['tracer_gp_' + tracer+'_conc'][:]
except KeyError:
    aps = f_sim.variables['aps'][:]
    tracer_sim_conc = f_sim.variables[tracer + '_ave_conc'][:]
'''
calculation of box height
'''
try:
    h_interface = f_sim.variables['COSMO_ORI_HHL_ave'][:]
    h = np.array([h_interface[:, i] - h_interface[:, i+1]
                  for i in range(0, 50)])
    h = h.T

except KeyError:
    prs_i = np.array([f_sim.variables['dhyai_ave'][i] +
                      (aps[:]*f_sim.variables['dhybi_ave'][i])
                      for i in range(0, 51)])
    prs_i[prs_i < 0] = np.nan
    h_interface = np.array([7.4*np.log(101325/prs_i[i, :])
                            for i in range(0, 51)])*1000   # in meters
    h = np.array([h_interface[i, :] - h_interface[i+1, :]
                  for i in range(0, 50)])
    h = h.transpose()*f_sim.variables['COSMO_tm1_ave'][:, :]/253
    # correct for assumed scale height at 250K
# except KeyError:
#     h = f_sim.variables['grvol_ave'][:]/f_sim.variables['gboxarea_ave'][:]
#############################
tracer_sim_vcd_box = tracer_sim_conc[:, :]*h*100  # (molecules/cm2)
if len(h[0, :]) == 60:
    tracer_sim_geovcd = np.nansum(tracer_sim_vcd_box[:, 43:60], 1)
elif len(h[0, :]) == 40:
    tracer_sim_geovcd = np.nansum(tracer_sim_vcd_box[:, 24:40], 1)
elif len(h[0, :]) == 50:
    tracer_sim_geovcd = np.nansum(tracer_sim_vcd_box[:, 26:50], 1)
# fig=plt.Figure()
# ax = fig.add_subplot(111)
# plt.title('Time series of ' + tracer + ' VCD (upto 1 km)')
# plt.plot(time_dsp,tracer_sim_geovcd,'b.-', label = 'COSMO')
# plt.ylabel(tracer + " VCD (molecules cm$^{-2}$)", fontsize=10)
# plt.minorticks_on()
# plt.gcf().autofmt_xdate()
# plt.legend(loc = 'upper left')
# %% doas data
if tracer == 'NO2':
    # dirname_meas = r'M:\home\jremmers\Projects\MAD_CAT2013\results\spectral_results\vis_171118_180313'
    dirname_meas = r'M:\nobackup\vinod\model_work\MECO\DOAS_COMP\4azim_doas'
    f_names = glob.glob(os.path.join(dirname_meas, tracer, 'no2_' + telescope+'_201805*.asc'))
elif tracer == 'HCHO':
    dirname_meas = r'M:\home\jremmers\Projects\MAD_CAT2013\results\spectral_results\HCHO_130618_170531'
    f_names = glob.glob(os.path.join(dirname_meas,'HCHO_'+telescope+'_201805*.asc'))
f_names.sort(key=os.path.basename)
col_names = pd.read_csv(f_names[0],dtype=None, delimiter='\t', header=45).columns
df = pd.concat(pd.DataFrame(np.loadtxt(f,comments='*')) for f in f_names)
df.columns = col_names
DOM = df['DOY'].values-117
df['date_time']=[datetime.strptime(f_sim.variables['time'].units[10:30],'%Y-%m-%d %H:%M:%S')+ timedelta(days=i) for i in DOM]
#df['date_time']=time_dsp_meas
df_geo = df[df['Elev_angle']==geo_angle]
geo_vcd = df_geo[tracer+'_DSCD']/((1/np.sin(geo_angle*np.pi/180)-1))
dscd_err = df_geo[tracer+'_error']
'''
plt.scatter(time_dsp_meas,geo_vcd,c='red', s=4, label = 'DOAS-'+telescope)
plt.plot(time_dsp,tracer_sim_geovcd,'b.-', label = 'COSMO')
plt.ylim([0,2E16])
plt.xlim([datetime.date(2015, 4, 1), datetime.date(2015, 4, 24)])
plt.ylabel(tracer + " geometric VCD (molecules cm$^{-2}$)", fontsize=10)
plt.minorticks_on()
plt.gcf().autofmt_xdate()
plt.legend(loc = 'upper left')


'''
# %% plotting
fig, ax = plt.subplots(figsize=[12,6])
ax.scatter(df_geo['date_time'],geo_vcd.values,c='red', s=4, label = 'DOAS-'+telescope)
ax.plot(time_dsp,tracer_sim_geovcd,'b.-', label = 'COSMO')
# ax.scatter(time_dsp_meas,dscd_err.values,c='red', s=4, label = 'DOAS-'+telescope)
ax.errorbar(df_geo['date_time'], geo_vcd.values, yerr=dscd_err.values, fmt='.',
            color='r', ecolor='pink', elinewidth=1, capsize=0)
ax.set_ylim([0, 3E16])
ax.set_xlim([date(2018, 5, 1), date(2018, 6, 1)])
ax.set_ylabel(tracer + " geometric VCD (molecules cm$^{-2}$)", fontsize=10)
plt.minorticks_on()
#plt.gcf().autofmt_xdate()
ax.legend(loc='upper left')
plt.tight_layout()
plt.savefig(os.path.join(dirname_meas, tracer, telescope+'_ts_comp.png'), format='png', dpi=300)
#'''

# %% plot radiance as estimaton of optically thick clouds
fig,ax = plt.subplots(figsize=[12,6])
df_90 = df[df['Elev_angle']==15]
ax.scatter(df_90['date_time'],df_90['Radiance440'],c='red', s=4, label = 'DOAS-'+telescope)
ax.set_xlim([date(2018, 5, 1), date(2018, 6, 1)])
ax.set_ylabel("Radiance @ 440nm", fontsize=10)
plt.minorticks_on()
plt.tight_layout()
plt.savefig(os.path.join(dirname_meas, tracer, telescope+'_rad_440.png'), format='png', dpi=300)


# %% interpolte model dta on doas grid
sfc_height = h_interface[0,-1]
h_centre = np.array(h_interface[:,1:]+h_interface[:,:-1])/2
h_centre = h_centre-sfc_height
doas_grid = np.arange(0,5.02,0.02)
doas_grid_centre = 1000*np.array(doas_grid[1:]+doas_grid[:-1])/2  # in km
no2_sim_intp = np.zeros([tracer_sim_conc.shape[0], len(doas_grid_centre)])
for dt in range(tracer_sim_conc.shape[0]):
    no2_sim_fn = interp1d(h_centre[0,:],tracer_sim_conc[dt,:])
    no2_sim_intp[dt, :] = no2_sim_fn(doas_grid_centre)
tracer_sim_geovcd_intp = np.sum(no2_sim_intp[:, 0:200]*20*100, 1)

# savedict = {}
# savedict['time'] = timeax+117
# savedict[tracer + '_conc'] = tracer_sim_conc
# savedict[tracer + '_vcd_box'] = tracer_sim_vcd_box
# savedict['box_height'] = h
# sio.savemat(r'M:\nobackup\vinod\model_work\MECO\DOAS_COMP\profile_model.mat',
#             mdict=savedict)
