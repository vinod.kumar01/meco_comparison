# -*- coding: utf-8 -*-
"""
Created on Tue Jun 25 09:36:42 2019

@author: Vinod
"""

# %% imports
import pandas as pd
import numpy as np
import netCDF4
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
from matplotlib.dates import DateFormatter, HourLocator
import os
from tools import doy_2_datetime
from mod_colormap import add_white
new_cm = add_white('Reds', replace_frac=0.01, start=None, transparent=False,
                   shrink_colormap=True)
plot_daily_profile = False

# %% load model data
dirname = r'M:\nobackup\vinod\model_work\MECO\DOAS_COMP'
tracer = 'NO2'
f = netCDF4.Dataset(os.path.join(dirname, 'SCOUT___201805_MNZ_new.nc'), 'r')
timeax = f.variables['time'][:]
time_dsp = [datetime.strptime(f.variables['time'].units[10:30],
                              '%Y-%m-%d %H:%M:%S') + timedelta(days=i)
            for i in timeax]
tracer_sim_conc = f.variables['tracer_gp_' + tracer + '_conc'][:]
h_interface = f.variables['COSMO_ORI_HHL_ave'][:]
h = np.array([h_interface[:, i] - h_interface[:, i+1] for i in range(0, 50)])
h = h.T

# %% plot daily model profile
if plot_daily_profile:
    plt.ioff()
    for day in range(31):
        fig, ax = plt.subplots(figsize=[8, 4])
        pcs = ax.pcolormesh(time_dsp[(day-1)*24+5:day*24-4],
                            h_interface[0, 30:52]/1000,
                            tracer_sim_conc[(day-1)*24+5:day*24-4, 30:52].T,
                            vmin=1E9, vmax=4E11, cmap=new_cm)
        ax.minorticks_on()
        ax.xaxis.set_major_formatter(DateFormatter('%d-%m %H'))
        ax.xaxis.set_major_locator(HourLocator(interval=4))
        ax.xaxis.set_minor_locator(HourLocator(interval=1))
        ax.set_ylabel('Altitude (km)')
        ax.set_xlabel('Date and time (UTC)')
        cb = plt.colorbar(pcs, extend="max", shrink=0.9)
        cb.set_label(tracer+' concentration (mcl cm$^{-3}$)', labelpad=2)
        plt.tight_layout()
        plt.savefig(os.path.join(dirname, 'inversion_results',
                                 'daily_profiles', 'sim_pro_'+str(day)+'.png'),
                    format='png', dpi=200)
        plt.close()
    plt.ion()

# %% load doas profiles
Telescope = 'T1'
meas_date_time = np.genfromtxt(os.path.join(dirname, 'inversion_results',
                                            'frac_day_'+Telescope+'.txt'))
meas_date_time = np.array([doy_2_datetime(i, 2018) for i in meas_date_time])
meas_profile = np.genfromtxt(os.path.join(dirname, 'inversion_results',
                                          'no2_pro_'+Telescope+'.txt'),
                             delimiter=',')
meas_grid = np.genfromtxt(os.path.join(dirname, 'inversion_results',
                                       'profile_grid.txt'), delimiter=',')
meas_grid = meas_grid+0.1
# %% plotting
day = 8
fig, ax = plt.subplots(2, 1, sharex=True, sharey=True, figsize=[10, 5])
pcm = ax[0].pcolormesh(meas_date_time, meas_grid, meas_profile.T, vmin=1E9,
                       vmax=4E11, cmap=new_cm, label='MAX-DOAS '+Telescope)
ax[0].set_ylabel('Altitude (amsl, Km)')
ax[0].set_ylim(0, 4)
ax[0].text(0.01, 0.9, 'MAX-DOAS ' + Telescope, transform=ax[0].transAxes,
           bbox=dict(facecolor='white', edgecolor='black'))
ax[0].minorticks_on()

pcs = ax[1].pcolormesh(time_dsp[(day-3)*24:day*24], h_interface[0, 30:52]/1000,
                       tracer_sim_conc[(day-3)*24:day*24, 30:52].T,
                       vmin=1E10, vmax=4E11, cmap=new_cm)
ax[1].set_ylabel('Altitude (amsl, Km)')
ax[1].set_xlabel('Date and time (UTC)')
ax[1].set_xlim([pd.to_datetime('05-06-2018 05:00:00'),
                pd.to_datetime('05-08-2018 19:00:00')])
ax[1].xaxis.set_major_formatter(DateFormatter('%d-%m %H'))
ax[1].text(0.01, 0.9, 'MECO(n)', transform=ax[1].transAxes,
           bbox=dict(facecolor='white', edgecolor='black'))
ax[1].minorticks_on()
plt.tight_layout()
fig.subplots_adjust(right=0.85)
cbar_ax = fig.add_axes([0.89, 0.15, 0.02, 0.7])
cbar = fig.colorbar(pcs, cax=cbar_ax, extend="max")
cbar.set_label(tracer+' concentration (mcl cm$^{-3}$)', labelpad=2)

plt.savefig(os.path.join(dirname, 'inversion_results',
                         'profile_'+Telescope+'.png'), format='png', dpi=300)
