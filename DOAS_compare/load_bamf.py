# -*- coding: utf-8 -*-
"""
Created on Sat May  2 18:05:39 2020

@author: Vinod
"""
from os import path
import scipy.io as sio
import h5py
import numpy as np
from tools import matlab2datetime
from geopy.distance import distance


def inversion_bAMF_laoder(dirname=None, day='180506'):
    if dirname is None:
        dirname = r'M:\home\jremmers\Projects\Vinod\results\3D_inversion_v2'
    mat_file = path.join(dirname, 'aerosol_'+day, 'aerosol_'+day+'.mat')
    try:
        f = h5py.File(mat_file, 'r')
    except OSError:
        f = h5py.File(mat_file.replace('.mat', '_incomplet.mat'), 'r')
    e_res_ref = f.get('inversion/e_res')[:]
    meas_ref = f.get('inversion/meas')[:]
    data = []
    date_time = []
    for i in range(e_res_ref.shape[0]):
        try:
            data.append(np.array(f[e_res_ref[i][0]]['resDirMinAMF'][0]))
            datenum_now = np.array(f[meas_ref[i][0]]['datenum'])[0, 0]
            date_time.append(matlab2datetime(datenum_now))
        except KeyError:
            pass
    return(np.array(data), np.array(date_time))


def bamf_loader(mat_file):
    f = sio.loadmat(mat_file)
    bAMF = {}
    elevs = [1, 2, 3, 4, 5, 6, 8, 10, 15, 30]
    for i, tel in enumerate(["T1", "T2", "T3", "T4"]):
        bAMF[tel] = {}
        if len(f['boxAMF'][i][0][:, 1, 1]) == 10:
            # Telescopes which include 1 degree elevation angle
            for e, ei in enumerate(elevs):
                bAMF[tel][ei] = f['boxAMF'][i][0][e, :]
        else:
            # Telescopes which miss 1 degree elevation angle
            for e, ei in enumerate(elevs[1:]):
                bAMF[tel][ei] = f['boxAMF'][i][0][e, :]
    f.clear()
    return bAMF


def bamf_dayloader(dirname=None, day='180506', year=2018):
    # if day is not specifiec, it will load bAMF for 06 may 2018
    if dirname is None:
        dirname = r'M:\home\jremmers\Projects\Vinod\results\3D_inversion_v2'
#    try:
#        frac_Days = np.genfromtxt(path.join(dirname, day, 'frac_day_' +
#                                            day+'.txt'))
#        date_time = np.array([doy_2_datetime(i, year) for i in frac_Days])
#    except OSError:
#        date_time = inversion_bAMF_laoder(dirname=None, day=day)[1]
    mpic = (49.9909, 8.2283)
    grid_path = r'M:\nobackup\vinod\model_work\MECO\DOAS_COMP\4azim_doas'
    h_grid = np.genfromtxt(path.join(grid_path, 'bamf_altitude.txt'))
    rms, date_time = inversion_bAMF_laoder(dirname=None, day=day)
    bAMF_day = {}
    coords = {}
    displacement = {}
    for tel in ["T1", "T2", "T3", "T4"]:
        coords[tel] = np.genfromtxt(path.join(grid_path,
                                              f'bamf_lat_lon_{tel}.txt'))
        displacement[tel] = [distance(mpic, i).kilometers for i in coords[tel]]
        displacement[tel] = np.array(displacement[tel])
        if tel in ["T1", "T2"]:
            displacement[tel][0:50] = 0. - displacement[tel][0:50]
        else:
            displacement[tel][50:] = 0. - displacement[tel][50:]
    for idx, frac_day in enumerate(date_time):
        filename = 'boxAMF_{}.mat'.format(idx+1)
        mat_file = path.join(dirname, 'aerosol_'+day, 'boxAMF_LUT', filename)
        datetext = frac_day.strftime('%Y%m%d_%H%M')
        try:
            bAMF = bamf_loader(mat_file)
            if not bAMF_day:
                bAMF_day['bAMF'] = {}
                bAMF_day['coords'] = coords
                bAMF_day['displacement'] = displacement
                bAMF_day['h_grid'] = h_grid
            bAMF_day['bAMF'][datetext] = bAMF
            for i, tel in enumerate(["T1", "T2", "T3", "T4"]):
                bAMF_day['bAMF'][datetext][tel]['rms'] = rms[idx, i]
        except (FileNotFoundError, TypeError) as e:
            print('bAMF not available for ' + datetext)
    return(bAMF_day)


def bamf_daylistloader(dirname=None, dtlist=['180506', '180507'], year=2018):
    if dirname is None:
        dirname = r'D:\postdoc\COSMO-CCLM\DOAS_COMP\bAMF'
    if len(dtlist) < 2:
        bamf_dtlist = bamf_dayloader(dirname=dirname, day=dtlist[0], year=year)
    else:
        bamf_dtlist = bamf_dayloader(dirname=dirname, day=dtlist[0], year=year)
        for day in dtlist[1:]:
            try:
                bamf_dtlist['bAMF'].update(bamf_dayloader(dirname=dirname,
                                           day=day, year=year)['bAMF'])
            except OSError:
                print('bAMF not available for ' + day)
    return bamf_dtlist


def bAMF_mean(dirname=None, dtlist=['180506', '180507'],
              rms_th=0.04, year=2018):
    if dirname is None:
        dirname = r'M:\home\jremmers\Projects\Vinod\results\3D_inversion_v2'
    bAMF_day = bamf_daylistloader(dirname=dirname, dtlist=dtlist, year=year)
    meas_keys = [i for i in bAMF_day['bAMF'].keys()]
    hourlist = set([i[:11] for i in meas_keys])
    sel_measkeys = {}
    bAMF_hm = {'bAMF': {}, 'coords': bAMF_day['coords'],
               'displacement': bAMF_day['displacement'],
               'h_grid': bAMF_day['h_grid']}
    for hour in hourlist:
        hhmm = hour+'00'
        keep = np.array([i[:11] for i in meas_keys]) == hour
        sel_measkeys[hhmm] = np.array(meas_keys)[keep]
        bAMF_hm['bAMF'][hhmm] = {'T1': {}, 'T2': {}, 'T3': {}, 'T4': {}}
        for tel in ["T1", "T2", "T3", "T4"]:
            bAMF_hm['bAMF'][hhmm][tel] = {}
            for ei in [2, 3, 4, 5, 6, 8, 10, 15, 30]:
                mean_bAMF = []
                for key in sel_measkeys[hhmm]:
                    if bAMF_day['bAMF'][key][tel]['rms'] <= rms_th:
                        mean_bAMF.append(bAMF_day['bAMF'][key][tel][ei])
                mean_bAMF = np.array(mean_bAMF)
                mean_bAMF = np.nanmean(mean_bAMF, 0)
                bAMF_hm['bAMF'][hhmm][tel][ei] = np.array(mean_bAMF)
    return(bAMF_hm)
