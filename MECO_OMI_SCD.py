# -*- coding: utf-8 -*-
"""
Created on Wed Jun 20 15:21:41 2018

@author: Vinod
"""

#script to compare MECO(n) and OMI SCD. It uses the gridded MECO(n) output and interpolated on OMI pressure grids and uses averaging kernels.
import netCDF4
import os
import numpy as np
import matplotlib.pyplot as plt
import datetime
from scipy import stats
from mpl_toolkits.basemap import Basemap
dirname = r'M:\home\vinod\nobackup\model_work\MECO'
filename_sim = 'TNO_201504_sorbit_AURA-A_omi25s.nc'
if "TNO_" in filename_sim:
    savepath = r'M:\home\vinod\nobackup\model_work\MECO\Plots\OMI_comp\TNO\ '
elif "EC_" in filename_sim:
    savepath = r'M:\home\vinod\nobackup\model_work\MECO\Plots\OMI_comp\EC\ '
elif "RCP_" in filename_sim:
    savepath = r'M:\home\vinod\nobackup\model_work\MECO\Plots\OMI_comp\RCP\ '
filename_sat = 'OMI-Aura_L2-OMDOMINO_2015m04_merge_025.nc'
#regr = linear_model.LinearRegression()
path_sim = os.path.join(dirname, filename_sim)
path_sat = os.path.join(dirname, filename_sat)
f_sim=netCDF4.Dataset(path_sim,'r')
counter = 0
timestamp = f_sim.variables['time']
f_sat=netCDF4.Dataset(path_sat,'r')
NO2_omitropcol = f_sat.variables['TroposphericVerticalColumn']
fv=NO2_omitropcol._FillValue
lats=f_sim.variables['lat'][:]
lons=f_sim.variables['lon'][:]
plt.ioff
for day_num in range(1,21):
    d = day_num-1
    date_display=datetime.datetime.strptime(timestamp.units[10:30],'%Y-%m-%d %H:%M:%S')+ datetime.timedelta(days=timestamp[d]-1)
    TropoPauseLevel = f_sat.variables['TropoPauseLevel'][d,:,:]
    if "_050s" in filename_sat:
        TropoPauseLevel.mask = np.ma.nomask
    TropoPauseLevel[TropoPauseLevel>34]=0
    ak = f_sat.variables['AveragingKernel'][d,:,:,:]*1E-3   #averaging kernel
    ap = f_sat.variables['TM4profile'][d,:,:,:]*1E15        #satellite a priori profile
    trop_amf_sat=f_sat.variables['AirMassFactorTropospheric'][d,:,:]
    #tropSCD_sat=f_sat.variables['SlantColumnAmountNO2'][:]-f_sat.variables['AssimilatedStratosphericSlantColumn'][:]  #striped SCD
    NO2_omitropcol = f_sat.variables['TroposphericVerticalColumn'][d,:,:]
    tropSCD_sat=NO2_omitropcol*trop_amf_sat
    tropSCD_sat[f_sat.variables['CloudFraction'][d,:]*1E-3>0.3]=np.nan ##cloud fraction
    tropSCD_sat[tropSCD_sat<0]=np.nan
    #tropSCD_sat[f_sat.variables['CloudFraction'][:]*1E-3>0.3]=np.nan ##cloud fraction
    aps=f_sim.variables['aps'][d-1,:,:]
    aps.mask = np.ma.nomask
    prs_i = np.array([f_sim.variables['hyai'][i]+(aps[:]*f_sim.variables['hybi'][i]) for i in range(0,35)])
    prs_i[prs_i<0]=np.nan
    h_interface=np.array([7.4*np.log(aps[:]/prs_i[i,:]) for i in range(0,35)])*1000   #in meters
    h=np.array([h_interface[i,:]-h_interface[i-1,:] for i in range(1,35)])
    h=h*f_sim.variables['COSMO_tm1'][d-1,:]/250    #correct for assumed scale height at 250K
    #h = np.array([f_sim.variables['grvol'][d,i,:,:]/f_sim.variables['gboxarea'][d,:,:] for i in range(0,34)])
    h=h*100
    h[h<=0]=np.nan
    NO2_conc=f_sim.variables['tracer_gp_NO2_conc'][d-1,:,:,:]
    NO2_conc[(NO2_conc<=1E7) | (NO2_conc>=1E12)]=np.nan
    NO2_tropvcdbox = NO2_conc[:,:,:]*h[:,:,:]  #without averaging kernels
    NO2_tropvcdbox[(NO2_tropvcdbox>1E18) | (NO2_tropvcdbox<1E7)]=np.nan
    NO2_tropvcd_sim = np.nansum(NO2_tropvcdbox,0)
    NO2_tropvcd_sat = np.nansum(ap,0)
    for i in range(0,len(lats)):
        for j in range(0,len(lons)):
            tpl = int(round(TropoPauseLevel[i,j]))
            TropoPauseLevel[i,j] = tpl
            NO2_tropvcd_sim[i,j] = np.nansum(NO2_tropvcdbox[0:tpl+1,i,j])
            NO2_tropvcd_sat[i,j] = np.nansum(ap[0:tpl+1,i,j])
    #NO2_vcd=np.nansum(h*NO2_conc,0)
    NO2_tropvcd_sim[(NO2_tropvcd_sim<=1E12) | (NO2_tropvcd_sim>=1E19)]=np.nan
    trop_profile_sim = np.array([NO2_tropvcdbox[i,:,:]/NO2_tropvcd_sim[:,:] for i in range(0,34)])
    trop_profile_sat = np.array([ap[i,:,:]/NO2_tropvcd_sat[:,:] for i in range(0,34)])
    trop_amf_scale=np.nansum(trop_profile_sim*ak,0)*trop_amf_sat/np.nansum(trop_profile_sat*ak,0)
    for i in range(0,len(lats)):
        for j in range(0,len(lons)):
            tpl = int(round(TropoPauseLevel[i,j]))
            TropoPauseLevel[i,j] = tpl
            trop_amf_scale[i,j] = np.nansum(trop_profile_sim[0:tpl+1,i,j]*ak[0:tpl+1,i,j])/np.nansum(trop_profile_sat[0:tpl+1,i,j]*ak[0:tpl+1,i,j])
    trop_amf_sim=trop_amf_scale*trop_amf_sat
    trop_amf_sim[trop_amf_sim==0]=np.nan
    trop_SCD_sim=trop_amf_sim*NO2_tropvcd_sim
    trop_SCD_sim[f_sat.variables['CloudFraction'][d,:]*1E-3>0.3]=np.nan ##cloud fraction filter on model product for comparison
    fig=plt.Figure()
    plt.title('MECO(n) Tropospheric NO$_{2}$ SCD for ' + date_display.strftime('%d-%b-%Y'))
    ax = fig.add_subplot(1, 1, 1)
    if "_050s" in filename_sat:
        map = Basemap(projection='cyl', resolution = 'l', llcrnrlon=-60, llcrnrlat=4,urcrnrlon=90, urcrnrlat=80)
        map.drawparallels(np.arange(5.,75.,15.),labels=[1,0,0,0])
        map.drawmeridians(np.arange(-60.,90.,30.),labels=[0,0,0,1])
    else:    
        map = Basemap(projection='cyl', resolution = 'l', llcrnrlon=1, llcrnrlat=43,urcrnrlon=20, urcrnrlat=56)
        map.drawparallels(np.arange(30.,60.,4.),labels=[1,0,0,0])
        map.drawmeridians(np.arange(0.,20.,4.),labels=[0,0,0,1])
    map.drawcoastlines()
    map.drawcountries()
    map.drawmapboundary()
    llons, llats = np.meshgrid(lons, lats)
    x,y = map(llons, llats)
    cs = map.pcolormesh(x, y, trop_SCD_sim*1E-15, cmap='jet', vmin = 0, vmax = 8)
    cb=map.colorbar(extend = 'both')
    cb.set_label('NO2_Trop_SCD * 10$^{15}$ molecules cm$^{-2}$' , fontsize=11)
    plt.savefig(savepath + 'MECO_SCD_3_'+ date_display.strftime('%Y%m%d') +'.png', format = 'png', dpi = 200)
    plt.close()
    #plot satellite vcd
    fig=plt.Figure()
    plt.title('OMI Tropospheric NO$_{2}$ SCD for ' + date_display.strftime('%d-%b-%Y'))
    ax = fig.add_subplot(1, 1, 1)
    if "_050s" in filename_sat:
        map = Basemap(projection='cyl', resolution = 'l', llcrnrlon=-60, llcrnrlat=4,urcrnrlon=90, urcrnrlat=80)
        map.drawparallels(np.arange(5.,75.,15.),labels=[1,0,0,0])
        map.drawmeridians(np.arange(-60.,90.,30.),labels=[0,0,0,1])
    else:    
        map = Basemap(projection='cyl', resolution = 'l', llcrnrlon=1, llcrnrlat=43,urcrnrlon=20, urcrnrlat=56)
        map.drawparallels(np.arange(30.,60.,4.),labels=[1,0,0,0])
        map.drawmeridians(np.arange(0.,20.,4.),labels=[0,0,0,1])
    map.drawcoastlines()
    map.drawcountries()
    map.drawmapboundary()
    x,y = map(llons, llats)
    cs = map.pcolormesh(x, y, tropSCD_sat, cmap='jet', vmin = 0, vmax = 8)
    #cs = map.pcolormesh(x, y, NO2_tropvcd*1E-15-V0, cmap='jet', vmin = -7, vmax = 4 )  #for bias
    cb=map.colorbar(extend = 'both')
    cb.set_label('NO2_Trop_col * 10$^{15}$ molecules cm$^{-2}$' , fontsize=10)
    plt.savefig(savepath + 'OMI_SCD_3_'+ date_display.strftime('%Y%m%d') +'.png', format = 'png', dpi = 200)
    plt.close()
    ##############comparison plot of old and new airmass factors
    fig, axes = plt.subplots(nrows=2, ncols=1, figsize=[8,9])
    plt.suptitle('Comparisons of Tropospheric AMF for ' + date_display.strftime('%d-%b-%Y'))
    axes[0,].set_title( 'OMI data product', fontsize=10)
    map = Basemap(projection='cyl', resolution = 'l', llcrnrlon=1, llcrnrlat=43,urcrnrlon=20, urcrnrlat=56, ax=axes[0])
    map.drawparallels(np.arange(30.,60.,4.),labels=[1,0,0,0])
    map.drawmeridians(np.arange(0.,20.,4.),labels=[0,0,0,1])
    map.drawcoastlines()
    map.drawcountries()
    map.drawmapboundary()
    cs1 = map.pcolormesh(x, y, trop_amf_sat, cmap='jet', vmin = 0, vmax = 2.5)
    cb1=fig.colorbar(cs1, cmap=plt.cm.get_cmap('jet'), ax=axes[0], orientation='vertical',extend = 'both', fraction = 0.1, shrink = 0.8)
            #######################
    axes[1].set_title( 'calculated usig model profile',fontsize=10)
    map = Basemap(projection='cyl', resolution = 'l', llcrnrlon=1, llcrnrlat=43,urcrnrlon=20, urcrnrlat=56, ax=axes[1])
    map.drawparallels(np.arange(30.,60.,4.),labels=[1,0,0,0])
    map.drawmeridians(np.arange(0.,20.,4.),labels=[0,0,0,1])
    map.drawcoastlines()
    map.drawcountries()
    map.drawmapboundary()
    cs2 = map.pcolormesh(x, y, trop_amf_sim, cmap='jet', vmin = 0, vmax = 2.5)
    cb2=fig.colorbar(cs2, cmap=plt.cm.get_cmap('jet'), ax=axes[1], orientation='vertical',extend = 'both', fraction = 0.1, shrink = 0.8)
    plt.savefig(savepath + 'comp_AMF_3'+ date_display.strftime('%Y%m%d') +'.png', format = 'png', dpi = 200)
    plt.close()
    #####################################
    #########calculate and plot average over given number of days###########
    if counter == 0:
        mean_meco=trop_SCD_sim[:,:,np.newaxis]
        mean_omi=tropSCD_sat[:,:,np.newaxis]
        mean_amf_old=trop_amf_sat[:,:,np.newaxis]
        mean_amf_new=trop_amf_sim[:,:,np.newaxis]
    else:
        mean_meco=np.dstack((mean_meco, trop_SCD_sim))
        mean_omi=np.dstack((mean_omi, tropSCD_sat))
        mean_amf_old=np.dstack((mean_amf_old, trop_amf_sat))
        mean_amf_new=np.dstack((mean_amf_new, trop_amf_sim))
    counter=counter+1
################################Overview plot of all days with correlation##########
plt.ion
fig, axes = plt.subplots(nrows=2, ncols=2, figsize=[12,10])
plt.suptitle('Comparisons of Tropospheric NO$_{2}$ SCD for ' + date_display.strftime('%b-%Y'))
axes[0, 0].set_title( 'COSMO NO2_Trop_SCD (*1E15 molecules cm-2)', fontsize=10)
map = Basemap(projection='cyl', resolution = 'l', llcrnrlon=1, llcrnrlat=43,urcrnrlon=20, urcrnrlat=56, ax=axes[0,0])
map.drawparallels(np.arange(30.,60.,4.),labels=[1,0,0,0])
map.drawmeridians(np.arange(0.,20.,4.),labels=[0,0,0,1])
map.drawcoastlines()
map.drawcountries()
map.drawmapboundary()
llons, llats = np.meshgrid(lons, lats)
x,y = map(llons, llats)
cs1 = map.pcolormesh(x, y, np.nanmean(mean_meco,2)*1E-15, cmap='jet', vmin = 0, vmax = 8)
cb1=fig.colorbar(cs1, cmap=plt.cm.get_cmap('jet'), ax=axes[0,0], orientation='vertical',extend = 'both', fraction = 0.1, shrink = 0.8)
################################
axes[0, 1].set_title( 'OMI NO2_Trop_SCD (*1E15 molecules cm-2)',fontsize=10)
map = Basemap(projection='cyl', resolution = 'l', llcrnrlon=1, llcrnrlat=43,urcrnrlon=20, urcrnrlat=56, ax=axes[0,1])
map.drawparallels(np.arange(30.,60.,4.),labels=[1,0,0,0])
map.drawmeridians(np.arange(0.,20.,4.),labels=[0,0,0,1])
map.drawcoastlines()
map.drawcountries()
map.drawmapboundary()
cs2 = map.pcolormesh(x, y, np.nanmean(mean_omi,2), cmap='jet', vmin = 0, vmax = 8)
cb2=fig.colorbar(cs2, cmap=plt.cm.get_cmap('jet'), ax=axes[0,1], orientation='vertical',extend = 'both', fraction = 0.1, shrink = 0.8)
################################
axes[1,0].set_title( '(COSMO-OMI) NO2_Trop_SCD(*1E15 molecules cm-2)', fontsize=10)
map = Basemap(projection='cyl', resolution = 'l', llcrnrlon=1, llcrnrlat=43,urcrnrlon=20, urcrnrlat=56, ax=axes[1,0])
map.drawparallels(np.arange(30.,60.,4.),labels=[1,0,0,0])
map.drawmeridians(np.arange(0.,20.,4.),labels=[0,0,0,1])
map.drawcoastlines()
map.drawcountries()
map.drawmapboundary()
cs3 = map.pcolormesh(x, y, np.nanmean(mean_meco,2)*1E-15-np.nanmean(mean_omi,2), cmap='jet', vmin = -3, vmax = 3)
cb3=fig.colorbar(cs3, cmap=plt.cm.get_cmap('jet'), ax=axes[1,0], orientation='vertical',extend = 'both', fraction = 0.1, shrink = 0.8)
################################
axes[1,1].set_title( 'Correlation')
SCD_sim_flat=np.nanmean(mean_meco,2).flatten()*1E-15
SCD_sat_flat=np.nanmean(mean_omi,2).flatten()
idx = np.isfinite(SCD_sat_flat) & np.isfinite(SCD_sim_flat)
#fit_param=np.polyfit(VCD_sat_flat[idx], VCD_sim_flat[idx],1)
fit_param = stats.linregress(SCD_sat_flat[idx], SCD_sim_flat[idx])[0:3]
fit_func=np.poly1d(fit_param[0:2])
r_sqr=fit_param[2]**2
axes[1,1].scatter(SCD_sat_flat,SCD_sim_flat, s=2)
axes[1,1].plot(SCD_sat_flat[idx], fit_func(SCD_sat_flat[idx]), color = "black", linewidth=2)
#axes[1,1].plot(SCD_sat_flat[idx], np.poly1d([1,0])(SCD_sat_flat[idx]), linewidth=1)
axes[1,1].plot(SCD_sat_flat[idx], np.poly1d([1,0])(SCD_sat_flat[idx]), color = "red", linewidth=1, linestyle = ":")
#axes[1,1].fill_between(VCD_sat_flat[idx], np.poly1d([0.8,0])(VCD_sat_flat[idx]), np.poly1d([1.2,0])(VCD_sat_flat[idx]), color = 'red', alpha = 0.5 )
axes[1,1].plot(SCD_sat_flat[idx], np.poly1d([1.2,0])(SCD_sat_flat[idx]), color = "red", linewidth=1, linestyle = ":", alpha=0.7)
axes[1,1].plot(SCD_sat_flat[idx], np.poly1d([0.8,0])(SCD_sat_flat[idx]), color = "red", linewidth=1, linestyle = ":", alpha=0.7)
axes[1,1].annotate("y = " + str('%.1f' % fit_param[0]) + "x + "+ str('%.1f' % fit_param[1]) + "\n r$^{2}$ = " + str('%.2f' % r_sqr), xy=(0.1, 0.8),  xycoords='axes fraction')
axes[1,1].set_xlabel("OMI NO2 SCD (*10$^{15}$ molec. cm$^{-2}$)", fontsize=10)
axes[1,1].set_ylabel("COSMO NO2 SCD (*10$^{15}$ molec. cm$^{-2}$)", fontsize=10)
plt.tight_layout()
plt.show()
plt.savefig(savepath +'summary_comp'+'.png', format = 'png', dpi = 200)