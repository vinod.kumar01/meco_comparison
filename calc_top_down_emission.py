# -*- coding: utf-8 -*-
"""
Created on Tue Jan 28 23:35:20 2020

@author: Vinod
"""

import netCDF4
import os
import numpy as np
from plot_tools import plt, map_prop, ccrs
from mod_colormap import add_white

llon, ulon, llat, ulat = 0, 19, 43, 56
prop_dict = {'extent': [llon, ulon, llat, ulat], 'name': 'MECO_DE',
             'border_res': '10m'}
projection = ccrs.PlateCarree()
map2plot = map_prop(**prop_dict)
new_cm = add_white("jet", shrink_colormap=True, replace_frac=0.01, start=None)
# %%
dirname_base = r'M:\nobackup\vinod\model_work\MECO\sorbit_subsample\2018_05\TNO_sfc\qa_75'
dirname_1_2 = r'M:\nobackup\vinod\model_work\MECO\sorbit_subsample\2018_05\TNO_sfc\emis_1_2_qa_75'
vcd_file = 'meanVCD05_regrid_inc_border.nc'
vcd_file_pert = 'meanVCD05_regrid_inc_border.nc'

base_sim = netCDF4.Dataset(os.path.join(dirname_base, vcd_file))
pert_sim = netCDF4.Dataset(os.path.join(dirname_1_2, vcd_file_pert))
lat = base_sim.variables['lat'][:]
lon = base_sim.variables['lon'][:]

c_tropomi_bu = base_sim.variables['NO2_tropcol_sat_corr'][:]
c_tropomi_bu = np.ma.masked_where(np.isnan(c_tropomi_bu), c_tropomi_bu)
c_tropomi_bu = np.ma.masked_where(c_tropomi_bu<0, c_tropomi_bu)
count_day = np.zeros(c_tropomi_bu.shape[1:], dtype=int)
# find number of days with valid measurements for each pixel
for i in range(len(count_day[:,0])):
    for j in range(len(count_day[0,:])):
        count_day[i, j] = np.ma.masked_array.count(c_tropomi_bu[:, i, j])
c_tropomi_bu = np.ma.mean(c_tropomi_bu[:], 0)

c_tropomi_1_2 = pert_sim.variables['NO2_tropcol_sat_corr'][:]
c_tropomi_1_2 = np.ma.masked_where(np.isnan(c_tropomi_1_2), c_tropomi_1_2)
c_tropomi_1_2 = np.ma.masked_where(c_tropomi_1_2<0, c_tropomi_1_2)
c_tropomi_1_2 = np.ma.mean(c_tropomi_1_2[:], 0)

c_model_base = base_sim.variables['NO2_tropcol_sim_i'][:]
c_model_base = np.ma.masked_where(np.isnan(c_model_base), c_model_base)
c_model_base = np.ma.mean(c_model_base[:], 0)

c_model_1_2 = pert_sim.variables['NO2_tropcol_sim_i'][:]
c_model_1_2 = np.ma.masked_where(np.isnan(c_model_1_2), c_model_1_2)
c_model_1_2 = np.ma.mean(c_model_1_2[:], 0)

beta = 0.2*c_model_base/(c_model_1_2-c_model_base)

gaama = (c_tropomi_1_2-c_tropomi_bu)/c_tropomi_bu
gaama /= (c_model_1_2-c_model_base)/c_model_base

scale_factor = (c_tropomi_bu-c_model_base)/c_model_base
scale_factor *= beta*(1+gaama)
scale_factor += 1
scale_factor = np.ma.masked_where(count_day<5, scale_factor)
scale_factor = np.ma.masked_where(scale_factor<=0, scale_factor)
scale_factor = np.ma.masked_where(scale_factor>10, scale_factor)
scale_factor_filled = np.ma.filled(scale_factor, fill_value=1)
base_sim.close()
pert_sim.close()

# %% plotting
datadict = {'lat': lat, 'lon': lon, 'vmin': 0, 'vmax': 2,
            'text': 'scale_factor', 'label': '',
            'plotable': scale_factor}
fig, ax = plt.subplots(subplot_kw=dict(projection=ccrs.PlateCarree()))
cs = map2plot.plot_map(datadict, ax, cmap=new_cm,
                       mode='pcolormesh',
                       projection=projection, alpha=1)
fig.canvas.draw()
plt.tight_layout()
fig.subplots_adjust(right=0.86, wspace=0.02)
cbar_ax = fig.add_axes([0.88, 0.2, 0.02, 0.6])
cbar = fig.colorbar(cs, cax=cbar_ax, extend='max', shrink=0.8)
cbar.set_label(datadict['text'], fontsize=10)
# cbar = plot_map(ax,scale_factor, vmin=0, vmax=2)
# cbar.set_label("scale_factor")
#plt.colorbar(scat)
