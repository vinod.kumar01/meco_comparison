# -*- coding: utf-8 -*-
"""
Created on Tue Aug  7 11:53:26 2018

@author: Vinod
"""

import time
tic = time.time()
import netCDF4
import numpy as np
import os
import glob
dirname = r'M:\home\vinod\nobackup\TNO_macc_iii'
out_dir = r'M:\home\vinod\nobackup\TNO_macc_iii\sectorwise'
f_names = glob.glob(dirname+'\TNO_MACC_III_emissions_*.nc')
f_names.sort(key=os.path.basename)
#####preparing dummy grids with dimestion time,emission category,lat,lon for tracers######
CO_grid = np.zeros((12,13,672,720), dtype = 'float')
PM2_5_grid = np.zeros((12,13,672,720), dtype = 'float')
PM10_grid = np.zeros((12,13,672,720), dtype = 'float')
CH4_grid = np.zeros((12,13,672,720), dtype = 'float')
SO2_grid = np.zeros((12,13,672,720), dtype = 'float')
NMVOC_grid = np.zeros((12,13,672,720), dtype = 'float')
NH3_grid = np.zeros((12,13,672,720), dtype = 'float')
NOx_grid = np.zeros((12,13,672,720), dtype = 'float')
#####data readout######
for f_num in range(0,len(f_names)): 
    year_num = 2000+f_num
    filename_in = f_names[f_num]
    f=netCDF4.Dataset(filename_in,'r')
    lon_idx = f.variables['longitude_index'][:]
    lon_src = f.variables['longitude_source'][:]
    lat_idx = f.variables['latitude_index'][:]
    lat_src = f.variables['latitude_source'][:]
    emission_category = f.variables['emission_category_index'][:]
    CO_src = f.variables['co'][:]
    PM2_5_src = f.variables['pm2_5'][:]
    PM10_src = f.variables['pm10'][:]
    CH4_src = f.variables['ch4'][:]
    SO2_src = f.variables['so2'][:]
    NMVOC_src = f.variables['nmvoc'][:]
    NH3_src = f.variables['nh3'][:]
    NOx_src = f.variables['nox'][:]
    ######arranging data in grids and assigning dimesions###########
    for emis_cat in range(1,14):
        for i in range(0,len(lon_idx)):
            if emission_category[i] == emis_cat:
                CO_grid[f_num,emis_cat-1,lat_idx[i]-1,lon_idx[i]-1]=CO_src[i]    #lat and lon idx in inventory are 1 based
                PM2_5_grid[f_num,emis_cat-1,lat_idx[i]-1,lon_idx[i]-1]=PM2_5_src[i]
                PM10_grid[f_num,emis_cat-1,lat_idx[i]-1,lon_idx[i]-1]=PM10_src[i]
                CH4_grid[f_num,emis_cat-1,lat_idx[i]-1,lon_idx[i]-1]=CH4_src[i]
                SO2_grid[f_num,emis_cat-1,lat_idx[i]-1,lon_idx[i]-1]=SO2_src[i]
                NMVOC_grid[f_num,emis_cat-1,lat_idx[i]-1,lon_idx[i]-1]=NMVOC_src[i]
                NH3_grid[f_num,emis_cat-1,lat_idx[i]-1,lon_idx[i]-1]=NH3_src[i]
                NOx_grid[f_num,emis_cat-1,lat_idx[i]-1,lon_idx[i]-1]=NOx_src[i]
#########sector numbers (0 based) Originally it was 1 based ###########
#0 : 'Energy industries', : ENE : FF
#1 : 'Non-industrial combustion', :  RCO : BF
#2 : 'Industry', : IND : BF
#3 : 'Fossil fuel production and distribution', : REF : FF
#4 : 'Solvent and other product use', : SOL : FF
#5 : 'Road transport, exhaust, gasoline', : TRA : FF
#6 : 'Road transport, exhaust, diesel', : TRA : FF
#7 : 'Road transport, exhaust, LPG and natural gas', : TRA : FF
#8 : 'Road transport, gasoline evaporation', : TRA : FF
#9 : 'Road transport, tyre, brake and road wear', : TRA : FF
#10: 'Non-road transport', : TRA : FF
#11: 'Waste', : WST : BF
#12: 'Agriculture' : AGR : BF
####################
for tracer_names in ['NMVOC','CO','CH4','SO2','NH3','NOx']:
    vars()[tracer_names+'_ENE'] = vars()[tracer_names+'_grid'][:,0,:,:]
    vars()[tracer_names+'_RCO'] = vars()[tracer_names+'_grid'][:,1,:,:]
    vars()[tracer_names+'_IND'] = vars()[tracer_names+'_grid'][:,2,:,:]
    vars()[tracer_names+'_REF'] = vars()[tracer_names+'_grid'][:,3,:,:]
    vars()[tracer_names+'_SOL'] = vars()[tracer_names+'_grid'][:,4,:,:]
    vars()[tracer_names+'_TRA'] = np.nansum(vars()[tracer_names+'_grid'][:,np.r_[5:11],:,:],1)    ### sector 5 to 10
    vars()[tracer_names+'_WST'] = vars()[tracer_names+'_grid'][:,11,:,:]
    vars()[tracer_names+'_AGR'] = vars()[tracer_names+'_grid'][:,12,:,:]
    for idx,sector in enumerate(['ENE','RCO', 'IND', 'REF', 'SOL', 'TRA', 'WST', 'AGR']):
        for years in range(2000, 2012):
            filename_out = 'TNO_MACC_III_'+ tracer_names + '_' + str(years) + '_' + sector + '.nc'
            f_out = netCDF4.Dataset(os.path.join(out_dir, filename_out), 'w', format='NETCDF4')
            f_out.createDimension('time', None)
            f_out.createDimension('lat', 672)
            f_out.createDimension('lon', 720)
            lat_out = f_out.createVariable('lat', 'f4', ('lat',))
            lon_out = f_out.createVariable('lon', 'f4', ('lon',))
            time_out = f_out.createVariable('time', 'f4', ('time',))
            area_out = f_out.createVariable('area',   'f4', ('lat', 'lon'))
            tracer_out = f_out.createVariable(tracer_names+'_flux',   'f4', ('time','lat', 'lon'))
            lon_out[:] = f.variables['longitude'][:];lon_out.long_name='longitude';lon_out.units='degree east'
            lat_out[:] = f.variables['latitude'][:];lat_out.long_name='latitude';lat_out.units='degree north'
            area_out[:] = f.variables['area'][:];area_out.long_name='area';area_out.units='m2'
            time_out[:] = years; time_out.long_name='time';time_out.units='year since 2000-01-01 00:00:00'
            tracer_out[0,:,:] = vars()[tracer_names + '_' + sector][years-2000,:,:]/365/86400/area_out[:]
            tracer_out.long_name = 'flux of '+ tracer_names ; tracer_out.units = "Kg m-2 s-1" 
            f_out.close()
toc = time.time()
print(toc-tic, 'sec Elapsed')