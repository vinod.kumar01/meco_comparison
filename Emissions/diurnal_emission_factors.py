# -*- coding: utf-8 -*-
"""
Created on Mon Jun 22 23:20:56 2020

@author: Vinod
"""

# %% definiton and imports
import numpy as np
from os import path
import matplotlib.pyplot as plt
import pandas as pd
from datetime import datetime, timedelta


def count_2_fac(count):
    daily_sum = np.sum(count, 1)
    week_fac = np.array([i/np.mean(daily_sum) for i in daily_sum])
    hour_fac = [np.array([tot_count[i, j]*week_fac[i]/np.mean(tot_count[i, :])
                for i in range(7)]) for j in range(24)]
    hour_fac = np.array(hour_fac).T
    return(hour_fac)

# %% load data
dirname = r'M:\home\jremmers\Projects\Vinod\Datasets\Car_counts'
savedir = r'M:\nobackup\vinod\Emissions\Time_profiles'
tot_count_file = 'carAll_counts_hourly_weekdays.txt'
tot_count = np.genfromtxt(path.join(dirname, tot_count_file), delimiter=',')
truck_count_file = 'truck_counts_hourly_weekdays.txt'
truck_count = np.genfromtxt(path.join(dirname, truck_count_file),
                            delimiter=',')
EDGAR_fac = [0.19, 0.09, 0.06, 0.05, 0.09, 0.22, 0.86, 1.84, 1.86, 1.41, 1.24,
             1.2, 1.32, 1.44, 1.45, 1.59, 2.03, 2.08, 1.51, 1.06, 0.74, 0.62,
             0.61, 0.44]

daylist = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday',
           'Saturday', 'Sunday']
car_count = tot_count - truck_count
tot_count_normalized = car_count + 10*truck_count
# assuming trucks emit 10 times that of car
truck_fac = count_2_fac(truck_count)
car_fac = count_2_fac(car_count)
tot_fac = count_2_fac(tot_count_normalized)
for Typ, fac in {'Truck': truck_fac, 'Car': car_fac, 'Total': tot_fac}.items():
    fig, ax = plt.subplots(2, 4, sharex=True, sharey=True, figsize=[14, 6])
    for day_num, day in enumerate(daylist):
        i = int(day_num/4)
        j = day_num % 4
        ax[i, j].set_title(day)
        ax[i, j].plot(np.arange(0, 24, 1), fac[day_num, :], 'r-*',
                      label='Actual count')
        ax[i, j].grid(alpha=0.4)
        ax[i, j].minorticks_on()
        ax[i, j].plot(np.arange(0, 24, 1), np.array(EDGAR_fac), 'k--',
                      alpha=0.8, label='EDGAR_factor')
    ax[1, 3].set_ylim(0, 2.7)
    ax[1, 3].plot(np.arange(25, 49, 1), np.zeros(24), 'r-*',
                  label='Actual count')
    ax[1, 3].plot(np.arange(25, 49, 1), np.zeros(24), 'k--', alpha=0.8,
                  label='EDGAR_factor')
    ax[1, 3].set_xlim(0, 24)
    ax[1, 3].legend(loc='upper right')
    ax[1, 3].text(0.02, 0.9, Typ, transform=ax[1, 3].transAxes)
    plt.tight_layout()
#    plt.savefig(path.join(savedir, 'fac_{}.png'.format(Typ)))
# %% plot for other sectors
dirname = r'M:\nobackup\vinod\Emissions\Time_profiles'
filename = 'emissions_time_profiles_hourly.xlsx'
fig, ax = plt.subplots(1, 4, figsize=[14, 3.5], sharey=True)
daylist = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday',
           'Saturday', 'Sunday']
for s, sector in enumerate(['TRA', 'ENE', 'RCO', 'IND']):
    fac = pd.read_excel(path.join(dirname, filename), sheet_name=sector,
                        nrows=7, header=None)
    fac = fac.values
    fac_dict = {}
    for d, day in enumerate(daylist):
        fac_dict[day] = {i: val for i, val in enumerate(fac[d])}
    start_dt = datetime(2017, 12, 31, 22)
    end_dt = datetime(2018, 12, 1, 21)
    fac_file = []
    for time_now in pd.date_range(start_dt, end_dt, freq='H'):
        day_lt = (time_now+timedelta(hours=2)).day_name()
        hour_lt = (time_now+timedelta(hours=2)).hour
        str_now = str(time_now.year) + '\t' + str(time_now.month) + '\t'
        str_now += str(time_now.day) + '\t' + str(time_now.hour) + '\t'
        str_now += str(fac_dict[day_lt][hour_lt])
        fac_file.append(str_now)
    fac_file = np.asarray(fac_file)
    np.savetxt(path.join(dirname, 'import_ts_{}.txt'.format(sector)),
               fac_file, fmt='%s')
    for day_num, day in enumerate(daylist):
        ax[s].plot(np.arange(0, 24, 1), fac[day_num, :], '-*', label=day[:3])
        ax[s].grid(alpha=0.4)
        ax[s].minorticks_on()
    ax[s].annotate('('+chr(97+s)+') '+sector, xy=(0.75, 0.9),
                   xycoords='axes fraction', size=14)
    ax[s].set_xlabel('Hour of the day (local time)', size=14)
    if s == 0:
        ax[s].plot(np.arange(0, 24, 1), np.array(EDGAR_fac), 'k--',
                   alpha=0.8, label='Schapp et al.')
ax[0].set_ylabel('Scaling factor', size=14)
ax[3].legend(loc='upper left', ncol=2)
plt.tight_layout(pad=0.2)

