# -*- coding: utf-8 -*-
"""
Created on Mon Jul 13 15:35:42 2020

@author: Vinod
"""

from netCDF4 import Dataset
from os import path
import numpy as np
from scipy.interpolate import griddata
from filetools import dict2nc3d
from utm_converter import unproject

year = 2018
dirname = r'M:\nobackup\vinod\Emissions\UBA\raw'
savedir = r'M:\nobackup\vinod\Emissions\UBA\grouped'
f = Dataset(path.join(dirname, str(year)+'_Sub2020_NFR.nc'))
utm_lat = f.variables['LAT'][:]
utm_lon = f.variables['LON'][:]
geolat = np.zeros((len(utm_lon), len(utm_lat)))
geolon = np.zeros((len(utm_lon), len(utm_lat)))
for x, u_lon in enumerate(utm_lon):
    for y, u_lat in enumerate(utm_lat):
        lon_now, lat_now = unproject(32, 'N', u_lon, u_lat)
        geolat[x, y] = lat_now
        geolon[x, y] = lon_now
lat_grid = np.arange(round(np.min(geolat), 2), round(np.max(geolat), 2), 0.01)
lon_grid = np.arange(round(np.min(geolon), 2), round(np.max(geolon), 2), 0.01)
xygrid = np.meshgrid(lon_grid, lat_grid)
area = Dataset(path.join(savedir, 'area.nc')).variables['cell_area'][:]

#0 : 'Energy industries', : ENE : FF
#1 : 'Non-industrial combustion', :  RCO : BF
#2 : 'Industry', : IND : BF
#3 : 'Fossil fuel production and distribution', : REF : FF
#4 : 'Solvent and other product use', : SOL : FF
#5 : 'Road transport, exhaust, gasoline', : TRA : FF
#6 : 'Road transport, exhaust, diesel', : TRA : FF
#7 : 'Road transport, exhaust, LPG and natural gas', : TRA : FF
#8 : 'Road transport, gasoline evaporation', : TRA : FF
#9 : 'Road transport, tyre, brake and road wear', : TRA : FF
#10: 'Non-road transport', : TRA : FF
#11: 'Waste', : WST : BF
#12: 'Agriculture' : AGR : BF
sectors = {'ENE': ['1A1a', '1A1c'],
           'REF': ['1A1b', '1B2ai', '1B2b', '1B2c'],
           'SOL': ['1A2e', '1A2f', '1B2aiv', '1B2av',
                   '2D3a', '2D3b', '2D3c', '2D3d', '2D3e', '2D3f', '2D3g',
                   '2D3h', '2D3i', '2G'],
           'IND': ['1A2a', '1A2b', '1A3ei', '1B1a', '1B1b', '1A2gvii',
                   '1A2gviii', '2A1', '2A2', '2A3', '2A5a', '2A5b', '2A6',
                   '2B1', '2B10a', '2B2', '2B3', '2B5', '2B7',
                   '2C1', '2C2', '2C3', '2C5', '2C6', '2C7a', '2C7c', '2H1',
                   '2H2', '2I', '2L'],
           'air': ['1A3ai_i_', '1A3aii_i_'],
           'TRA1': ['1A3bi', '1A3bii', '1A3biv', '1A3bv', '1A3bvi',
                    '1A3bvii'],
           'TRA2':  ['1A3biii'],
           'TRA3': ['1A3c', '1A4aii', '1A4bii', '1A4cii', '1A5b'],  # non road (rail)
           'SHP':  ['1A3dii', '1A4ciii'],
           'RCO': ['1A4ai', '1A4bi', '1A4ci', '1A5a'],
           'AGR': ['3B1a', '3B1b', '3B2', '3B3', '3B4d', '3B4e', '3B4gi',
                   '3B4gii', '3B4giii', '3B4giv', '3Da1', '3Da2a', '3Da2b',
                   '3Da2c', '3Da3', '3Dc', '3De', '3I'],
           'WST': ['5A', '5B1', '5B2', '5C1bv', '5C2', '5D1', '5E']
           }


def choose_sector(argument):
    switcher = {
        'ENE': 1, 'RCO': 2, 'IND': 3, 'REF': 5, 'SOL': 6, 'TRA1': 7, 'TRA2': 7,
        'TRA3': 8, 'WST': 9, 'AGR': 10, 'air': 11, 'SHP': 8
    }
    return switcher.get(argument, np.nan)


dirname_profile = r'M:\home\vinod\nobackup\TNO_macc_iii\emission_time_profiles\tab_del'
speclist = ['CO','NH3','NMVOC', 'NOx', 'SO2']  #'NOx'
# speclist = ['NOx']
month_fac = {}
for spec in speclist:
    with open(path.join(dirname_profile, "month_"+spec+'.txt')) as file:
        fac = np.array([line.replace('\n', '').split('\t', maxsplit=2)
                        for line in file])
        fac = fac[fac[:, 0] == '60']
        month_fac[spec] = {int(i): np.array(fac[int(i)-1, 2].split('\t'),
                                            dtype=float) for i in fac[:, 1]}
        # code for germany is 60
        file.close()
# check if list is complete and doesnot include any extra
ref = [i for i in f.variables.keys()]
ref.remove('LON')
ref.remove('LAT')
ref = list(set([i[:i.rfind('_')][2:] for i in ref]))
ref = list(set([i[:-4] if i.endswith('_PM2') else i for i in ref]))
target = []
for sec in sectors.keys():
    target.extend(sectors[sec])
print([list(set(ref).difference(target))])
print([list(set(target).difference(ref))])

# Merge subsectors
for tracer in speclist:
    for sector in sectors:
        savedict = {'lat': lat_grid, 'lon': lon_grid, 'area': area,
                    'time': np.arange(0.5, 12.5)/12}
        subsectors = sectors[sector]
        flux = np.zeros([len(utm_lon), len(utm_lat)])
        for subsector in subsectors:
            try:
                flux += f.variables['E_{}_{}'.format(subsector,
                                    tracer.upper())][:]
            except KeyError:
                print('Emissions of {} are not available for {}'.format(tracer,
                      subsector))
        # Warning if emission of complete sector is zero.
        if np.sum(flux) == 0:
            print('Zero emissions of {} for {}'.format(tracer, sector))
#        flux_grid = griddata((geolon.ravel(), geolat.ravel()), flux.ravel(),
#                             (xygrid[0], xygrid[1]), method='linear')
#        flux_grid[np.isnan(flux_grid)] = 0.
#        flux_grid /= area  # (Kt m-2 year-1)
#        flux_grid /= 365*86400   # (Kt m-2 s-1)
#        flux_grid *= 1E6   # (Kg m-2 s-1)
#        # apply monthly factors
#        flux3d = np.tile(flux_grid, (12, 1, 1))
#        for i in range(12):
#            flux3d[i, :] *= month_fac[spec][choose_sector(sector)][i]
#        savedict[tracer+'_flux'] = flux3d
#        savename = 'UBA_{}_{}_{}.nc'.format(tracer, sector, year)
#        dict2nc3d(path.join(savedir, savename), savedict,
#                  dimx='lon', dimy='lat', dimz='time')
#        f2 = Dataset(path.join(savedir, savename), 'r+')
#        lon_out = f2.variables['lon']
#        lon_out.long_name = 'longitude'
#        lat_out = f2.variables['lat']
#        lat_out.long_name = 'latitude'
#        time_out = f2.variables['time']
#        time_out.long_name = 'time'
#        time_out.units = 'year since {}-01-01 00:00:00'.format(year)
#        area_out = f2.variables['area']
#        area_out.units = 'm2'
#        flux_out = f2.variables[tracer+'_flux']
#        flux_out.units = "Kg m-2 s-1"
#        f2.close()
f.close()
