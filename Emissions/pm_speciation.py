# -*- coding: utf-8 -*-
"""
Created on Fri Sep 28 13:42:03 2018

@author: Vinod
"""

import time
tic = time.time()
import netCDF4
import glob
import os
import numpy as np
from collections import defaultdict
def choose_sector(argument):
    switcher = {
        0 : 1, 1 : 2, 2 : 34, 3 : 5, 4 : 6, 5 : 71, 6 : 72,
        7 : 73, 8 : 74, 9 : 75, 10 : 8, 11 : 9, 12 : 10
    }
    return switcher.get(argument, np.nan)
#####sectors
#0 : 'Energy industries', : ENE : FF					              1
#1 : 'Non-industrial combustion', :  RCO : BF				       2
#2 : 'Industry', : IND : BF						                    34
#3 : 'Fossil fuel production and distribution', : REF : FF		   5
#4 : 'Solvent and other product use', : SOL : FF			      6
#5 : 'Road transport, exhaust, gasoline', : TRA : FF			   71
#6 : 'Road transport, exhaust, diesel', : TRA : FF			      72
#7 : 'Road transport, exhaust, LPG and natural gas', : TRA : FF 73
#8 : 'Road transport, gasoline evaporation', : TRA : FF		  74
#9 : 'Road transport, tyre, brake and road wear', : TRA : FF	  75
#10: 'Non-road transport', : TRA : FF					            8
#11: 'Waste', : WST : BF						                   9
#12: 'Agriculture' : AGR : BF						               10
dirname = r'M:\home\vinod\nobackup\TNO_macc_iii'
out_dir = r'M:\nobackup\vinod\TNO_macc_iii\sectorwise\seasonal'
for size in ['coarse', 'fine']:   
    with open(os.path.join(dirname,"PM_"+size+"_split"'.txt')) as file: 
         input = [line.replace('\n', '').split('\t', maxsplit=3) for line in file]
         file.close()
    vars()[size+'_fac'] = defaultdict(lambda: defaultdict(lambda: defaultdict(list)))
    for x, y,z, value in input:
        vars()[size+'_fac'][x][y][z].append(value)
    Countries = ["ALB","ARM","ATL","AUT","AZE","BAS","BEL","BGR","BIH","BLR",
                 "BLS","CHE","CYP","CZE","DEU","DNK","ESP","EST","FIN","FRA",
                 "GBR","GEO","GRC","HRV","HUN","IRL","ISL","ITA","LTU","LUX",
                 "LVA","MDA","MED","MKD","MLT","NLD","NOR","NOS","POL","PRT",
                 "ROU","RUS","SVK","SVN","SWE","TUR","UKR","YUG"]    
    for years in range(2000, 2012):
        for Country_idx in Countries:
            for SNAP in ["1","2","5","6","8","9","10","34","71","72","73","74","75"]:
                myList = np.array([i.split('\t')[0:5] for i in vars()[size+'_fac'][str(years)][Country_idx][SNAP]])
                if Country_idx in ["ATL", "BAS", "BLS" , "MED" , "NOS"]:
                    if SNAP != "8":
                        myList = np.array(("0.0" , "0.0" , "0.0" , "0.0" , "0.0" )).reshape(1,5)
                vars()[size+'_fac'][str(years)][Country_idx][SNAP] = [float(i) for i in myList[0]]
############emission data readout
f_names = glob.glob(dirname+'\TNO_MACC_III_emissions_*.nc')
f_names.sort(key=os.path.basename)
#####preparing dummy annual grids with dimestion time,emission category,lat,lon for tracers######
PM2_5_grid = np.zeros((12,13,672,720), dtype = 'float')
PM10_grid = np.zeros((12,13,672,720), dtype = 'float')
BC_fine_grid = np.zeros((12,13,672,720), dtype = 'float')
OC_fine_grid = np.zeros((12,13,672,720), dtype = 'float')
Nap_fine_grid = np.zeros((12,13,672,720), dtype = 'float')
SO4mm_fine_grid = np.zeros((12,13,672,720), dtype = 'float')
BC_coarse_grid = np.zeros((12,13,672,720), dtype = 'float')
OC_coarse_grid = np.zeros((12,13,672,720), dtype = 'float')
Nap_coarse_grid = np.zeros((12,13,672,720), dtype = 'float')
SO4mm_coarse_grid = np.zeros((12,13,672,720), dtype = 'float')
Country_grid = np.zeros((12,13,672,720), dtype = 'float')
#####data readout######
for f_num in range(0,len(f_names)): 
    year_num = 2000+f_num
    filename_in = f_names[f_num]
    f=netCDF4.Dataset(filename_in,'r')
    lon_idx = f.variables['longitude_index'][:]
    lon_src = f.variables['longitude_source'][:]
    lat_idx = f.variables['latitude_index'][:]
    lat_src = f.variables['latitude_source'][:]
    emission_category = f.variables['emission_category_index'][:]
    country_idx = f.variables['country_index'][:]
    PM2_5_src = f.variables['pm2_5'][:]
    PM10_src = f.variables['pm10'][:]
    ######arranging data in grids and assigning dimesions###########
    for emis_cat in range(1,14):
        for i in range(0,len(lon_idx)):
            if emission_category[i] == emis_cat:
                PM2_5_grid[f_num,emis_cat-1,lat_idx[i]-1,lon_idx[i]-1]=PM2_5_src[i]   #lat and lon idx in inventory are 1 based
                PM10_grid[f_num,emis_cat-1,lat_idx[i]-1,lon_idx[i]-1]=PM10_src[i]
                Country_grid[f_num,emis_cat-1,lat_idx[i]-1,lon_idx[i]-1]=country_idx[i]
 
for lat in range(0,672):
    for lon in range(0,720):
        country_id = netCDF4.chartostring(f.variables['country_id'][Country_grid[10,10,lat,lon]-1], encoding='Latin-1')   ###check -1 in index
        for years in range(0,12):
            for sector in range (0,13):
                coarse_frac = coarse_fac[str(years+2000)][str(country_id)][str(int(choose_sector(sector)))]
                fine_frac = fine_fac[str(years+2000)][str(country_id)][str(int(choose_sector(sector)))]
                for idx,spec in enumerate(['BC_fine', 'OC_fine', 'Nap_fine', 'SO4mm_fine']):
                    vars()[spec+'_grid'][years,sector,lat,lon]= fine_frac[idx]*PM2_5_grid[years,sector,lat,lon]
                for idx,spec in enumerate(['BC_coarse', 'OC_coarse', 'Nap_coarse', 'SO4mm_coarse']):
                    vars()[spec+'_grid'][years,sector,lat,lon]= coarse_frac[idx]*PM10_grid[years,sector,lat,lon]
####################################
filename_out = 'gridded_TNO_MACC_III_PM_2000_2011.nc'
f_out = netCDF4.Dataset(os.path.join(dirname, filename_out), 'w', format='NETCDF4')
f_out.createDimension('time', None)
f_out.createDimension('lon', 720)
f_out.createDimension('lat', 672)
f_out.createDimension('emis_cat', 13)
f_out.createDimension('emis_cat_name_len', 44)
f_out.createDimension('country', 48)
f_out.createDimension('country_id_len', 3)
time_out = f_out.createVariable('time', 'f4', ('time',))
lon_out = f_out.createVariable('longitude', 'f4', ('lon',))
lat_out = f_out.createVariable('latitude', 'f4', ('lat',))
emis_cat_out = f_out.createVariable('emis_cat', 'S1', ('emis_cat','emis_cat_name_len'))
country_id_out = f_out.createVariable('country_id', 'S1', ('country','country_id_len'))
area_out = f_out.createVariable('area',   'f4', ('lat', 'lon'))
PM2_5_out = f_out.createVariable('PM2_5_emis',   'f4', ('time','emis_cat','lat', 'lon'))
PM10_out = f_out.createVariable('PM10_emis',   'f4', ('time','emis_cat','lat', 'lon'))
BC_fine_out = f_out.createVariable('BC_fine_emis',   'f4', ('time','emis_cat','lat', 'lon'))
OC_fine_out = f_out.createVariable('OC_fine_emis',   'f4', ('time','emis_cat','lat', 'lon'))
Nap_fine_out = f_out.createVariable('Nap_fine_emis',   'f4', ('time','emis_cat','lat', 'lon'))
SO4mm_fine_out = f_out.createVariable('SO4mm_fine_emis',   'f4', ('time','emis_cat','lat', 'lon'))
BC_coarse_out = f_out.createVariable('BC_coarse_emis',   'f4', ('time','emis_cat','lat', 'lon'))
OC_coarse_out = f_out.createVariable('OC_coarse_emis',   'f4', ('time','emis_cat','lat', 'lon'))
Nap_coarse_out = f_out.createVariable('Nap_coarse_emis',   'f4', ('time','emis_cat','lat', 'lon'))
SO4mm_coarse_out = f_out.createVariable('SO4mm_coarse_emis',   'f4', ('time','emis_cat','lat', 'lon'))
country_index_out = f_out.createVariable('country_index',   'f4', ('time','emis_cat','lat', 'lon'))
#time_out[:] = np.array([183+365*(i) for i in range(0,12)]);time_out.units='days since 2000-01-01 00:00:00'
time_out[:] = np.array([i for i in range(0,12)]);time_out.units='year since 2000-01-01 0:0:0'
lon_out[:] = f.variables['longitude'][:];lon_out.units='degree east';lon_out.long_name='longitude'
lat_out[:] = f.variables['latitude'][:];lat_out.units='degree north';lat_out.long_name='latitude'
country_id_out[:] = f.variables['country_id'][:]
area_out[:] = f.variables['area'][:];area_out.units='m2'
emis_cat_out[:] = f.variables['emis_cat_name'][:]
PM2_5_out[:,:,:,:] = PM2_5_grid; PM2_5_out.units = "kg"
PM10_out[:,:,:,:] = PM10_grid; PM10_out.units = "kg"
BC_fine_out[:,:,:,:] = BC_fine_grid; BC_fine_out.units = "kg"
OC_fine_out[:,:,:,:] = OC_fine_grid; OC_fine_out.units = "kg"
Nap_fine_out[:,:,:,:] = Nap_fine_grid; Nap_fine_out.units = "kg"
SO4mm_fine_out[:,:,:,:] = SO4mm_fine_grid; SO4mm_fine_out.units = "kg"
BC_coarse_out[:,:,:,:] = BC_coarse_grid; BC_coarse_out.units = "kg"
OC_coarse_out[:,:,:,:] = OC_coarse_grid; OC_coarse_out.units = "kg"
Nap_coarse_out[:,:,:,:] = Nap_coarse_grid; Nap_coarse_out.units = "kg"
SO4mm_coarse_out[:,:,:,:] = SO4mm_coarse_grid; SO4mm_coarse_out.units = "kg"
f_out.close()