# -*- coding: utf-8 -*-
"""
Created on Thu Jun 21 10:59:12 2018

@author: Vinod
"""

import time
tic = time.time()
import netCDF4
import numpy as np
import os
import glob
dirname = r'M:\home\vinod\nobackup\TNO_macc_iii'
f_names = glob.glob(dirname+'\TNO_MACC_III_emissions_*.nc')
f_names.sort(key=os.path.basename)
#####preparing dummy grids with dimestion time,emission category,lat,lon for tracers######
CO_grid = np.zeros((12,13,672,720), dtype = 'float')
PM2_5_grid = np.zeros((12,13,672,720), dtype = 'float')
PM10_grid = np.zeros((12,13,672,720), dtype = 'float')
CH4_grid = np.zeros((12,13,672,720), dtype = 'float')
SO2_grid = np.zeros((12,13,672,720), dtype = 'float')
NMVOC_grid = np.zeros((12,13,672,720), dtype = 'float')
NH3_grid = np.zeros((12,13,672,720), dtype = 'float')
NOx_grid = np.zeros((12,13,672,720), dtype = 'float')
#####data readout######
for f_num in range(0,len(f_names)): 
    year_num = 2000+f_num
    filename_in = f_names[f_num]
    f=netCDF4.Dataset(filename_in,'r')
    lon_idx = f.variables['longitude_index'][:]
    lon_src = f.variables['longitude_source'][:]
    lat_idx = f.variables['latitude_index'][:]
    lat_src = f.variables['latitude_source'][:]
    emission_category = f.variables['emission_category_index'][:]
    CO_src = f.variables['co'][:]
    PM2_5_src = f.variables['pm2_5'][:]
    PM10_src = f.variables['pm10'][:]
    CH4_src = f.variables['ch4'][:]
    SO2_src = f.variables['so2'][:]
    NMVOC_src = f.variables['nmvoc'][:]
    NH3_src = f.variables['nh3'][:]
    NOx_src = f.variables['nox'][:]
    ######arranging data in grids and assigning dimesions###########
    for emis_cat in range(1,14):
        for i in range(0,len(lon_idx)):
            if emission_category[i] == emis_cat:
                CO_grid[f_num,emis_cat-1,lat_idx[i]-1,lon_idx[i]-1]=CO_src[i]    #lat and lon idx in inventory are 1 based
                PM2_5_grid[f_num,emis_cat-1,lat_idx[i]-1,lon_idx[i]-1]=PM2_5_src[i]
                PM10_grid[f_num,emis_cat-1,lat_idx[i]-1,lon_idx[i]-1]=PM10_src[i]
                CH4_grid[f_num,emis_cat-1,lat_idx[i]-1,lon_idx[i]-1]=CH4_src[i]
                SO2_grid[f_num,emis_cat-1,lat_idx[i]-1,lon_idx[i]-1]=SO2_src[i]
                NMVOC_grid[f_num,emis_cat-1,lat_idx[i]-1,lon_idx[i]-1]=NMVOC_src[i]
                NH3_grid[f_num,emis_cat-1,lat_idx[i]-1,lon_idx[i]-1]=NH3_src[i]
                NOx_grid[f_num,emis_cat-1,lat_idx[i]-1,lon_idx[i]-1]=NOx_src[i]
###########Writing signle output for individual catetogy and year as dimesion for all the species##########
filename_out = 'gridded_TNO_MACC_III_emissions_2000_2011.nc'
f_out = netCDF4.Dataset(os.path.join(dirname, filename_out), 'w', format='NETCDF4')
f_out.createDimension('time', None)
f_out.createDimension('lon', 720)
f_out.createDimension('lat', 672)
f_out.createDimension('emis_cat', 13)
f_out.createDimension('emis_cat_name_len', 44)
time_out = f_out.createVariable('time', 'f4', ('time',))
lon_out = f_out.createVariable('lon', 'f4', ('lon',))
lat_out = f_out.createVariable('lat', 'f4', ('lat',))
emis_cat_out = f_out.createVariable('emis_cat', 'S1', ('emis_cat','emis_cat_name_len'))
area_out = f_out.createVariable('area',   'f4', ('lat', 'lon'))
CO_out = f_out.createVariable('CO_flux',   'f4', ('time','emis_cat','lat', 'lon'))
PM2_5_out = f_out.createVariable('PM2_5_flux',   'f4', ('time','emis_cat','lat', 'lon'))
PM10_out = f_out.createVariable('PM10_flux',   'f4', ('time','emis_cat','lat', 'lon'))
CH4_out = f_out.createVariable('CH4_flux',   'f4', ('time','emis_cat','lat', 'lon'))
SO2_out = f_out.createVariable('SO2_flux',   'f4', ('time','emis_cat','lat', 'lon'))
NMVOC_out = f_out.createVariable('NMVOC_flux',   'f4', ('time','emis_cat','lat', 'lon'))
NH3_out = f_out.createVariable('NH3_flux',   'f4', ('time','emis_cat','lat', 'lon'))
NOx_out = f_out.createVariable('NO_flux',   'f4', ('time','emis_cat','lat', 'lon'))
#time_out[:] = np.array([183+365*(i) for i in range(0,12)]);time_out.units='days since 2000-01-01 00:00:00'
time_out[:] = np.array([i for i in range(0,12)]);time_out.units='year since 2000-01-01 0:0:0'
lon_out[:] = f.variables['longitude'][:];lon_out.units='degree east';lon_out.long_name='longitude'
lat_out[:] = f.variables['latitude'][:];lat_out.units='degree north';lat_out.long_name='latitude'
area_out[:] = f.variables['area'][:];area_out.units='m2'
emis_cat_out[:] = f.variables['emis_cat_name'][:]
CO_out[:,:,:,:] = CO_grid*1000*6.023E23/28.01/365/86400/area_out[:]; CO_out.units = "molecules m-2 s-1" ; CO_out.molar_mass = 28.01
#Kg m-2 year-1 to molecules m-2 s-1
PM2_5_out[:,:,:,:] = PM2_5_grid/365/86400/area_out[:]; PM2_5_out.units = "kg m-2 s-1"
PM10_out[:,:,:,:] = PM10_grid/365/86400/area_out[:]; PM10_out.units = "kg m-2 s-1"
CH4_out[:,:,:,:] = CH4_grid*1000*6.023E23/16.04/365/86400/area_out[:]; CH4_out.units = "molecules m-2 s-1" ; CH4_out.molar_mass = 16.04
SO2_out[:,:,:,:] = SO2_grid*1000*6.023E23/64.07/365/86400/area_out[:]; SO2_out.units = "molecules m-2 s-1" ; SO2_out.molar_mass = 64.07
NMVOC_out[:,:,:,:] = NMVOC_grid/365/86400/area_out[:]; NMVOC_out.units = "kg m-2 s-1"
NH3_out[:,:,:,:] = NH3_grid*1000*6.023E23/17.03/365/86400/area_out[:]; NH3_out.units = "molecules m-2 s-1" ; NH3_out.molar_mass = 17.03
NOx_out[:,:,:,:] = NOx_grid*1000*6.023E23/46.01/365/86400/area_out[:]; NOx_out.units = "molecules m-2 s-1" ; NOx_out.molar_mass = 30.01
f_out.close()
###########Writing individual output for sum of all emis catetogries for all the species except NMVOC with year as dimesion ##########
#tracers = {'CO': 28.01, 'CH4': 16.04, 'SO2': 64.07, 'NH3': 17.03, 'NOx': 30.01}
#for i in tracers:
#    filename_out = i +'_tot_TNO_MACC_III_emissions_2000_2011.nc'
#    f_out = netCDF4.Dataset(os.path.join(dirname, filename_out), 'w', format='NETCDF4')
#    f_out.createDimension('time', None)
#    f_out.createDimension('lon', 720)
#    f_out.createDimension('lat', 672)
#    time_out = f_out.createVariable('time', 'f4', ('time',))
#    lon_out = f_out.createVariable('lon', 'f4', ('lon',))
#    lat_out = f_out.createVariable('lat', 'f4', ('lat',))
#    area_out = f_out.createVariable('area',   'f4', ('lat', 'lon'))
#    tracer_out = f_out.createVariable(i+'_flux',   'f4', ('time','lat', 'lon'))
#    time_out[:] = np.array([183+365*(i) for i in range(0,12)]);time_out.units='days since 2000-01-01 0:0:0'
#    lon_out[:] = f.variables['longitude'][:];lon_out.units='degree east';lon_out.long_name='longitude'
#    lat_out[:] = f.variables['latitude'][:];lat_out.units='degree north';lat_out.long_name='latitude'
#    area_out[:] = f.variables['area'][:];area_out.units='m2'
#    tracer_out[:,:,:] = np.nansum(vars()[i+'_grid']*1000*6.023E23/tracers[i]/365/86400/area_out[:],1); tracer_out.units = "molecules m-2 s-1" ; tracer_out.molar_mass = tracers[i]
#    f_out.close()
tracer_spec = {'CO': [28.01,1.0,1.0], 'CH4': [16.04,1.0,1.0], 'SO2': [64.07,1.0,1.0],
            'NH3': [17.03,1.0,1.0], 'NOx': [30.01,1.0,1.0],
            'HCHO' : [30.03,0.0609,0.00478], 'CH3OH' : [32.04,0.10759,0.0143], 
            'C2H6' : [30.06,0.1014,0.0597], 'C3H8' :  [44.1,0.0322,0.0946],
           'CH3COCH3' : [58.08,0.0501,0.02375], 'CH3CHO' : [44.05,0.0483,0.0],
           'CH3COOH' : [60.05,0.1136,0.0], 'HCOOH' : [46.03,0.0404,0.0], 
           'C2H4' : [28.05,0.1904,0.0371], 'C3H6' : [42.08,0.0849,0.0157],
           'C4H10' : [58.12,0.0416,0.7154], 'MEK' : [72.11,0.1281,0.03447]}  ####### [mol_wt, BF_frac, FF_frac]
#bb_frac = {'HCHO' : 0.0609, 'CH3OH' : 0.10759, 'C2H6' : 0.1014, 'C3H8' :  0.0322,
#           'CH3COCH3' : 0.0501, 'CH3CHO' : 0.0483, 'CH3COOH' : 0.1136, 
#          'HCOOH' : 0.0404, 'C2H4' : 0.1904, 'C3H6' : 0.0849, 'C4H10' : 0.0416, 'MEK' : 0.1281}
#ff_frac = {'HCHO' : 0.00478, 'CH3OH' : 0.0143, 'C2H6' : 0.0597, 'C3H8' :  0.0946,
#           'CH3COCH3' : 0.02375, 'CH3CHO' : 0.0, 'CH3COOH' : 0.0, 
#           'HCOOH' : 0.0, 'C2H4' : 0.0371, 'C3H6' : 0.0157, 'C4H10' : 0.7154, 'MEK' : 0.03447}
#########sector numbers (0 based) Originally it was 1 based ###########
#0 : 'Energy industries', : ENE : FF
#1 : 'Non-industrial combustion', :  RCO : BF
#2 : 'Industry', : IND : BF
#3 : 'Fossil fuel production and distribution', : REF : FF
#4 : 'Solvent and other product use', : SOL : FF
#5 : 'Road transport, exhaust, gasoline', : TRA : FF
#6 : 'Road transport, exhaust, diesel', : TRA : FF
#7 : 'Road transport, exhaust, LPG and natural gas', : TRA : FF
#8 : 'Road transport, gasoline evaporation', : TRA : FF
#9 : 'Road transport, tyre, brake and road wear', : TRA : FF
#10: 'Non-road transport', : TRA : FF
#11: 'Waste', : WST : BF
#12: 'Agriculture' : AGR : BF
####################
for tracer_names in ['NMVOC','CO','CH4','SO2','NH3','NOx']:
    vars()[tracer_names+'_BF'] = np.nansum(vars()[tracer_names+'_grid'][:,np.r_[1:3,11:13],:,:],1)
    vars()[tracer_names+'_FF'] = np.nansum(vars()[tracer_names+'_grid'][:,np.r_[0,3:11],:,:],1)    ### sector 0, 3 to 10
for i in tracer_spec:
    for idx,types in enumerate(['BF','FF']):
        filename_out = i + '_' + types + '_tot_TNO_MACC_III_emissions_2000_2011.nc'
        f_out = netCDF4.Dataset(os.path.join(dirname, filename_out), 'w', format='NETCDF4')
        f_out.createDimension('time', None)
        f_out.createDimension('lev', 1)
        f_out.createDimension('lat', 672)
        f_out.createDimension('lon', 720)
        time_out = f_out.createVariable('time', 'f4', ('time',))
        lat_out = f_out.createVariable('lat', 'f4', ('lat',))
        lon_out = f_out.createVariable('lon', 'f4', ('lon',))        
        lev_out = f_out.createVariable('lev', 'f4', ('lev',))
        height_out = f_out.createVariable('height', 'f4', ('lev',))
        area_out = f_out.createVariable('area',   'f4', ('lat', 'lon'))
        tracer_out = f_out.createVariable(i+'_flux',   'f4', ('time','lev','lat', 'lon'))
        #time_out[:] = np.array([183+365*(i) for i in range(0,12)]);time_out.long_name='time';time_out.units='days since 2000-01-01 00:00:00'
        time_out[:] = np.array([i for i in range(0,12)]);time_out.long_name='time';time_out.units='year since 2000-01-01 00:00:00'
        lon_out[:] = f.variables['longitude'][:];lon_out.long_name='longitude';lon_out.units='degree east'
        lat_out[:] = f.variables['latitude'][:];lat_out.long_name='latitude';lat_out.units='degree north'
        lev_out[:] = 1;lev_out.long_name='level index';lev_out.units='level'
        height_out[:] = 140;height_out.long_name='emission height';height_out.units='m'
        area_out[:] = f.variables['area'][:];area_out.long_name='area';area_out.units='m2'
        if i in ['HCHO', 'CH3OH', 'C2H6', 'C3H8', 'CH3COCH3', 'CH3CHO', 'CH3COOH', 'HCOOH', 'C2H4', 'C3H6', 'C4H10', 'MEK']:
            tracer_out[:,0,:,:] = vars()['NMVOC_'+types]*tracer_spec[i][idx+1]*1000*6.023E23/tracer_spec[i][0]/365/86400/area_out[:]            
        else:
            tracer_out[:,0,:,:] = vars()[str(i)+'_'+types]*tracer_spec[i][idx+1]*1000*6.023E23/tracer_spec[i][0]/365/86400/area_out[:]
        tracer_out.long_name = 'flux of '+ i ; tracer_out.units = "molec./m^2/s" ; tracer_out.molar_mass = tracer_spec[i][0]  
        f_out.close()
toc = time.time()
print(toc-tic, 'sec Elapsed')