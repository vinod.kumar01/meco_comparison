# -*- coding: utf-8 -*-
"""
Created on Sat Feb  8 22:25:00 2020

@author: Vinod
"""

import netCDF4
import numpy as np
import cartopy.crs as ccrs
import matplotlib.pyplot as plt
from matplotlib.colors import LinearSegmentedColormap
import os
from plot_tools import map_prop
#from mod_colormap import man_cm, add_white
def man_cm(vals, c_names):
    norm = plt.Normalize(min(vals), max(vals))
    tuples = list(zip(map(norm, vals), c_names))
    new_cm = LinearSegmentedColormap.from_list("", tuples)
    return new_cm

# %% data import and averaging
dirname_sim = r'M:\nobackup\vinod\Emissions\UBA\grouped'
# dirname_sim = r'M:\nobackup\vinod\TNO_macc_iii\sectorwise\seasonal'
#new_cm = add_white("jet", shrink_colormap=False, replace_frac=0.01,
#                   cut_frac=0.2, start=None)
vals = [5e4, 1e5, 1.2e5, 1.5e5, 1.8e5, 2e5, 4e5, 5e5, 7e5]
c_names = ["azure", "deepskyblue", "aquamarine", "greenyellow", "darkorange",
           "orangered", "red", "darkred", "purple"]
new_cm = man_cm(vals, c_names)
#dirname_sim = r'M:\nobackup\vinod\model_work'
#filename = 'X_X_X_X_VOC_X-X.nc'

#llon, ulon, llat, ulat = 0, 19, 43, 56
llon, ulon, llat, ulat = 5.5, 15.5, 47, 55.1
#llon, ulon, llat, ulat = 7.5, 9.5, 49.0, 50.5
prop_dict = {'extent': [llon, ulon, llat, ulat],
             'name': ' ',
             'border_res': '10m',
             'bkg': None}
map2plot = map_prop(**prop_dict)
var = 'NOx'  # emfact_isop, NOx
year = 2011
month = 5
# for sector in ['AGR', 'ENE', 'IND', 'RCO', 'REF',
#                'SHP', 'SOL', 'TRA', 'WST']:
for sector in ['TRA']:
    # filename = 'TNO_MACC_III_NOx_{}_{}.nc'.format(year, sector)
    # filename = 'UBA_NOx_{}_2018.nc'.format(sector)
    filename = 'UBA_TNO_{}_sfc_2018.nc'.format(sector)
    if 'TNO_MACC' in filename:
        keep = np.loadtxt(r'M:\nobackup\vinod\Emissions\TNO_macc_iii\De_mask_TNO_7km.txt')
    else:
        keep = np.loadtxt(r'M:\nobackup\vinod\Emissions\UBA\De_mask_UBA_1km.txt')
    keep = keep.copy().astype(bool)
#    sector = filename.split('_')[2]
    f = netCDF4.Dataset(os.path.join(dirname_sim, filename))
    lons = f.variables['lon'][:]
    lats = f.variables['lat'][:]
    flux = f.variables[var+'_flux'][:]
    if len(flux.shape) == 4:      # verticul sum
        flux = np.sum(flux, 1)
    mean_flux = flux[month-1, :, :]
    mean_flux[mean_flux == 0] = np.nan
    if f.variables[var+'_flux'].units == 'mcl m-2 s-1':
        mw = f.variables[var+'_flux'].molar_mass
        mean_flux = mw*mean_flux/6.023E23/1000  # Kg m-2 s-1
    try:
        area = f.variables['area'][:]  # m2
    except KeyError:
        area = f.variables['cell_area'][:]  # m2
    emis = mean_flux*area*3600*24*365  # Kg year-1
    print('Total emission of '+ var + ' '+ str(np.nansum(emis[keep])/1000/1000)+
          ' Kilotonnes per year' )
    f.close()
    # plotting
    datadict = {'lat': lats, 'lon': lons, 'plotable': mean_flux*1E3*1E6,
                'vmin': 0, 'vmax': 1, 'label': '',
                'text': 'NO$_x$'+' emission flux ($\mu$g $m^{-2}$ $s^{-1}$)'}
#    datadict = {'lat': lats, 'lon': lons, 'plotable': emis,
#                'vmin': 1e5, 'vmax': 6e6, 'label': '',
#                'text': 'NOx'+' emission (Kg year$^{-1}$)'}
    
#    new_cm=man_cm("cmap_diff", 6)
    projection = ccrs.PlateCarree()  # ccrs.Orthographic(8, 40), ccrs.PlateCarree()
    fig, ax = plt.subplots(subplot_kw=dict(projection=projection))
    scat = map2plot.plot_map(datadict, ax, cmap=new_cm, projection=projection,
                             alpha=1)
    plt.title(str(month).zfill(2)+"-"+str(year)+ ' ' + sector +
              ' mean emissions of '+var)
    cb = fig.colorbar(scat, ax=ax, orientation='horizontal',
                      extend='max', fraction=0.03, shrink=0.5, pad=0.07)
                        # fraction=0.05, shrink=0.8, pad=0.05
    cb.set_label(datadict['text'], fontsize=10)
    fig.canvas.draw()
    plt.tight_layout()
    # plt.savefig(os.path.join(dirname_sim, 'plots', 'TNO_NOX_{}_maps.png'.format(sector)),
    #             format='png', dpi=300)
    # plt.close()

#try:
##    mean_flux = flux[month-1, :, :]
##except:
##    mean_flux = flux[:,:,:]
## %% load total and other for checking internal consistency
#tra_file = netCDF4.Dataset(r'/mnt/lustre01/work/mm0062/b302069/emissions/TNO_MACC_III/creation/output/TNO_MACC_III_NOx_TRA_2000-2011.nc') 
#nox_tra_flux = tra_file.variables[var+'_flux'][:]
#if len(nox_tra_flux.shape)==4:      # verticul sum
#    nox_tra_flux = np.sum(nox_tra_flux,1)
#mean_nox_tra_flux = nox_tra_flux[12*(year-2000):12*(year+1-2000), :, :]
#
#tot_file = netCDF4.Dataset(r'/mnt/lustre01/work/mm0062/b302069/emissions/TNO_MACC_III/merge/output/TNO_MACC_III_NOx_anth_2000-2011.nc')
#nox_tot_flux = tot_file.variables[var+'_flux'][:] 
#if len(nox_tot_flux.shape)==4:      # verticul sum
#    nox_tot_flux = np.sum(nox_tot_flux,1)
#mean_nox_tot_flux = nox_tot_flux[12*(year-2000):12*(year+1-2000), :, :]
#mean_flux = mean_nox_tot_flux-mean_flux-mean_nox_tra_flux
## %% end checking here
#mean_flux = np.mean(mean_flux,0)  # yearly mean; molecules m-2 s-1

# %% plot map

