# -*- coding: utf-8 -*-
"""
Created on Sun Jun 28 11:39:58 2020

@author: Vinod
"""
# %% import and definition
import netCDF4
import os
import numpy as np
import shapefile
import shutil
from shapely.geometry import shape, Point, box


def check(lon, lat):
    # build a shapely point from your geopoint
    point = Point(lon, lat)
    # the contains function does exactly what you want
    return polygon.contains(point)


create_keep = False
# %% load data
#inner_file = r'M:\nobackup\vinod\Emissions\UBA\grouped\UBA_anth_m_tra_rco_2018.nc'
#outer_file =  r'M:\nobackup\vinod\Emissions\UBA\TNO_regrid\TNO_MACC_III_anth_m_tra_rco_2011_crop.nc'
#basedestdir = r'M:\nobackup\vinod\Emissions\UBA'
inner_file = r'M:\nobackup\vinod\Emissions\TNO_macc_iii\merge_UBA\UBA_anth_m_tra_rco_2018_regrid.nc'
outer_file =  r'M:\nobackup\vinod\Emissions\TNO_macc_iii\merge_UBA\TNO_MACC_III_anth_m_tra_rco_2011.nc'
basedestdir = r'M:\nobackup\vinod\Emissions\TNO_macc_iii'

#dirname_sim = r'M:\nobackup\vinod\Emissions\UBA\grouped'
dirname_sim = r'M:\nobackup\vinod\Emissions\TNO_macc_iii\merge_UBA'
# %% create mask for data within shape
if create_keep:
    year = 2018
    sector = 'TRA'
    De_shp = r'D:\python_toolbox\Igismap\Germany_Boundary.shp'
    r = shapefile.Reader(De_shp)
    # get the shapes
    shapes = r.shapes()
    # build a shapely polygon from your shape
    polygon = shape(shapes[0])
    minx, miny, maxx, maxy = polygon.bounds
    bounding_box = box(minx, miny, maxx, maxy)
    filename = 'UBA_NOx_{}_{}.nc'.format(sector, year)
    f_outer = netCDF4.Dataset(outer_file)
    f_inner = netCDF4.Dataset(inner_file)
    lat = f_outer.variables['lat'][:]
    lon = f_outer.variables['lon'][:]
    flux_outer = f_outer.variables['NOx_flux'][:]
    flux_inner = f_inner.variables['NOx_flux'][:]
    keep = np.zeros(flux_outer.shape[-2:], dtype=bool)
    for j in range(lat.shape[0]):
        for i in range(lon.shape[0]):
            if (miny < lat[j] < maxy) & (minx < lon[i] < maxx):
                keep[j, i] = check(lon[i], lat[j])
#                keep[j, i] = 1
        print(j)
        # monitor progress
    keep_bin = keep.copy().astype(int)
    outname = os.path.join(basedestdir, 'De_mask_UBA_1km.txt')
    np.savetxt(outname, keep_bin, fmt='%i')
    f_outer.close()
    f_inner.close()
# %% remove data within shapefile
shutil.copy(outer_file, inner_file.replace('UBA_', 'UBA_TNO_'))
area = netCDF4.Dataset(os.path.join(dirname_sim, 'area.nc')).variables['cell_area'][:]
#keep = np.loadtxt(os.path.join(basedestdir, 'De_mask_UBA_1km.txt'))
keep = np.loadtxt(os.path.join(basedestdir, 'De_mask_TNO_7km.txt'))
keep = keep.copy().astype(bool)
f_inner = netCDF4.Dataset(inner_file)
f_outer = netCDF4.Dataset(inner_file.replace('UBA_', 'UBA_TNO_'), 'r+')
#if 'area' not in f_outer.variables.keys():
#    f_outer.createVariable('area', 'f4', ('lat', 'lon'))
f_outer.variables['area'][:] = area
for tracer in ['NOx', 'NH3', 'SO2', 'NC4H10', 'MEK', 'HCHO', 'CO',
               'CH3OH', 'CH3COCH3', 'C3H8', 'C3H6', 'C2H6', 'C2H4',
               'CH3CO2H', 'CH3CHO', 'HCOOH']:
    flux_outer = f_outer.variables[tracer+'_flux'][:]
    flux_inner = f_inner.variables[tracer+'_flux'][:]
    flux_join = flux_outer.copy()
    if len(flux_outer.shape) == 4:
        flux_join[:, :, keep] = flux_inner[:, :, keep]
    elif len(flux_outer.shape) == 3:
        flux_join[:, keep] = flux_inner[:, keep]
    else:
        flux_join[keep] = flux_inner[keep]
    f_outer.variables[tracer+'_flux'][:] = flux_join
    print(tracer + ' Done')
f_inner.close()
f_outer.close()
