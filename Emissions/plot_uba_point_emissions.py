# -*- coding: utf-8 -*-
"""
Created on Fri Jun 26 22:51:01 2020

@author: Vinod
"""

import pandas as pd
import os
import matplotlib.pyplot as plt
from mod_colormap import add_white
from plot_tools import map_prop
import cartopy.crs as ccrs
import numpy as np


def grid_emis(df, **kwargs):
    resol_lat = kwargs.get('resol_lat', 0.02)  # degrees, 0.0625, 0.02
    resol_lon = kwargs.get('resol_lon', 0.02)  # degrees, 0.125, 0.02
    lat_col = kwargs.get('lat_col', 'geo_lat_wgs84')
    lon_col = kwargs.get('lon_col', 'geo_long_wgs84')
    emis_col = kwargs.get('emis_col', 'jahresfracht_freisetzung')
    lat_st, lat_end, lon_st, lon_end = 47.46875, 54.82, 5.9375, 15.07
    lat_length = len(np.arange(lat_st, lat_end, resol_lat))
    lon_length = len(np.arange(lon_st, lon_end, resol_lon))
    rast = np.zeros((int(lat_length), int(lon_length)))
    lat = np.zeros((int(lat_length)))
    lon = np.zeros((int(lon_length)))
    counter_lat = 0
    counter_lon = 0
    for lats in np.arange(lat_st, lat_end, resol_lat):
        for lons in np.arange(lon_st, lon_end, resol_lon):
            cond = (df[lat_col] >= lats) & (df[lat_col] < (lats + resol_lat))
            cond &= (df[lon_col] >= lons) & (df[lon_col] < (lons + resol_lon))
            df_sel = df[cond]
            rast[counter_lat, counter_lon] = df_sel[emis_col].sum()
            lon[counter_lon] = lons
            counter_lon = counter_lon+1
        lat[counter_lat] = lats
        counter_lat = counter_lat+1
        counter_lon = 0
    return lat, lon, rast

new_cm = add_white("jet", shrink_colormap=False, replace_frac=0.01,
                   cut_frac=0.2, start=None)
dirname = r'M:\nobackup\vinod\Emissions\UBA'
filename = '2020-05-19_PRTR-Deutschland_Freisetzungen.xlsx'
df = pd.read_excel(os.path.join(dirname, filename))
years = df['jahr'].unique()
tracers = df['pollutant'].unique()
sectors = {'IND': ['Chemical industry', 'Mineral industry',
                   'Paper- and wood industry', 'Other industry',
                   'Food industry', 'Metal industry'],
           'ENE': ['Energy sector'],
           'WST': ['Waste and waste water management'],
           'AGR': ['Intensive livestock production and aquaculture']}
lat_col = 'geo_lat_wgs84'
lon_col = 'geo_long_wgs84'
emis_col = 'jahresfracht_freisetzung'
year = 2017
tracer_list = ['Nitrogen oxides (NOx/NO2)']
#tracer_list = ['Carbon dioxide (CO2)', 'Nitrogen oxides (NOx/NO2)',
#               'Sulphur oxides (SOx/SO2)', 'Particulate matter (PM10)',
#               'Non-methane volatile organic compounds (NMVOC)']
for tracer in tracer_list:
    for sector in sectors:
        cond = (df['pollutant'] == tracer) & (df['jahr'] == year)
        cond &= (df['sector']. isin(sectors[sector]))
        df_sel = df[cond]
        datadict = {'lat': df_sel[lat_col], 'lon': df_sel[lon_col],
                    'plotable': df_sel[emis_col],
                    'vmin': 1e5, 'vmax': 6e6, 'label': '',
                    'text': 'NOx'+' emission (Kg year$^{-1}$)'}
#        llon, ulon, llat, ulat = 7.5, 9.5, 49.0, 50.5
        llon, ulon, llat, ulat = 0, 19, 43, 56
        prop_dict = {'extent': [llon, ulon, llat, ulat],
                     'name': ' ',
                     'border_res': '10m',
                     'bkg': 'OSM'}
        map2plot = map_prop(**prop_dict)
        projection = ccrs.PlateCarree()
        fig, ax = plt.subplots(subplot_kw=dict(projection=projection))
        ax.set_title('{} {} emissions ({})'.format(year, tracer, sector))
        scat = map2plot.plot_map(datadict, ax, cmap=new_cm, projection=projection,
                                 bkg_alpha=0.3, alpha=1, mode='scatter', s=24)
        cb = fig.colorbar(scat, ax=ax, orientation='horizontal',
                          extend='max', fraction=0.03, shrink=0.5, pad=0.07)
        cb.set_label(datadict['text'], fontsize=10)
        plt.tight_layout()
        savename = 'NOx_{}_{}_points_full.png'.format(sector, year)
        plt.savefig(os.path.join(dirname, 'plots', savename),
                    dpi=300, format='png')
        plt.close()
        # Gridded emissions
        grid_lat, grid_lon, gridded_emis = grid_emis(df_sel, resol_lat=0.0625,
                                                     resol_lon=0.125)
        gridded_emis[gridded_emis <= 0] = np.nan
        datadict = {'lat': grid_lat, 'lon': grid_lon,
                    'plotable': gridded_emis,
                    'vmin': 1e5, 'vmax': 6e6, 'label': '',
                    'text': 'NOx'+' emission (Kg year$^{-1}$)'}
        fig, ax = plt.subplots(subplot_kw=dict(projection=projection))
        ax.set_title('Gridded {} {} emissions ({})'.format(year, tracer,
                     sector))
        scat = map2plot.plot_map(datadict, ax, cmap=new_cm,
                                 projection=projection, bkg_alpha=0.7)
        cb = fig.colorbar(scat, ax=ax, orientation='horizontal',
                          extend='max', fraction=0.03, shrink=0.5, pad=0.07)
        cb.set_label(datadict['text'], fontsize=10)
        plt.tight_layout()
        savename = 'NOx_{}_{}_grid_full.png'.format(sector, year)
        plt.savefig(os.path.join(dirname, 'plots', savename),
                    dpi=300, format='png')
