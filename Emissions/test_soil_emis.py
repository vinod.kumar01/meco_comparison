# -*- coding: utf-8 -*-
"""
Created on Fri May 21 14:42:44 2021

@author: Vinod
"""
from os import path
import numpy as np
from netCDF4 import Dataset
from utm_converter import unproject
from plot_tools import map_prop, plt
import cartopy.crs as ccrs
from matplotlib.colors import LinearSegmentedColormap


def man_cm(vals, c_names):
    norm = plt.Normalize(min(vals), max(vals))
    tuples = list(zip(map(norm, vals), c_names))
    new_cm = LinearSegmentedColormap.from_list("", tuples)
    return new_cm


vals = [5e4, 1e5, 1.2e5, 1.5e5, 1.8e5, 2e5, 4e5, 5e5, 7e5]
c_names = ["azure", "deepskyblue", "aquamarine", "greenyellow", "darkorange",
           "orangered", "red", "darkred", "purple"]
new_cm = man_cm(vals, c_names)

# %%

year = 2018
dirname = r'M:\nobackup\vinod\Emissions\UBA\raw'

with Dataset(path.join(dirname, str(year)+'_Sub2020_NFR.nc')) as f:
    utm_lat = f.variables['LAT'][:]
    utm_lon = f.variables['LON'][:]
    geolat = np.zeros((len(utm_lon), len(utm_lat)))
    geolon = np.zeros((len(utm_lon), len(utm_lat)))
    for x, u_lon in enumerate(utm_lon):
        for y, u_lat in enumerate(utm_lat):
            lon_now, lat_now = unproject(32, 'N', u_lon, u_lat)
            geolat[x, y] = lat_now
            geolon[x, y] = lon_now

sectors = ['3B1a', '3B1b', '3B2', '3B3', '3B4d', '3B4e', '3B4gi',
                   '3B4gii', '3B4giii', '3B4giv', '3Da1', '3Da2a', '3Da2b',
                   '3Da2c', '3Da3', '3Dc', '3De', '3I']
# %% Contribution of sub sectors to AGR in UBA
emis = {}
with Dataset(path.join(dirname, str(year)+'_Sub2020_NFR.nc')) as f:
    for sector in sectors:
        try:
            emis[sector] = f.variables['E_{}_NOX'.format(sector)][:]
            print('{} - {}'.format(sector, np.ma.sum(emis[sector])))
        except KeyError:
            print('NOX not emitted from sector {}'.format(sector))
sum_emis = 0
for key in emis.keys():
    sum_emis += np.ma.sum(emis[key])
print('Total - {}'.format(sum_emis))
# %% Sector contribution to UBA emission
nox_emis_UBA = {}
sectors = {'ENE': ['1A1a', '1A1c'],
           'REF': ['1A1b', '1B2ai', '1B2b', '1B2c'],
           'SOL': ['1A2e', '1A2f', '1B2aiv', '1B2av',
                   '2D3a', '2D3b', '2D3c', '2D3d', '2D3e', '2D3f', '2D3g',
                   '2D3h', '2D3i', '2G'],
           'IND': ['1A2a', '1A2b', '1A3ei', '1B1a', '1B1b', '1A2gvii',
                   '1A2gviii', '2A1', '2A2', '2A3', '2A5a', '2A5b', '2A6',
                   '2B1', '2B10a', '2B2', '2B3', '2B5', '2B7',
                   '2C1', '2C2', '2C3', '2C5', '2C6', '2C7a', '2C7c', '2H1',
                   '2H2', '2I', '2L'],
           # 'air': ['1A3ai_i_', '1A3aii_i_'],
           'TRA': ['1A3bi', '1A3bii', '1A3biv', '1A3bv', '1A3bvi',
                    '1A3bvii', '1A3biii', '1A3c', '1A4aii', '1A4bii', '1A4cii', '1A5b'],
           'SHP':  ['1A3dii', '1A4ciii'],
           'RCO': ['1A4ai', '1A4bi', '1A4ci', '1A5a'],
           'AGR': ['3B1a', '3B1b', '3B2', '3B3', '3B4d', '3B4e', '3B4gi',
                   '3B4gii', '3B4giii', '3B4giv', '3Da1', '3Da2a', '3Da2b',
                   '3Da2c', '3Da3', '3Dc', '3De', '3I'],
           'WST': ['5A', '5B1', '5B2', '5C1bv', '5C2', '5D1', '5E']
           }
with Dataset(path.join(dirname, str(year)+'_Sub2020_NFR.nc')) as f:
    for sec_borad, sub_sectors in sectors.items():
        nox_emis_UBA[sec_borad] = np.zeros((642, 873), dtype='float')
        for sub_sector in sub_sectors:
            try:
                emis_temp = f['E_{}_NOX'.format(sub_sector)][:]*14/46
                nox_emis_UBA[sec_borad] += emis_temp
            except IndexError:
                print('NOX not emitted from sector {}'.format(sub_sector))
            if sec_borad == 'AGR':
                print('{} - {}'.format(sub_sector, np.sum(emis_temp)))
# %% plotting
savedir = r'D:\postdoc\COSMO-CCLM\Manuscript_MECO_DOAS_eval\response_reviewer'
prop_dict = {
             # 'extent': [llon, ulon, llat, ulat],
             # 'extent': [7.2, 9.2, 49, 51],
             'extent': [8.02, 8.42, 49.8, 50.2],
             'name': 'sector',
             'border_res': '10m'}
map2plot = map_prop(**prop_dict)
projection = ccrs.PlateCarree()  #ccrs.Orthographic(8, 40)  # ccrs.PlateCarree()
for sector in ['ENE', 'REF', 'SOL', 'IND', 'TRA', 'SHP', 'RCO', 'AGR', 'WST']:
    fig, ax = plt.subplots(subplot_kw=dict(projection=projection))
    v0 = nox_emis_UBA[sector].copy()
    v0[v0 == 0] = np.nan
    datadict = {'lat': geolat, 'lon': geolon, 'plotable': v0*1e3,
                'vmin': 0, 'vmax': 6,
                'label': sector,
                'text': 'NO$_{x}$ emission (10$^{-3}$ Kg(N)$~year^{-1}$)'}
    scat = map2plot.plot_map(datadict, ax, cmap=new_cm, projection=projection,
                             alpha=0.8)
    cb = fig.colorbar(scat, ax=ax, orientation='vertical',
                      extend='max', fraction=0.03, shrink=0.9, pad=0.05)
    cb.set_label(datadict['text'], fontsize=14)
    
    df = [{'lon': 8.2283, 'lat': 49.9909, 'site': 'MPIC'}]
    
    for point in df:
        lo, la = point['lon'], point['lat']
        plt.annotate(point['site'], xy=(lo, la),  xycoords='data', size=14,
                     xytext=(lo, la), textcoords='data', color='k',
                     weight='bold',
                     horizontalalignment='right', verticalalignment='top',
                     arrowprops=dict(facecolor='black', shrink=0.05))
        plt.plot(lo, la, 'ro')
    angles = {'T1': 321, 'T2': 51, 'T3': 141, 'T4': 231}
    length = 0.2   # 0.8
    x0, y0 = df[0]['lon'], df[0]['lat']
    for telescope, angle in angles.items():
        endx = length * np.sin(np.radians(angle))
        endy = length * np.cos(np.radians(angle))
        ax.plot([x0, x0+endx], [y0, y0+endy], c='k', linewidth=3)
        ax.annotate(telescope, xy=(x0+endx, y0+endy),  xycoords='data',
                    xytext=(x0+endx, y0+endy), textcoords='data', color='k',
                    horizontalalignment='right', verticalalignment='top', size=14,
                    weight='bold',
                    arrowprops=dict(facecolor='k', shrink=0.05))
    fig.canvas.draw()
    plt.tight_layout()
    plt.savefig(path.join(savedir, 'UBA_map_{}.png'.format(sector)),
                format='png', dpi=300)
# %% sector contribution to TNO emission

dirname = r'M:\home\vinod\nobackup\TNO_macc_iii'
filename = 'TNO_MACC_III_emissions_2011.nc'
nox_emis_TNO = {}
with Dataset(path.join(dirname, filename)) as f:
    keep_DEU = f.variables['country_index'][:]==15  # Germany is 15
    lats_sel = f.variables['latitude_source'][keep_DEU]
    lons_sel = f.variables['longitude_source'][keep_DEU]
    emission_category = f.variables['emission_category_index'][:]
    nox_emis = f.variables['nox'][:]  # Kg/year
    nox_emis *= 1e-6  # Gg/year
sectors = {'ENE': [1],
           'REF': [4],
           'SOL': [5],
           'IND': [3],
           'air': [-1],    # air not available in TNO
           'TRA1': [6, 7, 8, 9],
           'TRA3': [10],  # non road (rail)
           'SHP':  [11],
           'RCO': [2],
           'AGR': [13],
           'WST': [12]
           }
with Dataset(path.join(dirname, filename)) as f:
    for sec_borad, sub_sectors in sectors.items():
        nox_emis_TNO[sec_borad] = 0
        for sub_sector in sub_sectors:
            keep = (keep_DEU) & (emission_category == sub_sector)
            nox_emis_TNO[sec_borad] += np.ma.sum(nox_emis[keep])

#0 : 'Energy industries', : ENE : FF
#1 : 'Non-industrial combustion', :  RCO : BF
#2 : 'Industry', : IND : BF
#3 : 'Fossil fuel production and distribution', : REF : FF
#4 : 'Solvent and other product use', : SOL : FF
#5 : 'Road transport, exhaust, gasoline', : TRA : FF
#6 : 'Road transport, exhaust, diesel', : TRA : FF
#7 : 'Road transport, exhaust, LPG and natural gas', : TRA : FF
#8 : 'Road transport, gasoline evaporation', : TRA : FF
#9 : 'Road transport, tyre, brake and road wear', : TRA : FF
#10: 'Non-road transport', : TRA : FF
#11: 'Waste', : WST : BF
#12: 'Agriculture' : AGR : BF

# %% sector contribution to TNO emission
nox_emis_TNO = {}
dirname = r'M:\nobackup\vinod\Emissions\TNO_macc_iii'
keep = np.loadtxt(path.join(dirname, 'De_mask_TNO_7km.txt'))
keep = keep.copy().astype(bool)
sectors = ['ENE', 'REF', 'SOL', 'IND', 'TRA', 'SHP', 'RCO', 'AGR', 'WST']
for sector in sectors:
    with Dataset(path.join(dirname, 'sectorwise\seasonal',
                           'TNO_MACC_III_NOx_2011_{}.nc'.format(sector))) as f:
        area = f.variables['area'][:]
        emis_now = np.mean(f.variables['NOx_flux'][:], 0)*area  # Kg/s
        emis_now *= 1e-6  # Gg/s
        emis_now *= 3600*24*365   # Gg/year
        emis_now *= 14/46   # Gg(N)/year
        nox_emis_TNO[sector] = np.sum(emis_now[keep])
# %% calculating total emissions within Germnay
filename = r'M:\nobackup\vinod\model_work\MECO\pak\EDGAR5\v50_NOx_2015_5_MNM.0.1x0.1.nc'
# filename = r'M:\nobackup\vinod\model_work\MECO\pak\EDGAR_4.3.2\v432_NOx_2012.0.1x0.1.nc'
dirname = r'M:\nobackup\vinod\Emissions\TNO_macc_iii'
keep = np.loadtxt(path.join(dirname, 'De_mask_EDGAR_01.txt'))
keep = keep.copy().astype(bool)
with Dataset(filename) as f:
    area = f.variables['cell_area'][:]
    emis_now = f.variables['emi_nox'][:]*area   # Kg/s
    emis_now *= 1e-6  # Gg/s
    emis_now *= 3600*24*365   # Gg/year
    emis_now *= 14/46   # Gg(N)/year
    nox_emis_edgar = np.sum(emis_now[keep])
    # print(f.variables['emi_nox'].units)
