# -*- coding: utf-8 -*-
"""
Created on Mon Sep 24 12:29:18 2018

@author: Vinod
"""
import time
tic = time.time()
import netCDF4
import glob
import os
import numpy as np
from collections import defaultdict
def convert_country_code(argument):
    switcher = {
        'ALB' : 1, 'AUT' : 2, 'BEL' : 3, 'BGR' : 4, 'DNK' : 6, 'FIN' : 7, 'FRA' : 8,
        'FGD' : 9, 'FFR' : 10, 'GRC' : 11, 'HUN' : 12, 'IRL' : 14, 'ITA' : 15, 
        'LUX' : 16, 'NLD' : 17, 'NOR' : 18, 'ISL' : 18, 'POL' : 19, 'PRT' : 20, 'ROU' : 21, 
        'ESP' : 22, 'SWE' : 23, 'CHE' : 24, 'TUR' : 25, 'GBR' : 27, 'BLR' : 39, 
        'UKR' : 40, 'MKD' : 41, 'MDA' : 42, 'EST' : 43, 'LVA' : 44, 'LTU' : 45, 
        'CZE' : 46, 'SVK' : 47, 'SVN' : 48, 'HRV' : 49, 'BIH' : 50, 'YUG' : 51, 
        'GEO' : 54, 'MLT' : 57, 'DEU' : 60, 'RUS' : 61, 'ARM' : 56, 'AZE' : 58, 'CYP' : 55,
        'ATL' : 99, 'BAS' : 99, 'BLS' : 99, 'MED' : 99, 'NOS' : 99
    }   ## Romania = ROM = ROU, ISL (iceland) is same as norway, 99 is for sea
    return switcher.get(argument, np.nan)
#####sectors
#0 : 'Energy industries', : ENE : FF					1
#1 : 'Non-industrial combustion', :  RCO : BF				2
#2 : 'Industry', : IND : BF						3
#3 : 'Fossil fuel production and distribution', : REF : FF		5
#4 : 'Solvent and other product use', : SOL : FF			6
#5 : 'Road transport, exhaust, gasoline', : TRA : FF			7
#6 : 'Road transport, exhaust, diesel', : TRA : FF			7
#7 : 'Road transport, exhaust, LPG and natural gas', : TRA : FF		7
#8 : 'Road transport, gasoline evaporation', : TRA : FF			7
#9 : 'Road transport, tyre, brake and road wear', : TRA : FF		7
#10: 'Non-road transport', : TRA : FF					8
#11: 'Waste', : WST : BF						9
#12: 'Agriculture' : AGR : BF						10
def choose_sector(argument):
    switcher = {
        0 : 1, 1 : 2, 2 : 3, 3 : 5, 4 : 6, 5 : 7, 6 : 7,
        7 : 7, 8 : 7, 9 : 7, 10 : 8, 11 : 9, 12 : 10
    }
    return switcher.get(argument, np.nan)
###############import and arrange time profiles####
dirname_profile = r'M:\home\vinod\nobackup\TNO_macc_iii\emission_time_profiles\tab_del'
out_dir = r'M:\nobackup\vinod\TNO_macc_iii\sectorwise\seasonal'
speclist = ['CO','NH3','NMVOC','NOx','SO2']
for spec in speclist:   
    with open(os.path.join(dirname_profile,"month_"+spec+'.txt')) as file: 
         input = [line.replace('\n', '').split('\t', maxsplit=2) for line in file]
         file.close()
    vars()[spec+'_fac'] = defaultdict(lambda: defaultdict(list))
    for x, y, value in input:
        vars()[spec+'_fac'][x][y].append(value)
    Countries = ["1","2","3","4","6","7","8","9","10","11","12","13","14","15","16",
               "17","18","19","20","21","22","23","24","25","27","36","37","38",
               "39","40","41","42","43","44","45","46","47","48","49","50","51",
               "52","53","54","55","56","57","58","59","60","61","62","63", "99"]    
    for Country_idx in Countries:
        for SNAP in range(1,12):
            myList = np.array([i.split('\t')[0:12] for i in vars()[spec+'_fac'][str(Country_idx)][str(SNAP)]])
            if Country_idx == "99":
                myList = np.array(("0.85" , "0.85" , "0.9" , "1" , "1.05" , "1.1" , "1.2" , "1.2" , "1.1" , "1" , "0.9" , "0.85")).reshape(1,12)
            vars()[spec+'_fac'][str(Country_idx)][str(SNAP)] = [float(i) for i in myList[0]]
CH4_fac = NH3_fac.copy()
for keys in CH4_fac:
    for keys2 in CH4_fac[keys]:
        CH4_fac[keys][keys2] = [1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0]
###################################################
dirname = r'M:\home\vinod\nobackup\TNO_macc_iii'
f_names = glob.glob(dirname+'\TNO_MACC_III_emissions_*.nc')
f_names.sort(key=os.path.basename)
#####preparing dummy annual grids with dimestion time,emission category,lat,lon for tracers######
NOx_grid = np.zeros((12,13,672,720), dtype = 'float')
CO_grid = np.zeros((12,13,672,720), dtype = 'float')
CH4_grid = np.zeros((12,13,672,720), dtype = 'float')
SO2_grid = np.zeros((12,13,672,720), dtype = 'float')
NMVOC_grid = np.zeros((12,13,672,720), dtype = 'float')
NH3_grid = np.zeros((12,13,672,720), dtype = 'float')
Country_grid = np.zeros((12,13,672,720), dtype = 'float')
#####data readout######
for f_num in range(0,len(f_names)): 
    year_num = 2000+f_num
    filename_in = f_names[f_num]
    f=netCDF4.Dataset(filename_in,'r')
    lon_idx = f.variables['longitude_index'][:]
    lon_src = f.variables['longitude_source'][:]
    lat_idx = f.variables['latitude_index'][:]
    lat_src = f.variables['latitude_source'][:]
    emission_category = f.variables['emission_category_index'][:]
    country_idx = f.variables['country_index'][:]
    CO_src = f.variables['co'][:]
    NOx_src = f.variables['nox'][:]
    CH4_src = f.variables['ch4'][:]
    SO2_src = f.variables['so2'][:]
    NMVOC_src = f.variables['nmvoc'][:]
    NH3_src = f.variables['nh3'][:]
    ######arranging data in grids and assigning dimesions###########
    for emis_cat in range(1,14):
        for i in range(0,len(lon_idx)):
            if emission_category[i] == emis_cat:
                CO_grid[f_num,emis_cat-1,lat_idx[i]-1,lon_idx[i]-1]=CO_src[i]    #lat and lon idx in inventory are 1 based
                CH4_grid[f_num,emis_cat-1,lat_idx[i]-1,lon_idx[i]-1]=CH4_src[i]
                SO2_grid[f_num,emis_cat-1,lat_idx[i]-1,lon_idx[i]-1]=SO2_src[i]
                NMVOC_grid[f_num,emis_cat-1,lat_idx[i]-1,lon_idx[i]-1]=NMVOC_src[i]
                NH3_grid[f_num,emis_cat-1,lat_idx[i]-1,lon_idx[i]-1]=NH3_src[i]
                NOx_grid[f_num,emis_cat-1,lat_idx[i]-1,lon_idx[i]-1]=NOx_src[i]
                Country_grid[f_num,emis_cat-1,lat_idx[i]-1,lon_idx[i]-1]=country_idx[i]
###############################################
Country_grid_remap = Country_grid.copy()
for spec in ['CO','NH3','NMVOC','NOx','SO2', 'CH4']:
    vars()[spec+'_month'] = np.repeat(vars()[spec+'_grid'], 12, axis=0)    
    for lat in range(0,672):
        for lon in range(0,720):
            if spec == "CO":  #remap country grid for the first time
                Country_grid_remap[:,:,lat,lon]=convert_country_code(str(netCDF4.chartostring(f.variables['country_id'][Country_grid[10,10,lat,lon]-1], encoding='Latin-1')))
            for years in range(0,12):
                for sector in range (0,13):
                    month_factor = vars()[spec+'_fac'][str(int(Country_grid_remap[years,sector,lat,lon]))][str(int(choose_sector(sector)))]
                    ref_grid = vars()[spec+'_grid']
                    vars()[spec+'_month'][12*years:12*years+12,sector,lat,lon]= [i*ref_grid[years,sector,lat,lon] for i in month_factor]
#######################################creating sectorwise netcdf files################
#str(netCDF4.chartostring(f.variables['country_id'][f.variables['country_index'][23000]], encoding='Latin-1'))
for tracer_names in ['CO','NH3','NMVOC','NOx','SO2', 'CH4']:
    vars()[tracer_names+'_ENE'] = vars()[tracer_names+'_month'][:,0,:,:]
    vars()[tracer_names+'_RCO'] = vars()[tracer_names+'_month'][:,1,:,:]
    vars()[tracer_names+'_IND'] = vars()[tracer_names+'_month'][:,2,:,:]
    vars()[tracer_names+'_REF'] = vars()[tracer_names+'_month'][:,3,:,:]
    vars()[tracer_names+'_SOL'] = vars()[tracer_names+'_month'][:,4,:,:]
    vars()[tracer_names+'_TRA'] = np.nansum(vars()[tracer_names+'_month'][:,np.r_[5:11],:,:],1)    ### sector 5 to 10
    vars()[tracer_names+'_WST'] = vars()[tracer_names+'_month'][:,11,:,:]
    vars()[tracer_names+'_AGR'] = vars()[tracer_names+'_month'][:,12,:,:]
    for idx,sector in enumerate(['ENE','RCO', 'IND', 'REF', 'SOL', 'TRA', 'WST', 'AGR']):
        for years in range(2000, 2012):
            filename_out = 'TNO_MACC_III_'+ tracer_names + '_' + str(years) + '_' + sector + '.nc'
            f_out = netCDF4.Dataset(os.path.join(out_dir, filename_out), 'w', format='NETCDF4')
            f_out.createDimension('time', None)
            f_out.createDimension('lat', 672)
            f_out.createDimension('lon', 720)
            lat_out = f_out.createVariable('lat', 'f4', ('lat',))
            lon_out = f_out.createVariable('lon', 'f4', ('lon',))
            time_out = f_out.createVariable('time', 'f4', ('time',))
            area_out = f_out.createVariable('area',   'f4', ('lat', 'lon'))
            tracer_out = f_out.createVariable(tracer_names+'_flux',   'f4', ('time','lat', 'lon'))
            lon_out[:] = f.variables['longitude'][:];lon_out.long_name='longitude';lon_out.units='degree east'
            lat_out[:] = f.variables['latitude'][:];lat_out.long_name='latitude';lat_out.units='degree north'
            area_out[:] = f.variables['area'][:];area_out.long_name='area';area_out.units='m2'
            time_out[:] = np.array([years + i/12 for i in range(0,12)]); time_out.long_name='time';time_out.units='year since 2000-01-01 00:00:00'
            tracer_out[:,:,:] = vars()[tracer_names + '_' + sector][12*(years-2000):12*(years-2000)+12,:,:]/365/86400/area_out[:]
            tracer_out.long_name = 'flux of '+ tracer_names ; tracer_out.units = "Kg m-2 s-1" 
            f_out.close()
toc = time.time()
print(toc-tic, 'sec Elapsed')