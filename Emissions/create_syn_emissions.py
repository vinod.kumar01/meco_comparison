# -*- coding: utf-8 -*-
"""
Created on Fri Feb  5 14:32:56 2021

@author: Vinod
"""

import netCDF4
import os
import matplotlib.pyplot as plt
import shutil
from plot_tools import map_prop, ccrs
from matplotlib.colors import LinearSegmentedColormap
def man_cm(vals, c_names):
    norm = plt.Normalize(min(vals), max(vals))
    tuples = list(zip(map(norm, vals), c_names))
    new_cm = LinearSegmentedColormap.from_list("", tuples)
    return new_cm


llon, ulon, llat, ulat = 0, 19, 43, 56
prop_dict = {'extent': [llon, ulon, llat, ulat], 'name': 'MECO_DE',
             'border_res': '10m'}
projection = ccrs.PlateCarree()
map2plot = map_prop(**prop_dict)

vals = [5e4, 1e5, 1.2e5, 1.5e5, 1.8e5, 2e5, 4e5, 5e5, 7e5]
c_names = ["azure", "deepskyblue", "aquamarine", "greenyellow", "darkorange",
           "orangered", "red", "darkred", "purple"]
new_cm = man_cm(vals, c_names)

month = 5

dirname = r'M:\nobackup\vinod\Emissions\UBA\TNO_regrid\syn_emis'

emis_file = 'UBA_TNO_TRA_sfc_2018_regrid.nc'
dest_file = 'UBA_TNO_TRA_sfc_2018_syn.nc'
shutil.copy2(os.path.join(dirname, emis_file),
             os.path.join(dirname, dest_file))
# %%plotting
f = netCDF4.Dataset(os.path.join(dirname, dest_file), 'r')
lat = f.variables['lat'][:]
lon = f.variables['lon'][:]
mw = f.variables['NOx_flux'].molar_mass
emis_flux = f.variables['NOx_flux'][month-1, :]
mean_flux = mw*emis_flux/6.023E23/1000  # Kg m-2 s-1
mean_flux *= 1e9  # ug m-2 s-1
# Plot test emissions
datadict = {'lat': lat, 'lon': lon, 'vmin': 0, 'vmax': 1,
            'text': 'NOx emission flux (${\mu}g ~m^{-2} ~s^{-1}$)',
            'label': '', 'plotable': mean_flux}
fig, ax = plt.subplots(subplot_kw=dict(projection=ccrs.PlateCarree()))
cs = map2plot.plot_map(datadict, ax, cmap=new_cm,
                        mode='pcolormesh',
                        projection=projection, alpha=1)
fig.canvas.draw()
plt.tight_layout()
fig.subplots_adjust(right=0.86, wspace=0.02)
cbar_ax = fig.add_axes([0.88, 0.2, 0.02, 0.6])
cbar = fig.colorbar(cs, cax=cbar_ax, extend='max', shrink=0.8)
cbar.set_label(datadict['text'], fontsize=10)
f.close()
# %% reshuffling
with netCDF4.Dataset(os.path.join(dirname, dest_file), 'r+') as f:
    for cur_flux in f.variables.keys():
        if '_flux' in cur_flux:
            print(cur_flux)
            src_val = f.variables[cur_flux][:].copy()
            temp_val = src_val.copy()
            temp_val[:, 352:387, 324:340] = src_val[:, 320:355, 288:304]
            temp_val[:, 328:353, 320:333] = src_val[:, 320:345, 267:280]
            temp_val[:, 272:304, 284:292] = src_val[:, 304:336, 304:312]
            temp_val[:, 357:373, 284:301] = src_val[:, 240:256, 307:324]
            temp_val[:, 324:340, 331:341] = src_val[:, 304:320, 283:293]
            f.variables[cur_flux][:] = temp_val
