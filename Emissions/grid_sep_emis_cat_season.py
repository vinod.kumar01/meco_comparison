# -*- coding: utf-8 -*-
"""
Created on Mon Sep 24 12:21:20 2018

@author: Vinod
"""

import time
tic = time.time()
import netCDF4
import numpy as np
import os
import glob
dirname = r'M:\home\vinod\nobackup\TNO_macc_iii'
f_names = glob.glob(dirname+'\TNO_MACC_III_emissions_*.nc')
f_names.sort(key=os.path.basename)
#####preparing dummy grids with dimestion time,emission category,lat,lon for tracers######
CO_grid = np.zeros((12,13,672,720), dtype = 'float')
PM2_5_grid = np.zeros((12,13,672,720), dtype = 'float')
PM10_grid = np.zeros((12,13,672,720), dtype = 'float')
CH4_grid = np.zeros((12,13,672,720), dtype = 'float')
SO2_grid = np.zeros((12,13,672,720), dtype = 'float')
NMVOC_grid = np.zeros((12,13,672,720), dtype = 'float')
NH3_grid = np.zeros((12,13,672,720), dtype = 'float')
NOx_grid = np.zeros((12,13,672,720), dtype = 'float')
#####data readout######
for f_num in range(0,len(f_names)): 
    year_num = 2000+f_num
    filename_in = f_names[f_num]
    f=netCDF4.Dataset(filename_in,'r')
    lon_idx = f.variables['longitude_index'][:]
    lon_src = f.variables['longitude_source'][:]
    lat_idx = f.variables['latitude_index'][:]
    lat_src = f.variables['latitude_source'][:]
    emission_category = f.variables['emission_category_index'][:]
    CO_src = f.variables['co'][:]
    PM2_5_src = f.variables['pm2_5'][:]
    PM10_src = f.variables['pm10'][:]
    CH4_src = f.variables['ch4'][:]
    SO2_src = f.variables['so2'][:]
    NMVOC_src = f.variables['nmvoc'][:]
    NH3_src = f.variables['nh3'][:]
    NOx_src = f.variables['nox'][:]
    ######arranging data in grids and assigning dimesions###########
    for emis_cat in range(1,14):
        for i in range(0,len(lon_idx)):
            if emission_category[i] == emis_cat:
                CO_grid[f_num,emis_cat-1,lat_idx[i]-1,lon_idx[i]-1]=CO_src[i]    #lat and lon idx in inventory are 1 based
                PM2_5_grid[f_num,emis_cat-1,lat_idx[i]-1,lon_idx[i]-1]=PM2_5_src[i]
                PM10_grid[f_num,emis_cat-1,lat_idx[i]-1,lon_idx[i]-1]=PM10_src[i]
                CH4_grid[f_num,emis_cat-1,lat_idx[i]-1,lon_idx[i]-1]=CH4_src[i]
                SO2_grid[f_num,emis_cat-1,lat_idx[i]-1,lon_idx[i]-1]=SO2_src[i]
                NMVOC_grid[f_num,emis_cat-1,lat_idx[i]-1,lon_idx[i]-1]=NMVOC_src[i]
                NH3_grid[f_num,emis_cat-1,lat_idx[i]-1,lon_idx[i]-1]=NH3_src[i]
                NOx_grid[f_num,emis_cat-1,lat_idx[i]-1,lon_idx[i]-1]=NOx_src[i]