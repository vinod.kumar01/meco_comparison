# -*- coding: utf-8 -*-
"""
Created on Fri Jul 20 15:33:14 2018

@author: Vinod
"""

import netCDF4
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from mpl_toolkits.basemap import Basemap, shiftgrid
f_rcp=netCDF4.Dataset(r'M:\home\vinod\RCP_EC_comp\RCP_NO.nc','r')
f_ec=netCDF4.Dataset(r'M:\home\vinod\RCP_EC_comp\EC_NO_regrid.nc','r')
lats=f_ec.variables['lat'][:]
lons=f_ec.variables['lon'][:]
var_rcp = 'NO_flux'
var_ec = 'NOX_flux'
l=0
V_rcp = f_rcp.variables[var_rcp][40,l,:,:]
V_ec = np.nanmean(f_ec.variables[var_ec][:,l,:,:],0)
V_ec[V_ec==0]=np.nan
#V0=V_ec-V_rcp
fig=plt.Figure()
plt.title('NO surface emission flux for 2010')
#ax = fig.add_subplot(1, 1, 1)
#map = Basemap(projection = 'moll', lon_0 = 0,resolution='c')
#map = Basemap(projection='robin',lon_0=0,resolution='c')
#map = Basemap(projection='cyl', resolution = 'i', llcrnrlon=-31, llcrnrlat=30,urcrnrlon=61, urcrnrlat=72)
map = Basemap(projection='cyl', resolution = 'l', llcrnrlon=1, llcrnrlat=43,urcrnrlon=20, urcrnrlat=56)
map.drawcoastlines()
map.drawcountries()
map.drawrivers(linewidth=0.2)
#map.fillcontinents(color = 'coral')
map.drawmapboundary()
map.drawparallels(np.arange(-90.,120.,10.),labels=[1,0,0,0])
map.drawmeridians(np.arange(-180.,180.,20.),labels=[0,0,0,1])
V_ec,lons = shiftgrid(180.,V_ec,lons,start=False)
llons, llats = np.meshgrid(lons, lats)
x,y = map(llons, llats)
#map.plot(x, y, '.', markersize=2)
#cs = map.pcolormesh(x, y, V0, norm=colors.LogNorm(vmin=np.nanmin(V0), vmax=np.nanmax(V0)), cmap='jet')
cs = map.pcolormesh(x, y, V_ec, cmap='jet', vmin=1E14, vmax = 5E15, alpha = 0.9)
cb=map.colorbar(extend = 'both')
cb.set_label(var_ec + ' (molecules m$^{-2}$ s$^{-1}$)')
#cb.set_label(V_rcp + ' (molecules m$^{-2}$ s$^{-1}$)')