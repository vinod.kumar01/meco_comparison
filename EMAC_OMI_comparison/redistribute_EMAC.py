# -*- coding: utf-8 -*-
"""
Created on Tue Jul 14 12:23:27 2020

@author: Vinod
"""

from os import path
from filetools import nc2dict, dict2nc2d
import numpy as np

datadir = r'M:\nobackup\vinod\model_work\EMAC\processed'
filename = 'mean_2015.nc'
datafile = path.join(datadir, filename)
savevars = ['SIM_VCD', 'SAT_VCD_corr', 'SAT_VCD']
data = nc2dict(datafile)
data_mod = data.copy()
lat = data['lat']
lon = data['lon']
SAT_VCD_corr = data['SAT_VCD_corr'].filled(np.nan)
SIM_VCD = data['SIM_VCD'].filled(np.nan)
SIM_VCD_MOD = np.zeros(SIM_VCD.shape)*np.nan
spatial_kernel = np.zeros(SIM_VCD.shape)*np.nan
coarse_lat = np.arange(-85.4, 85.4, 1.4)
coarse_lon = np.arange(-179.4, 179.4, 1.4)

for i in coarse_lon:
    for j in coarse_lat:
        lon_idx = np.argwhere((lon >= i) & (lon < i+1.4))[:, 0]
        lat_idx = np.argwhere((lat >= j) & (lat < j+1.4))[:, 0]
        if (len(lat_idx) > 0) & (len(lon_idx) > 0):
            omi_vcd_corr_now = SAT_VCD_corr[lat_idx, :][:, lon_idx]
            spatial_kernel_now = np.array([i/np.nanmean(omi_vcd_corr_now)
                                          for i in omi_vcd_corr_now])
            spatial_kernel[lat_idx[0]:lat_idx[-1]+1,
                           lon_idx[0]:lon_idx[-1]+1] = spatial_kernel_now
data_mod['SIM_VCD'] = data_mod['SIM_VCD']*spatial_kernel
outname = 'mean_2015_redist.nc'
dict2nc2d(path.join(datadir, outname),
          data_mod, dimx='lon', dimy='lat', mode='w')
