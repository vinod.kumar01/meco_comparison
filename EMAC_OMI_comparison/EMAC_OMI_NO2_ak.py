# -*- coding: utf-8 -*-
"""
Created on Wed Jul  8 16:06:32 2020

@author: Vinod
"""

import os
import numpy as np
from datetime import datetime, timedelta, date
from pandas import date_range
import netCDF4
from scipy.interpolate import griddata
import tools
from filetools import dict2nc1d
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)

start_date = date(2015, 1, 1)
end_date = date(2015, 1, 31)
daterange = date_range(start_date, end_date)
dirname_sim = r'M:\nobackup\vinod\model_work\EMAC'
filename_sim = 'sorbit_no2_2014_QA4ECV.nc'
f_sim = netCDF4.Dataset(os.path.join(dirname_sim, filename_sim))
sim_lon = f_sim.variables['lon'][:]
sim_lat = f_sim.variables['lat'][:]
llon, llat = np.meshgrid(sim_lon, sim_lat)
no2_sim = f_sim.variables['tracer_gp_NO2_conc'][:]
no2_sim[no2_sim < 0] = np.nan
no2_sim[no2_sim > 2.5e13] = np.nan  # ~ 1000ppb
temp_sim = f_sim.variables['tm1'][:]
temp_sim[temp_sim < 0] = np.nan
temp_sim[temp_sim > 350] = np.nan
ps = f_sim.variables['aps'][:]
ps[ps < 0] = np.nan
# fill nan in ps with interpolated values
mask = np.isnan(ps)
ps[mask] = np.interp(np.flatnonzero(mask), np.flatnonzero(~mask), ps[~mask])
sim_dt = f_sim.variables['time']
date_display = [datetime.strptime(sim_dt.units[10:30], '%Y-%m-%d %H:%M:%S') +
                timedelta(days=d-1) for d in sim_dt[:]]


'''
calculation of box heights
'''
prs_i = np.array([f_sim.variables['hyai'][i] +
                  (ps[:]*f_sim.variables['hybi'][i])
                  for i in range(0, 35)])
h_interface = np.array([7.4*np.log(ps[:]/prs_i[i, :])
                        for i in range(0, 35)])*1000
h = np.array([h_interface[i, :]-h_interface[i-1, :]
              for i in range(1, 35)])
h = np.transpose(h, [1, 0, 2, 3])
h = h*temp_sim[:]/250    # correct for assumed scale height at 250K
h = h*100
h = np.nanmean(h, 0)

satdir = r"M:\nobackup\vinod\QA4ECV\NO2"
filenames = {}
varlist = {'lats':       'PRODUCT/latitude',
           'lons':       'PRODUCT/longitude',
           'CF':         'PRODUCT/SUPPORT_DATA/DETAILED_RESULTS/cloud_radiance_fraction_no2',
           'amf_geo':    'PRODUCT/SUPPORT_DATA/DETAILED_RESULTS/amf_geo',
           'SZA':        'PRODUCT/SUPPORT_DATA/GEOLOCATIONS/solar_zenith_angle',
           'TVCD':       'PRODUCT/tropospheric_no2_vertical_column',
           'TAMF':       'PRODUCT/amf_trop',
           'TOTAMF':     'PRODUCT/amf_total',
           'AK':         'PRODUCT/averaging_kernel',
           'qa':         'PRODUCT/processing_error_flag',
           'snow_flag':   'PRODUCT/SUPPORT_DATA/INPUT_DATA/snow_ice_flag',
           'tpl':         'PRODUCT/tm5_tropopause_layer_index'}

for d, single_day in enumerate(date_display):
    if single_day not in daterange:
        continue
    print('processing ' + single_day.strftime("%d/%m/%Y"))
    try:
        cur_dir = os.path.join(satdir, single_day.strftime("%Y/%m/%d"))
        lv2_filenames = tools.get_lv2_filenames_omi(cur_dir, 'NO2')
        orbits = sorted(list(lv2_filenames.keys()))
        for orbit in orbits:
            no2_sim_intp = []   # Interpolated NO2 VCD at h levels
            h_intp = []
            try:
                dct = tools.nc2dict_lv2(lv2_filenames[orbit], varlist)
                keep = (dct["CF"] <= 0.5) & (dct["qa"] == 0) & (dct["SZA"] < 80)
                keep &= (dct["snow_flag"] < 10) | (dct["snow_flag"] == 255)
                keep &= (dct["TAMF"]/dct["amf_geo"] > 0.2)
                keepdict = {key: dct[key][keep] for key in dct}
                # interpolate model VCD and model height at OMI orbit points
                for lev in range(0, 34):    # can reduce from 34 to 25 or less
                    # Interpolated NO2 VCD at individual levels
                    conc_now = griddata((llon.ravel(), llat.ravel()),
                                        no2_sim[d, lev, :].ravel(),
                                        (keepdict['lons'], keepdict['lats']),
                                        method='linear')
                    h_l = griddata((llon.ravel(), llat.ravel()),
                                   h[lev, :].ravel(),
                                   (keepdict['lons'], keepdict['lats']),
                                   method='linear')
                    no2_sim_intp.append(conc_now)
                    h_intp.append(h_l)
                no2_sim_intp = np.array(no2_sim_intp)
                h_intp = np.array(h_intp)
                # calculation of VCD
                tropvcdbox = no2_sim_intp*h_intp
                tropvcd_sim = np.sum(tropvcdbox[0:21, :], 0)
                # first estimate of simulated tropospheric VCD
                # assuming tropopause at level 21
                trop_profile_sim = np.array([tropvcdbox[i, :]/tropvcd_sim[:]
                                             for i in range(0, 34)])
                trop_amf_scale = trop_profile_sim*keepdict['AK'].T
                trop_amf_scale = np.nansum(trop_amf_scale[:21, :], 0)
                trop_amf_scale /= np.nansum(trop_profile_sim[:21, :], 0)

                trp_idx_fill = keepdict['tpl'].filled(0)
                # accurante calculation using TM5 tropopause levels
                for i in range(0, len(keepdict['lats'])):
                    tpl = trp_idx_fill[i]
                    trop_amf_scale[i] = np.nansum(trop_profile_sim[0:tpl+1, i]*keepdict['AK'].T[0:tpl+1, i])
                    trop_amf_scale[i] /= np.nansum(trop_profile_sim[0:tpl+1, i])
                    tropvcd_sim[i] = np.sum(tropvcdbox[0:tpl+1, i], 0)
                trop_amf_sim = trop_amf_scale*keepdict['TOTAMF']
                tropVCD_satcorr = keepdict['TVCD']*keepdict['TAMF']
                tropVCD_satcorr /= trop_amf_sim
                tropVCD_satcorr[keepdict['lats']>80] = np.nan
                tropVCD_satcorr[keepdict['lats']<-80] = np.nan
                savedir = {'lat': keepdict['lats'], 'lon': keepdict['lons'],
                           'AMF': keepdict['TAMF'], 'AMF_corr': trop_amf_sim,
                           'SIM_VCD': tropvcd_sim, 'SAT_VCD': keepdict['TVCD'],
                           'SAT_VCD_corr': tropVCD_satcorr}
                savename = os.path.join(dirname_sim, 'processed',
                                        single_day.strftime("%Y/%m/%d"))
                os.makedirs(savename, exist_ok=True)
                dict2nc1d(os.path.join(savename, '{}.nc'.format(orbit)),
                          savedir)

            except OSError:
                print('Corrupt file {}'.format(lv2_filenames[orbit]))

    except FileNotFoundError:
        print('no data for day {}'.format(single_day.strftime("%d/%m/%Y")))