# -*- coding: utf-8 -*-
"""
Created on Tue Jul 14 09:45:37 2020

@author: Vinod
"""

import pandas as pd
from os import path
from datetime import date
from filetools import nc2dict, dict2nc2d
from plot_tools import plt, ccrs, map_prop
from tools import iterative_mean
import warnings


def find_season(month):
    for season, months in seasons.items():
        if month in months:
            return season

# %% processing
start_date = date(2009, 1, 1)
end_date = date(2009, 12, 31)
seasons = {'DJF': [12, 1, 2], 'MAM': [3, 4, 5],
           'JJA': [6, 7, 8], 'SON': [9, 10, 11]}
daterange = pd.date_range(start_date, end_date)
exp = "lnoxPaR2"
datadir = r'M:\nobackup\vinod\model_work\EMAC\Tamara\processed'
savevars = ['SIM_VCD', 'SAT_VCD_corr', 'SAT_VCD']
meandct = {}
meandct_season = {season: {} for season in seasons.keys()}
itmeans = {key: iterative_mean() for key in savevars}
itmeans_season = {season: {key: iterative_mean() for key in savevars}
                  for season in seasons.keys()}
for single_day in daterange:
    season = find_season(single_day.month)
    print('Processing '+single_day.strftime("%Y/%m/%d"))
    datafile = path.join(datadir, exp, single_day.strftime("%Y/%m/%d"),
                         'gridded_{}.nc'.format(single_day.strftime("%Y_%m_%d")))
    try:
        data = nc2dict(datafile)
        for key in savevars:
            itmeans[key].append(data[key])
            itmeans_season[season][key].append(data[key])
    except FileNotFoundError:
        warnings.warn('Data not available for ' +
                      single_day.strftime("%Y/%m/%d"))
for key in savevars:
    meandct[key] = itmeans[key].mean_std(thresh_N=3)[0]
    for season in seasons.keys():
        meandct_season[season][key] = itmeans_season[season][key].mean_std(thresh_N=3)[0]
meandct['lat'] = data['lat']
meandct['lon'] = data['lon']
outname = 'mean_{}_{}.nc'.format(start_date.strftime("%Y"),
                                 end_date.strftime("%Y"))
dict2nc2d(path.join(datadir, exp, outname),
          meandct, dimx='lon', dimy='lat', mode='w')
for season in seasons.keys():
    meandct_season[season]['lat'] = data['lat']
    meandct_season[season]['lon'] = data['lon']
    outname = 'mean_{}.nc'.format(season)
    dict2nc2d(path.join(datadir, exp, outname),
               meandct_season[season], dimx='lon', dimy='lat', mode='w')
# %% plotting
datadir = r'M:\nobackup\vinod\model_work\EMAC\Tamara\processed'
filename = 'mean_JJA.nc'
meandct = nc2dict(path.join(datadir, exp, filename))
llon, ulon, llat, ulat = -179, 179, -85, 85
prop_dict = {'extent': [llon, ulon, llat, ulat],
             'name': 'World',
             'border_res': '110m'}
projection = ccrs.PlateCarree()
map2plot = map_prop(**prop_dict)
qt2plt = 'SIM_VCD'   # 'SIM_VCD', 'SAT_VCD_corr', 'SAT_VCD'
datadict = {'lat': meandct['lat'], 'lon': meandct['lon'],
            'plotable': 1E-15*meandct['SIM_VCD'], 'label': 'EMAC',
            'vmin': -0.1, 'vmax': 5,
            'text': 'NO$_{2}$ TROP. VCD (10 $^{15}$molec cm$^{-2}$)'}
fig, ax = plt.subplots(3, 1, subplot_kw=dict(projection=projection),
                       figsize=[10, 12], sharex=True, sharey=True)
fig.suptitle('Trop. NO$_2$ VCDs for ' + filename[5:-3])
cs1 = map2plot.plot_map(datadict, ax[0], cmap='jet',
                        mode='pcolormesh',
                        projection=projection, alpha=1)
cb = plt.colorbar(cs1, ax=ax[0], shrink=0.8, extend='max')
cb.set_label(datadict['text'])
datadict['plotable'] = 1E-15*meandct['SAT_VCD']
datadict['label'] = 'OMI (QA4ECV)'
cs2 = map2plot.plot_map(datadict, ax[1], cmap='jet',
                        mode='pcolormesh',
                        projection=projection, alpha=1)
cb = plt.colorbar(cs2, ax=ax[1], shrink=0.8, extend='max')
cb.set_label(datadict['text'])
datadict['plotable'] = 1E-15*meandct['SAT_VCD_corr']
datadict['label'] = 'OMI (Modified)'
cs3 = map2plot.plot_map(datadict, ax[2], cmap='jet',
                        mode='pcolormesh',
                        projection=projection, alpha=1)
cb = plt.colorbar(cs3, shrink=0.8, extend='max')
cb.set_label(datadict['text'])
fig.canvas.draw()
plt.tight_layout()
fig.subplots_adjust(top=0.95)
plt.savefig(path.join(datadir, exp, 'no2_comp_' +
                      filename.replace('.nc', '.png')),
            format='png', dpi=300)
