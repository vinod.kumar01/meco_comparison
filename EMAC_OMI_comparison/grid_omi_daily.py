# -*- coding: utf-8 -*-
"""
Created on Sun Jul 12 00:15:05 2020

@author: Vinod
"""

from scipy.interpolate import griddata
import numpy as np
import pandas as pd
from datetime import date
import os
from filetools import nc2dict, dict2nc2d
from scipy.spatial import cKDTree
from scipy.interpolate.interpnd import _ndim_coords_from_arrays

basedir = r'M:\nobackup\vinod\model_work\EMAC\processed'
llon, ulon, llat, ulat = -179, 179, -85, 85
res = 1/5  # grid resolution in °, corresponding to a 20 km grid in latitude
latgrid = np.arange(llat-2*res, ulat+2*res, res)
longrid = np.arange(llon-2*res, ulon+2*res, res)
xygrid = np.meshgrid(longrid, latgrid)

start_date = date(2015, 1, 1)
end_date = date(2015, 1, 31)
daterange = pd.date_range(start_date, end_date)
for single_day in daterange:
    print('Gridding ' + single_day.strftime("%d/%m/%Y"))
    filenames = {}
    root = os.path.join(basedir, single_day.strftime("%Y/%m/%d"))
    try:
        files = os.listdir(root)
    except FileNotFoundError:
        print('Data not available for ' + single_day.strftime("%Y/%m/%d"))
        continue
    if len(files) > 0:
        griddct = {'lat': latgrid, 'lon': longrid}
        savevars = {'SIM_VCD': 'SIM_VCD',
                    'SAT_VCD': 'SAT_VCD',
                    'SAT_VCD_corr': 'SAT_VCD_corr'}
#        itmeans = {key: iterative_mean() for key in savevars}
        lat, lon = [], []
        grid_vars = {}
        for key in savevars:
            grid_vars[key] = []     
        for file in files:
            if file.endswith(".nc") & ~file.startswith('gridded'):
                orbit = int(os.path.basename(file).split('.')[0])
                filenames[orbit] = os.path.join(root, file)
                inpdict = nc2dict(filenames[orbit])
                lat.extend(inpdict['lat'])
                lon.extend(inpdict['lon'])
                for key in savevars:
                    grid_vars[key].extend(inpdict[key])
        for key in savevars:
            griddct[key] = griddata((lon, lat), grid_vars[key],
                                    (xygrid[0], xygrid[1]),
                                    method="linear")
        # remove interpolated data from places
        # where original data was not present
        THRESHOLD = 0.2
        grid = [xygrid[0], xygrid[1]]
        xy = np.vstack((lon, lat)).T
        tree = cKDTree(xy)
        xi = _ndim_coords_from_arrays(tuple(grid), ndim=xy.shape[1])
        dists, indexes = tree.query(xi)
        for key in savevars:
            griddct[key][dists > THRESHOLD] = np.nan
#                    itmeans[key].append(griddata((lon, lat),
#                                        inpdict[savevars[key]],
#                                        (xygrid[0], xygrid[1]),
#                                        method="linear"))
#        for key in savevars:
#            griddct[key] = itmeans[key].mean_std(thresh_N=0)[0]
        outname = 'gridded_{}.nc'.format(single_day.strftime("%Y_%m_%d"))
        dict2nc2d(os.path.join(root, outname),
                  griddct, dimx='lon', dimy='lat', mode='w')
