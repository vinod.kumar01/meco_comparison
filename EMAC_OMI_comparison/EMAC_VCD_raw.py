# -*- coding: utf-8 -*-
"""
Created on Sun Jan 24 19:54:04 2021

@author: Vinod
"""

import netCDF4
import os
import numpy as np
from plot_tools import plt, ccrs, map_prop


def calc_vcd(EMAC_out):
    f_sim = netCDF4.Dataset(EMAC_out, 'r')
    tracer = 'NO2'
    try:
        aps = f_sim.variables['ps'][:]
    except KeyError:
        aps = f_sim.variables['g3b_aps'][:]
    tm1 = f_sim.variables['ECHAM5_tm1'][:]
    l4km = 29 if tm1.shape[1] == 31 else 36
    hyam = f_sim.variables['hyam'][:]
    prs_m = np.array([f_sim.variables['hyam'][i] +
                      (aps[:]*f_sim.variables['hybm'][i])
                      for i in range(0, len(hyam))])
    prs_m = np.transpose(prs_m, (1, 0, 2, 3))
    prs_m[prs_m < 0] = np.nan
    try:
        tracer_mr = f_sim.variables[tracer+'_ave'][:]
    except KeyError:
        tracer_mr = f_sim.variables['tracer_gp_'+tracer][:]
    h_interface = f_sim.variables['geopoti_ave'][:]/9.8
    tracer_conc = tracer_mr*6.023E23*prs_m*1E-6/8.314/tm1
    '''
    calculation of box height
    '''
    h = np.ma.masked_array([h_interface[i, :] - h_interface[i+1, :]
                            for i in range(0, len(hyam))])
    tracer_sim_vcd_box = np.ma.masked_array([tracer_conc[i, :]*h*100
                                             for i in range(tracer_conc.shape[0])])
    tracer_sim_geovcd = np.nansum(tracer_sim_vcd_box[:, l4km:len(hyam), :, :], 1)
    f_sim.close()
    return tracer_sim_geovcd


dirname = r'M:\nobackup\vinod\model_work\EMAC\Tamara'
filename_sim = 'sorbit_AURA-A_2011.nc'
EMAC_out = os.path.join(dirname, filename_sim)
EMAC_VCD = calc_vcd(EMAC_out)

# %% plotting
with netCDF4.Dataset(EMAC_out) as f:
    lon = f.variables['lon'][:]
    lat = f.variables['lat'][:]
llon, ulon, llat, ulat = -179, 179, -85, 85
prop_dict = {'extent': [llon, ulon, llat, ulat],
             'name': 'World',
             'border_res': '110m'}
projection = ccrs.PlateCarree()
map2plot = map_prop(**prop_dict)
datadict = {'lat': lat, 'lon': lon,
            'plotable': 1E-15*np.nanmean(EMAC_VCD, 0), 'label': 'EMAC',
            'vmin': -0.1, 'vmax': 5,
            'text': 'NO$_{2}$ TROP. VCD (10 $^{15}$molec cm$^{-2}$)'}
fig, ax = plt.subplots(subplot_kw=dict(projection=projection),
                       figsize=[10, 4])
fig.suptitle('Trop. NO$_2$ VCDs for 2011')
cs1 = map2plot.plot_map(datadict, ax, cmap='jet',
                        mode='pcolormesh',
                        projection=projection, alpha=1)
cb = plt.colorbar(cs1, ax=ax, shrink=0.8, extend='max')
cb.set_label(datadict['text'])