# -*- coding: utf-8 -*-
"""
Created on Thu Jul  9 10:17:33 2020

@author: Vinod
"""

import matplotlib.pyplot as plt
from os import path
import pandas as pd
from plot_tools import ccrs, map_prop
from filetools import nc2dict
from datetime import date

llon, ulon, llat, ulat = -179, 179, -85, 85
prop_dict = {'extent': [llon, ulon, llat, ulat],
             'name': 'World',
             'border_res': '110m'}
projection = ccrs.PlateCarree()
map2plot = map_prop(**prop_dict)
datadir = r'M:\nobackup\vinod\model_work\EMAC\Tamara\processed'
start_date = date(2015, 1, 2)
end_date = date(2015, 1, 2)
daterange = pd.date_range(start_date, end_date)
# %% Plotting
for single_day in daterange:
    datafile = path.join(datadir, single_day.strftime("%Y/%m/%d"),
                         'gridded_{}.nc'.format(single_day.strftime("%Y_%m_%d")))
    print('Plotting ' + single_day.strftime("%d/%m/%Y"))
    qt2plt = 'SIM_VCD'   # 'SIM_VCD', 'SAT_VCD_corr', 'SAT_VCD'
    data = nc2dict(datafile)
    datadict = {'lat': data['lat'], 'lon': data['lon'],
                'plotable': 1E-15*data[qt2plt], 'label': 'EMAC',
                'vmin': -0.1, 'vmax': 3,
                'text': 'NO$_{2}$ TROP. VCD (10 $^{15}$molec cm$^{-2}$)'}
    fig, ax = plt.subplots(3, 1, subplot_kw=dict(projection=projection),
                           figsize=[10, 12], sharex=True, sharey=True)
    fig.suptitle('Trop. NO$_2$ VCDs for ' + single_day.strftime("%d-%m-%Y"))
    cs1 = map2plot.plot_map(datadict, ax[0], cmap='jet',
                            mode='pcolormesh',
                            projection=projection, alpha=1)
    cb = plt.colorbar(cs1, ax=ax[0], shrink=0.8, extend='max')
    cb.set_label(datadict['text'])
    datadict['plotable'] = 1E-15*data['SAT_VCD']
    datadict['label'] = 'OMI (QA4ECV)'
    cs2 = map2plot.plot_map(datadict, ax[1], cmap='jet',
                            mode='pcolormesh',
                            projection=projection, alpha=1)
    cb = plt.colorbar(cs2, ax=ax[1], shrink=0.8, extend='max')
    cb.set_label(datadict['text'])
    datadict['plotable'] = 1E-15*data['SAT_VCD_corr']
    datadict['label'] = 'OMI (Modified)'
    cs3 = map2plot.plot_map(datadict, ax[2], cmap='jet',
                            mode='pcolormesh',
                            projection=projection, alpha=1)
    cb = plt.colorbar(cs3, shrink=0.8, extend='max')
    cb.set_label(datadict['text'])
    fig.canvas.draw()
    plt.tight_layout()
    fig.subplots_adjust(top=0.95)
    plt.savefig(path.join(datadir, 'plots',
                          single_day.strftime("%Y_%m_%d")+'.png'),
                format='png', dpi=300)
    plt.close()
