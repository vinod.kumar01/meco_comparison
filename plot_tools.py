# -*- coding: utf-8 -*-
"""
Created on Wed May  6 22:15:39 2020

@author: Vinod
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib import colors, dates
import cartopy.crs as ccrs
import cartopy.feature as cfeature
import cartopy.io.img_tiles as cimgt
from cartopy.feature import ShapelyFeature
from cartopy.io.shapereader import Reader
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
# from matplotlib import rcParams
# rcParams['image.interpolation'] = 'spline36'


def plot_layer(lat, lon, height, color_dimension, **kwargs):
    cmap = kwargs.get('cmap', 'jet')
    fig, ax = plt.subplots(figsize=(6, 6),
                           subplot_kw={'projection': '3d'})
    x, z = np.meshgrid(lon, height)
    Y, zg = np.meshgrid(lat, height)
    # add colors
    if color_dimension.shape[1] != lat.shape[0]:
        raise('Check color dimension shape \n x dimension mismatch')
    elif color_dimension.shape[0] != height.shape[0]:
        raise('Check color dimension shape\n z dimension mismatch')
    else:
        vmin = kwargs.get('vmin', color_dimension.min())
        vmax = kwargs.get('vmax', color_dimension.max())
        norm = colors.Normalize(vmin, vmax)
        m = plt.cm.ScalarMappable(norm=norm, cmap=cmap)
        m.set_array([])
        fcolors = m.to_rgba(color_dimension)
        ax.plot_surface(x, Y, z, facecolors=fcolors, rcount=100, ccount=100,
                        antialiased=False, vmin=vmin, vmax=vmax, shade=False)
        cbar = fig.colorbar(m, extend='max', shrink=0.5)
    return(fig, ax, cbar)


class map_prop:
    def __init__(self, **kwargs):
        self.extent = kwargs.get('extent', [0, 19, 43, 56])
        self.name = kwargs.get('name', ' ')
        self.border_res = kwargs.get('border_res', '10m')
        self.bkg = kwargs.get('bkg', ' ')

    def plot_map(self, datadict, ax, **kwargs):
        '''
        @ax: axis to plot
        @datadict['legend_text']: legend text
        @datadict[label']: Data label
        @datadict['lon']: longitude
        @datadict['lat']: latitude
        @datadict['plotable']: quantity to plot
        @datadict[vmin]: lower limit
        @datadict[vmax]: upper limit
        '''
        cmap = kwargs.get('cmap', 'jet')
        mode = kwargs.get('mode', 'pcolormesh')
        bkg_alpha = kwargs.get('bkg_alpha', 0.5)
        projection = kwargs.get('projection', ccrs.PlateCarree())
        if self.bkg == 'stamen':
            self.bkg_level = kwargs.get('bkg_level', 8)
            bkg = cimgt.Stamen('terrain-background')
            ax.add_image(bkg, self.bkg_level, alpha=bkg_alpha)
        elif self.bkg == 'OSM':
            self.bkg_level = kwargs.get('bkg_level', 8)
            bkg = cimgt.OSM()
            ax.add_image(bkg, self.bkg_level)
        elif self.bkg == 'google_maps':
            self.bkg_level = kwargs.get('bkg_level', 8)
            bkg = cimgt.GoogleTiles()
            ax.add_image(bkg, self.bkg_level, alpha=bkg_alpha,
                         interpolation='spline36')
        alpha = kwargs.get('alpha', 0.5)
        if 'vmin' not in datadict:
            vmin = np.nanpercentile(datadict['plotable'], 1)
        else:
            vmin = datadict['vmin']
        if 'vmax' not in datadict:
            vmax = np.nanpercentile(datadict['plotable'], 99)
        else:
            vmax = datadict['vmax']
        if projection == ccrs.PlateCarree():
            ax.set_extent(self.extent)
        if self.name == "In":
            In_st_shp = r'D:\python_toolbox\Igismap\Indian_States.shp'
            state_feature = ShapelyFeature(Reader(In_st_shp).geometries(),
                                           projection, edgecolor='k',
                                           alpha=0.3)
            In_boun_shp = r'D:\python_toolbox\Igismap\India_Boundary.shp'
            In_boun_feature = ShapelyFeature(Reader(In_boun_shp).geometries(),
                                             projection, edgecolor='k')
            ax.add_feature(state_feature, facecolor="none")
            ax.add_feature(In_boun_feature, facecolor="none")
        else:
            ax.coastlines(color='black', linewidth=1,
                          resolution=self.border_res)
            ax.add_feature(cfeature.BORDERS.with_scale(self.border_res),
                           linestyle='-', alpha=0.5)
        if projection == ccrs.PlateCarree():
            gl = ax.gridlines(crs=projection, draw_labels=True,
                              linewidth=1, color='gray', alpha=0.9,
                              linestyle='--')
            gl.xformatter = LONGITUDE_FORMATTER
            gl.yformatter = LATITUDE_FORMATTER
            gl.top_labels = False
            gl.right_labels = False
        else:
            ax.set_global()
            ax.gridlines()
        ax.text(0.8, 0.92, datadict['label'], transform=ax.transAxes,
                bbox=dict(facecolor='white', edgecolor='black'))
        if mode == 'scatter':
            scat_size = kwargs.get('s', 1)
            scat = ax.scatter(datadict['lon'], datadict['lat'],
                              c=datadict['plotable'], cmap=cmap, s=scat_size,
                              vmin=vmin, vmax=vmax,
                              transform=ccrs.PlateCarree(), alpha=alpha)
        elif mode == "pcolormesh":
            scat = ax.pcolormesh(datadict['lon'], datadict['lat'],
                     datadict['plotable'],
                     cmap=cmap, vmin=vmin, vmax=vmax,
                     transform=ccrs.PlateCarree(), alpha=alpha)
        elif mode == "contourf":
            scat = ax.contourf(datadict['lon'], datadict['lat'],
                     datadict['plotable'],
                     cmap=cmap, vmin=vmin, vmax=vmax,
                     transform=ccrs.PlateCarree(), alpha=alpha)
        else:
            raise("Only scatter, pcolormesh or contourf plots possible")
            # try:
            #     scat = ax.pcolormesh(datadict['lon'], datadict['lat'],
            #                          datadict['plotable'],
            #                          cmap=cmap, vmin=vmin, vmax=vmax,
            #                          transform=ccrs.PlateCarree(), alpha=alpha)
            # except (TypeError, ValueError):
            #     llons, llats = np.meshgrid(datadict['lon'], datadict['lat'])
            #     scat = ax.pcolormesh(llons, llats, datadict['plotable'],
            #                          cmap=cmap, vmin=vmin, vmax=vmax,
            #                          transform=ccrs.PlateCarree(), alpha=alpha)
    #    if idv_cbar==True:
    #        fig.colorbar(scat, cmap=plt.cm.get_cmap(cmap),
    #                     ax=ax, orientation='horizontal', extend='max',
    #                     fraction=0.1, shrink=0.8, pad=0.1)
        return(scat)


def plot_diurnal(data, date_time, ax, label='Mean', freq='H', color='r'):
    df2 = pd.DataFrame(data={'dt': date_time, 'data': data})
    df2 = df2.set_index('dt')
    df2 = df2.resample(freq).mean()
    df2['Time'] = df2.index.map(lambda x: x.strftime("%H:%M"))
    g_data = df2.groupby('Time').describe()
    g_data.index = pd.to_datetime(g_data.index.astype(str))
    dft = g_data.index.to_pydatetime()
    ax.plot(dft, g_data['data']['mean'], color, marker='o',
            linewidth=2.0, label=label)
    ax.xaxis.set_major_formatter(dates.DateFormatter('%H:%M'))
    ax.plot(dft, g_data['data']['75%'], color=color, alpha=0.6,
            label='_nolegend_')
    ax.plot(dft, g_data['data']['25%'], color=color, alpha=0.6,
            label='_nolegend_')
    ax.fill_between(dft, g_data['data']['mean'], g_data['data']['75%'],
                    alpha=.4, facecolor=color)
    ax.fill_between(dft, g_data['data']['mean'], g_data['data']['25%'],
                    alpha=.4, facecolor=color)
    ax.legend(loc='upper left')
    return(ax, g_data)


def plot_scatter_ax(**kwargs):
    figsize = kwargs.get('figsize', [7.4, 5.5])
    histx = kwargs.get('histx', False)
    histy = kwargs.get('histy', False)
    fig = plt.figure(constrained_layout=True, figsize=figsize)
    if histx & histy:
        axd = fig.subplot_mosaic([['.', 'histx'], ['histy', 'scat']],
                                 gridspec_kw={'width_ratios': [1, 7],
                                 'height_ratios': [2, 7]})
        axd['histx'].sharex(axd['scat'])
        axd['histy'].sharey(axd['scat'])
        axd['histy'].invert_xaxis()
        axd['histy'].set_xlabel('Frequency')
        axd['histx'].set_ylabel('Frequency')
    elif histx:
        axd = fig.subplot_mosaic([['histx'], ['scat']],
                                 gridspec_kw={'height_ratios': [2, 7]})
        axd['histx'].sharex(axd['scat'])
        axd['histx'].set_ylabel('Frequency')
    elif histy:
        axd = fig.subplot_mosaic([['histy', 'scat']],
                                 gridspec_kw={'width_ratios': [1, 7]})
        axd['histy'].sharey(axd['scat'])
        axd['histy'].invert_xaxis()
        axd['histy'].set_xlabel('Frequency')
    else:
        axd = fig.add_subplot()
    try:
        for ax in axd.values():
            ax.grid(alpha=0.3)
    except AttributeError:
        axd.grid(alpha=0.3)
    return(fig, axd)
