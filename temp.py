# -*- coding: utf-8 -*-
"""
Created on Sun Jan 31 00:31:53 2021

@author: Vinod
"""

from os import path
import netCDF4
import numpy as np
from scipy.interpolate import griddata
from plot_tools import plot_diurnal
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
import pandas as pd

target_lon = 8.216498
target_lat = 50.017078


exp_list = ['UBA_di', 'UBA_fl']
dirname = r'M:\nobackup\vinod\model_work\MECO\DOAS_COMP'
vcd = {i: [] for i in exp_list}
vmr = {i: [] for i in exp_list}
for exp in exp_list:
    filename_sim = exp + '_18_5_no2_1hr.nc'
    with netCDF4.Dataset(path.join(dirname, 'vcd_'+filename_sim), 'r') as f:
        lat = f.variables['lat'][:]
        lon = f.variables['lon'][:]
        no2_vcd = f.variables['NO2_tropcol_sim'][:]
        no2_vmr = f.variables['NO2_vmr_sim'][:]
        timeax = f.variables['time'][:]
        time_units = f.variables['time'].units
    for i in range(timeax.shape[0]):
        if i % 10 == 0:
            print(i)
        vcd_now = griddata((lon.ravel(), lat.ravel()),
                                no2_vcd[i, :].ravel(), (target_lon, target_lat),
                                method='linear')
        vmr_now = griddata((lon.ravel(), lat.ravel()),
                                no2_vmr[i, :].ravel(), (target_lon, target_lat),
                                method='linear')
        vcd[exp].append(vcd_now)
        vmr[exp].append(vmr_now)
    vcd[exp] = np.asarray(vcd[exp])
    vmr[exp] = np.asarray(vmr[exp])
    time_dsp = [datetime.strptime(time_units[10:],
                                  '%Y-%m-%d %H:%M:%S') + timedelta(days=float(i))
                for i in timeax]

# %% Plotting
df = pd.DataFrame(data={'date_time': time_dsp,
                        'vcd_di': vcd['UBA_di'], 'vmr_di': vmr['UBA_di'],
                        'vcd_fl': vcd['UBA_fl'], 'vmr_fl': vmr['UBA_fl']})
df['date_time'] = df['date_time'].dt.round('h')
fig, ax = plt.subplots(2, 1, figsize=[9, 8], sharex=True)
vcd_fl = plot_diurnal(df['vcd_fl'], df['date_time'], label='UBA_fl',
                    color='r', ax=ax[0])
vcd_di = plot_diurnal(df['vcd_di'], df['date_time'], label='UBA_di',
                    color='b', ax=ax[0])
vmr_fl = plot_diurnal(df['vmr_fl'], df['date_time'], label='UBA_fl',
                    color='r', ax=ax[1])
vmr_di = plot_diurnal(df['vmr_di'], df['date_time'], label='UBA_di',
                    color='b', ax=ax[1])
# plot_diurnal(df_sim[tracer.name+'_sim'], df_sim.index, label='CM02',
#              color='b', ax=ax)
# ax.text(0.65, 0.85, site+'\n'+stn_coords['station name'][site],
#         transform=ax.transAxes, size=14)
bbox_props = dict(boxstyle="round", fc="w", ec="0.5", alpha=0.9)
for axes in ax:
    axes.legend(loc='upper left', prop={'size':14})
    axes.set_ylabel('NO$_2$ VCD (molecules cm$^{-2}$)', size=14)
    axes.set_xlabel('Time (UTC)', size=14)
    axes.tick_params(axis='both', which='major', labelsize=14)
    axes.grid(alpha=0.4)
ax[1].set_ylabel('NO$_2$ surface VMR (ppb)', size=14)
ax[1].set_ylim(0, 30)
ax[0].set_xlabel('')
# ax.set_ylim(0, 70)
plt.tight_layout()
