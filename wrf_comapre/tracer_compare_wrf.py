# -*- coding: utf-8 -*-
"""
Created on Thu Oct 11 13:41:53 2018

@author: Vinod
"""

import netCDF4
import os
import numpy as np
import matplotlib.pyplot as plt
#from sklearn import linear_model
#from scipy import stats
dirname = r'M:\nobackup\vinod\model_work\MECO'
#filename_COSMO = 'COSMO_pbl_2018_04_regrid.nc'
filename_COSMO = 'tracer_meco_bot_regrid.nc'
#filename_ECMWF = 'ECMWF_pbl_2018_04_gridded.nc'
filename_WRF = 'tracer_wrf_bot_regrid.nc'
#from scipy import stats
path_COSMO = os.path.join(dirname, filename_COSMO)
path_WRF = os.path.join(dirname, filename_WRF)
f_COSMO=netCDF4.Dataset(path_COSMO,'r')
f_WRF=netCDF4.Dataset(path_WRF,'r')
lat_range = [52.75,53]   #48,48.25##   ##50,50.25##
lon_range = [10.75,11]    ##16,16.25##   ##6,6.25##
parameter = "O3"  # humidity  2m_temperature pblh
lats=f_COSMO.variables['lat'][:]
lons=f_COSMO.variables['lon'][:]
lat_idx_start,lat_idx_end = np.asscalar(np.array(np.where(lats[:]==lat_range[0]))),np.asscalar(np.array(np.where(lats[:]==lat_range[1])))
lon_idx_start,lon_idx_end = np.asscalar(np.array(np.where(lons[:]==lon_range[0]))),np.asscalar(np.array(np.where(lons[:]==lon_range[1])))
var_cosmo = parameter + "_ave"
var_WRF = parameter.lower()
data_WRF = f_WRF.variables[var_WRF][:,np.r_[lat_idx_start:lat_idx_end],np.r_[lon_idx_start:lon_idx_end]]
data_WRF = np.nanmean(np.nanmean(data_WRF,axis = 1),axis = 1)*1E-6
data_cosmo = f_COSMO.variables[var_cosmo][:,np.r_[lat_idx_start:lat_idx_end],np.r_[lon_idx_start:lon_idx_end]]
data_cosmo = np.nanmean(np.nanmean(data_cosmo,axis = 1),axis = 1)
data_cosmo = data_cosmo[0:697]   ##0:116
time = f_COSMO.variables['time'][0:697]-31
#############plotting timeseries######
fig=plt.Figure()
ax = fig.add_subplot(111)
plt.title('comparison of time series of WRF-CHEM, and MECO ' + parameter )
plt.plot(time,data_cosmo,'b.-', label = 'COSMO')
plt.plot(time,data_WRF,'r.-',label = 'WRF')
plt.legend(loc = 'upper left')
#plt.plot(VCD1_flat[idx], fit_func(VCD1_flat[idx]), color = "black", linewidth=2)
lat_obs_label = str('%.1f' % abs(lats[lat_idx_start])) + ' - ' + str('%.1f' % abs(lats[lat_idx_end])) + (u' \N{DEGREE SIGN}''N' if lats[lat_idx_start] > 0 else u' \N{DEGREE SIGN}''S')
lon_obs_label = str('%.1f' % abs(lons[lon_idx_start])) + ' - ' + str('%.1f' % abs(lons[lon_idx_end])) + (u' \N{DEGREE SIGN}''E' if lons[lon_idx_start] > 0 else u' \N{DEGREE SIGN}''W')
plt.text(0.6,0.9,lat_obs_label + ', \n' + lon_obs_label , transform=ax.transAxes)
plt.xlabel("Days since 01 Apr. 2015", fontsize=10)
plt.ylabel(parameter + " (" +  f_COSMO.variables[var_cosmo].units + ")", fontsize=10)
###########plotting map########