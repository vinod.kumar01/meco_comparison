# -*- coding: utf-8 -*-
"""
Created on Mon Oct  8 22:47:09 2018

@author: Vinod
"""

import netCDF4
import os
import numpy as np
import matplotlib.pyplot as plt
#from sklearn import linear_model
#from scipy import stats
dirname = r'M:\nobackup\vinod\model_work\MECO'
#filename_COSMO = 'COSMO_pbl_2018_04_regrid.nc'
filename_COSMO = 'COSMO_met_2015_04_bot_regrid.nc'
#filename_ECMWF = 'ECMWF_pbl_2018_04_gridded.nc'
filename_WRF = 'temp_wrf_regrid_6h.nc'
filename_ECMWF = 'met_03-04_2015_6h_regrid.nc'
path_ECMWF = os.path.join(dirname, filename_ECMWF)
#from scipy import stats
path_COSMO = os.path.join(dirname, filename_COSMO)
path_WRF = os.path.join(dirname, filename_WRF)
f_COSMO=netCDF4.Dataset(path_COSMO,'r')
f_WRF=netCDF4.Dataset(path_WRF,'r')
f_ECMWF=netCDF4.Dataset(path_ECMWF,'r')
lat_range = [52,52.25]   #48,48.25##   ##50,50.25##
lon_range = [6,6.25]    ##16,16.25##   ##6,6.25##
parameter = "pblh"  # humidity  2m_temperature pblh
lats=f_COSMO.variables['lat'][:]
lons=f_COSMO.variables['lon'][:]
lat_idx_start,lat_idx_end = np.asscalar(np.array(np.where(lats[:]==lat_range[0]))),np.asscalar(np.array(np.where(lats[:]==lat_range[1])))
lon_idx_start,lon_idx_end = np.asscalar(np.array(np.where(lons[:]==lon_range[0]))),np.asscalar(np.array(np.where(lons[:]==lon_range[1])))
if parameter == "temperature" :
    var_cosmo='T_2M_ave'
    var_WRF='T2'
    var_ecmwf='t2m'
elif parameter == "humidity" :
    var_cosmo='qm1_ave'
    var_WRF='QVAPOR'
    var_ecmwf='q_fc'
elif parameter == "pblh" :
    var_cosmo='pblh_ave'
    var_WRF='PBLH'
    var_ecmwf='blh'
#var_cosmo='pblh_ave'
#var_ecmwf='blh'
data_WRF = f_WRF.variables[var_WRF][:,np.r_[lat_idx_start:lat_idx_end],np.r_[lon_idx_start:lon_idx_end]]
data_WRF = np.nanmean(np.nanmean(data_WRF,axis = 1),axis = 1)
data_WRF = data_WRF[0:116]
data_cosmo = f_COSMO.variables[var_cosmo][:,np.r_[lat_idx_start:lat_idx_end],np.r_[lon_idx_start:lon_idx_end]]
data_cosmo = np.nanmean(np.nanmean(data_cosmo,axis = 1),axis = 1)
data_cosmo = data_cosmo[0:116]   ##0:116
data_ecmwf = f_ECMWF.variables[var_ecmwf][:,np.r_[lat_idx_start:lat_idx_end],np.r_[lon_idx_start:lon_idx_end]]
data_ecmwf = np.nanmean(np.nanmean(data_ecmwf,axis = 1),axis = 1)
data_ecmwf = data_ecmwf[124:240]
#############plotting######
fig=plt.Figure()
ax = fig.add_subplot(111)
plt.title('comparison of time series of WRF, ECMWF and COSMO ' + var_WRF )
plt.plot(np.arange(0,29,0.25),data_cosmo,'b.-', label = 'COSMO')
plt.plot(np.arange(0,29,0.25),data_WRF,'r.-',label = 'WRF')
plt.plot(np.arange(0,29,0.25),data_ecmwf,'g.-',label = 'ECMWF')
plt.legend(loc = 'upper left')
#plt.plot(VCD1_flat[idx], fit_func(VCD1_flat[idx]), color = "black", linewidth=2)
lat_obs_label = str('%.1f' % abs(lats[lat_idx_start])) + ' - ' + str('%.1f' % abs(lats[lat_idx_end])) + (u' \N{DEGREE SIGN}''N' if lats[lat_idx_start] > 0 else u' \N{DEGREE SIGN}''S')
lon_obs_label = str('%.1f' % abs(lons[lon_idx_start])) + ' - ' + str('%.1f' % abs(lons[lon_idx_end])) + (u' \N{DEGREE SIGN}''E' if lons[lon_idx_start] > 0 else u' \N{DEGREE SIGN}''W')
plt.text(0.6,0.9,lat_obs_label + ', \n' + lon_obs_label , transform=ax.transAxes)
plt.xlabel("Days since 01 Apr. 2015", fontsize=10)
plt.ylabel(parameter + " (" +  f_COSMO.variables[var_cosmo].units + ")", fontsize=10)
