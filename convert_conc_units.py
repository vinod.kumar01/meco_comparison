# -*- coding: utf-8 -*-
"""
Created on Tue Apr 17 12:19:32 2018

@author: Vinod
"""

import netCDF4
#import matplotlib.pyplot as plt 
f_sim = netCDF4.Dataset('M:\\home\\vinod\\model_work\\co_mm_2011_2015.nc' , 'a')
CO_concn = f_sim.createVariable("CO_conc","f4",("time","lev","lat","lon",), fill_value=-999)
CO_concn.units = "molecules cm-3"
CO_concn.long_name = 'CO concentration'
f_sim.variables['CO_conc'][:] = f_sim.variables['CO_ave'][:]*6.023E23*f_sim.variables['grmass_ave'][:]*1E-6/0.02897/f_sim.variables['grvol_ave'][:]
f_sim.close()