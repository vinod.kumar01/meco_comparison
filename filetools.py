# -*- coding: utf-8 -*-
"""
Created on Mon May  4 18:49:48 2020

@author: Vinod
"""

import numpy as np
import h5py
import netCDF4


def dict2hdf5(filename, dic):
    """
    ....
    """
    with h5py.File(filename, 'w') as h5file:
        recursive_dict2hdf5(h5file, '/', dic)


def recursive_dict2hdf5(h5file, path, dic):
    """
    ....
    """
    for key, item in dic.items():
        if not isinstance(key, str):
            key = str(key)
        if isinstance(item, (np.ndarray, np.int64, np.float64, str, bytes)):
            h5file[path + key] = item
        elif isinstance(item, list):
            h5file[path + key] = np.array(item)
        elif isinstance(item, dict):
            recursive_dict2hdf5(h5file, path + key + '/',
                                item)
        else:
            raise ValueError('Cannot save %s type' % type(item))


def nc2dict(fname):
    # convert normal nc files to dictionary
    data = netCDF4.Dataset(fname, "r")
    dct = {key: data[key][:] for key in data.variables.keys()}
    data.close()
    return(dct)


def hdf2dict(filename):
    """
    ....
    """
    with h5py.File(filename, 'r') as h5file:
        return recursive_hdf2dict(h5file, '/')


def recursive_hdf2dict(h5file, path):
    """
    ....
    """
    ans = {}
    for key, item in h5file[path].items():
        if isinstance(item, h5py._hl.dataset.Dataset):
            ans[key] = item[()]
        elif isinstance(item, h5py._hl.group.Group):
            ans[key] = recursive_hdf2dict(h5file, path + key + '/')
    return ans


def dict2nc1d(fname, dict_out, mode='w'):
    with netCDF4.Dataset(fname, mode) as nc:
        nc.createDimension('dim', len(next(iter(dict_out.values()))))
        for key in dict_out:
            try:
                x = nc.createVariable(key, 'f4', 'dim')
                x[:] = dict_out[key]
            except:
                print(key, "not written to output")


def dict2nc2d(fname, dict_out, dimx='lons', dimy='lats', mode='w'):
    with netCDF4.Dataset(fname, mode) as nc:
        nc.createDimension(dimx, len(dict_out[dimx]))
        nc.createDimension(dimy, len(dict_out[dimy]))
        for key in dict_out:
            if key == dimx:
                x = nc.createVariable(dimx, 'f4', dimx)
                x.units = 'degreeE'
                x[:] = dict_out[key]
            elif key == dimy:
                y = nc.createVariable(dimy, 'f4', dimy)
                y.units = 'degreeN'
                y[:] = dict_out[key]
            else:
                try:
                    d = nc.createVariable(key, 'f4', (dimy, dimx))
                    d[:] = dict_out[key]
                except:
                    print(key, "not written to output")


def dict2nc3d(fname, dict_out, dimz='time', dimx='lons', dimy='lats',
              mode='w'):
    with netCDF4.Dataset(fname, mode) as nc:
        # for working with CDO, order is important (time/lev/lat/lon)
        if dimz == 'time':
            nc.createDimension(dimz, None)
        else:
            nc.createDimension(dimz, len(dict_out[dimz]))
        nc.createDimension(dimy, len(dict_out[dimy]))
        nc.createDimension(dimx, len(dict_out[dimx]))
        for key in dict_out:
            if key == dimz:
                z = nc.createVariable(dimz, 'f4', dimz)
                z[:] = dict_out[key]
            elif key == dimy:
                y = nc.createVariable(dimy, 'f4', dimy)
                y.units = 'degreeN'
                y[:] = dict_out[key]
            elif key == dimx:
                x = nc.createVariable(dimx, 'f4', dimx)
                x.units = 'degreeE'
                x[:] = dict_out[key]
            else:
                if len(dict_out[key].shape) == 2:
                    d = nc.createVariable(key, 'f4', (dimy, dimx))
                    d[:] = dict_out[key]
                else:
                    try:
                        d = nc.createVariable(key, 'f4', (dimz, dimy, dimx))
                        d[:] = dict_out[key]
                    except:
                        print(key, "not written to output")
