# -*- coding: utf-8 -*-
"""
Created on Sat Aug 10 16:22:17 2019

@author: Vinod
"""

# %%read data
import os
import numpy as np
dirname = r'M:\nobackup\vinod\model_work\MECO\china'
inp_file = 'mobile_model.txt'
inp = np.genfromtxt(os.path.join(dirname, inp_file))
# %%separate data
months = list(set(inp[:,1]))
days = list(set(inp[:,2]))

for month in months:
    for day in days:
        out_name = 'chn_'+str(2018)+str(int(month)).zfill(2)
        out_name += str(int(day)).zfill(2) + '.pos'
        idx = (inp[:,1] == month) &  (inp[:,2] == day)
        if np.sum(idx) > 0:
            out = inp[idx, :]
            np.savetxt(os.path.join(dirname, out_name), out, delimiter='\t',
                       fmt='%d\t%02d\t%02d\t%02d\t%02d\t%02d\t%f\t%f\t%d')
        