# -*- coding: utf-8 -*-
"""
Created on Sat Jun 22 11:19:45 2019

@author: Vinod
"""

import netCDF4
import os
import glob
import numpy as np
import matplotlib.pyplot as plt
year=2018
month=5
day=8
dirname_sat = r'M:\nobackup\cborger\TROPOMI\v1\L2_RPRO\NO2'
f_names_sat = glob.glob(os.path.join(dirname_sat, str(year), str(month).zfill(2),
                                     str(day).zfill(2), 'S5P_????_L2__NO2____'+str(year)+'*.nc'))

lat_bound = [51., 52.]
lon_bound = [7., 8.]
path_sat=f_names_sat[6]
f=netCDF4.Dataset(path_sat,'r')
Data_fields=f.groups['PRODUCT']
lats=Data_fields.variables['latitude'][0,:]
lons=Data_fields.variables['longitude'][0,:]
amf_trop = Data_fields.variables['air_mass_factor_troposphere'][0,:]
keep = (lats>lat_bound[0]) & (lats<lat_bound[1]) & (lons>lon_bound[0]) & (lons<lon_bound[1])
amf_trop = amf_trop[keep==True]
averaging_kernel = Data_fields.variables['averaging_kernel'][0,:]
fv = Data_fields.variables['averaging_kernel']._FillValue
ps = Data_fields.groups['SUPPORT_DATA'].groups['INPUT_DATA'].variables['surface_pressure'][0,:]
ps = ps[keep==True]
ak = []
for i in np.arange(0,34,1):
    ak.append(averaging_kernel[:,:,i][keep==True])
averaging_kernel = np.array(ak)
averaging_kernel = np.ma.masked_where(averaging_kernel==fv,averaging_kernel)

hya = np.mean(Data_fields.variables['tm5_constant_a'][:],axis=1)
hyb = np.mean(Data_fields.variables['tm5_constant_b'][:],axis=1)
press = hya+np.ma.mean(ps)*hyb

fig,ax=plt.subplots()
ax.scatter(np.ma.mean(averaging_kernel,axis=1),press/100)
ax.set_ylim(200,1003)
ax.set_xlim(-0.05,2.8)
plt.gca().invert_yaxis()
ax.set_ylabel('Pressure (hpa)')
ax.set_xlabel('Averaging kernel')
ax.text(0.05,0.9, '7-8 $^\circ$E, 51-52 $^\circ$N', transform=ax.transAxes)
