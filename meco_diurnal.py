# -*- coding: utf-8 -*-
"""
Created on Wed Jul 17 13:44:36 2019

@author: Vinod
"""

import netCDF4
import os
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from matplotlib import dates as d
basepath = r'M:\nobackup\vinod\model_work\MECO\emis_height'
file1 = 'nox_45dm.nc'
file2 = 'nox_10dm.nc'
idx_loc = [96,63]
f1 = netCDF4.Dataset(os.path.join(basepath, file1))
f2 = netCDF4.Dataset(os.path.join(basepath, file2))
timestamp = f1.variables['time']
date_display=[datetime.strptime(timestamp.units[10:30],'%Y-%m-%d %H:%M:%S') +
              timedelta(days=dt) for dt in timestamp[:]]
no2_1 = f1.variables['NO2_ave'][:]
no2_2 = f2.variables['NO2_ave'][:]
df = pd.DataFrame(np.array([date_display, 1E9*no2_1[:,39,idx_loc[0],idx_loc[1]], 1E9*no2_2[:,39,idx_loc[0],idx_loc[1]]]).T, columns=['Date_time', 'NO2_sfc_1', 'NO2_sfc_2'])
df['Date_time'] = pd.to_datetime(df['Date_time'])
df['NO2_sfc_1'] = pd.to_numeric(df['NO2_sfc_1'])
df['NO2_sfc_2'] = pd.to_numeric(df['NO2_sfc_2'])
df = df.set_index('Date_time')
# sort data according to hours
df['Time'] = df.index.map(lambda x: x.strftime("%H:%M"))
df2 = df.copy()
df2 = df2.groupby('Time').describe()
df2.index = pd.to_datetime(df2.index.astype(str))
dft = df2.index.to_pydatetime()

fig, ax = plt.subplots(1, figsize=(12,6))
ax.set_title('Diurnal Profile', fontsize=14)
ax.set_ylabel('NO$_{2}$ Concentration (ppb)', fontsize=14, weight='bold')
ax.set_xlabel('Time of Day (UTC)', fontsize=14)
ax.plot(dft, df2["NO2_sfc_1"]['mean'], 'k', linewidth=2.0, label = 'emis_45')
ax.plot(dft, df2["NO2_sfc_2"]['mean'], 'r', linewidth=2.0, label = 'emis_10')
ticks = ax.get_xticks()
ax.set_xticks(np.linspace(ticks[0], d.date2num(d.num2date(ticks[-1]) + timedelta(hours=3)), 5))
ax.set_xticks(np.linspace(ticks[0], d.date2num(d.num2date(ticks[-1]) + timedelta(hours=3)), 25), minor=True)
ax.xaxis.set_major_formatter(d.DateFormatter('%I:%M %p'))
ax.plot(dft, df2["NO2_sfc_1"]['75%'], color='k', label='_nolegend_')
ax.plot(dft, df2["NO2_sfc_1"]['25%'], color='k', label='_nolegend_')
ax.fill_between(dft, df2["NO2_sfc_1"]['mean'], df2["NO2_sfc_1"]['75%'], alpha=.5, facecolor='k')
ax.fill_between(dft, df2["NO2_sfc_1"]['mean'], df2["NO2_sfc_1"]['25%'], alpha=.5, facecolor='k')
ax.plot(dft, df2["NO2_sfc_2"]['75%'], color='r', label='_nolegend_')
ax.plot(dft, df2["NO2_sfc_2"]['25%'], color='r', label='_nolegend_')
ax.fill_between(dft, df2["NO2_sfc_2"]['mean'], df2["NO2_sfc_2"]['75%'], alpha=.5, facecolor='r')
ax.fill_between(dft, df2["NO2_sfc_2"]['mean'], df2["NO2_sfc_2"]['25%'], alpha=.5, facecolor='r')
ax.legend(loc='upper right')
ax.text(0.05, 0.9,'lon '+ str('%.1f' % f1.variables['geolon_ave'][idx_loc[0],idx_loc[1]]) +
        ', lat ' + str('%.1f' % f1.variables['geolat_ave'][idx_loc[0],idx_loc[1]]),
        transform=ax.transAxes)
