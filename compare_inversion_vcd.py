# -*- coding: utf-8 -*-
"""
Created on Mon Nov 11 10:23:04 2019

@author: Vinod
"""

# %% imports
import netCDF4
import os
import numpy as np
import matplotlib.pyplot as plt
from datetime import datetime, timedelta, date
from scipy.interpolate import interp1d
from date_converter import doy_2_datetime
#import scipy.io as sio

# %% model data
dirname_sim = r'M:\nobackup\vinod\model_work\MECO\DOAS_COMP'
filename_sim = 'SCOUT___201805_MNZ_new.nc'
# SCOUT___201504_MNZ.nc  # scout_mnz_2015_04_03.nc
f_sim = netCDF4.Dataset(os.path.join(dirname_sim, filename_sim), 'r')
tracer = 'NO2'
timeax = f_sim.variables['time'][:]
time_dsp = [datetime.strptime(f_sim.variables['time'].units[10:30],
                              '%Y-%m-%d %H:%M:%S') + timedelta(days=i)
            for i in timeax]
try:
    aps = f_sim.variables['COSMO_ps_ave'][:]
    tracer_sim_conc = f_sim.variables['tracer_gp_' + tracer+'_conc'][:]
except KeyError:
    aps = f_sim.variables['aps'][:]
    tracer_sim_conc = f_sim.variables[tracer + '_ave_conc'][:]
'''
calculation of box height
'''
try:
    h_interface = f_sim.variables['COSMO_ORI_HHL_ave'][:]
    h = np.array([h_interface[:, i] - h_interface[:, i+1]
                  for i in range(0, 50)])
    h = h.T

except KeyError:
    prs_i = np.array([f_sim.variables['dhyai_ave'][i] +
                      (aps[:]*f_sim.variables['dhybi_ave'][i])
                      for i in range(0, 51)])
    prs_i[prs_i < 0] = np.nan
    h_interface = np.array([7.4*np.log(101325/prs_i[i, :])
                            for i in range(0, 51)])*1000   # in meters
    h = np.array([h_interface[i, :] - h_interface[i+1, :]
                  for i in range(0, 50)])
    h = h.transpose()*f_sim.variables['COSMO_tm1_ave'][:, :]/253
    # correct for assumed scale height at 250K
# except KeyError:
#     h = f_sim.variables['grvol_ave'][:]/f_sim.variables['gboxarea_ave'][:]
#############################
tracer_sim_vcd_box = tracer_sim_conc[:, :]*h*100  # (molecules/cm2)
if len(h[0, :]) == 60:
    tracer_sim_geovcd = np.nansum(tracer_sim_vcd_box[:, 43:60], 1)
elif len(h[0, :]) == 40:
    tracer_sim_geovcd = np.nansum(tracer_sim_vcd_box[:, 24:40], 1)
elif len(h[0, :]) == 50:
    tracer_sim_geovcd = np.nansum(tracer_sim_vcd_box[:, 26:50], 1)
# fig=plt.Figure()
# ax = fig.add_subplot(111)
# plt.title('Time series of ' + tracer + ' VCD (upto 1 km)')
# plt.plot(time_dsp,tracer_sim_geovcd,'b.-', label = 'COSMO')
# plt.ylabel(tracer + " VCD (molecules cm$^{-2}$)", fontsize=10)
# plt.minorticks_on()
# plt.gcf().autofmt_xdate()
# plt.legend(loc = 'upper left')
# %% doas data
telescope = 'T3'
dirname_meas = r'M:\nobackup\vinod\model_work\MECO\DOAS_COMP\inversion_results'
vcd_meas = np.genfromtxt(os.path.join(dirname_meas, 'vcd_'+telescope+'.txt'))
date_time_meas = np.genfromtxt(os.path.join(dirname_meas, 'frac_day_'+telescope+'.txt'))
date_time_meas = np.array([doy_2_datetime(dt, 2018) for dt in date_time_meas])

'''
plt.scatter(time_dsp_meas,geo_vcd,c='red', s=4, label = 'DOAS-'+telescope)
plt.plot(time_dsp,tracer_sim_geovcd,'b.-', label = 'COSMO')
plt.ylim([0,2E16])
plt.xlim([datetime.date(2015, 4, 1), datetime.date(2015, 4, 24)])
plt.ylabel(tracer + " geometric VCD (molecules cm$^{-2}$)", fontsize=10)
plt.minorticks_on()
plt.gcf().autofmt_xdate()
plt.legend(loc = 'upper left')


'''
# %% plotting
fig, ax = plt.subplots(figsize=[12,6])
ax.scatter(date_time_meas,vcd_meas,c='red', s=4, label = 'DOAS-Inversion')
ax.plot(time_dsp,tracer_sim_geovcd,'b.-', label = 'COSMO')
# ax.scatter(time_dsp_meas,dscd_err.values,c='red', s=4, label = 'DOAS-'+telescope)

ax.set_ylim([0, 3E16])
ax.set_xlim([date(2018, 5, 1), date(2018, 6, 1)])
ax.set_ylabel(tracer + " geometric VCD (molecules cm$^{-2}$)", fontsize=10)
plt.minorticks_on()
#plt.gcf().autofmt_xdate()
ax.legend(loc='upper left')
plt.tight_layout()
#plt.savefig(os.path.join(dirname_meas, tracer, telescope+'_ts_comp.png'), format='png', dpi=300)
#'''
