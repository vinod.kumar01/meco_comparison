# -*- coding: utf-8 -*-
"""
Created on Mon Nov 11 21:40:04 2019

@author: Vinod
"""

import numpy as np
raw_data = np.genfromtxt(r'M:\nobackup\mrazi\For_Vinod_model\Pos_file_2017.txt', skip_header=0)
raw_data = raw_data[1:, :]
year = 2017
for month in [5,6]:
    for day in range(31):
        cond = (raw_data[:, 1]==month) & (raw_data[:, 2]==day)
        data_sel = raw_data[cond, :]
        if(len(data_sel)>0):
            np.savetxt(r'M:\nobackup\vinod\model_work\MECO\pak\pos\2017\pak_'+
                       str(year)+str(month).zfill(2)+str(day).zfill(2)+'.pos',data_sel,
                       fmt='%4d\t%02d\t%02d\t%02d\t%02d\t%02d\t%1.4f\t%1.4f\t%4d')