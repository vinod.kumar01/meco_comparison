# -*- coding: utf-8 -*-
"""
Created on Tue Nov 27 10:17:44 2018

@author: Vinod
"""
import netCDF4
import matplotlib.pyplot as plt 
from matplotlib import cm
from mpl_toolkits.basemap import Basemap
import numpy as np
f =netCDF4.Dataset(r'D:\postdoc\COSMO-CCLM\namelist_setup\domain2018112616018.nc', 'r')
lon = f.variables['lon'][:]
lat = f.variables['lat'][:]
param = f.variables['HSURF']
V0 = param[:]
fig=plt.Figure()
plt.title(param.standard_name)
ax = fig.add_subplot(1, 1, 1)
map = Basemap(projection='cyl', resolution = 'l', llcrnrlon=np.floor(np.min(f.variables['lon'][:])), llcrnrlat=np.floor(np.min(f.variables['lat'][:])),urcrnrlon=np.ceil(np.max(f.variables['lon'][:])), urcrnrlat=np.ceil(np.max(f.variables['lat'][:])))
map.drawcoastlines()
#map.drawcountries(linewidth=1)
#map.fillcontinents(color = 'coral')
map.drawmapboundary()
map.drawparallels(np.arange(np.floor(np.min(f.variables['lat'][:])),np.ceil(np.max(f.variables['lat'][:])),8.),labels=[1,0,0,0])
map.drawmeridians(np.arange(np.floor(np.min(f.variables['lon'][:])),np.ceil(np.max(f.variables['lon'][:])),8.),labels=[0,0,0,1])
x,y = map(lon, lat)
cs = map.pcolormesh(x, y, V0, cmap=cm.terrain)
cb=map.colorbar(extend = 'both')
cb.set_label(param.standard_name + " (" + param.units + ")"  , fontsize=10)
plt.savefig(r'D:\postdoc\COSMO-CCLM\namelist_setup\domain2018112616018'+'.png', format = 'png', dpi = 200)
plt.show()
#plt.close()