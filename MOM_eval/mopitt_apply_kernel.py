# -*- coding: utf-8 -*-
"""
Created on Wed Apr 18 09:26:07 2018

@author: Vinod
"""
# %% imports and definitions
import netCDF4
import os
import numpy as np
import matplotlib.pyplot as plt
from datetime import datetime, timedelta
import cartopy.crs as ccrs
import calendar
import sys
sys.path.append('..')
from plot_tools import map_prop
# %% load data and VCD calculation
dirname = r'M:\nobackup\vinod\model_work\MOM'
savedir = r'M:\nobackup\vinod\model_work\MOM\plots'
filename_sim = 'MOM_new_co_nox_ave2010_pli_regrid.nc'
filename_sat = 'MOPITT_co_2010mm.nc'
path_sim = os.path.join(dirname, filename_sim)
path_sat = os.path.join(dirname, filename_sat)
f_sim = netCDF4.Dataset(path_sim, 'r')
f_sat = netCDF4.Dataset(path_sat, 'r')
lats = f_sim.variables['YDim'][:]
lons = f_sim.variables['XDim'][:]
vlevs = f_sim.variables['Prs'][:]
timestamp = f_sim.variables['time']
co_vmr = f_sim.variables['CO_ave'][:]*1E9
ak = f_sim.variables['TotalColumnAveragingKernelDay'][:]
# it is dimension time,ydim,xdim
fv_sim = f_sim.variables['TotalColumnAveragingKernelDay']._FillValue
ak[ak == fv_sim] = np.nan
ap_totcol = f_sim.variables['APrioriCOTotalColumnDay'][:]
ap_vmr = f_sim.variables['APrioriCOMixingRatioProfileDay'][:]
ak2 = ak[:, 1:10, :, :]
co_box_vcd = ak2*(np.ma.log(co_vmr)-np.ma.log(ap_vmr))
#CO_vcd_sim = np.nansum(co_box_vcd,1)
CO_vcd_sim = ap_totcol + np.nansum(co_box_vcd, 1)
#CO_vcd_sim = np.empty((60,180,360)) * np.nan
#for i in range(0,60):
#    for j in range(0,180):
#        for k in range(0,360):
#            CO_vcd_sim[i,j,k]=np.dot(ak2[i,:,j,k],np.log(co_vmr[i,:,j,k])-np.log(ap_vmr[i,:,j,k]))        
#CO_vcd_sim = ap_totcol + CO_vcd_sim
CO_vcd_sat = f_sat.variables['RetrievedCOTotalColumnDay'][:]
# it has dimension time,xdim,ydim
CO_vcd_sat = np.transpose(CO_vcd_sat, (0, 2, 1))
# convert to time,ydim,xdim
fv_sat = f_sat.variables['RetrievedCOTotalColumnDay']._FillValue
CO_vcd_sat[CO_vcd_sat == fv_sat] = np.nan
# %% plotting
'''
for one panel figure
'''
#EMAC
prop_dict = {'extent': [-180, 180, -90, 90],
             'name': ' ',
             'border_res': '110m',
             'bkg': None}
map2plot = map_prop(**prop_dict)
projection = ccrs.Robinson()
for years in np.arange(2010, 2017, 1):
    if years != 2010:
        continue
    fig, ax = plt.subplots(4, 3, figsize=(16, 10),
                           subplot_kw=dict(projection=projection))
#    plt.suptitle('EMAC (' + str(years)+')')
    for i in range(12):
        row = int(i/3)
        col = int(i % 3)
        v0 = CO_vcd_sim[(years-2010)*12+i, :, :]
        date_display = datetime.strptime(timestamp.units[10:30],
                                         '%Y-%m-%d %H:%M:%S')
        date_display += timedelta(days=timestamp[:][(years-2010)*12+i])
        datadict = {'lat': lats, 'lon': lons, 'plotable': v0*1e-18,
                    'vmin': 0, 'vmax': 5.5, 'label': '',
                    'text': 'Average CO vertical column (10$^{18}$ molec. cm$^{-2}$)'}
        scat = map2plot.plot_map(datadict, ax[row, col], cmap='jet',
                                 projection=projection, alpha=0.8)
        ax[row, col].annotate(calendar.month_abbr[i+1] + ' ' + str(years),
                              xy=(0.01, 0.01), xycoords='axes fraction')
    fig.canvas.draw()
    plt.tight_layout()
    fig.subplots_adjust(right=0.90, wspace=0.02)
    cbar_ax = fig.add_axes([0.92, 0.15, 0.02, 0.7])
    cbar = fig.colorbar(scat, cax=cbar_ax, extend='max')
    cbar.set_label(datadict['text'], fontsize=12)
    plt.savefig(os.path.join(savedir, 'EMAC_CO_' + str(years) + '.png'),
                format='png', dpi=300)
    plt.close()
# MOPITT
    fig, ax = plt.subplots(4, 3, figsize=(16, 10),
                           subplot_kw=dict(projection=projection))
#    plt.suptitle('MOPITT ('+ str(years)+')')
    for i in range(12):
        row = int(i/3)
        col = int(i % 3)
        v0 = CO_vcd_sat[(years-2010)*12+i, :, :]
        date_display = datetime.strptime(timestamp.units[10:30],
                                         '%Y-%m-%d %H:%M:%S')
        date_display += timedelta(days=timestamp[:][(years-2010)*12+i])
        datadict = {'lat': lats, 'lon': lons, 'plotable': v0*1e-18,
                    'vmin': 0, 'vmax': 5.5, 'label': '',
                    'text': 'Average CO vertical column (10$^{18}$ molec. cm$^{-2}$)'}
        scat = map2plot.plot_map(datadict, ax[row, col], cmap='jet',
                                 projection=projection, alpha=0.8)
        ax[row, col].annotate(calendar.month_abbr[i+1] + ' ' + str(years),
                              xy=(0.01, 0.01), xycoords='axes fraction')
    fig.canvas.draw()
    plt.tight_layout()
    fig.subplots_adjust(right=0.90, wspace=0.02)
    cbar_ax = fig.add_axes([0.92, 0.15, 0.02, 0.7])
    cbar = fig.colorbar(scat, cax=cbar_ax, extend='max')
    cbar.set_label(datadict['text'], fontsize=12)
    plt.savefig(os.path.join(savedir, 'MOPITT_CO_' + str(years) + '.png'),
                format='png', dpi=300)
    plt.close()
# %% Seasonal plot
prop_dict = {'extent': [-180, 180, -90, 90],
             'name': ' ',
             'border_res': '110m',
             'bkg': None}
map2plot = map_prop(**prop_dict)
projection = ccrs.Robinson()
seasons = {'MAM': [2, 3, 4], 'JJA': [5, 6, 7], 'SON': [8, 9, 10],
           'DJF': [11, 0, 1]}
len_time_idx = CO_vcd_sim.shape[0]
fig, ax = plt.subplots(3, 4, figsize=(16, 6),
                       subplot_kw=dict(projection=projection))
for s, (season, month) in enumerate(seasons.items()):
    sel_idx = [i % 12 in month for i in np.arange(len_time_idx)]
    v0 = np.nanmean(CO_vcd_sim[sel_idx, :, :], 0)
    datadict = {'lat': lats, 'lon': lons, 'plotable': v0*1e-18,
                'vmin': 0, 'vmax': 5.5, 'label': '',
                'text': 'CO VCD\n(10$^{18}$ molec. cm$^{-2}$)'}
    scat1 = map2plot.plot_map(datadict, ax[0, s], cmap='jet',
                              projection=projection, alpha=0.8)
    ax[0, s].set_title(season, loc='center', y=1.1)
    v1 = np.nanmean(CO_vcd_sat[sel_idx, :, :], 0)
    datadict.update({'plotable': v1*1e-18})
    scat2 = map2plot.plot_map(datadict, ax[1, s], cmap='jet',
                              projection=projection, alpha=0.8)
    datadict.update({'plotable': (v0-v1)*1e-18, 'vmin': -2.2, 'vmax': 2.2})
    scat3 = map2plot.plot_map(datadict, ax[2, s], cmap='RdBu_r',
                              projection=projection, alpha=0.8)
fig.canvas.draw()
plt.tight_layout()
fig.subplots_adjust(right=0.9, wspace=0.02)
cax1 = fig.add_axes([0.92, 0.65, 0.01, 0.25])
cax2 = fig.add_axes([0.92, 0.34, 0.01, 0.25])
cax3 = fig.add_axes([0.92, 0.03, 0.01, 0.25])
cbar1 = fig.colorbar(scat1, cax=cax1, extend='max')
cbar2 = fig.colorbar(scat2, cax=cax2, extend='max')
cbar3 = fig.colorbar(scat3, cax=cax3, extend='both')
cbar1.set_label('EMAC '+datadict['text'], fontsize=10)
cbar2.set_label('MOPITT ' + datadict['text'], fontsize=10)
cbar3.set_label('EMAC-MOPITT ' + datadict['text'], fontsize=10)
plt.savefig(os.path.join(savedir, 'Seasonal_CO_vcd.png'),
            format='png', dpi=300)
