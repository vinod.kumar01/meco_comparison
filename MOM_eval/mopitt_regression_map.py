# -*- coding: utf-8 -*-
"""
Created on Mon May  7 22:25:23 2018
script to get a map of liner regression parameters of
monthly average model and mopitt vcds
@author: Vinod
"""
# %% imports and functions
import netCDF4
import os
import numpy as np
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import sys
sys.path.append('..')
from tools import lin_fit
from plot_tools import map_prop

# %% load data
dirname = r'M:\nobackup\vinod\model_work\MOM'
savedir = r'M:\nobackup\vinod\model_work\MOM\plots'
filename_sim = 'co_merge_mm_pli_regrid.nc'
filename_sat = 'MOPITT_co_2010mm.nc'
path_sim = os.path.join(dirname, filename_sim)
path_sat = os.path.join(dirname, filename_sat)
f_sim = netCDF4.Dataset(path_sim, 'r')
f_sat = netCDF4.Dataset(path_sat, 'r')
lats = f_sim.variables['YDim'][:]
lons = f_sim.variables['XDim'][:]
vlevs = f_sim.variables['Prs'][:]
timestamp = f_sim.variables['time']
co_vmr = f_sim.variables['CO_ave'][:]*1E9
ak = f_sim.variables['TotalColumnAveragingKernelDay'][:]
# it is dimension time,ydim,xdim
fv_sim = f_sim.variables['TotalColumnAveragingKernelDay']._FillValue
ak[ak == fv_sim] = np.nan
ap_totcol = f_sim.variables['APrioriCOTotalColumnDay'][:]
ap_vmr = f_sim.variables['APrioriCOMixingRatioProfileDay'][:]
ak2 = ak[:, 1:10, :, :]
co_box_vcd = ak2*(np.log(co_vmr)-np.log(ap_vmr))
# CO_vcd_sim = np.nansum(co_box_vcd, 1)
CO_vcd_sim = ap_totcol + np.nansum(co_box_vcd, 1)
#CO_vcd_sim = np.empty((60,180,360)) * np.nan
#for i in range(0,60):
#    for j in range(0,180):
#        for k in range(0,360):
#            CO_vcd_sim[i,j,k]=np.dot(ak2[i,:,j,k],np.log(co_vmr[i,:,j,k])-np.log(ap_vmr[i,:,j,k]))        
#CO_vcd_sim = ap_totcol + CO_vcd_sim
CO_vcd_sat = f_sat.variables['RetrievedCOTotalColumnDay'][:]
# it is dimension time,xdim,ydim
CO_vcd_sat = np.transpose(CO_vcd_sat, (0, 2, 1))  # convert to time,ydim,xdim
fv_sat = f_sat.variables['RetrievedCOTotalColumnDay']._FillValue
# %% calcualte statistics
slopes = np.empty((len(CO_vcd_sim[1, :, 1]), len(CO_vcd_sim[1, 1, :])))*np.nan
gofs, rmse, bias = slopes.copy(), slopes.copy(), slopes.copy()
for lat_idx in range(0, len(CO_vcd_sim[1, :, 1])):
# for lat_index in range(50,51):    
    for lon_idx in range(0, len(CO_vcd_sim[1, 1, :])):
        sat_vcd = CO_vcd_sat[:, lat_idx, lon_idx].filled(np.nan)
        sim_vcd = CO_vcd_sim[:, lat_idx, lon_idx].filled(np.nan)
        mask = np.isnan(sat_vcd) | np.isnan(sim_vcd)
        if len(sat_vcd[~mask]) > 2:
            fit_stats = lin_fit(sat_vcd[~mask],
                                sim_vcd[~mask])
            fit_param = fit_stats.stats_calc()
            slopes[lat_idx, lon_idx] = fit_param['m']
            gofs[lat_idx, lon_idx] = fit_param['r']**2
            rmse[lat_idx, lon_idx] = 100*fit_param['rmse']/np.ma.mean(sat_vcd[~mask])
            bias[lat_idx, lon_idx] = 100*fit_param['bias']/np.ma.mean(sat_vcd[~mask])
rmse[np.isinf(rmse)] = np.nan

# %% polotting
prop_dict = {'extent': [-180, 180, -90, 90],
             'name': ' ',
             'border_res': '110m',
             'bkg': None}
map2plot = map_prop(**prop_dict)
projection = ccrs.Robinson()
# GOF
fig, ax = plt.subplots(subplot_kw=dict(projection=projection))
#plt.title('Gridwise (r$^{2}$) between EMAC and MOPITT CO (2010)')
datadict = {'lat': lats, 'lon': lons, 'plotable': gofs,
            'vmin': 0, 'vmax': 1, 'label': '',
            'text': 'Goodness of fit (r$^{2}$)'}
scat = map2plot.plot_map(datadict, ax, cmap='jet',
                         projection=projection, alpha=0.8)
cb = plt.colorbar(scat, extend='max', fraction=0.1, shrink=0.5)
cb.set_label(datadict['text'], fontsize=10)
plt.tight_layout()
plt.savefig(os.path.join(savedir, 'gof_CO_2010.png'), format='png', dpi=300)

# Percentage bias
fig, ax = plt.subplots(subplot_kw=dict(projection=projection))
#plt.title('Gridwise (r$^{2}$) between EMAC and MOPITT CO (2010)')
datadict = {'lat': lats, 'lon': lons, 'plotable': bias,
            'vmin': -40, 'vmax': 40, 'label': '',
            'text': 'Relative bias (%)'}
scat = map2plot.plot_map(datadict, ax, cmap='RdBu_r',
                         projection=projection, alpha=0.9)
cb = plt.colorbar(scat, extend='max', fraction=0.1, shrink=0.5)
cb.set_label(datadict['text'], fontsize=10)
fig.canvas.draw()
plt.tight_layout()
# plt.savefig(os.path.join(savedir, 'bias_CO_2010.png'), format='png', dpi=300)
    
# %%
fig, ax = plt.subplots(3, 1, figsize=[6, 10],
                       subplot_kw=dict(projection=projection))
# EMAC
datadict.update({'plotable': 1e-18*np.ma.mean(CO_vcd_sim, 0),
                 'vmin': 0, 'vmax': 5.5})
scat0 = map2plot.plot_map(datadict, ax[0], cmap='jet',
                          projection=projection, alpha=0.8)
cb0 = fig.colorbar(scat0, ax=ax[0], orientation='horizontal', pad=0.02,
                   extend='max', fraction=0.05, shrink=0.8)
cb0.set_label('EMAC CO VCD (10$^{18}$ molecules cm$^{-2}$)', fontsize=12,
              labelpad=0.01)

datadict.update({'plotable': 1e-18*np.ma.mean(CO_vcd_sat, 0),
                 'vmin': 0, 'vmax': 5.5})
scat1 = map2plot.plot_map(datadict, ax[1], cmap='jet',
                          projection=projection, alpha=0.8)
cb1 = fig.colorbar(scat1, ax=ax[1], orientation='horizontal', pad=0.02,
                   extend='max', fraction=0.05, shrink=0.8)
cb1.set_label('MOPITT CO VCD (10$^{18}$ molecules cm$^{-2}$)', fontsize=12,
              labelpad=0.01)

datadict.update({'plotable': 1e-18*(np.ma.mean(CO_vcd_sim, 0)-np.ma.mean(CO_vcd_sat, 0)),
                 'vmin': -2.2, 'vmax': 2.2})
scat2 = map2plot.plot_map(datadict, ax[2], cmap='RdBu_r',
                          projection=projection, alpha=0.9)
cb2 = fig.colorbar(scat2, ax=ax[2], orientation='horizontal', pad=0.02,
                   extend='both', fraction=0.05, shrink=0.8)
cb2.set_label('Bias (EMAC-MOPITT, 10$^{18}$ molecules cm$^{-2}$)', fontsize=12,
              labelpad=0.01)
fig.canvas.draw()
plt.tight_layout()
plt.savefig(os.path.join(savedir, 'summary_CO_2010.png'),
            format='png', dpi=300)
