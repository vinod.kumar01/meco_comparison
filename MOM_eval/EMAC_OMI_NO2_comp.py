# -*- coding: utf-8 -*-
"""
Created on Fri Mar 15 16:05:30 2019

@author: Vinod
"""
# %% imports and functions
from netCDF4 import Dataset
import os
import numpy as np
import matplotlib.pyplot as plt
from datetime import datetime, timedelta
from scipy import stats
import cartopy.crs as ccrs
import calendar
import sys
sys.path.append('..')
from shiftgrid_bm import shiftgrid
from plot_tools import map_prop
from tools import lin_fit

# %% load data
dirname = r'M:\home\vinod\nobackup\model_work\MOM'
f_sim = r'M:\nobackup\vinod\model_work\MOM\co_no2_merge_mm.nc'
filename_sat = 'OMI_QA4ECV_NO2_2010_16_mm_regird.nc'
savedir = r'M:\nobackup\vinod\model_work\MOM\plots'
path_sim = os.path.join(dirname, f_sim)
path_sat = os.path.join(dirname, filename_sat)
f_sim = Dataset(path_sim, 'r')
f_sat = Dataset(path_sat, 'r')

lats = f_sim.variables['lat'][:]
lons = f_sim.variables['lon'][:]
timestamp = f_sim.variables['time']
num_mnts = 12

# %% calculate VCD

'''
calculate layer heights
'''
aps = f_sim.variables['aps_ave'][:]
#aps.mask = np.ma.nomask
prs_i = np.ma.MaskedArray([f_sim.variables['hyai'][i]+(aps[:]*f_sim.variables['hybi'][i])
                           for i in range(0, 32)])
prs_i = np.ma.masked_where(prs_i<0, prs_i)
#prs_i[prs_i<0]=np.nan
h_interface = np.ma.MaskedArray([7.4*np.log(aps[:]/prs_i[i, :])
                                 for i in range(0, 32)])*1000   # in meters
h = np.ma.MaskedArray([h_interface[i-1, :]-h_interface[i, :]
                       for i in range(1, 32)])
h = h.swapaxes(0, 1)
h *= f_sim.variables['tm1_ave'][:]/250  # correct for scale height at 250K
#h = np.array([f_sim.variables['grvol'][d,i,:,:]/f_sim.variables['gboxarea'][d,:,:] for i in range(0,34)])
h *= 100
#h[h<=0]=np.nan
'''
Calculate subcolumn and tropospheric VCD
'''
NO2_conc = f_sim.variables['NO2_conc'][:]
#NO2_conc[(NO2_conc<=1E7) | (NO2_conc>=1E12)]=np.nan
tropvcdbox = NO2_conc[:num_mnts, :, :]*h[:num_mnts, :, :]  #without averaging kernels
tropvcd_sim = np.nansum(tropvcdbox[:, 9:, :, :], 1)
tropvcd_sat = f_sat.variables['NO2_VCD'][:num_mnts]*1E13

for i in np.arange(0, num_mnts):
    tropvcd_sim[i, :, :], lon_sim = shiftgrid(180., tropvcd_sim[i, :, :],
                                              lons, start=False)
    tropvcd_sat[i, :, :], lon_sat = shiftgrid(180., tropvcd_sat[i, :, :],
                                              lons, start=False)

# %% plotting
'''
for one panel figure
'''
#EMAC
prop_dict = {'extent': [-180, 180, -90, 90],
             'name': ' ',
             'border_res': '110m',
             'bkg': None}
map2plot = map_prop(**prop_dict)
projection = ccrs.Robinson()
for years in np.arange(2010, 2017, 1):
    if years != 2010:
        continue
    fig, ax = plt.subplots(4, 3, figsize=(16, 10),
                           subplot_kw=dict(projection=projection))
#    plt.suptitle('EMAC (' + str(years)+')')
    for i in range(12):
        row = int(i/3)
        col = int(i % 3)
        date_display = datetime.strptime(timestamp.units[10:30],
                                         '%Y-%m-%d %H:%M:%S')
        date_display += timedelta(days=timestamp[:][(years-2010)*12+i])
        v0 = tropvcd_sim[(years-2010)*12+i, :, :]
        datadict = {'lat': lats, 'lon': lon_sim, 'plotable': v0*1e-16,
                    'vmin': -0.05, 'vmax': 1, 'label': '',
                    'text': 'Average NO$_{2}$ trop. VCD (10$^{16}$ molecules cm$^{-2}$)'}
        scat = map2plot.plot_map(datadict, ax[row, col], cmap='jet',
                                 projection=projection, alpha=0.8)
        ax[row, col].annotate(calendar.month_abbr[i+1] + ' ' + str(years),
                              xy=(0.01, 0.01), xycoords='axes fraction')
    fig.canvas.draw()
    plt.tight_layout()
    fig.subplots_adjust(right=0.90, wspace=0.02)
    cbar_ax = fig.add_axes([0.92, 0.15, 0.02, 0.7])
    cbar = fig.colorbar(scat, cax=cbar_ax, extend='max')
    cbar.set_label(datadict['text'], fontsize=12)
    plt.savefig(os.path.join(savedir, 'EMAC_NO2_' + str(years) + '.png'),
                format='png', dpi=300)
    plt.close()

    fig, ax = plt.subplots(4, 3, figsize=(16, 10),
                           subplot_kw=dict(projection=projection))
#    plt.suptitle('EMAC (' + str(years)+')')
    for i in range(12):
        row = int(i/3)
        col = int(i % 3)
        date_display = datetime.strptime(timestamp.units[10:30],
                                         '%Y-%m-%d %H:%M:%S')
        date_display += timedelta(days=timestamp[:][(years-2010)*12+i])
        v0 = tropvcd_sat[(years-2010)*12+i, :, :]
        datadict = {'lat': lats, 'lon': lon_sat, 'plotable': v0*1e-16,
                    'vmin': -0.05, 'vmax': 1, 'label': '',
                    'text': 'Average NO$_{2}$ trop. VCD (10$^{16}$ molecules cm$^{-2}$)'}
        scat = map2plot.plot_map(datadict, ax[row, col], cmap='jet',
                                 projection=projection, alpha=0.8)
        ax[row, col].annotate(calendar.month_abbr[i+1] + ' ' + str(years),
                              xy=(0.01, 0.01), xycoords='axes fraction')
    fig.canvas.draw()
    plt.tight_layout()
    fig.subplots_adjust(right=0.90, wspace=0.02)
    cbar_ax = fig.add_axes([0.92, 0.15, 0.02, 0.7])
    cbar = fig.colorbar(scat, cax=cbar_ax, extend='max')
    cbar.set_label(datadict['text'], fontsize=12)
    plt.savefig(os.path.join(savedir, 'OMI QA4ECV_NO2_' + str(years) + '.png'),
                format='png', dpi=300)
    plt.close()
# %% calculate statistics
'''
Regression map
'''
slopes = np.empty((len(tropvcd_sim[1, :, 1]), len(tropvcd_sim[1, 1, :])))*np.nan
gofs, rmse, bias = slopes.copy(), slopes.copy(), slopes.copy()

for lat_idx in range(len(tropvcd_sim[1, :, 1])):
#for lat_index in range(50,51):    
    for lon_idx in range(len(tropvcd_sim[1, 1, :])):
        sat_vcd = tropvcd_sat[:, lat_idx, lon_idx].filled(np.nan)
        sim_vcd = tropvcd_sim[:, lat_idx, lon_idx].filled(np.nan)
        mask = np.isnan(sat_vcd) | np.isnan(sim_vcd)
        if len(sat_vcd[~mask]) > 2:
            fit_stats = lin_fit(sat_vcd[~mask],
                                sim_vcd[~mask])
            fit_param = fit_stats.stats_calc()
            slopes[lat_idx, lon_idx] = fit_param['m']
            gofs[lat_idx, lon_idx] = fit_param['r']**2
            rmse[lat_idx, lon_idx] = 100*fit_param['rmse']/np.ma.mean(sat_vcd[~mask])
            bias[lat_idx, lon_idx] = 100*fit_param['bias']/np.ma.mean(sat_vcd[~mask])
rmse[np.isinf(rmse)] = np.nan
# %% plotting
prop_dict = {'extent': [-180, 180, -90, 90],
             'name': ' ',
             'border_res': '110m',
             'bkg': None}
map2plot = map_prop(**prop_dict)
projection = ccrs.Robinson()
# GOF
fig, ax = plt.subplots(subplot_kw=dict(projection=projection))
#plt.title('Gridwise (r$^{2}$) between EMAC and MOPITT CO (2010)')
datadict = {'lat': lats, 'lon': lon_sim, 'plotable': gofs,
            'vmin': 0, 'vmax': 1, 'label': '',
            'text': 'Goodness of fit (r$^{2}$)'}
scat = map2plot.plot_map(datadict, ax, cmap='jet',
                         projection=projection, alpha=0.8)
cb = plt.colorbar(scat, extend='max', fraction=0.1, shrink=0.5)
cb.set_label(datadict['text'], fontsize=10)
plt.tight_layout()
plt.savefig(os.path.join(savedir, 'gof_no2_2010.png'), format='png', dpi=300)

# Percentage bias
fig, ax = plt.subplots(subplot_kw=dict(projection=projection))
#plt.title('Gridwise (r$^{2}$) between EMAC and MOPITT CO (2010)')
datadict = {'lat': lats, 'lon': lon_sim, 'plotable': bias,
            'vmin': -100, 'vmax': 100, 'label': '',
            'text': 'Relative bias (%)'}
scat = map2plot.plot_map(datadict, ax, cmap='RdBu_r',
                         projection=projection, alpha=0.9)
cb = plt.colorbar(scat, extend='max', fraction=0.1, shrink=0.5)
cb.set_label(datadict['text'], fontsize=10)
plt.tight_layout()
plt.savefig(os.path.join(savedir, 'bias_no2_2010.png'), format='png', dpi=300)
    
# %%
fig, ax = plt.subplots(1, 3, figsize=[12, 3.5],
                       subplot_kw=dict(projection=projection))
# EMAC
datadict.update({'plotable': np.ma.mean(tropvcd_sim, 0),
                 'vmin': -5e14, 'vmax': 1e16})
scat0 = map2plot.plot_map(datadict, ax[0], cmap='jet',
                          projection=projection, alpha=0.8)
cb0 = fig.colorbar(scat0, cmap='jet', ax=ax[0], orientation='horizontal',
                   extend='max', fraction=0.1, shrink=0.8)
cb0.set_label('EMAC NO$_2$ VCD (molecules cm$^{-2}$)', fontsize=10)

datadict.update({'plotable': np.ma.mean(tropvcd_sat, 0),
                 'vmin': -5e14, 'vmax': 1e16})
scat1 = map2plot.plot_map(datadict, ax[1], cmap='jet',
                          projection=projection, alpha=0.8)
cb1 = fig.colorbar(scat1, cmap='jet', ax=ax[1], orientation='horizontal',
                   extend='max', fraction=0.1, shrink=0.8)
cb1.set_label('OMI NO$_2$ VCD (molecules cm$^{-2}$)', fontsize=10)

datadict.update({'plotable': bias,
                 'vmin': -100, 'vmax': 100})
scat2 = map2plot.plot_map(datadict, ax[2], cmap='RdBu_r',
                          projection=projection, alpha=0.9)
cb2 = fig.colorbar(scat2, cmap='RdBu_r', ax=ax[2], orientation='horizontal',
                   extend='both', fraction=0.1, shrink=0.8)
cb2.set_label('Relative Bias (%)', fontsize=10)
plt.tight_layout()
plt.savefig(os.path.join(savedir, 'summary_no2_2010.png'),
            format='png', dpi=300)
