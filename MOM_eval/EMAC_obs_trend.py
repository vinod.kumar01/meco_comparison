# -*- coding: utf-8 -*-
"""
Created on Mon Mar 26 17:31:38 2018

@author: Vinod
"""
# %% definitions and imports
import os
from netCDF4 import Dataset, chartostring
import matplotlib.pyplot as plt
import numpy as np
from datetime import datetime, date
from scipy.interpolate import interp2d
from dateutil.relativedelta import relativedelta
import matplotlib.dates as mdates
import sys
sys.path.append('..')
from shiftgrid_bm import shiftgrid
from plot_tools import map_prop, ccrs


# %%load data
f_obs = Dataset(r'M:\nobackup\vinod\model_work\MOM\co_obs_mm.nc', 'r')
f_sim = Dataset(r'M:\nobackup\vinod\model_work\MOM\MOM_new_co_nox_ave2010_mm.nc', 'r')
savepath = r'M:\nobackup\vinod\model_work\MOM\plots'
var_obs = 'CO'
var_sim = 'CO_ave'
num_mnts = 12
data_sim = f_sim.variables[var_sim][:, -1, :, :]
lat_sim = f_sim.variables['lat'][:]
timestamp_obs = f_obs.variables['time']
for time in range(0, len(data_sim[:, 1, 1])):
    lon_sim = f_sim.variables['lon'][:]
    data_sim[time, :, :], lon_sim = shiftgrid(180., data_sim[time, :, :],
                                              lon_sim, start=False)
start_obs = datetime.strptime(timestamp_obs.units[12:31], '%Y-%m-%d %H:%M:%S')
date_display_obs = np.array([start_obs + relativedelta(months=+int(i))
                             for i in timestamp_obs])
date_display_obs = date_display_obs[372:372+num_mnts]
# 372 corresponds to jan 2010
date_display_obs = [d.date() for d in date_display_obs]
date_display_obs = [d.replace(year=2012, day = 15) for d in date_display_obs]
timestamp_sim = f_sim.variables['time']
start_sim = datetime.strptime(timestamp_sim.units[10:30], '%Y-%m-%d %H:%M:%S')
date_display_sim = np.array([start_sim + relativedelta(days=+int(j))
                             for j in timestamp_sim])
date_display_sim = date_display_sim[0:12]
date_display_sim = [d.date() for d in date_display_sim]
date_display_sim = [d.replace(year=2012, day = 15) for d in date_display_sim]
fn_sim = {}
for t in range(0, len(data_sim[:, 0, 0])):
    fn_sim[t] = interp2d(lat_sim, lon_sim, data_sim[t, :].T,
                         bounds_error=False, fill_value=None)

# %% processing and plotting for each station
lat_stat = {'obs_lat': [], 'obs_lon': [], 'obs_avg': [], 'obs_sd': [],
            'sim_avg': [], 'sim_sd': [], 'bias': [], 'bias_per': []}
for x in range(len(f_obs.variables['stations'])):
# for x in range(9, 11): 
        v_obs = f_obs.variables[var_obs][:, x]
        v_obs = v_obs[372:372+num_mnts].filled(np.nan)
        v_sim = []
        for t in range(len(data_sim[:, 0, 0])):
            v_sim.append(fn_sim[t](f_obs.variables['lat'][x],
                         f_obs.variables['lon'][x]))
        v_sim = np.array(v_sim[:num_mnts])
        lat_stat['obs_lat'].append(f_obs.variables['lat'][x])
        lat_stat['obs_lon'].append(f_obs.variables['lon'][x])
        # averaging over month
        x_obs, y_obs, std_obs, idx_obs = [], [], [], []
        x_sim, y_sim, std_sim, idx_sim = [], [], [], []
        for day_num, d in enumerate([date(2012, m, 15) for m in range(1, 13)]):
            j = [j for j,x in enumerate(date_display_obs) if x == d]
            idx_obs.append(j)
            if len(idx_obs[day_num]) == 0:
                y_obs.append(np.nan)
                std_obs.append(np.nan)
            else:
                y_obs.append(np.nanmean([v_obs[idx_obs[day_num]]]))
                std_obs.append(np.nanstd([v_obs[idx_obs[day_num]]]))
            x_obs.append(d)
            j = [j for j,x in enumerate(date_display_sim) if x == d]
            idx_sim.append(j)
            if len(idx_sim[day_num]) == 0:
                y_sim.append(np.nan)
                std_sim.append(np.nan)
            else:
                y_sim.append(np.nanmean([v_sim[idx_sim[day_num]]]))
                std_sim.append(np.nanstd([v_sim[idx_sim[day_num]]]))
            x_sim.append(d)
        y_obs, std_obs = np.array(y_obs), np.array(std_obs)
        #y_obs[y_obs==-999], std_obs[std_obs==-999]=np.nan, np.nan
        obs_avg_str = ' -- ' if np.isnan(np.nanmean(v_obs)) else str('%.1f' %(np.nanmean(v_obs)))
        lat_stat['obs_avg'].append(np.nan if np.isnan(np.nanmean(v_obs)) else np.nanmean(v_obs))
        lat_stat['obs_sd'].append(np.nan if np.isnan(np.nanmean(v_obs)) else np.nanstd(v_obs))
        y_sim, std_sim = np.array(y_sim), np.array(std_sim)
        #y_sim[y_obs==-999], std_sim[std_sim==-999]=np.nan, np.nan
        sim_avg_str = ' -- ' if np.isnan(np.nanmean(v_sim)) else str('%.1f' %(np.nanmean(1.0E+9*v_sim))) 
        lat_stat['sim_avg'].append(np.nan if np.isnan(np.nanmean(v_sim)) else np.nanmean(1.0E+9*v_sim))
        lat_stat['sim_sd'].append(np.nan if np.isnan(np.nanmean(v_sim)) else np.nanstd(1.0E+9*v_sim))
        lat_stat['bias'].append(np.nan if np.isnan(np.nanmean(v_obs)) else np.nanmean(1.0E+9*v_sim)-np.nanmean(v_obs))
        lat_stat['bias_per'].append(np.nan if np.isnan(np.nanmean(v_obs)) else 100*(np.nanmean(1.0E+9*v_sim)-np.nanmean(v_obs))/np.nanmean(v_obs))
        #
        #date_display = np.array([start + datetime.timedelta(days=i) for i in timestamp])
        #print(date_display)
        plt.ioff()
        fig, ax = plt.subplots()
        ax.plot(np.array(x_obs), y_obs, 'b.-',
                label='Obs : ' + obs_avg_str + ' ppb')
        ax.fill_between(np.array(x_obs), y_obs-std_obs, y_obs+std_obs,
                        color='blue', alpha=0.5)
        ax.plot(np.array(x_sim), 1.0E+9*y_sim, 'r.-',
                label='Model : '+ sim_avg_str + ' ppb')
        ax.fill_between(np.array(x_obs), 1.0E+9*(y_sim-std_sim),
                        1.0E+9*(y_sim+std_sim), color='red', alpha=0.5)
        ax.legend(loc = 'upper right')
        lat_obs_label = str('%.1f' % abs(f_obs.variables['lat'][x]))
        lat_obs_label += (u' \N{DEGREE SIGN}''N'
                          if f_obs.variables['lat'][x] > 0
                          else u' \N{DEGREE SIGN}''S')
        lon_obs_label = str('%.1f' % abs(f_obs.variables['lon'][x]))
        lon_obs_label += (u' \N{DEGREE SIGN}''E'
                          if f_obs.variables['lon'][x] > 0
                          else u' \N{DEGREE SIGN}''W')
        ax.text(0.05, 0.9, str(chartostring(f_obs.variables['name'][x],
                                            encoding='Latin-1')) + '\n' +
                lat_obs_label + ', ' + lon_obs_label + ', ' +
                str('%.1f' % f_obs.variables['alt'][x]) + ' m',
                transform=ax.transAxes)
        #ax.text(0.05,0.85,'lat = '+ str(lat_sim[sim_lat_idx]) +
        #        ', lon = '+ str(lon_sim[sim_lon_idx]-180) + 
        #       ' (model)', transform=ax.transAxes)
        ax.set_xlabel('Month')
        ax.set_ylabel('CO (ppb)')
        bottom, top = ax.get_ylim()
        ax.set_ylim(bottom, top*1.1)
        ax.set_xlim([date(2012, 1, 1), date(2013, 1, 1)])
        ax.xaxis.set_major_formatter(mdates.DateFormatter('%b'))
        #plt.minorticks_on()
        ax.xaxis.set_major_locator(plt.MaxNLocator(13))
        # plt.savefig(os.path.join(savepath, 'stn'+str(x)+'.png'),
        #             format='png', dpi=100)
        plt.close()
        plt.ion()
        print(x, f_obs.variables['lat'][x],f_obs.variables['lon'][x])
f_obs.close()
f_sim.close()
for key in lat_stat.keys():
    lat_stat[key] = np.asarray(lat_stat[key])
# %% overview plots
fig, ax = plt.subplots(figsize=[10, 6])
lat_stat['sim_avg'] = np.ma.masked_where(np.isnan(lat_stat['obs_avg']),
                                         lat_stat['sim_avg'])
lat_stat['obs_avg'] = np.ma.masked_invalid(lat_stat['obs_avg'])
ax.errorbar(lat_stat['obs_lat'], lat_stat['obs_avg'], yerr=lat_stat['obs_sd'],
            fmt='bo', label='Obs', alpha=0.6)
ax.errorbar(lat_stat['obs_lat'], lat_stat['sim_avg'], yerr=lat_stat['sim_sd'],
            fmt='ro', label='Model', alpha=0.6)
ax.legend(loc='upper left', prop={'size': 14})
ax.set_xlabel('Latitudes', size=16)
ax.set_ylabel('CO (nmol mol$^{-1}$)', size=16)
ax.tick_params(axis='both', which='major', labelsize=14)
ax.minorticks_on()
plt.gca().invert_xaxis()
plt.grid(which='major', axis='both', alpha=0.4)
plt.tight_layout()
plt.savefig(os.path.join(savepath, 'lat_trend.png'), format='png', dpi=300)
ax.set_ylim([0, 600])
plt.savefig(os.path.join(savepath, 'lat_trend_zoom.png'),
            format='png', dpi=300)
# plt.close()
#plot map of bias
prop_dict = {'extent': [-180, 180, -90, 90],
             'name': ' ',
             'border_res': '110m',
             'bkg': None}
map2plot = map_prop(**prop_dict)
projection = ccrs.Robinson()
datadict = {'lat': lat_stat['obs_lat'], 'lon': lat_stat['obs_lon'],
            'plotable': lat_stat['bias_per'],
            'vmin': -30, 'vmax': 30, 'label': '',
            'text': 'Percentage bias (EMAC - Observation)'}
fig, ax = plt.subplots(subplot_kw=dict(projection=projection))
scat = map2plot.plot_map(datadict, ax=ax, cmap='RdBu_r',
                         projection=projection, mode='scatter', s=30, alpha=1)
cb = fig.colorbar(scat, cmap='RdBu_r', ax=ax, orientation='horizontal',
                  extend='both', fraction=0.1, shrink=0.8, pad=0.05)
cb.set_label(datadict['text'], fontsize=11)
fig.canvas.draw()
plt.tight_layout()
plt.savefig(os.path.join(savepath, 'obs_bias_per.png'), format='png', dpi=300)
# %% count stations within 1 sigma

def is_overlapping(x1, x2, y1, y2):
    return max(x1, y1) <= min(x2, y2)
meas_upper = lat_stat['obs_avg']+lat_stat['obs_sd']
meas_lower = lat_stat['obs_avg']-lat_stat['obs_sd']
sim_lower = lat_stat['sim_avg']-lat_stat['sim_sd']
sim_upper = lat_stat['sim_avg']+lat_stat['sim_sd']
count = 0
for i in range(155):
    if is_overlapping(meas_lower[i],meas_upper[i],sim_lower[i], sim_upper[i]):
        count += 1
