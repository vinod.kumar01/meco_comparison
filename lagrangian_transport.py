# -*- coding: utf-8 -*-
"""
Created on Mon Dec 28 12:01:55 2020

@author: Vinod
"""
# %% imports and functions
import geopy
from geopy.distance import distance
from shapely.geometry import Polygon
from matplotlib import patches
import matplotlib.pyplot as plt
from tools import wind
import numpy as np
import netCDF4
import os


def transport_box(orig, u, v, t):
    '''
    Linearly move back te grid cell along the wind vector for a given time.
    Parameters
    ----------
    orig : dict
        four coordinates of original box {p1:(lat,lon), p2: (lat, lon)...}
    u : float
        U componet of wind vector (ms-1)
    v : float
        v componet of wind vector (ms-1)
    t : float
        allowed time for transport (s)

    Returns dict
    -------
    New box transported by the wind vector

    '''
    wv = wind(u=u, v=v)
    ws, wd = wv.merge_uv()
    # # Direction where wind is blowing
    # wd -= 180.
    # wd = wd+360 if wd < 0 else wd
    dest = orig.copy()
    for vertex in orig.keys():
        start = geopy.Point(orig[vertex])
        d = distance(kilometers=1e-3*ws*t)
        dest[vertex] = d.destination(point=start, bearing=wd)[:2]
    return dest


def correct_polygon(vertices):
    poly = Polygon(vertices)
    if not poly.is_valid:
        vertices[1], vertices[2] = vertices[2], vertices[1]  # Swap
    poly = Polygon(vertices)
    return poly


def calc_intersection(src, dest):
    '''
    Parameters
    ----------
    src : dict
        four coordinates of original box {p1:(lat,lon), p2: (lat, lon)...}
    dest : dict
        four coordinates of original box {p1:(lat,lon), p2: (lat, lon)...}

    Returns
    -------
    overlap area as a ftaction of the source rectangle

    '''
    # fast Check if any of corner point overlap
    m0 = [src[i][0] for i in src.keys()]
    m1 = [src[i][1] for i in src.keys()]
    n0 = [dest[i][0] for i in dest.keys()]
    n1 = [dest[i][1] for i in dest.keys()]
    if not [any(min(n0) <= x <= max(n0) for x in m0)][0]:
        return 0.
    elif not [any(min(n1) <= x <= max(n1) for x in m1)][0]:
        return 0.
    else:
        src_lst, dest_lst = [], []
        for vertex in src.keys():
            src_lst.append(src[vertex])
            dest_lst.append(dest[vertex])
        poly_src = correct_polygon(src_lst)
        poly_dest = correct_polygon(dest_lst)
        intersection = poly_src.intersection(poly_dest)
        return intersection.area/poly_src.area


# if __name__ == "__main__":
#     src = {'p1': (50, 8), 'p2': (50, 9), 'p3': (51, 9), 'p4': (51, 8)}
#     dest = transport_box(src, 5, 5, 3*3600)
#     print(calc_intersection(src, dest))


# %% load data
dirname = r'M:\nobackup\vinod\model_work\MECO\sorbit_subsample\2018_05\TNO_fl\qa_75'
with netCDF4.Dataset(r'D:/postdoc/COSMO-CCLM/coordinate_edges_cm07.nc') as f:
    lon_bnds = f.variables['lon_bnds'][:]
    lat_bnds = f.variables['lat_bnds'][:]
    lon = f.variables['lon'][:]
    lat = f.variables['lat'][:]
with netCDF4.Dataset(os.path.join(dirname, 'meanVCD05_merge.nc')) as f:
    wind_u = np.nanmean(f.variables['um1'][:, 1, :], 0)
    wind_v = np.nanmean(f.variables['vm1'][:, 1, :], 0)
dt = 3*3600   # NOx lifetime in summer

# %% transport individual grids
lat_bnds_tra = lat_bnds.copy()
lon_bnds_tra = lon_bnds.copy()
for i in range(180):
    for j in range(160):
        src_grid_now = {'p1': (lat_bnds[i, j, 0], lon_bnds[i, j, 0]),
                        'p2': (lat_bnds[i, j, 1], lon_bnds[i, j, 1]),
                        'p3': (lat_bnds[i, j, 2], lon_bnds[i, j, 2]),
                        'p4': (lat_bnds[i, j, 3], lon_bnds[i, j, 3])}
        dest_grid_now = transport_box(src_grid_now,
                                      wind_u[i, j], wind_v[i, j], dt)
        lat_bnds_tra[i, j, :] = [vals[0]
                                 for key, vals in dest_grid_now.items()]
        lon_bnds_tra[i, j, :] = [vals[1]
                                 for key, vals in dest_grid_now.items()]
# for i in range(len(lon_bnds)):
#     src_grid_now = {'p1': (lat_bnds[i, 0], lon_bnds[i, 0]),
#                     'p2': (lat_bnds[i, 1], lon_bnds[i, 1]),
#                     'p3': (lat_bnds[i, 2], lon_bnds[i, 2]),
#                     'p4': (lat_bnds[i, 3], lon_bnds[i, 3])}
#     src_grid.append(src_grid_now)
#     dest_grid.append(transport_box(src_grid_now, wind_u[i], wind_v[i], dt))

# %% example plot
fig, ax = plt.subplots()
for i in range(10, 25):
    for j in range(10, 25):
        llons = np.asarray([lon_bnds[i, j, 0], lon_bnds[i, j, 1],
                           lon_bnds[i, j, 2], lon_bnds[i, j, 3]])
        llats = np.asarray([lat_bnds[i, j, 0], lat_bnds[i, j, 1],
                            lat_bnds[i, j, 2], lat_bnds[i, j, 3]])
        if (i == 16) & (j == 15):
            alpha = 0.9
            linestyle = '-'
            llons2 = np.asarray([lon_bnds_tra[i, j, 0], lon_bnds_tra[i, j, 1],
                                 lon_bnds_tra[i, j, 2], lon_bnds_tra[i, j, 3]])
            llats2 = np.asarray([lat_bnds_tra[i, j, 0], lat_bnds_tra[i, j, 1],
                                 lat_bnds_tra[i, j, 2], lat_bnds_tra[i, j, 3]])
        else:
            alpha = 0.3
            linestyle = ':'
        poly = patches.Polygon(np.vstack([llons, llats]).T, facecolor='grey',
                               alpha=alpha, edgecolor='k', linestyle=linestyle)
        ax.add_patch(poly)
ax.set_xlim(3.5, 4.4)
ax.set_ylim(44.5, 45.3)
# poly2 = patches.Polygon(np.vstack([llons2, llats2]).T, facecolor='r',
#                         alpha=0.9, edgecolor='r')
# ax.add_patch(poly2)
# ax.quiver(lon[15, 15], lat[15, 15], wind_u[15, 15], wind_v[15, 15])
# plt.tight_layout()
