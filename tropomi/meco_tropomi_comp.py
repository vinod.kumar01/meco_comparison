# -*- coding: utf-8 -*-
"""
Created on Fri Aug 31 11:18:52 2018

@author: Vinod
"""

import netCDF4
import os
import numpy as np
import matplotlib.pyplot as plt
import datetime
from scipy import stats
from mpl_toolkits.basemap import Basemap
dirname = r'M:\nobackup\vinod\model_work\MECO'
dirname_sat = r'M:\nobackup\vinod\tropomi_raster\datasets'
# filename_sim = 'TNO_2018_03_s5p_sorbit_s5p_0625.nc'
filename_sim = 'TNO_2018_04_sorbit_s5p_0625.nc'
if "TNO_" in filename_sim:
    savepath = r'M:\home\vinod\nobackup\model_work\MECO\Plots\TROPOMI_comp\TNO\ '
elif "EC_" in filename_sim:
    savepath = r'M:\home\vinod\nobackup\model_work\MECO\Plots\TROPOMI_comp\EC\ '
elif "RCP_" in filename_sim:
    savepath = r'M:\home\vinod\nobackup\model_work\MECO\Plots\TROPOMI_comp\RCP\ '
filename_sat = 'S5P_OFFL_L2__NO2__201804_merge_0625.nc'
#regr = linear_model.LinearRegression()
path_sim = os.path.join(dirname, filename_sim)
path_sat = os.path.join(dirname_sat, filename_sat)
f_sim=netCDF4.Dataset(path_sim,'r')
counter = 0
timestamp = f_sim.variables['time']
f_sat=netCDF4.Dataset(path_sat,'r')
tropcol = f_sat.variables['nitrogendioxide_tropospheric_column']
fv=tropcol._FillValue
lats=f_sim.variables['lat'][:]
lons=f_sim.variables['lon'][:]
#plt.ioff()
date_display = {}
start_day = 1
end_day = 29
for day_num in range(start_day,end_day):    #1,19
    d = day_num-1
    date_display[d]=datetime.datetime.strptime(timestamp.units[10:30],'%Y-%m-%d %H:%M:%S')+ datetime.timedelta(days=timestamp[d])
    TropoPauseLevel = f_sat.variables['tm5_tropopause_layer_index'][d,:,:]
    TropoPauseLevel.mask = np.ma.nomask
    TropoPauseLevel[TropoPauseLevel>34]=0
    ak = f_sat.variables['averaging_kernel'][d,:,:,:]   #averaging kernel
    ak = np.transpose(ak, (2, 0, 1))
    trop_amf_sat=f_sat.variables['air_mass_factor_troposphere'][d,:,:]
    tot_amf_sat=f_sat.variables['air_mass_factor_total'][d,:,:]
    sat_tropcol = tropcol[d,:,:]*6.02214e+19/1E15
    tropSCD_sat=sat_tropcol*trop_amf_sat
    flag = f_sat.variables['qa_value'][d,:]
    tropSCD_sat = np.ma.masked_where(flag<0.75, tropSCD_sat)
    #tropSCD_sat[tropSCD_sat<0]=np.nan
    ###########calculate layer heights
    aps=f_sim.variables['aps'][d,:,:]
    aps.mask = np.ma.nomask
    prs_i = np.array([f_sim.variables['hyai'][i]+(aps[:]*f_sim.variables['hybi'][i]) for i in range(0,35)])
    prs_i[prs_i<0]=np.nan
    h_interface=np.array([7.4*np.log(aps[:]/prs_i[i,:]) for i in range(0,35)])*1000   #in meters
    h=np.array([h_interface[i,:]-h_interface[i-1,:] for i in range(1,35)])
    h=h*f_sim.variables['COSMO_tm1'][d,:]/250    #correct for assumed scale height at 250K
    #h = np.array([f_sim.variables['grvol'][d,i,:,:]/f_sim.variables['gboxarea'][d,:,:] for i in range(0,34)])
    h=h*100
    h[h<=0]=np.nan
    ############calculate subcolumn #######
    NO2_conc=f_sim.variables['tracer_gp_NO2_conc'][d,:,:,:]
    #NO2_conc[(NO2_conc<=1E7) | (NO2_conc>=1E12)]=np.nan
    tropvcdbox = NO2_conc[:,:,:]*h[:,:,:]  #without averaging kernels
    #tropvcdbox[(tropvcdbox>1E18) | (tropvcdbox<1E7)]=np.nan
    #################### calculate tropospheric VCD
    #tropvcd_sim = np.nansum(tropvcdbox,0)
    tropvcd_sim = np.ma.sum(tropvcdbox,0)    
    for i in range(0,len(lats)):
        for j in range(0,len(lons)):
            tpl = int(round(TropoPauseLevel[i,j]))
            TropoPauseLevel[i,j] = tpl
            #tropvcd_sim[i,j] = np.nansum(tropvcdbox[0:tpl+1,i,j])
            tropvcd_sim[i,j] = np.ma.sum(tropvcdbox[0:tpl+1,i,j])            
    #tropvcd_sim[(tropvcd_sim<=1E12) | (tropvcd_sim>=1E19)]=np.nan
    #trop_profile_sim = np.array([tropvcdbox[i,:,:]/tropvcd_sim[:,:] for i in range(0,34)])
    trop_profile_sim = np.ma.MaskedArray([np.ma.divide(tropvcdbox[i,:,:],tropvcd_sim[:,:]) for i in range(0,34)])
    trop_amf_sim=np.ma.divide(np.ma.sum(np.ma.multiply(trop_profile_sim,ak),0)*trop_amf_sat,np.ma.sum(trop_profile_sim,0))
    trop_amf_scale=np.ma.sum(trop_profile_sim*ak,0)*trop_amf_sat/np.ma.sum(trop_profile_sim,0)
    #trop_amf_sim=np.nansum(trop_profile_sim*ak,0)*trop_amf_sat/np.nansum(trop_profile_sim,0)
    #trop_amf_scale=np.nansum(trop_profile_sim*ak,0)*trop_amf_sat/np.nansum(trop_profile_sim,0)
    for i in range(0,len(lats)):
        for j in range(0,len(lons)):
            tpl = int(round(TropoPauseLevel[i,j]))
            TropoPauseLevel[i,j] = tpl
            #trop_amf_scale[i,j] = np.nansum(trop_profile_sim[0:tpl+1,i,j]*ak[0:tpl+1,i,j])/np.nansum(trop_profile_sim[0:tpl+1,i,j])
            trop_amf_scale[i,j] = np.ma.divide(np.ma.sum(trop_profile_sim[0:tpl+1,i,j]*ak[0:tpl+1,i,j]), np.ma.sum(trop_profile_sim[0:tpl+1,i,j]))            
    trop_amf_sim=trop_amf_scale*tot_amf_sat
    trop_amf_sat = np.ma.masked_where(np.ma.getmask(trop_amf_sim), trop_amf_sat)
    #trop_amf_sim[trop_amf_sim==0]=np.nan
    tropVCD_sat=np.ma.divide(tropSCD_sat,trop_amf_sim)
    #tropVCD_sat=tropSCD_sat/trop_amf_sim
    tropvcd_sim = np.ma.masked_where(flag<0.75, tropvcd_sim)
    tropVCD_sat = np.ma.masked_where(flag<0.75, tropVCD_sat)
    #tropvcd_sim[f_sat.variables['qa_value'][d,:]<0.75]=np.nan ##cloud fraction filter on model product for comparison
    #tropvcd_sim[np.where(np.isnan(tropVCD_sat)==True)]=np.nan
    #tropVCD_sat[np.where(np.isnan(tropvcd_sim)==True)]=np.nan
    sat_tropcol[np.where(np.isnan(tropvcd_sim)==True)]=np.nan
    fig=plt.figure()
    plt.title('COSMO Tropospheric NO$_{2}$ VCD for ' + date_display[d].strftime('%d-%b-%Y'))
    ax = fig.add_subplot(1, 1, 1)
    map = Basemap(projection='cyl', resolution = 'l', llcrnrlon=1, llcrnrlat=43,urcrnrlon=20, urcrnrlat=56)
    map.drawparallels(np.arange(30.,60.,4.),labels=[1,0,0,0])
    map.drawmeridians(np.arange(0.,20.,4.),labels=[0,0,0,1])
    map.drawcoastlines()
    map.drawcountries()
    map.drawmapboundary()
    llons, llats = np.meshgrid(lons, lats)
    x,y = map(llons, llats)
    cs = map.pcolormesh(x, y, tropvcd_sim*1E-15, cmap='jet', vmin = 0, vmax = 12)
    cb=map.colorbar(extend = 'both')
    cb.set_label('NO2_Trop_VCD * 10$^{15}$ molecules cm$^{-2}$' , fontsize=11)
    ############ wind vectors ###############
    um1=f_sim.variables['COSMO_um1'][d, 0, :, :]
    vm1=f_sim.variables['COSMO_vm1'][d, 0, :, :]
    ws= np.sqrt(um1*um1 + vm1*vm1)
    xx = np.arange(0, llons.shape[0], 8)
    yy = np.arange(0, llats.shape[0], 8)
    points = np.meshgrid(yy, xx)
    map.quiver(x[points],y[points],um1[points],vm1[points],ws[points],latlon=True, cmap=plt.cm.autumn)
    #######################################
    plt.savefig(savepath + 'MECO_VCD_3_'+ date_display[d].strftime('%Y%m%d') +'.png', format = 'png', dpi = 200)
    plt.close()
    #plot satellite vcd original
    fig=plt.Figure()
    plt.title('TROPOMI Tropospheric NO$_{2}$ VCD for ' + date_display[d].strftime('%d-%b-%Y'))
    ax = fig.add_subplot(1, 1, 1)
    if "_050s" in filename_sat:
        map = Basemap(projection='cyl', resolution = 'l', llcrnrlon=-60, llcrnrlat=4,urcrnrlon=90, urcrnrlat=80)
        map.drawparallels(np.arange(5.,75.,15.),labels=[1,0,0,0])
        map.drawmeridians(np.arange(-60.,90.,30.),labels=[0,0,0,1])
    else:    
        map = Basemap(projection='cyl', resolution = 'l', llcrnrlon=1, llcrnrlat=43,urcrnrlon=20, urcrnrlat=56)
        map.drawparallels(np.arange(30.,60.,4.),labels=[1,0,0,0])
        map.drawmeridians(np.arange(0.,20.,4.),labels=[0,0,0,1])
    map.drawcoastlines()
    map.drawcountries()
    map.drawmapboundary()
    x,y = map(llons, llats)
    cs = map.pcolormesh(x, y, tropVCD_sat*trop_amf_sim/trop_amf_sat, cmap='jet', vmin = 0, vmax = 12)
    #cs = map.pcolormesh(x, y, NO2_tropvcd*1E-15-V0, cmap='jet', vmin = -7, vmax = 4 )  #for bias
    cb=map.colorbar(extend = 'both')
    cb.set_label('NO2_Trop_col * 10$^{15}$ molecules cm$^{-2}$' , fontsize=10)
    plt.savefig(savepath + 's5p_VCD_3_'+ date_display[d].strftime('%Y%m%d') +'.png', format = 'png', dpi = 200)
    plt.close()
    #plot satellite vcd corrected
    fig=plt.Figure()
    plt.title('TROPOMI Tropospheric NO$_{2}$ VCD for ' + date_display[d].strftime('%d-%b-%Y') + ' (new AMF)')
    ax = fig.add_subplot(1, 1, 1)
    if "_050s" in filename_sat:
        map = Basemap(projection='cyl', resolution = 'l', llcrnrlon=-60, llcrnrlat=4,urcrnrlon=90, urcrnrlat=80)
        map.drawparallels(np.arange(5.,75.,15.),labels=[1,0,0,0])
        map.drawmeridians(np.arange(-60.,90.,30.),labels=[0,0,0,1])
    else:    
        map = Basemap(projection='cyl', resolution = 'l', llcrnrlon=1, llcrnrlat=43,urcrnrlon=20, urcrnrlat=56)
        map.drawparallels(np.arange(30.,60.,4.),labels=[1,0,0,0])
        map.drawmeridians(np.arange(0.,20.,4.),labels=[0,0,0,1])
    map.drawcoastlines()
    map.drawcountries()
    map.drawmapboundary()
    x,y = map(llons, llats)
    cs = map.pcolormesh(x, y, tropVCD_sat, cmap='jet', vmin = 0, vmax = 12)
    #cs = map.pcolormesh(x, y, NO2_tropvcd*1E-15-V0, cmap='jet', vmin = -7, vmax = 4 )  #for bias
    cb=map.colorbar(extend = 'both')
    cb.set_label('NO2_Trop_col * 10$^{15}$ molecules cm$^{-2}$' , fontsize=10)
    plt.savefig(savepath + 's5p_corr_VCD_3_'+ date_display[d].strftime('%Y%m%d') +'.png', format = 'png', dpi = 200)
    plt.close()
    ##############comparison plot of old and new airmass factors
    fig, axes = plt.subplots(nrows=2, ncols=1, figsize=[8,9])
    plt.suptitle('Comparisons of Tropospheric AMF for ' + date_display[d].strftime('%d-%b-%Y'))
    axes[0,].set_title( 'TROPOMI data product', fontsize=10)
    map = Basemap(projection='cyl', resolution = 'l', llcrnrlon=1, llcrnrlat=43,urcrnrlon=20, urcrnrlat=56, ax=axes[0])
    map.drawparallels(np.arange(30.,60.,4.),labels=[1,0,0,0])
    map.drawmeridians(np.arange(0.,20.,4.),labels=[0,0,0,1])
    map.drawcoastlines()
    map.drawcountries()
    map.drawmapboundary()
    cs1 = map.pcolormesh(x, y, trop_amf_sat, cmap='jet', vmin = 0, vmax = 2.5)
    cb1=fig.colorbar(cs1, cmap=plt.cm.get_cmap('jet'), ax=axes[0], orientation='vertical',extend = 'both', fraction = 0.1, shrink = 0.8)
            #######################
    axes[1].set_title( 'calculated usig model profile',fontsize=10)
    map = Basemap(projection='cyl', resolution = 'l', llcrnrlon=1, llcrnrlat=43,urcrnrlon=20, urcrnrlat=56, ax=axes[1])
    map.drawparallels(np.arange(30.,60.,4.),labels=[1,0,0,0])
    map.drawmeridians(np.arange(0.,20.,4.),labels=[0,0,0,1])
    map.drawcoastlines()
    map.drawcountries()
    map.drawmapboundary()
    cs2 = map.pcolormesh(x, y, trop_amf_sim, cmap='jet', vmin = 0, vmax = 2.5)
    cb2=fig.colorbar(cs2, cmap=plt.cm.get_cmap('jet'), ax=axes[1], orientation='vertical',extend = 'both', fraction = 0.1, shrink = 0.8)
    plt.savefig(savepath + 'comp_AMF_3'+ date_display[d].strftime('%Y%m%d') +'.png', format = 'png', dpi = 200)
    plt.close()
    #####################################
    #################Daily correlation##############
    #################################
    fig=plt.Figure()
    VCD_sim_flat=tropvcd_sim.flatten()*1E-15
    VCD_sat_flat=tropVCD_sat.flatten()
    idx = np.isfinite(VCD_sat_flat) & np.isfinite(VCD_sim_flat)
    #fit_param=np.ma.polyfit(VCD_sat_flat, VCD_sim_flat,1)
    fit_param = stats.mstats.linregress(VCD_sat_flat, VCD_sim_flat)[0:3]
    fit_func=np.poly1d(fit_param[0:2])
    try:
        r_sqr = fit_param[2]**2
        plt.scatter(VCD_sat_flat,VCD_sim_flat, s=2)
        plt.plot(VCD_sat_flat[idx], fit_func(VCD_sat_flat[idx]), color = "black", linewidth=2)
        plt.plot(VCD_sat_flat[idx], np.poly1d([1,0])(VCD_sat_flat[idx]), color = "red", linewidth=1, linestyle = ":")
        plt.plot(VCD_sat_flat[idx], np.poly1d([1.2,0])(VCD_sat_flat[idx]), color = "red", linewidth=1, linestyle = ":", alpha=0.7)
        plt.plot(VCD_sat_flat[idx], np.poly1d([0.8,0])(VCD_sat_flat[idx]), color = "red", linewidth=1, linestyle = ":", alpha=0.7)
        plt.annotate("y = " + str('%.1f' % fit_param[0]) + "x + "+ str('%.1f' % fit_param[1]) + "\n r$^{2}$ = " + str('%.2f' % r_sqr), xy=(0.1, 0.8),  xycoords='axes fraction')
        plt.xlabel("TROPOMI NO2 VCD (*10$^{15}$ molec. cm$^{-2}$)", fontsize=10)
        plt.ylabel("COSMO NO2 VCD (*10$^{15}$ molec. cm$^{-2}$)", fontsize=10)
        plt.savefig(savepath +'correlation'+ date_display[d].strftime('%Y%m%d') + '.png', format = 'png', dpi = 200)
        plt.close()
    except TypeError:
        plt.close()
        print("NO good data for day "+ str(counter+1))
##########calculate and plot average over given number of days###########
    if counter == 0:
        mean_meco=tropvcd_sim[:,:,np.newaxis]
        mean_tropomi=tropVCD_sat[:,:,np.newaxis]
        mean_tropomi_old=(tropVCD_sat*trop_amf_sim/trop_amf_sat)[:,:,np.newaxis]
        
    else:
        mean_meco=np.ma.dstack((mean_meco, tropvcd_sim))
        mean_tropomi=np.ma.dstack((mean_tropomi, tropVCD_sat))
        mean_tropomi_old=np.ma.dstack((mean_tropomi_old, (tropVCD_sat*trop_amf_sim/trop_amf_sat)))
    print("day "+ str(counter+1))
    counter=counter+1
    mean_tropomi[mean_tropomi==fv]=np.nan
    mean_tropomi_old[mean_tropomi_old==fv]=np.nan
#    
##################average map over the given period
fig=plt.Figure()
plt.title('COSMO Tropospheric NO$_{2}$ VCD for ' + date_display[start_day-1].strftime('%d') + ' - ' + date_display[end_day-2].strftime('%d-%b-%Y'))
ax = fig.add_subplot(1, 1, 1)
map = Basemap(projection='cyl', resolution = 'l', llcrnrlon=1, llcrnrlat=43,urcrnrlon=20, urcrnrlat=56)
map.drawparallels(np.arange(30.,60.,4.),labels=[1,0,0,0])
map.drawmeridians(np.arange(0.,20.,4.),labels=[0,0,0,1])
map.drawcoastlines()
map.drawcountries()
map.drawmapboundary()
llons, llats = np.meshgrid(lons, lats)
x,y = map(llons, llats)
cs = map.pcolormesh(x, y, np.nanmean(mean_meco,2)*1E-15, cmap='jet', vmin = 0, vmax = 12)
cb=map.colorbar(extend = 'both')
cb.set_label('NO2_Trop_VCD * 10$^{15}$ molecules cm$^{-2}$' , fontsize=11)
plt.savefig(savepath + 'MECO_VCD_3_'+ date_display[d].strftime('%Y%m') +'.png', format = 'png', dpi = 200)
plt.close()
#plot satellite vcd
fig=plt.Figure()
plt.title('TROPOMI Tropospheric NO$_{2}$ VCD for ' + date_display[start_day-1].strftime('%d') + ' - ' + date_display[end_day-2].strftime('%d-%b-%Y')+" (new AMF)")
ax = fig.add_subplot(1, 1, 1)
map = Basemap(projection='cyl', resolution = 'l', llcrnrlon=1, llcrnrlat=43,urcrnrlon=20, urcrnrlat=56)
map.drawparallels(np.arange(30.,60.,4.),labels=[1,0,0,0])
map.drawmeridians(np.arange(0.,20.,4.),labels=[0,0,0,1])
map.drawcoastlines()
map.drawcountries()
map.drawmapboundary()
x,y = map(llons, llats)
cs = map.pcolormesh(x, y, np.nanmean(mean_tropomi,2), cmap='jet', vmin = 0, vmax = 12)
#cs = map.pcolormesh(x, y, NO2_tropvcd*1E-15-V0, cmap='jet', vmin = -7, vmax = 4 )  #for bias
cb=map.colorbar(extend = 'both')
cb.set_label('NO2_Trop_col * 10$^{15}$ molecules cm$^{-2}$' , fontsize=10)
plt.savefig(savepath + 's5p_corr_VCD_3_'+ date_display[d].strftime('%Y%m') +'.png', format = 'png', dpi = 200)
plt.close()
#plot satellite VCD old
fig=plt.Figure()
plt.title('TROPOMI Tropospheric NO$_{2}$ VCD for ' + date_display[start_day-1].strftime('%d') + ' - ' + date_display[end_day-2].strftime('%d-%b-%Y')+" (old AMF)")
ax = fig.add_subplot(1, 1, 1)
map = Basemap(projection='cyl', resolution = 'l', llcrnrlon=1, llcrnrlat=43,urcrnrlon=20, urcrnrlat=56)
map.drawparallels(np.arange(30.,60.,4.),labels=[1,0,0,0])
map.drawmeridians(np.arange(0.,20.,4.),labels=[0,0,0,1])
map.drawcoastlines()
map.drawcountries()
map.drawmapboundary()
x,y = map(llons, llats)
cs = map.pcolormesh(x, y, np.nanmean(mean_tropomi_old,2), cmap='jet', vmin = 0, vmax = 12)
#cs = map.pcolormesh(x, y, NO2_tropvcd*1E-15-V0, cmap='jet', vmin = -7, vmax = 4 )  #for bias
cb=map.colorbar(extend = 'both')
cb.set_label('NO2_Trop_col * 10$^{15}$ molecules cm$^{-2}$' , fontsize=10)
plt.savefig(savepath + 's5p_VCD_3_'+ date_display[d].strftime('%Y%m') +'.png', format = 'png', dpi = 200)
plt.close()

# ###############################time series at selected box##########
# ###############################Overview plot of all days with correlation##########
# plt.ion()
# fig, axes = plt.subplots(nrows=2, ncols=2, figsize=[12,10])
# plt.suptitle('Comparisons of Tropospheric NO$_{2}$ SCD for ' + date_display.strftime('%b-%Y'))
# axes[0, 0].set_title( 'COSMO NO2_Trop_VCD (*1E15 molecules cm-2)', fontsize=10)
# map = Basemap(projection='cyl', resolution = 'l', llcrnrlon=1, llcrnrlat=43,urcrnrlon=20, urcrnrlat=56, ax=axes[0,0])
# map.drawparallels(np.arange(30.,60.,4.),labels=[1,0,0,0])
# map.drawmeridians(np.arange(0.,20.,4.),labels=[0,0,0,1])
# map.drawcoastlines()
# map.drawcountries()
# map.drawmapboundary()
# llons, llats = np.meshgrid(lons, lats)
# x,y = map(llons, llats)
# cs1 = map.pcolormesh(x, y, np.nanmean(mean_meco,2)*1E-15, cmap='jet', vmin = 0, vmax = 12)
# cb1=fig.colorbar(cs1, cmap=plt.cm.get_cmap('jet'), ax=axes[0,0], orientation='vertical',extend = 'both', fraction = 0.1, shrink = 0.8)
# ################################
# axes[0, 1].set_title( 'OMI NO2_Trop_VCD (*1E15 molecules cm-2)',fontsize=10)
# map = Basemap(projection='cyl', resolution = 'l', llcrnrlon=1, llcrnrlat=43,urcrnrlon=20, urcrnrlat=56, ax=axes[0,1])
# map.drawparallels(np.arange(30.,60.,4.),labels=[1,0,0,0])
# map.drawmeridians(np.arange(0.,20.,4.),labels=[0,0,0,1])
# map.drawcoastlines()
# map.drawcountries()
# map.drawmapboundary()
# cs2 = map.pcolormesh(x, y, np.nanmean(mean_omi,2), cmap='jet', vmin = 0, vmax = 12)
# cb2=fig.colorbar(cs2, cmap=plt.cm.get_cmap('jet'), ax=axes[0,1], orientation='vertical',extend = 'both', fraction = 0.1, shrink = 0.8)
# ################################
# axes[1,0].set_title( '(COSMO-OMI) NO2_Trop_VCD(*1E15 molecules cm-2)', fontsize=10)
# map = Basemap(projection='cyl', resolution = 'l', llcrnrlon=1, llcrnrlat=43,urcrnrlon=20, urcrnrlat=56, ax=axes[1,0])
# map.drawparallels(np.arange(30.,60.,4.),labels=[1,0,0,0])
# map.drawmeridians(np.arange(0.,20.,4.),labels=[0,0,0,1])
# map.drawcoastlines()
# map.drawcountries()
# map.drawmapboundary()
# cs3 = map.pcolormesh(x, y, np.nanmean(mean_meco,2)*1E-15-np.nanmean(mean_omi,2), cmap='jet', vmin = -3, vmax = 3)
# cb3=fig.colorbar(cs3, cmap=plt.cm.get_cmap('jet'), ax=axes[1,0], orientation='vertical',extend = 'both', fraction = 0.1, shrink = 0.8)
# ################################
# axes[1,1].set_title( 'Correlation (corrected AMF)')
# VCD_sim_flat=np.ma.mean(mean_meco,2).flatten()*1E-15
# VCD_sat_flat=np.ma.mean(mean_tropomi,2).flatten()
# idx = np.isfinite(VCD_sat_flat) & np.isfinite(VCD_sim_flat)
# #fit_param=np.polyfit(VCD_sat_flat[idx], VCD_sim_flat[idx],1)
# fit_param = stats.linregress(VCD_sat_flat[idx], VCD_sim_flat[idx])[0:3]
# fit_func=np.poly1d(fit_param[0:2])
# r_sqr=fit_param[2]**2
# axes[1,1].scatter(VCD_sat_flat,VCD_sim_flat, s=2)
# axes[1,1].plot(VCD_sat_flat[idx], fit_func(VCD_sat_flat[idx]), color = "black", linewidth=2)
# #axes[1,1].plot(SCD_sat_flat[idx], np.poly1d([1,0])(SCD_sat_flat[idx]), linewidth=1)
# axes[1,1].plot(VCD_sat_flat[idx], np.poly1d([1,0])(VCD_sat_flat[idx]), color = "red", linewidth=1, linestyle = ":")
# #axes[1,1].fill_between(VCD_sat_flat[idx], np.poly1d([0.8,0])(VCD_sat_flat[idx]), np.poly1d([1.2,0])(VCD_sat_flat[idx]), color = 'red', alpha = 0.5 )
# axes[1,1].plot(VCD_sat_flat[idx], np.poly1d([1.2,0])(VCD_sat_flat[idx]), color = "red", linewidth=1, linestyle = ":", alpha=0.7)
# axes[1,1].plot(VCD_sat_flat[idx], np.poly1d([0.8,0])(VCD_sat_flat[idx]), color = "red", linewidth=1, linestyle = ":", alpha=0.7)
# axes[1,1].annotate("y = " + str('%.1f' % fit_param[0]) + "x + "+ str('%.1f' % fit_param[1]) + "\n r$^{2}$ = " + str('%.2f' % r_sqr), xy=(0.1, 0.8),  xycoords='axes fraction')
# axes[1,1].set_xlabel("TROPOMI NO2 VCD (*10$^{15}$ molec. cm$^{-2}$)", fontsize=10)
# axes[1,1].set_ylabel("COSMO NO2 VCD (*10$^{15}$ molec. cm$^{-2}$)", fontsize=10)
# plt.tight_layout()
# plt.show()
# plt.savefig(savepath +'summary_comp'+'.png', format = 'png', dpi = 200)