# -*- coding: utf-8 -*-
"""
Created on Tue Jun 18 17:56:19 2019

@author: Vinod
"""

import netCDF4
import numpy as np
import matplotlib.pyplot as plt
import os
import glob
from scipy.interpolate import interp2d, griddata
from mpl_toolkits.basemap import Basemap
from scipy.interpolate.interpnd import _ndim_coords_from_arrays
from scipy.spatial import cKDTree
year = 2018
month = 5
st_day =1
end_day = 31
qa_filter = 0.5

for day in np.arange(st_day, end_day+1, 1):
    dirname_sat = r'M:\nobackup\cborger\TROPOMI\v1\L2_RPRO\HCHO'
    f_names_sat = glob.glob(os.path.join(dirname_sat, str(year),
                                         str(month).zfill(2), str(day).zfill(2),
                                         'S5P_????_L2__HCHO___' + str(year) + '*_2019*.nc'))
    dirname_sim = r'M:\nobackup\vinod\model_work\MECO'
    filename_sim = 'TNO_2018_05_sorbit_s5p_0625.nc'
    f_sim = netCDF4.Dataset(os.path.join(dirname_sim, filename_sim), 'r')
    model_lon = f_sim.variables['lon'][:]
    model_lat = f_sim.variables['lat'][:]
    ps = f_sim.variables['COSMO_ps'][day-1, :]
    tm = f_sim.variables['COSMO_tm1'][day-1, :]
    # ### wind vectors
    um1 = f_sim.variables['COSMO_um1'][day-1, 1, :, :]
    vm1 = f_sim.variables['COSMO_vm1'][day-1, 1, :, :]
    ws = np.sqrt(um1*um1 + vm1*vm1)
    xx = np.arange(0, len(model_lon), 8)
    yy = np.arange(0, len(model_lat), 8)
    points = np.meshgrid(yy, xx)
    ###
    hcho_conc = f_sim.variables['tracer_gp_HCHO_conc'][day-1, :]
    counter_swath = 0
    counter_imp_swath = 0
    for path_sat in f_names_sat:
        f=netCDF4.Dataset(path_sat,'r')
        Data_fields=f.groups['PRODUCT']
        lats=Data_fields.variables['latitude'][0,:]
        lons=Data_fields.variables['longitude'][0,:]
        hcho_tropcol = Data_fields.variables['formaldehyde_tropospheric_vertical_column'][0,:]
        fv = Data_fields.variables['formaldehyde_tropospheric_vertical_column']._FillValue
        # amf_trop = Data_fields.groups['SUPPORT_DATA'].groups['DETAILED_RESULTS'].variables['formaldehyde_tropospheric_air_mass_factor'][0,:]
        # amf_total = Data_fields.variables['air_mass_factor_total'][0,:]

        # averaging_kernel = Data_fields.groups['SUPPORT_DATA'].groups['DETAILED_RESULTS'].variables['averaging_kernel'][0,:]
        qa_value = Data_fields.variables['qa_value'][0,:]
        #qa_value = qa_value*0.01
        # trop_idx =  Data_fields.variables['tm5_tropopause_layer_index'][0,:]
        # cloud_frac = Data_fields.groups['SUPPORT_DATA'].groups['DETAILED_RESULTS'].variables['cloud_fraction_intensity_weighted'][0,:]
        lat_bound = [42., 56.]
        lon_bound = [1., 20.]
        grid_x, grid_y = np.meshgrid(np.arange(lon_bound[0],lon_bound[1],0.0625), np.arange(lat_bound[0],lat_bound[1],0.0625))
        keep = (lats>lat_bound[0]) & (lats<lat_bound[1]) & (lons>lon_bound[0]) & (lons<lon_bound[1])
        hcho_tropcol = hcho_tropcol[keep==True]
#        ak = []
#        for i in np.arange(0, 34, 1):
#            ak.append(averaging_kernel[:, :, i][keep==True])
#        averaging_kernel = np.array(ak)
        llats, llons = lats[keep==True], lons[keep==True]
        sat_points = np.vstack((llons, llats)).T
        qa_value = qa_value[keep==True]
        hcho_tropcol = np.ma.masked_where(qa_value < qa_filter, hcho_tropcol)
        # amf_trop = amf_trop[keep==True]
        # cloud_frac = cloud_frac[keep==True]
        sat_tropcol = hcho_tropcol*6.02214e+19/1e+16
        # tropSCD_sat = sat_tropcol*amf_trop

        '''
        model data interpolation on satellite points
        '''
        fn_ps = interp2d(model_lon, model_lat, ps)
        ps_intp = np.array([fn_ps(llons[i], llats[i])[0]
                            for i in np.arange(0, len(llats), 1)])
        ps_intp[ps_intp < 0] = np.nan

        tm1_intp = []
        hcho_intp = []
        for lev in np.arange(0, 34, 1):
            fn_tm1_l = interp2d(model_lon, model_lat, tm[lev, :])
            fn_hcho_l = interp2d(model_lon, model_lat, hcho_conc[lev, :])

            tm1_intp_l = [fn_tm1_l(llons[i], llats[i])[0]
                          for i in np.arange(0, len(llats), 1)]
            hcho_intp_l = [fn_hcho_l(llons[i], llats[i])[0]
                          for i in np.arange(0, len(llats), 1)]
            tm1_intp.append(tm1_intp_l)
            hcho_intp.append(hcho_intp_l)
        tm1_intp = np.array(tm1_intp)
        tm1_intp[tm1_intp < 0] = np.nan
        hcho_intp = np.array(hcho_intp)
        hcho_intp[hcho_intp < 0] = np.nan
        '''
        calculation of box heights
        '''
        prs_i = np.array([f_sim.variables['hyai'][i]+(ps_intp[:]*f_sim.variables['hybi'][i]) for i in range(0,35)])
        h_interface = np.array([7.4*np.log(ps_intp[:]/prs_i[i, :])
                                for i in range(0, 35)])*1000
        h = np.array([h_interface[i, :]-h_interface[i-1, :]
                      for i in range(1, 35)])
        h = h*tm1_intp[:]/250    # correct for assumed scale height at 250K
        h = h*100
        '''
        calculation of box vcd
        '''
        tropvcdbox = hcho_intp*h
        tropvcd_sim = np.sum(tropvcdbox[0:16, :], 0)
        
        mask = np.ma.getmask(sat_tropcol)
        tropvcd_sim = np.ma.masked_where(mask, tropvcd_sim)
        tropvcd_sim = np.ma.masked_invalid(tropvcd_sim)
        sat_tropcol = np.ma.masked_where(np.ma.getmask(tropvcd_sim), sat_tropcol)

        plt.ioff()

        if len(llats>0):


            fig, axes = plt.subplots(nrows=1, ncols=2, figsize=[13, 6])
            plt.tight_layout(pad=0.4, w_pad=2.5, h_pad=1)
            plt.suptitle('Comparisons of Tropospheric HCHO VCD for ' +
                         str(year) + str(month).zfill(2) + str(day).zfill(2) +
                         '_swath' + str(counter_swath+1))
            axes[0].set_title('COSMO HCHO_Trop_VCD (*1E16 mcl cm-2)',
                                 fontsize=10)
            map1 = Basemap(projection='cyl', resolution='l', llcrnrlon=1,
                           llcrnrlat=42, urcrnrlon=20, urcrnrlat=56,
                           ax=axes[0])
            map1.drawparallels(np.arange(40., 56., 4.), labels=[1, 0, 0, 0])
            map1.drawmeridians(np.arange(0., 20., 4.), labels=[0, 0, 0, 1])
            map1.drawcoastlines()
            map1.drawcountries()
            map1.drawmapboundary()
            x, y = map1(llons, llats)
            scat1 = map1.scatter(x, y, c=tropvcd_sim[:]*1E-16, cmap='jet', s=1,
                                 vmin=-0.25, vmax=2)
            cb1 = fig.colorbar(scat1, cmap=plt.cm.get_cmap('jet'),
                               ax=axes[0], orientation='vertical',
                               extend='both', fraction=0.1, shrink=0.8)
            # ########### wind vectors ###############
            model_llon, model_llat = np.meshgrid(model_lon, model_lat)
            x_wind, y_wind = map1(model_llon, model_llat)
            map1.quiver(x_wind[points], y_wind[points],
                        um1[points], vm1[points], ws[points],
                        latlon=True, cmap=plt.cm.autumn)

            # ###############################
            axes[1].set_title('TROPOMI HCHO_Trop_VCD (*1E16 mcl cm$-{2}$)',
                                 fontsize=10)
            map1 = Basemap(projection='cyl', resolution='l', llcrnrlon=1,
                           llcrnrlat=42, urcrnrlon=20, urcrnrlat=56,
                           ax=axes[1])
            map1.drawparallels(np.arange(40., 56., 4.), labels=[1, 0, 0, 0])
            map1.drawmeridians(np.arange(0., 20., 4.), labels=[0, 0, 0, 1])
            map1.drawcoastlines()
            map1.drawcountries()
            map1.drawmapboundary()
            scat2 = map1.scatter(x, y, c=sat_tropcol, cmap='jet', s=1,
                                 vmin=-0.25, vmax=2)
            cb2 = fig.colorbar(scat2, cmap=plt.cm.get_cmap('jet'),
                               ax=axes[1], orientation='vertical',
                               extend='both', fraction=0.1, shrink=0.8)


            plt.savefig(os.path.join(r'M:\nobackup\vinod\model_work\MECO\Plots\TROPOMI_comp\TNO\swath\HCHO',
                                     'summary'+str(month).zfill(2)+str(day).zfill(2)+'_swath'+str(counter_swath+1)+'.png'), format = 'png', dpi = 300)
            plt.close()

            ## regrid back to regular grid and create daily and monthly mean
            grid_vcd_sim = griddata((llons,llats), tropvcd_sim*1E-16, (grid_x, grid_y), method='nearest')
            grid_vcd_sat = griddata((llons,llats), sat_tropcol, (grid_x, grid_y), method='nearest')
            '''
            if I do linear interpolation, I need to limit the interpolation
            range around data removed by cloud filter. It doesnot matter for
            nearest interpolation method
            '''
            THRESHOLD = 0.07
            grid = [grid_x,grid_y]
            xy = np.vstack((llons,llats)).T
            xy = xy[~mask]
            if len(xy) > 0:
                tree = cKDTree(xy)
                xi = _ndim_coords_from_arrays(tuple(grid), ndim=xy.shape[1])
                dists, indexes = tree.query(xi)
                grid_vcd_sim[dists > THRESHOLD] = np.nan
                grid_vcd_sat[dists > THRESHOLD] = np.nan
            
            if counter_imp_swath == 0:
                mean_meco=grid_vcd_sim[:,:]
                mean_tropomi=grid_vcd_sat[:,:]

            else:
                mean_meco=np.ma.dstack((mean_meco, grid_vcd_sim))
                mean_tropomi=np.ma.dstack((mean_tropomi, grid_vcd_sat))

            counter_imp_swath += 1
        counter_swath+=1
    if counter_imp_swath==0:
        mean_meco = np.empty((np.shape(grid_x)))*np.nan
        mean_tropomi = np.empty((np.shape(grid_x)))*np.nan
    else:
        mean_meco = np.nanmean(mean_meco,2)
        mean_tropomi = np.nanmean(mean_tropomi,2)
    plt.ion()
    f.close()
    if day == st_day:
        mean_meco_month=mean_meco[:,:,np.newaxis]
        mean_tropomi_month=mean_tropomi[:,:,np.newaxis]
    else:
        mean_meco_month=np.ma.dstack((mean_meco_month, mean_meco))
        mean_tropomi_month=np.ma.dstack((mean_tropomi_month, mean_tropomi))
    print(str(day))    
ff_out = netCDF4.Dataset(os.path.join(r'M:\nobackup\vinod\model_work\MECO\Plots\TROPOMI_comp\TNO\swath\HCHO',
                                     'meanVCD'+str(month).zfill(2)+'_regrid.nc'), 'w', format='NETCDF4')
ff_out.createDimension('time', None)
ff_out.createDimension('lon', grid_x.shape[1])
ff_out.createDimension('lat', grid_y.shape[0])
time_out = ff_out.createVariable('time', 'f4', ('time',))
lon_out = ff_out.createVariable('lon', 'f4', ('lon'))
lat_out = ff_out.createVariable('lat', 'f4', ('lat'))
HCHO_tropcol_sat_out = ff_out.createVariable('HCHO_tropcol_sat',   'f4', ('lat','lon', 'time'), fill_value=fv)
HCHO_tropcol_sim_out = ff_out.createVariable('HCHO_tropcol_sim',   'f4', ('lat','lon', 'time'), fill_value=fv)
   
time_out[:] = np.array([i for i in range(0,end_day-st_day+1)])
time_out.long_name='time';time_out.units='days since 2018-05-01 00:00:00'
lon_out[:] = np.arange(lon_bound[0],lon_bound[1],0.0625)
lat_out[:] = np.arange(lat_bound[0],lat_bound[1],0.0625)
HCHO_tropcol_sat_out[:] = mean_tropomi_month[:]; HCHO_tropcol_sat_out.units = "1E16 molecules cm-3"
HCHO_tropcol_sim_out[:] = mean_meco_month[:]; HCHO_tropcol_sim_out.units = "1E16 molecules cm-3"
ff_out.close()