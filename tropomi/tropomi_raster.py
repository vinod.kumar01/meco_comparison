# -*- coding: utf-8 -*-
"""
Created on Mon Mar 18 14:03:55 2019

@author: Vinod
"""

import numpy as np
import os
import sys
import glob
import netCDF4
from datetime import datetime
from mpl_toolkits.basemap import Basemap
import matplotlib.pyplot as plt
from matplotlib.colors import LinearSegmentedColormap
# path to folder containing rasterizer
path = os.path.join('/gpfs/airsat/satfs/nobackup/vinod/tropomi_raster', 'rasterizer_holger_py3')
sys.path.append(path)
#import C_rasterizer as Cr
# dict of properties
propdict = {'MiddleEast': {'Ymin': 15, 'Ymax': 45, 'Xmin': 20, 'Xmax': 60,
                           'dx': 0.05, 'dy': 0.05, 'figsize': (16, 9),
                           'vmin': 0, 'vmax': 3.0, 'colsteps': 24, 'dlat': 10,
                           'minhour': 4, 'maxhour': 14, 'rivers': False,
                           'res': 'i'},
            'World': {'Ymin': -70, 'Ymax': 70, 'Xmin': -179, 'Xmax': 179,
                      'dx': 0.2, 'dy': 0.2, 'figsize': (16, 9), 'vmin': -0.25,
                      'vmax': 3, 'colsteps': 20, 'dlat': 45,
                      'minhour': 0, 'maxhour': 26, 'rivers': False,
                      'res': 'c'},
            }
# iterate over region
for name, pdict in propdict.items():
    print(name)
    if name != 'World':
        continue

    Xmin = pdict['Xmin']
    Xmax = pdict['Xmax']
    Ymin = pdict['Ymin']
    Ymax = pdict['Ymax']
    dx = pdict['dx']
    dy = pdict['dy']

    xsteps = int((Xmax-Xmin) / dx)
    ysteps = int((Ymax-Ymin) / dy)
    longrid = np.arange(Xmin, Xmin+xsteps*dx+dx, dx)
    latgrid = np.arange(Ymin, Ymin+ysteps*dy+dy, dy)

    # insert directory/folder containing OMI L2 NO2 data
    daylist = glob.glob(r'M:\nobackup\cborger\TROPOMI\v1\L2\NO2\2018\10\01\*NO2*.nc')

    mdata = []
    msum = []
    
    for k, fn in enumerate(sorted(daylist)):
        print(fn)
        date = fn.split('\\')[-1].split('\\')[-1].split('_')[8].split('T')[0]
        time = fn.split('\\')[-1].split('\\')[-1].split('_')[8].split('T')[1]
        date = datetime.strptime(date+time, '%Y%m%d%H%M%S')
    
        # if we know the overpass time, we can filter the files and save time
        # if not (date.hour > 6 and date.hour < 20) or date.day !=29:
        #     continue
    
        print(date)
        try:
            with netCDF4.Dataset(fn, 'r') as f:
                Data_fields = f.groups['PRODUCT']
                vcd = Data_fields.variables['nitrogendioxide_tropospheric_column'][0,:]
                amf = Data_fields.variables['air_mass_factor_troposphere'][0,:]
                xtra_node = f.groups['PRODUCT'].groups['SUPPORT_DATA'].groups['DETAILED_RESULTS']
                scd = xtra_node.variables['nitrogendioxide_slant_column_density'][0,:]
                geonode = f.groups['PRODUCT'].groups['SUPPORT_DATA'].groups['GEOLOCATIONS']
                lonbo = geonode.variables['longitude_bounds'][0,:]
                latbo = geonode.variables['latitude_bounds'][0,:]
                fv = Data_fields.variables['nitrogendioxide_tropospheric_column']._FillValue    
                sel = np.where(vcd == fv)
                vcd[sel] = np.nan
                vcd = vcd*6.02214e+19
                scd = vcd * amf
        except Exception as e:
            print(e)
            continue
        try:
            nshape = vcd.shape[0] * vcd.shape[1]
            nlonbo = lonbo.T.reshape((nshape, 4))
            nlatbo = latbo.T.reshape((nshape, 4))
            nvals = scd.T.reshape((nshape, 1))
            sel2 = np.where(~np.isnan(nvals))[0]
    
            nlonbo = nlonbo[sel2, :]
            nlatbo = nlatbo[sel2, :]
            nvals = nvals[sel2, :]
    
            grid, gridsum = Cr.RasterTab(nlonbo, nlatbo, nvals, Xmin, xsteps,
                                         Xmax, Ymin, ysteps, Ymax, interp=1,
                                         use_openmp=True)
            gridsum = np.ma.masked_equal(gridsum, 0)
            print(grid.min(), grid.max())
            grid = np.ma.MaskedArray(grid, mask=gridsum.mask)
            grid.mask = np.ma.mask_or(grid.mask, np.isnan(grid))
    
            mdata.append(grid)
            msum.append(gridsum)
        except Exception as e:
            print(e)
    # flip array and scale values
    mmdata = np.ma.MaskedArray(mdata)
    mmsum = np.ma.MaskedArray(msum)
    mfindata = mmdata.sum(axis=0) / mmsum.sum(axis=0) / (10**16.)
    cc = np.flip(mfindata, axis=0)
    
    # plot data
    fig = plt.figure(figsize=pdict['figsize'])
    bmap = Basemap(projection='cyl',llcrnrlat=Ymin, urcrnrlat=Ymax,
                   llcrnrlon=Xmin, urcrnrlon=Xmax, resolution=pdict['res'])
    cc = np.flip(mfindata, axis=0)
    cm = plt.cm.get_cmap('jet')
    cmcols = ['aliceblue', 'royalblue', 'mediumspringgreen', 'yellow', 'r']
    mycm = LinearSegmentedColormap.from_list(name='test', colors=cmcols,
                                             N=pdict['colsteps'])
    mycm = LinearSegmentedColormap.from_list(name='test', colors=cmcols)

    bmap.pcolormesh(longrid, latgrid, cc, latlon=True, vmin=-0.25,
                    vmax=3, cmap=mycm)
    cbar = bmap.colorbar()
    cbar.set_label('NO2 L2 SCD [$10^{16}$ molec. cm$^{-2}$]')
    bmap.drawcountries(linewidth=0.5)
    bmap.drawcoastlines(linewidth=0.5)
    dlat = pdict['dlat']
    bmap.drawparallels(np.arange(-90+dlat, 90, dlat),
                       labels=[True, False, False, False], linewidth=0.5)
    bmap.drawmeridians(np.arange(-90+dlat, 90, dlat),
                       labels=[False, False, False, True], linewidth=0.5)
    ofn = os.path.join(r'M:\nobackup\vinod\tropomi_raster\plots',
                       'OMI_SCD_NO2_{}_v20180131.png'.format(name))
    plt.savefig(ofn, format='png', dpi=1200)
    plt.close(fig)
    plt.clf()
