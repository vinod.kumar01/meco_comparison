# -*- coding: utf-8 -*-
"""
Created on Tue May  1 15:18:10 2018

@author: Vinod
"""
import netCDF4
import numpy as np
import pandas as pd
import math
f = netCDF4.Dataset(r'M:\home\vinod\nobackup\model_work\wrfbiochemi_d01.nc' , 'r+')
new_vals = pd.read_csv(r'M:\home\vinod\nobackup\model_work\wrf-chem_corrected_Mohalinew.txt', dtype=None, delimiter='\t', header=0)
lats = f.variables['XLAT'][0,:,0]
lons = f.variables['XLONG'][0,0,:]
chk_lat = new_vals['Lat'].values
chk_lon = new_vals['Long'].values
for i in range(0,len(new_vals)):  #
    #lat_idx=np.where(lats[:] >= chk_lat[i])
    lat_idx = np.where([math.ceil(j * 100.0) / 100.0 for j in lats[:]] >= chk_lat[i])
    lat_idx = lat_idx[0][0]
    lon_idx = np.where([math.ceil(j * 100.0) / 100.0 for j in lons[:]] >= chk_lon[i])
    lon_idx = lon_idx[0][0]
    f.variables['PFTP_BT'][0,lat_idx,lon_idx]=new_vals['Btr'].values[i]
    f.variables['PFTP_NT'][0,lat_idx,lon_idx]=new_vals['Ntr'].values[i]
    f.variables['PFTP_SB'][0,lat_idx,lon_idx]=new_vals['Shrubs'].values[i]
    f.variables['PFTP_HB'][0,lat_idx,lon_idx]=new_vals['herbs'].values[i]
    #f.variables['PFTP_BR'][0,lat_idx,lon_idx]=new_vals['Barren'].values[i]
f.close()