# -*- coding: utf-8 -*-
"""
Created on Tue May 19 10:50:42 2020

@author: Vinod
"""

from datetime import datetime, timedelta
from decimal import Decimal
import numpy as np
from scipy import stats
from sklearn.metrics import mean_squared_error
from sklearn.linear_model import LinearRegression as LR
from scipy.odr import Model, Data, ODR
from scipy.stats import linregress
from scipy.special import erf
import os
import pandas as pd
import netCDF4

nc = {'universal_gas_constant': 8.3144621,
      'Avogadros_number': 6.022140857E23,
      'msl_pressure': 1.01325E5,
      'scale_height': 7400}


class wind():
    def __init__(self, ws=None, wd=None, u=None, v=None):
        self.ws = ws
        self.wd = wd
        self.u = u
        self.v = v

    def sep_wind(self):
        '''
        input
        @ws = wind speed numpy array
        @wd = incoming wind direction numpy array)
        '''
        wd = np.asarray([self.wd], dtype=float) if np.isscalar(self.wd) else np.asarray(self.wd)
        ws = np.asarray([self.ws], dtype=float) if np.isscalar(self.ws) else np.asarray(self.ws)
        wd = 270. - wd   # make realistic, north is 0, east is 90, south is 180
        wd[wd < 0] = wd[wd < 0] + 360.  # make realistic, north is 0, east is 90
        u = ws*np.array([np.cos(float(x)*np.pi/180) for x in wd])
        v = ws*np.array([np.sin(float(x)*np.pi/180) for x in wd])
        u = np.asscalar(u) if len(u) == 1 else u
        v = np.asscalar(v) if len(v) == 1 else v
        return(u, v)

    def merge_uv(self):
        '''
        input
        @ws = wind speed numpy array
        @wd = incoming wind direction numpy array)
        '''
        u = np.asarray([self.u], dtype=float) if np.isscalar(self.u) else np.asarray(self.u)
        v = np.asarray([self.v], dtype=float) if np.isscalar(self.v) else np.asarray(self.v)
        ws = np.sqrt(u**2 + v**2)
        wd = np.arctan2(u/ws, v/ws)
        wd *= 180./np.pi
        wd += 180.
#        wd = 90. - wd
        wd[wd < 0] = wd[wd < 0] + 360.
        wd = np.asscalar(wd) if len(wd) == 1 else wd
        ws = np.asscalar(ws) if len(ws) == 1 else ws
        return(ws, wd)


def O4_VCD_sfc(sfc_temp, sfc_height):    # sfc temp in Kelvin, SFC_height in m
    '''
    calculate O4 VCD assuming lapse rate of -0.65k per 100m
    if surface temp and sfc height is provided
    '''
    h = np.arange(sfc_height, 20000, 100)
    temp_profile = [sfc_temp+i*(-0.65) if j <= 12000 else 215
                    for i, j in enumerate(h)]
    pressure_profile = np.exp((-h)*temp_profile/nc['scale_height']/250)
    pressure_profile *= nc['msl_pressure']
    o2 = tracer_prop(name='O2', mol_wt='32', label='O$_2$')
    O2_conc = o2.vmr2conc(0.20946*1E9, pressure_profile, temp_profile)
    O4_VCD = np.sum(100*100*O2_conc**2)  # box height 100m= 100*100cm
    return O4_VCD


def mode(xs):
    try:
        return stats.mode(xs)[0][0]
    except IndexError:
        return np.nan


def load_cloud(tel):
    inp_dir = r'D:\wat\minimax\Mainz_4az\results'
    file_name = 'cloud_classified_Mainz_' + tel + '.csv'
    df = pd.read_csv(os.path.join(inp_dir, file_name), delimiter=',',
                     escapechar='#')
    df.columns = df.columns.str.strip()
    df['Date_time'] = pd.to_datetime(df['Date_time'].apply(str),
                                     format='%Y-%m-%d %H:%M:%S')
    df = df.set_index('Date_time')
    df = df.resample('H').apply(mode)
    return df


def doy_2_datetime(doy, year):
    '''
    It converts fractional day into datetime
    '''
    year = datetime(year, 1, 1)
    dt = timedelta(days=doy-1)
    #  1 Jan is 1 and not 0
    calander_day = year + dt
    return calander_day


def matlab2datetime(matlab_datenum):
    day = datetime.fromordinal(int(matlab_datenum))
    dayfrac = timedelta(days=matlab_datenum % 1) - timedelta(days=366)
    return day + dayfrac


def datetime_2_doy(date):
    '''
    It converts datetime into day of year
    the input datetime should be in format datetime.datetime(yyyy, mm, dd)
    or datetime.datetime(yyyy, mm, dd, hh, mm, ss)
    '''
    detail_time = date.timetuple()
    doy = detail_time.tm_yday
    doy += detail_time.tm_hour/24 + detail_time.tm_min/24/60
    doy += detail_time.tm_sec/24/60/60
    return doy


def orthoregress(x, y, sx, sy):
    """Perform an Orthogonal Distance Regression on the given data,
    using the same interface as the standard scipy.stats.linregress function.
    Arguments:
    x: x data
    y: y data
    sx: standard deviation array of x
    sy: stanard deviation array of y
    Returns:
    [m, c, chi_square, nan, nan]
    Uses standard ordinary least squares to estimate the starting parameters
    then uses the scipy.odr interface to the ODRPACK Fortran code to do the
    orthogonal distance calculations.
    """
    linreg = linregress(x, y)
    mod = Model(f)
    dat = Data(x, y, wd=1./np.power(sx, 2), we=1./np.power(sy, 2))
    od = ODR(dat, mod, beta0=linreg[0:2])
    out = od.run()

    return list(out.beta) + [out.res_var, np.nan, np.nan]


def f(p, x):
    """Basic linear regression 'model' for use with ODR"""
    return (p[0] * x) + p[1]


class tracer_prop:
    def __init__(self, **kwargs):
        self.name = kwargs.get('name')
        self.label = kwargs.get('label')
        self.dscd_lim = kwargs.get('dscd_lim')
        self.vcd_lim = kwargs.get('vcd_lim')
        self.conc_lim = kwargs.get('conc_lim')
        self.m_w = kwargs.get('mol_wt')
        self.vcd_unit = 'molecules cm$^{-2}$'
        self.conc_unit = 'molecules cm$^{-3}$'

    def conc2vmr(self, conc, P, T):
        '''
        returns VMR in ppb from concentration in molecules cm-3
        Pressure should be in pa
        '''
        P = np.asarray([P], dtype=float) if np.isscalar(P) else np.asarray(P)
        T = np.asarray([T], dtype=float) if np.isscalar(T) else np.asarray(T)
        N_A = nc['Avogadros_number']
        if np.nanmean(T < 60):
            T += 273.15
        vmr = conc*(nc['universal_gas_constant']*T*1E6)
        vmr /= N_A*1E-9*P
        vmr = np.asscalar(vmr) if len(vmr) == 1 else vmr
        return vmr

    def vmr2conc(self, vmr, P, T):
        P = np.asarray([P], dtype=float) if np.isscalar(P) else np.asarray(P)
        T = np.asarray([T], dtype=float) if np.isscalar(T) else np.asarray(T)
        N_A = nc['Avogadros_number']
        conc = vmr*1E-9*N_A*P*1E-6/nc['universal_gas_constant']/T
        conc = np.asscalar(conc) if len(conc) == 1 else conc
        return conc

    def massconc2vmr(self, mass_conc, P, T):
        '''converts ug/m3 in ppb'''
        conc = mass_conc*1e-6  # g/m3
        conc /= 1e6  # g/cm3
        conc /= self.m_w  # moles/cm3
        conc *= nc['Avogadros_number']  # molecules/cm3
        return self.conc2vmr(conc, P, T)


def qdoas_loader(qdoas_file, clean_data=True, header=0, skip_rows=0):
    data = pd.read_csv(qdoas_file, dtype=None, delimiter='\t',
                       skiprows=skip_rows)
    data = data.iloc[:, :-1]
    data['Date_time'] = data['Date (DD/MM/YYYY)'] + data['Time (hh:mm:ss)']
    data['Date_time'] = pd.to_datetime(data['Date_time'],
                                       format='%d/%m/%Y%H:%M:%S')
    data = data.replace(9.9692e+36, np.nan)
    data = data.loc[data['SZA'] < 85]
    return data


class lin_fit:
    def __init__(self, x, y, **kwargs):
        sx = kwargs.get('sx', None)
        sy = kwargs.get('sy', None)
        for param in [x, y, sx, sy]:
            if np.ma.is_masked(param):
                param = param.filled(np.nan)
        mask = np.isnan(x) | np.isnan(y)
        mask |= np.isinf(x) | np.isinf(y)
        self.mask = mask
        # for param in [x, y, sx, sy]:
        #     if param is not None:
        #         param = param[~mask]
        self.x = x[~mask]
        self.y = y[~mask]
        if sx is not None:
            self.sx = sx[~mask]
        if sy is not None:
            self.sy = sy[~mask]
        self.res = {}

    def stats_calc(self, **kwargs):
        '''

        Parameters
        ----------
        **kwargs : fr is the list showing range in which
        user wants to calculate the percenatge of y data.
        Returns
        -------
        Dictionary of Fit statistics

        '''
        if self.res:
            return self.res
        else:
            fr = kwargs.get('frac_range', [0.5, 2])
            method = kwargs.get("method", "normal")
            # options : "normal", "via_origin", "odr"
            res = {}
            x = self.x
            y = self.y
            if len(self.x) == 0:
                res['m'], res['c'], res['r'] = np.nan, np.nan, np.nan
                res['rmse'], res['bias'], res['frac'] = np.nan, np.nan, np.nan
                res['x_mean'], res['y_mean'] = np.nan, np.nan
            else:
                if method == 'via_origin':
                    reg = LR(fit_intercept=False).fit(x.reshape((len(x), 1)),
                                                      y)
                    res['m'] = reg.coef_
                    res['c'] = 0.
                elif method == 'odr':
                    fit_param = orthoregress(x, y, self.sx, self.sy)
                    res['m'], res['c'] = fit_param[0], fit_param[1]
                else:
                    fit_param = stats.linregress(x, y)
                    res['m'], res['c'] = fit_param[0], fit_param[1]
                res['r'] = stats.pearsonr(x, y)[0]
                res['rmse'] = mean_squared_error(x, y, squared=False)
                res['bias'] = (np.mean(y)-np.mean(x))
                res['x_mean'] = np.mean(x)
                res['y_mean'] = np.mean(y)
    #        res['bias'] /= np.sum(~mask)
                counter = 0
                for i in range(len(x)):
                    if fr[0]*x[i] < y[i] <= fr[1]*x[i]:
                        counter += 1
                res['f'] = 100*counter/len(x)
            self.res = res
            return res

    def add_fit_lines(self, **kwargs):
        ax = kwargs.get('ax', 'ax')
        add_regression = kwargs.get('add_regression', True)
        fr = kwargs.get('frac_range', [0.5, 2])
        add_fill_area = kwargs.get('add_fill_area', True)
        fit_label = kwargs.get('fit_label', 'linear regression')
        line_color = kwargs.get('line_color', 'k')
        res = self.res
        fit_func = np.poly1d([res['m'], res['c']])
        if add_regression:
            ax.plot(self.x, fit_func(self.x), c=line_color, label=fit_label)
        if add_fill_area:
            ax.plot([0, 1], [0, 1], transform=ax.transAxes, ls="--", c=".3",
                    label='1:1')
            ax.fill_between([0, 1], [0, fr[1]], [0, 1], transform=ax.transAxes,
                            ls=":", color=".3", alpha=.2)
            ax.fill_between([0, 1], [0, fr[0]], [0, 1], transform=ax.transAxes,
                            ls=":", color=".3", alpha=.2,
                            label='{:.0f}%-{}%'.format(fr[0]*100, fr[1]*100))

    def add_fit_param(self, **kwargs):
        pos_x = kwargs.get('pos_x', 0.1)
        pos_y = kwargs.get('pos_y', 0.9)
        ax = kwargs.get('ax', 'ax')
        color = kwargs.get('color', 'k')
        show_fit = kwargs.get('show_fit_eqn', True)
        showr = kwargs.get('showr', True)
        showf = kwargs.get('showfrac', False)
        c_fmt = kwargs.get('c_fmt', '%.2E')
        m_fmt = kwargs.get('m_fmt', '%.2f')
        annotation = kwargs.get('annotation', '')
        bbox_props = dict(boxstyle="round", fc="w", ec="0.5", alpha=0.8)
        fit_param = self.res
        corr_coeff = "\n R = " + str('%.2f' % fit_param['r']) if showr else ''
        frac = ", f = " + str('%.2f' % fit_param['f']) + "%" if showf else ''
        fit_eqn = "y = " + str(m_fmt % fit_param['m']) + "x"
        if fit_param['c'] > 0:
            fit_eqn += " + " + str(c_fmt % Decimal(str(abs(fit_param['c']))))
        elif fit_param['c'] < 0:
            fit_eqn += " - " + str(c_fmt % Decimal(str(abs(fit_param['c']))))
        if show_fit:
            annotation += fit_eqn + corr_coeff + frac
        else:
            annotation += corr_coeff[2:] + frac
        ax.annotate(annotation, xy=(pos_x, pos_y), xycoords='axes fraction',
                    color=color, bbox=bbox_props)


def get_lv2_filenames_omi(root, spec):
    # create dict with orbit as key
    filenames = {}
    for root, dirs, files in os.walk(root):
        for file in files:
            if (file.startswith("QA4ECV_L2_"+spec) & file.endswith(".nc")):
                orbit = get_orbit_omi(file)
                filenames[orbit] = os.path.join(root, file)
    return filenames


def get_orbit_omi(fname):
    # get orbit number from level2 filename
    return int(os.path.basename(fname).split("_")[-3][1:])


def nc2dict_lv2(fname, varlist):
    # Convert TROPOMI l2 nc files to dictionary
    data = netCDF4.Dataset(fname, "r")
    dct = {key: data[varlist[key]][0] for key in varlist}
    data.close()
    return(dct)


def datetime2time(datetime):
    '''
    convert datetime.datetime to time such that date is 01-01-1900
    '''
    time = datetime.replace(year=1900, month=1, day=1)
    return time


def derivative(y, dx=1, order=2):

    d = np.zeros_like(y)

    if order == 1:
        for k in range(2, len(y)-2):
            d[k] = 1/2*(-y[k-1]+y[k+1])

    else:
        # Derivative based on y_(-2) : y_(+2):
        for k in range(2, len(y)-2):
            d[k] = 1/12*(y[k-2]-8*y[k-1]+8*y[k+1]-y[k+2])

    # At edges: best possible difference quotient
    d[0] = y[1] - y[0]
    d[1] = 0.5*(y[2] - y[0])
    d[-2] = 0.5*(y[-1] - y[-3])
    d[-1] = y[-1] - y[-2]

    return d/dx


class iterative_mean():

    def __init__(self):
        self.sum = None
        self.sum2 = None
        self.N = None

    def append(self, M, N=None):
        if N is None:
            N = 1-np.asarray(np.isnan(M).astype(float))
        M_ = M.copy()
        M_[np.isnan(M)] = 0
        M_[N == 0] = 0
        if self.sum is None:
            self.sum = M_
            self.sum2 = M_*M_
            self.N = N
        else:
            self.sum += M_
            self.sum2 += M_*M_
            self.N += N

    def mean_std(self, thresh_N=10):
        mean = self.sum/self.N
        std = np.sqrt((self.sum2-self.sum**2/self.N)/(self.N-1))
        mean[self.N < thresh_N] *= np.nan
        std[self.N < thresh_N] = np.nan
        return mean, std


class gmxe():
    def __init__(self, filename, **kwargs):
        self.filename = filename
        with netCDF4.Dataset(self.filename, 'r') as f:
            try:
                self.lon = f.variables['geolon_ave'][:]
                self.lat = f.variables['geolat_ave'][:]
            except KeyError:
                self.lon = f.variables['lon'][:]
                self.lat = f.variables['lat'][:]
            self.time = f.variables['time'][:]

    def calc_mass_mode(self, dia, sigma, num, ddens, **kwargs):
        '''
        Parameters
        ----------
        dia : numpy array
            dry aerosol diameter --[m]
        sigma : numpy array
            DESCRIPTION.
        num : TYPE
            aerosol number -- [1/cm^3]
        ddens : numpy array
            dry aerosol density
        **kwargs : TYPE
            DESCRIPTION.

        Returns
        -------
        m_25 : numpy array
            pm 2.5 mass of mode in mug/m3
    
        '''
        ul = kwargs.get('upper_limit', 2.5e-6)
    # =============================================================================
    #         volume as number but different momentum:
    #         ln(diameter_volume)=ln(diameter)+3log(sigma)^2
    # =============================================================================
        diameter_V = np.exp(np.log(dia)+3.0*(np.log(sigma))**2)
        factor_V = np.exp(3.0*np.log(dia)+9.0/2.0*(np.log(sigma))**2)
        factor_V *= np.pi/6.0
    # =============================================================================
    # volume below 2.5 um
    # =============================================================================
        V_25 = erf((np.log(ul)-np.log(diameter_V))/(np.sqrt(2.0)*np.log(sigma)))
        V_25 += 1.0
        V_25 *= factor_V*num/2.0*(100**3)
        m_25 = V_25*ddens*1E9
        return m_25

    def calc_pm(self, **kwargs):
        modes = kwargs.get('modes', ['M1', 'M2', 'M3', 'M4', 'M5', 'M6', 'M7'])
        sigmas = kwargs.get('sigmas',
                            [1.59, 1.59, 1.59, 2.20, 1.59, 1.59, 2.0])
        ul = kwargs.get('upper_limit', 2.5e-6)
        sigmas = np.asarray(sigmas)
        with netCDF4.Dataset(self.filename, 'r') as f:
            try:
                arr_length = f['AERNUMB_M1_ave']
            except KeyError:
                arr_length = f['AERNUMB_M1']
            aernumb = {}
            rdryaer = {}
            ddryaer = {}
            pm25 = []
            for mode in modes:
                try:
                    # ## aerosol number -- [1/cm^3]
                    aernumb[mode] = f['AERNUMB_'+mode+'_ave'][:, -1, :, :]
                    # ## dry aerosol radius -- [m]
                    rdryaer[mode] = f['RDRYAER_'+mode+'_ave'][:, -1, :, :]
                    # ## dry aerosol density -- [kg/m^3]
                    ddryaer[mode] = f['DDRYAER_'+mode+'_ave'][:, -1, :, :]
                except KeyError:
                    aernumb[mode] = f['AERNUMB_'+mode][:, -1, :, :]
                    rdryaer[mode] = f['RDRYAER_'+mode][:, -1, :, :]
                    ddryaer[mode] = f['DDRYAER_'+mode][:, -1, :, :]

                aernumb[mode] = np.ma.masked_where(aernumb[mode] == 0,
                                                   aernumb[mode])
                rdryaer[mode] = np.ma.masked_where(rdryaer[mode] == 0,
                                                   rdryaer[mode])
                ddryaer[mode] = np.ma.masked_where(ddryaer[mode] == 0,
                                                   ddryaer[mode])
            num = {}
            dia = {}
            ddens = {}
            for t in range(len(arr_length)):
                for mode in modes:
                    num[mode] = aernumb[mode][t, :]
                    dia[mode] = 2.*rdryaer[mode][t, :]
                    ddens[mode] = ddryaer[mode][t, :]
                m25 = {}
                for i, mode in enumerate(modes):
                    m25[mode] = self.calc_mass_mode(dia[mode], sigmas[i],
                                                    num[mode], ddens[mode],
                                                    upper_limit=ul)
                PMT_below_25 = np.ma.sum([m25[m] for m in modes], 0)
                # PMT_below_25 = m25['M1']+m25['M2']+m25['M3']+m25['M4']
                # PMT_below_25 += m25['M5']+m25['M6']+m25['M7']
                pm25.append(PMT_below_25)
        return np.asarray(pm25)
