# -*- coding: utf-8 -*-
"""
Created on Sun May 13 19:12:45 2018

@author: Vinod
"""
#script to calculate VCD using MECO(n) output. It uses the gridded MECO(n) output and interpolated on OMI pressure grids and uses averaging kernels.
import netCDF4
import os
import numpy as np
import matplotlib.pyplot as plt
#from sklearn import linear_model
import datetime
from mpl_toolkits.basemap import Basemap
from scipy import stats
dirname_sim = r'M:\home\vinod\nobackup\model_work\MECO'
filename_sim = 'TNO_201504_sorbit_AURA-A_omi25s.nc'
#filename_sim = 'TNO_201504_sorbit_AURA-A_QA4ECV_omi25s.nc'
#filename_sat = 'QA4ECV_L2G_HCHO_OMI_201504_merge_025.nc'
filename_sat = 'OMI-Aura_full-OMDOMINO_2015m04_merge_025.nc'
tracer = 'NO2'    ##NO2
if tracer == 'HCHO':
    dirname_sat = r'M:\nobackup\vinod\OMI_HCHO\QA4ECV\raster'
    path_sat = os.path.join(dirname_sat, filename_sat)
    f_sat=netCDF4.Dataset(path_sat,'r')
    omitropcol = f_sat.variables['tropospheric_hcho_vertical_column']
    sf = 1
else:
    dirname_sat = r'M:\nobackup\vinod\omi_raster\datasets'
    path_sat = os.path.join(dirname_sat, filename_sat)
    f_sat=netCDF4.Dataset(path_sat,'r')
    omitropcol = f_sat.variables['TroposphericVerticalColumn']
    sf = omitropcol.ScaleFactor
if "TNO_" in filename_sim:
    savepath = r'M:\home\vinod\nobackup\model_work\MECO\Plots\OMI_comp\TNO\ '
elif "EC_" in filename_sim:
    savepath = r'M:\home\vinod\nobackup\model_work\MECO\Plots\OMI_comp\EC\ '
elif "RCP_" in filename_sim:
    savepath = r'M:\home\vinod\nobackup\model_work\MECO\Plots\OMI_comp\RCP\ '
#regr = linear_model.LinearRegression()
path_sim = os.path.join(dirname_sim, filename_sim)
f_sim=netCDF4.Dataset(path_sim,'r')
counter = 0
timestamp = f_sim.variables['time']
fv=omitropcol._FillValue
lats=f_sim.variables['lat'][:]
lons=f_sim.variables['lon'][:]
plt.ioff()
for day_num in range(6,12):
    d = day_num-1
    ##for sorbit variables, d-1 woll be used in place of d as it writes the outpur for the end of the day
    date_display=datetime.datetime.strptime(timestamp.units[10:30],'%Y-%m-%d %H:%M:%S')+ datetime.timedelta(days=timestamp[d]-1)
    tracer_conc=f_sim.variables['tracer_gp_'+tracer+'_conc'][d-1,:,:,:]
    ###########calculation of layer height###########
    aps=f_sim.variables['aps'][d-1,:,:]   #surface pressure
    aps.mask = np.ma.nomask
    prs_i = np.array([f_sim.variables['hyai'][i]+(aps[:]*f_sim.variables['hybi'][i]) for i in range(0,35)])
    prs_i[prs_i<0]=np.nan
    h_interface=np.array([7.4*np.log(aps[:]/prs_i[i,:]) for i in range(0,35)])*1000   #in meters calculate h using hybrid sigma coefficients
    #h_interface=f_sim.variables['geopoti_ave'][d,:]/9.8   calculate h using geopotential
    h=np.array([h_interface[i,:]-h_interface[i-1,:] for i in range(1,35)])
    h=h*f_sim.variables['COSMO_tm1'][d-1,:]/250    #correct for assumed scale height at 250K
    #h = np.array([f_sim.variables['grvol'][d,i,:,:]/f_sim.variables['gboxarea'][d,:,:] for i in range(0,34)]) vertical interpolation doesnot work for grid volume
    h=h*100
    h[h<=0]=np.nan
    ######################
    if tracer == 'HCHO':
        TropoPauseLevel = f_sat.variables['tm5_tropopause_layer_index'][d,:,:]
        ak = f_sat.variables['averaging_kernel'][d,:,:,:]
        ap = f_sat.variables['hcho_profile_apriori'][d,:,:,:]   # it is in mol/mol
        aps_sat = f_sat.variables['aps'][d,:,:]*100
        prs_mpnt = np.array([f_sim.variables['hyam'][i]+(aps_sat[:]*f_sim.variables['hybm'][i]) for i in range(0,34)])
        prs_mpnt [prs_mpnt<0]=np.nan
        ap = ap*6.023E23*prs_mpnt/f_sim.variables['COSMO_tm1'][d,:,:,:]/8.314/1E6  #in molecules/cm3
        ap = ap*h #in molecules/cm2
        cf = f_sat.variables['cloud_fraction'][d,:]
        #flag=f_sat.variables['TropColumnFlag'][d,:]  #row anamoly flg
    else:
        TropoPauseLevel = f_sat.variables['TropoPauseLevel'][d,:,:]
        ak = f_sat.variables['AveragingKernel'][d,:,:,:]*f_sat.variables['AveragingKernel'].ScaleFactor   #averaging kernel
        ap = f_sat.variables['TM4profile'][d,:,:,:]*f_sat.variables['TM4profile'].ScaleFactor   #it is in molecules cm-2
        cf = f_sat.variables['CloudFraction'][d,:]*f_sat.variables['CloudFraction'].ScaleFactor
        flag=f_sat.variables['TropColumnFlag'][d,:]  #row anamoly flg
    tropvcd_sat=omitropcol[d,:,:]*sf
    tropvcd_sat[cf>0.3]=np.nan ##cloud fraction
    tropvcd_sat[flag<=-0.5]=np.nan
    tropvcd_sat[tropvcd_sat==fv]=np.nan
    tropvcd_sat[tropvcd_sat<-1E15]=np.nan  ###########################check this
    if "_050s" in filename_sat:
        TropoPauseLevel.mask = np.ma.nomask
    TropoPauseLevel[TropoPauseLevel>34]=0
    ###################plot profile#############
    fig=plt.Figure()
    lat_pt=42
    plt.title('COSMO ' + tracer + ' profiles for ' + date_display.strftime('%d-%b-%Y') + " at latitude " + str(lats[lat_pt]) + (u' \N{DEGREE SIGN}''N'), fontsize=9)
    ax = fig.add_subplot(1, 1, 1)
    ll, hh= np.meshgrid(lons, h_interface[1:12,lat_pt,40]/1000)
    #hh=hh.filled()     ##remove mask
    #cs = plt.pcolormesh(ll, hh, f_sim.variables['tracer_gp_NO2'][d,0:11,lat_pt,:]*1E9, cmap='jet', vmin = 0)
    cs = plt.pcolormesh(ll, hh, tracer_conc[0:11,lat_pt,:], cmap='jet', vmin = 0)
    plt.xlabel("Longitudes", fontsize=10)
    plt.ylabel("Height above ground level (km)", fontsize=10)
    cb=plt.colorbar(extend = 'both')
    cb.set_label(tracer + ' concentration (molecules cm$^{-3}$)' , fontsize=11)
    plt.savefig(savepath + 'profile_'+ date_display.strftime('%Y%m%d') +'.png', format = 'png', dpi = 200)
    plt.close()
    ############# profile at a box
    fig=plt.Figure()
    plt.title(tracer + ' profiles for ' + date_display.strftime('%d-%b-%Y'), fontsize=11)
    lat_range=[40,48]
    lon_range=[16,32]
    sel_mean_sim=np.nanmean(tracer_conc[0:16,lat_range[0]:lat_range[1],lon_range[0]:lon_range[1]],2)
    sel_mean_sim=np.nanmean(sel_mean_sim,1)
    #sel_mean_sim=sel_mean_sim/np.nansum(sel_mean_sim,0)  #for relative profile
    sel_mean_sat=np.nanmean(ap[0:16,lat_range[0]:lat_range[1],lon_range[0]:lon_range[1]]/h[0:16,lat_range[0]:lat_range[1],lon_range[0]:lon_range[1]],2)
    sel_mean_sat=np.nanmean(sel_mean_sat,1)
    #sel_mean_sat=sel_mean_sat/np.nansum(sel_mean_sat,0)  #for relative profile
    plt.scatter(sel_mean_sim,h_interface[1:17,lat_range[0],lon_range[0]]/1000)
    plt.scatter(sel_mean_sat,h_interface[1:17,40,16]/1000)
    plt.gca().legend(('COSMO','OMI'))
    plt.xlabel(tracer + " concentration (molecules cm$^{-3}$)", fontsize=10)
    plt.ylabel("Height (km)", fontsize=10)
    plt.annotate("Lat : %2.1f " % lats[lat_range[0]] + " - : %2.1f " % lats[lat_range[1]] +
                 " N\nLon : %2.1f"  % lons[lon_range[0]] + " - : %2.1f " % lons[lon_range[1]] + " E", xy=(0.2, 0.8),  xycoords='axes fraction')
    plt.savefig(savepath+ 'profilepp_'+ date_display.strftime('%Y%m%d') +'.png', format = 'png', dpi = 200)
    plt.close()
    ##############################################
    tracer_conc[(tracer_conc<=1E7) | (tracer_conc>=1E13)]=np.nan
    #NO2_tropvcdbox = NO2_conc[0:15,:,:]*h[0:15,:,:]*ak[0:15,:,:]
    NO2_tropvcdbox = tracer_conc[:,:,:]*h[:,:,:]  #without averaging kernels
    tracer_tropvcdbox = ap[:,:,:]+ak[:,:,:]*(tracer_conc[:,:,:]*h[:,:,:]-ap[:,:,:])   #with averaging kernels
    tracer_tropvcdbox[(tracer_tropvcdbox>1E18) | (tracer_tropvcdbox<1E7)]=np.nan
    tracer_tropvcd_sim = np.nansum(tracer_tropvcdbox,0)
    for i in range(0,len(lats)):
        for j in range(0,len(lons)):
            tpl = int(round(TropoPauseLevel[i,j]))
            TropoPauseLevel[i,j] = tpl
            tracer_tropvcd_sim[i,j] = np.nansum(tracer_tropvcdbox[0:tpl+1,i,j])
    #NO2_vcd=np.nansum(h*NO2_conc,0)
    tracer_tropvcd_sim[(tracer_tropvcd_sim<=1E12) | (tracer_tropvcd_sim>=1E20)]=np.nan
    tropvcd_pnt1_sim=np.nanmean(tracer_tropvcd_sim[40:48,16:32])  ##model VCD for a given area
    tropvcd_pnt1_sat=np.nanmean(tropvcd_sat[40:48,16:32])*1E-15  ##satellite VCD for a given area
    tropvcd_pnt2_sim=np.nanmean(tracer_tropvcd_sim[44:52,40:56])  ##model VCD for a given area
    tropvcd_pnt2_sat=np.nanmean(tropvcd_sat[44:52,40:56])*1E-15  ##satellite VCD for a given area
    tracer_tropvcd_sim[cf>0.3]=np.nan ##cloud fraction filter on model product for comparison
    tracer_tropvcd_sim[flag<=-0.5]=np.nan
    fig=plt.Figure()
    plt.title('MECO(n) Tropospheric  ' + tracer + '  VCD for ' + date_display.strftime('%d-%b-%Y'))
    tropvcd_sat[np.where(np.isnan(tracer_tropvcd_sim)==True)]=np.nan
    ax = fig.add_subplot(1, 1, 1)
    if "_050s" in filename_sat:
        map = Basemap(projection='cyl', resolution = 'l', llcrnrlon=-60, llcrnrlat=4,urcrnrlon=90, urcrnrlat=80)
        map.drawparallels(np.arange(5.,75.,15.),labels=[1,0,0,0])
        map.drawmeridians(np.arange(-60.,90.,30.),labels=[0,0,0,1])
    else:    
        map = Basemap(projection='cyl', resolution = 'l', llcrnrlon=1, llcrnrlat=43,urcrnrlon=20, urcrnrlat=56)
        map.drawparallels(np.arange(30.,60.,4.),labels=[1,0,0,0])
        map.drawmeridians(np.arange(0.,20.,4.),labels=[0,0,0,1])
    map.drawcoastlines()
    map.drawcountries()
    map.drawmapboundary()
    llons,llats = map(lons, lats)
    x, y= np.meshgrid(llons, llats)
    cs = map.pcolormesh(x, y, tracer_tropvcd_sim*1E-15, cmap='jet', vmin = 0, vmax = 12)
    cb=map.colorbar(extend = 'both')
    cb.set_label(tracer + '_Trop_col * 10$^{15}$ molecules cm$^{-2}$' , fontsize=11)
    ############ wind vectors ###############
    um1=f_sim.variables['COSMO_um1'][d-1,0,:,:]
    vm1=f_sim.variables['COSMO_vm1'][d-1,0,:,:]
    ws= np.sqrt(um1*um1 + vm1*vm1)
    xx = np.arange(0, llons.shape[0], 4)
    yy = np.arange(0, llats.shape[0], 4)
    points = np.meshgrid(yy, xx)
    map.quiver(x[points],y[points],um1[points],vm1[points],ws[points],latlon=True, cmap=plt.cm.autumn)
    #######################################
    plt.savefig(savepath + 'MECO_VCD_3_'+ date_display.strftime('%Y%m%d') +'.png', format = 'png', dpi = 200)
    plt.close()
    #plot satellite vcd
    fig=plt.Figure()
    plt.title('OMI Tropospheric ' + tracer + ' VCD for ' + date_display.strftime('%d-%b-%Y'))
    ax = fig.add_subplot(1, 1, 1)
    if "_050s" in filename_sat:
        map = Basemap(projection='cyl', resolution = 'l', llcrnrlon=-60, llcrnrlat=4,urcrnrlon=90, urcrnrlat=80)
        map.drawparallels(np.arange(5.,75.,15.),labels=[1,0,0,0])
        map.drawmeridians(np.arange(-60.,90.,30.),labels=[0,0,0,1])
    else:    
        map = Basemap(projection='cyl', resolution = 'l', llcrnrlon=1, llcrnrlat=43,urcrnrlon=20, urcrnrlat=56)
        map.drawparallels(np.arange(30.,60.,4.),labels=[1,0,0,0])
        map.drawmeridians(np.arange(0.,20.,4.),labels=[0,0,0,1])
    map.drawcoastlines()
    map.drawcountries()
    map.drawmapboundary()
    cs = map.pcolormesh(x, y, tropvcd_sat*1E-15, cmap='jet', vmin = 0, vmax = 12)
    cb=map.colorbar(extend = 'both')
    cb.set_label(tracer + '_Trop_col * 10$^{15}$ molecules cm$^{-2}$' , fontsize=10)
    plt.savefig(savepath + 'OMI_VCD_3'+ date_display.strftime('%Y%m%d') +'.png', format = 'png', dpi = 200)
    plt.close()
#########calculate and plot average over given number of days###########
    if counter == 0:
        mean_meco=tracer_tropvcd_sim[:,:,np.newaxis]
        mean_omi=tropvcd_sat[:,:,np.newaxis]
        ts=np.array([d,tropvcd_pnt1_sim*1E-15,tropvcd_pnt1_sat,tropvcd_pnt2_sim*1E-15,tropvcd_pnt2_sat])
    else:
        mean_meco=np.ma.dstack((mean_meco, tracer_tropvcd_sim))
        mean_omi=np.ma.dstack((mean_omi, tropvcd_sat))
        ts=np.vstack((ts,np.array([d,tropvcd_pnt1_sim*1E-15,tropvcd_pnt1_sat,tropvcd_pnt2_sim*1E-15,tropvcd_pnt2_sat])))
    counter=counter+1
    mean_omi[mean_omi==fv]=np.nan
################################time series at selected box##########
fig = plt.figure(figsize=[9,5])  # a new figure window
ax = fig.add_subplot(1, 1, 1)  # specify (nrows, ncols, axnum)
ax.plot(ts[:,0]+1, ts[:,1],'b+--' )
ax.plot(ts[:,0]+1, ts[:,2],'r+--')
ax.plot(ts[:,0]+1, ts[:,3],'b.-')
ax.plot(ts[:,0]+1, ts[:,4],'r.-')
plt.gca().legend(('COSMO (h)','OMI (h)','COSMO (l)','OMI (l)'))
plt.ylabel(tracer + " VCD (molecules cm$^{-3}$)", fontsize=10)
plt.xlabel("Day of month (April 2015)", fontsize=10)
plt.annotate("Lat : 50 - 52 $^\circ$N\nLon : 04 - 08 $^\circ$E", xy=(0.6, 0.9),  xycoords='axes fraction')
plt.annotate("Lat : 51 - 53 $^\circ$N\nLon : 10 - 14 $^\circ$E", xy=(0.6, 0.8),  xycoords='axes fraction')
ax.set_xlim(-0.3,29.3)
plt.minorticks_on()
plt.savefig(savepath +'ts'+'.png', format = 'png', dpi = 200)
plt.close()
################################Overview plot of all days with correlation##########
plt.ion()
fig, axes = plt.subplots(nrows=2, ncols=2, figsize=[11,9])
plt.suptitle('Comparisons of Tropospheric ' + tracer + ' VCD for ' + date_display.strftime('%b-%Y'))
axes[0, 0].set_title( 'COSMO ' + tracer + '_Trop_VCD (*1E15 molecules cm-2)', fontsize=10)
map = Basemap(projection='cyl', resolution = 'l', llcrnrlon=1, llcrnrlat=43,urcrnrlon=20, urcrnrlat=56, ax=axes[0,0])
map.drawparallels(np.arange(30.,60.,4.),labels=[1,0,0,0])
map.drawmeridians(np.arange(0.,20.,4.),labels=[0,0,0,1])
map.drawcoastlines()
map.drawcountries()
map.drawmapboundary()
#cs1 = map.pcolormesh(x, y, NO2_tropvcd*1E-15, cmap='jet', vmin = 0, vmax = 12)  #for indicidual days
cs1 = map.pcolormesh(x, y, np.nanmean(mean_meco,2)*1E-15, cmap='jet', vmin = 0, vmax = 12)
cb1=fig.colorbar(cs1, cmap=plt.cm.get_cmap('jet'), ax=axes[0,0], orientation='vertical',extend = 'both', fraction = 0.09, shrink = 0.8)
################################
axes[0, 1].set_title('OMI ' + tracer + '_Trop_VCD (*1E15 molecules cm-2)',fontsize=10)
map = Basemap(projection='cyl', resolution = 'l', llcrnrlon=1, llcrnrlat=43,urcrnrlon=20, urcrnrlat=56, ax=axes[0,1])
map.drawparallels(np.arange(30.,60.,4.),labels=[1,0,0,0])
map.drawmeridians(np.arange(0.,20.,4.),labels=[0,0,0,1])
map.drawcoastlines()
map.drawcountries()
map.drawmapboundary()
#cs2 = map.pcolormesh(x, y, V0, cmap='jet', vmin = 0, vmax = 12) #for indicidual days
cs2 = map.pcolormesh(x, y, np.nanmean(mean_omi,2)*1E-15, cmap='jet', vmin = 0, vmax = 12)
cb2=fig.colorbar(cs2, cmap=plt.cm.get_cmap('jet'), ax=axes[0,1], orientation='vertical',extend = 'both', fraction = 0.09, shrink = 0.8)
################################
axes[1,0].set_title( '(COSMO-OMI)' + tracer + '_Trop_VCD(*1E15 molecules cm-2)', fontsize=10)
map = Basemap(projection='cyl', resolution = 'l', llcrnrlon=1, llcrnrlat=43,urcrnrlon=20, urcrnrlat=56, ax=axes[1,0])
map.drawparallels(np.arange(30.,60.,4.),labels=[1,0,0,0])
map.drawmeridians(np.arange(0.,20.,4.),labels=[0,0,0,1])
map.drawcoastlines()
map.drawcountries()
map.drawmapboundary()
#cs3 = map.pcolormesh(x, y, NO2_tropvcd*1E-15-V0, cmap='jet', vmin = -4, vmax = 4)   #for indicidual days
cs3 = map.pcolormesh(x, y, np.nanmean(mean_meco,2)*1E-15-np.nanmean(mean_omi,2)*1E-15, cmap='jet', vmin = -4, vmax = 4)
cb3=fig.colorbar(cs3, cmap=plt.cm.get_cmap('jet'), ax=axes[1,0], orientation='vertical',extend = 'both', fraction = 0.1, shrink = 0.8)
################################
axes[1,1].set_title( 'Correlation')
#VCD_sim_flat=NO2_tropvcd.flatten()*1E-15 #for indicidual days
#VCD_sat_flat=V0.flatten() #for indicidual days
VCD_sim_flat=np.nanmean(mean_meco,2).flatten()*1E-15
VCD_sat_flat=np.nanmean(mean_omi,2).flatten()*1E-15
idx = np.isfinite(VCD_sat_flat) & np.isfinite(VCD_sim_flat)
#fit_param=np.polyfit(VCD_sat_flat[idx], VCD_sim_flat[idx],1)
fit_param = stats.linregress(VCD_sat_flat[idx], VCD_sim_flat[idx])[0:3]
fit_func=np.poly1d(fit_param[0:2])
r_sqr=fit_param[2]**2
axes[1,1].scatter(VCD_sat_flat,VCD_sim_flat, s=2)
axes[1,1].plot(VCD_sat_flat[idx], fit_func(VCD_sat_flat[idx]), color = "black", linewidth=2)
#axes[1,1].plot(VCD_sat_flat[idx], np.poly1d([1,0])(VCD_sat_flat[idx]), linewidth=1)
axes[1,1].plot(VCD_sat_flat[idx], np.poly1d([1,0])(VCD_sat_flat[idx]), color = "red", linewidth=1, linestyle = ":")
#axes[1,1].fill_between(VCD_sat_flat[idx], np.poly1d([0.8,0])(VCD_sat_flat[idx]), np.poly1d([1.2,0])(VCD_sat_flat[idx]), color = 'red', alpha = 0.5 )
axes[1,1].plot(VCD_sat_flat[idx], np.poly1d([1.2,0])(VCD_sat_flat[idx]), color = "red", linewidth=1, linestyle = ":", alpha=0.7)
axes[1,1].plot(VCD_sat_flat[idx], np.poly1d([0.8,0])(VCD_sat_flat[idx]), color = "red", linewidth=1, linestyle = ":", alpha=0.7)
axes[1,1].annotate("y = " + str('%.1f' % fit_param[0]) + "x + "+ str('%.1f' % fit_param[1]) + "\n r$^{2}$ = " + str('%.2f' % r_sqr), xy=(0.1, 0.8),  xycoords='axes fraction')
axes[1,1].set_xlabel("OMI "+ tracer + " VCD (*10$^{15}$ molec. cm$^{-2}$)", fontsize=10)
axes[1,1].set_ylabel("COSMO "+ tracer + " VCD (*10$^{15}$ molec. cm$^{-2}$)", fontsize=10)
plt.tight_layout()
plt.show()
plt.savefig(savepath +'summary_comp'+'.png', format = 'png', dpi = 200)