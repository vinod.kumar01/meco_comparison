# -*- coding: utf-8 -*-
"""
Created on Wed Jun 24 23:55:15 2020

@author: Vinod
"""
from os import path
import pandas as pd
import netCDF4
from datetime import datetime, timedelta
from scipy.interpolate import griddata
import numpy as np
import matplotlib.pyplot as plt
import skill_metrics as sm
from tools import lin_fit, wind
from plot_tools import map_prop, ccrs
from pyorbital.astronomy import sun_zenith_angle
# %% load measurement data
plot_station = False
sel_day = False
sel_night = False
meas_dir = r'M:\nobackup\vinod\model_work\MECO\station_data\cdc_temp_may_2018\data'
tracer = "ws"
filename = {"tm1": "TT_TU_MN009.csv", "rhum": "RF_TU_MN009.csv",
            "um1": "F_MN003.csv", "vm1": "F_MN003.csv", "ws": "F_MN003.csv"}
coord_file = 'sdo_' + filename[tracer]
stn_coords = pd.read_csv(path.join(meas_dir, coord_file))
stn_coords = stn_coords.set_index('SDO_ID')
#
meas_file = 'data_' + filename[tracer]
df = pd.read_csv(path.join(meas_dir, meas_file))
if tracer in ['um1', 'vm1']:
    df['wd'] = pd.read_csv(path.join(meas_dir,
                                     'data_D_MN003.csv'))['Wert']
    winds = wind(ws=df['Wert'], wd=df['wd'])
    df['wind_u'], df['wind_v'] = winds.sep_wind()
    df[tracer] = df['wind_u'] if tracer == 'um1' else df['wind_v']
else:
    df[tracer] = df['Wert']
df['Date_time'] = pd.to_datetime(df['Zeitstempel'], format='%Y%m%d%H%M%S')
# model data at end hour while meas data at start hour
df['Date_time'] = [i + timedelta(hours=1) for i in df['Date_time']]
# df['SZA'] = df.apply(lambda x: sun_zenith_angle(x['Date_time'], 8., 50.),
#                       axis=1)
sitelist = df['SDO_ID'].unique()
target_lat, target_lon = [], []
for site in sitelist:
    target_lon.append(stn_coords['Geogr_Laenge'][site])
    target_lat.append(stn_coords['Geogr_Breite'][site])

# %% Model data
sim_dir = r'M:\nobackup\vinod\model_work\MECO\DOAS_COMP'
f = netCDF4.Dataset(path.join(sim_dir, 'UBA_di_18_5_tr_1hr_sfc.nc'))
timeax = f.variables['time'][:]
try:
    time_unit = f.variables['time'].units[11:30]
    time_dsp = [datetime.strptime(time_unit,
                                  '%Y-%m-%d %H:%M:%S') + timedelta(days=i)
                for i in timeax]
except ValueError:
    time_unit = f.variables['time'].units[10:30]
    time_dsp = [datetime.strptime(time_unit,
                                  '%Y-%m-%d %H:%M:%S') + timedelta(days=i)
                for i in timeax]
geolon = f.variables['lon'][:]
geolat = f.variables['lat'][:]
if tracer == 'ws':
    sim_tracer = np.sqrt(f.variables['um1_ave'][:, 0, :]**2 +
                         f.variables['um1_ave'][:, 0, :]**2)
else:
    sim_tracer = f.variables[tracer+'_ave'][:, 0, :]
dict_comp = {s: [] for s in sitelist}
dict_comp['Date_time'] = []
for i in range(sim_tracer.shape[0]):
    dict_comp['Date_time'].append(time_dsp[i])
    val_now = griddata((geolon.ravel(), geolat.ravel()),
                       sim_tracer[i, :].ravel(), (target_lon, target_lat),
                       method='linear')
    for s, site in enumerate(sitelist):
        dict_comp[site].append(val_now[s])
for key in dict_comp:
    dict_comp[key] = np.array(dict_comp[key])
# %% plotting and comparison
fit_summary = {'lat': [], 'lon': [], 'meas': [],
               'm': [], 'r': [], 'c': [], 'rmse': [], 'bias': []}
taylor_stats = {}
plt.ioff()
dt_index = pd.to_datetime(dict_comp['Date_time'])
SZA = np.asarray(list(map(lambda x: sun_zenith_angle(x, 8., 50.),
                          dt_index)))
if sel_day:
    idx_sel = SZA < 85
elif sel_night:
    idx_sel = SZA > 85
else:
    idx_sel = SZA > 0
for site in sitelist:
    data_sel = df[df['SDO_ID'] == site]
    if len(data_sel) == 0:
        continue
    data_sel = data_sel.set_index('Date_time')
    data_sel = data_sel.reindex(dt_index)
    if plot_station:
        fig, ax = plt.subplots(figsize=[11, 4])
        ax.scatter(dict_comp['Date_time'], dict_comp[site]-273.15,
                   s=4, label='MECO(4)')
        ax.scatter(data_sel.index, data_sel[tracer], s=4, label='Measurement')
        ax.legend(loc='upper right')
        ax.text(0.02, 0.9, '{}\n{:.2f}E {:.2f}N'.format(stn_coords['SDO_Name'][site],
                stn_coords['Geogr_Laenge'][site], stn_coords['Geogr_Breite'][site]),
                transform=ax.transAxes)
        ax.grid(alpha=0.4, axis='y')
        ax.set_ylabel('Surface temperature ($^{\circ}$C)')
        plt.tight_layout()
        plt.savefig(path.join(meas_dir, 'plots', 'comp_{}.png'.format(site)),
                    format='png', dpi=100)
        plt.close()
    y = dict_comp[site]-273.15 if tracer == "tm1" else dict_comp[site]
    x = data_sel[tracer].values
    y = y[idx_sel]
    x = x[idx_sel]
    mask = np.isnan(x) | np.isnan(y)
    if np.sum(~mask) != 0:
        fit_data = lin_fit(x[~mask], y[~mask])
        fit_stats = fit_data.stats_calc()
        fit_summary['lat'].append(stn_coords['Geogr_Breite'][site])
        fit_summary['lon'].append(stn_coords['Geogr_Laenge'][site])
        fit_summary['m'].append(fit_stats['m'])
        fit_summary['r'].append(fit_stats['r'])
        fit_summary['c'].append(fit_stats['c'])
        fit_summary['rmse'].append(fit_stats['rmse'])
        fit_summary['bias'].append(fit_stats['bias'])
        fit_summary['meas'].append(np.mean(x[~mask]))
        taylor_stats[site] = sm.taylor_statistics(y[~mask], x[~mask])
plt.ion()
# %% plot overall stats on a map
loc = [{'lon': 8.2283, 'lat': 49.9909, 'site': 'MPIC'},
       {'lon': 8.68, 'lat': 50.11, 'site': 'Frankfurt'}]
llon, ulon, llat, ulat = 5.9, 11.2, 48.3, 52.05
prop_dict = {'extent': [llon, ulon, llat, ulat],
             'name': ' ',
             'border_res': '10m',
             'bkg': None}
map2plot = map_prop(**prop_dict)
projection = ccrs.PlateCarree()  # ccrs.Orthographic(8, 40)
for param, name in {'r': 'pearson correlation coefficient',
                    'm': 'slope'}.items():
    datadict = {'lat': fit_summary['lat'], 'lon': fit_summary['lon'],
                'plotable': fit_summary[param],
                'vmin': 0.5, 'vmax': 0.94, 'label': '',
                'text': name}
    fig, ax = plt.subplots(subplot_kw=dict(projection=projection))
    # plt.title('Surface temperature')
    scat = map2plot.plot_map(datadict, ax, cmap='jet', projection=projection,
                             alpha=1, mode='scatter', s=30)
    cb = fig.colorbar(scat, ax=ax, extend='both',
                      fraction=0.07, shrink=0.7, pad=0.07)
    cb.set_label(datadict['text'], fontsize=11)
    for point in loc:
        lo, la = point['lon'], point['lat']
        plt.annotate(point['site'], xy=(lo, la),  xycoords='data',
                     xytext=(lo, la), textcoords='data', color='k',
                     horizontalalignment='left', verticalalignment='bottom',
                     arrowprops=dict(facecolor='black', shrink=0.05))
        plt.plot(lo, la, 'ko')
    fig.canvas.draw()
    plt.tight_layout()
    plt.savefig(path.join(meas_dir, 'plots',
                          'map_{}_{}_meco3.png'.format(param, tracer)),
                format='png', dpi=300)
fig, ax = plt.subplots(subplot_kw=dict(projection=projection))
datadict = {'lat': geolat, 'lon': geolon,
            'plotable': np.nanmean(sim_tracer, 0),
            'vmin': 50, 'vmax': 90, 'label': '',
            'text': 'Relative humidity (%)'}
if tracer == "tm1":
    datadict.update({'vmin': 8, 'vmax': 20,
                     'plotable': np.nanmean(sim_tracer, 0)-273.15,
                     'text': '2m tenperature ($^\circ$C)'})
scat = map2plot.plot_map(datadict, ax, cmap='jet', projection=projection,
                         alpha=0.8, mode='pcolormesh')
cb = fig.colorbar(scat, ax=ax, extend='both',
                  fraction=0.07, shrink=0.7, pad=0.07)
cb.set_label(datadict['text'], fontsize=11)
ax.scatter(fit_summary['lon'], fit_summary['lat'], c=fit_summary['meas'],
           cmap='jet', s=30, alpha=1, edgecolor='k',
           vmin=datadict['vmin'], vmax=datadict['vmax'])
fig.canvas.draw()
plt.tight_layout()
# plt.savefig(path.join(meas_dir, 'plots',
#                       'map_{}_meco3.png'.format(tracer)),
#             format='png', dpi=300)
# %% taylor diagram
bias = [0]
bias.extend(fit_summary['bias'])
sdev = [1]
sdev.extend([vals['sdev'][1]/vals['sdev'][0]
             for key, vals in taylor_stats.items()])
sdev = np.array(sdev)
crmsd = [0]
crmsd.extend([vals['crmsd'][1] for key, vals in taylor_stats.items()])
crmsd = np.asarray(crmsd)
ccoef = [1]
ccoef.extend([vals['ccoef'][1] for key, vals in taylor_stats.items()])
fig, ax = plt.subplots(figsize=[6, 6])
# sm.taylor_diagram(sdev, crmsd, np.array(ccoef),
#                   widthRMS=0.5, widthCOR=0.5, axismax=2,
#                   styleCOR='--',
#                   colormap='on', cmap='RdBu_r', cmap_vmin=-6, cmap_vmax=6,
#                   cmap_marker='o', markerDisplayed='colorBar',
#                   titleColorbar='bias ($^{\circ}C$)', cmapzdata=np.array(bias),
#                   colOBS='r', markerobs='o', titleOBS='observation')
sm.taylor_diagram(np.array(sdev), np.array(crmsd), np.array(ccoef),
                  widthRMS=0.5, widthCOR=0.5, axismax=2,
                  styleCOR='--',
                  colormap='on', cmap='RdBu_r', cmap_vmin=-12, cmap_vmax=12,
                  cmap_marker='o', markerDisplayed='colorBar',
                  titleColorbar='bias ($\%$)', cmapzdata=np.array(bias),
                  colOBS='r', markerobs='o', titleOBS='observation')
ax.set_ylabel('Standard Deviation (Normalized)')
# plt.savefig(path.join(meas_dir, 'plots',
#                       'taylor_{}_meco3.png'.format(tracer)),
#             format='png', dpi=300)
