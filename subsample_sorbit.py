# -*- coding: utf-8 -*-
"""
Created on Tue Sep  3 17:58:38 2019

@author: Vinod
for speed reasons, I have used a fixed tropopause layer number for model VCD
Save temperature and pressure in final output
"""
import pandas as pd
from os import path
import glob
import numpy as np
from scipy.interpolate import griddata
from scipy.interpolate.interpnd import _ndim_coords_from_arrays
from scipy.spatial import cKDTree
from plot_tools import plt, map_prop, ccrs
from tools import netCDF4, lin_fit, datetime
from mod_colormap import add_white

new_cm = add_white('rainbow', replace_frac=0.01, start=None, transparent=False)
llon, ulon, llat, ulat = 0, 19, 43, 56
prop_dict = {'extent': [llon, ulon, llat, ulat], 'name': 'MECO_DE',
             'border_res': '10m'}
projection = ccrs.PlateCarree()
map2plot = map_prop(**prop_dict)


def calc_vcd_sim(sim):
    aps = sim.variables['aps'][:]

    prs_i = np.array([sim.variables['hyai'][i] +
                      (aps[:]*sim.variables['hybi'][i])
                      for i in range(0, 35)])
    h_interface = np.array([7.4*np.log(aps[:]/prs_i[i, :])
                            for i in range(0, 35)])*1000
    h = np.array([h_interface[i, :]-h_interface[i-1, :]
                  for i in range(1, 35)])
    h = np.transpose(h, (1, 0, 2, 3))
    tm_c = sim.variables['COSMO_tm1'][:]
    h = h*tm_c[:]/250    # correct for assumed scale height at 250K
    h = h*100
    '''
    calculation of box vcd
    '''
    tropvcdbox = no2_c*h
    tropvcd_sim = np.sum(tropvcdbox[:, 0:16, :], 1)
    return tropvcd_sim


# %% import files
plt.ioff()
tic = datetime.now()
exp = 'UBA_di'
year = 2018
month = 5
st_day, end_day = 1, 31
qa_filter = 0.75
sorbit_wf = np.genfromtxt(r'D:\postdoc\COSMO-CCLM\TROPOMI_comp\sorbit_wf.txt')
dirname_sim = r'M:\nobackup\vinod\model_work\MECO\sorbit_subsample'
dirname_sat = r'M:\nobackup\cborger\TROPOMI\v1\L2\NO2'
savepath = path.join(dirname_sim, '{}_{}'.format(year, str(month).zfill(2)),
                     exp, 'qa_{}'.format(int(qa_filter*100)))
sorbit_c = '{}_{}_sorbit_s5a_inc_border.nc'.format(exp, str(month).zfill(2))
sorbit_l = '{}_{}_sorbit_s5al_inc_border.nc'.format(exp, str(month).zfill(2))
sorbit_r = '{}_{}_sorbit_s5ar_inc_border.nc'.format(exp, str(month).zfill(2))
with netCDF4.Dataset(path.join(dirname_sim, sorbit_c)) as sim_c:
    glon = sim_c.variables['glon'][:]
    glat = sim_c.variables['glat'][:]
    model_lon = sim_c.variables['lon'][:]
    model_lat = sim_c.variables['lat'][:]
    hyai = sim_c.variables['hyai'][:]
    hybi = sim_c.variables['hybi'][:]
    hyam = sim_c.variables['hyam'][:]
    hybm = sim_c.variables['hybm'][:]
    lat_bound = [43.5, 55.25]
    lon_bound = [0.75, 18.25]
    no2_c = sim_c.variables['tracer_gp_NO2_conc'][:]
    tm_c = sim_c.variables['COSMO_tm1'][:]
    um1 = sim_c.variables['COSMO_um1'][:, :13, :]  # only upto ~1km
    vm1 = sim_c.variables['COSMO_vm1'][:, :13, :]  # only upto ~1km
    aps = sim_c.variables['aps'][:]
    tropvcd_sim_orig = 1e-16*calc_vcd_sim(sim_c)
    try:
        hsurf = sim_c.variables['HSURF_ave'][:]
        pblh = sim_c.variables['tropop_pblh'][:]
    except KeyError:
        print('PBLH and HSURF not available in sorbit output')


with netCDF4.Dataset(path.join(dirname_sim, sorbit_l)) as sim_l:
    no2_l = sim_l.variables['tracer_gp_NO2_conc'][:]
with netCDF4.Dataset(path.join(dirname_sim, sorbit_r)) as sim_r:
    no2_r = sim_r.variables['tracer_gp_NO2_conc'][:]

# idx_gpx = np.append(np.arange(0, 30), np.arange(420,450))
idx_gpx = np.arange(0, 450)
# select indices of groud pixel. use 0:450 for all
sorbit_wf = sorbit_wf[idx_gpx, :]
fit_data = pd.DataFrame(columns=['day', 'meas_tropomi', 'sim_mean', 'slope',
                                 'r', 'c', 'bias', 'rmse'])
ocnt = 0
for days in range(st_day, end_day+1):
    day = days-1
    no2_conc_l = no2_l[day, :]
    no2_conc_c = no2_c[day, :]
    no2_conc_r = no2_r[day, :]
    tm = tm_c[day, :]

# %% loop through days and orbits
    f_names_sat = glob.glob(path.join(dirname_sat, str(year),
                                      str(month).zfill(2), str(days).zfill(2),
                                      'S5P_????_L2__NO2____' + str(year) + '*.nc'))
    counter_swath = 0
    counter_imp_swath = 0
    for path_sat in f_names_sat:
        counter_swath += 1
        orbit_num = path.basename(path_sat).split('_')[-4]
        f = netCDF4.Dataset(path_sat, 'r')
        Data_fields = f.groups['PRODUCT']
        NO2_tropcol = Data_fields.variables['nitrogendioxide_tropospheric_column'][0, :, idx_gpx]
        lats = Data_fields.variables['latitude'][0, :, idx_gpx]
        lons = Data_fields.variables['longitude'][0, :, idx_gpx]
        scanline, ground_pix = lats.shape
        keep = (lats > lat_bound[0]) & (lats < lat_bound[1])
        keep &= (lons > lon_bound[0]) & (lons < lon_bound[1])
        NO2_tropcol = NO2_tropcol[keep]
        lats = lats[keep]
        lons = lons[keep]
        if np.ma.masked_array.count(NO2_tropcol) > 1:
            print('Orbit '+orbit_num)
            print(np.ma.masked_array.count(NO2_tropcol))
            fv = Data_fields.variables['nitrogendioxide_tropospheric_column']._FillValue
            averaging_kernel = Data_fields.variables['averaging_kernel'][0, :, idx_gpx]
            qa_value = Data_fields.variables['qa_value'][0, :, idx_gpx]
            amf_trop = Data_fields.variables['air_mass_factor_troposphere'][0, :, idx_gpx]
            amf_total = Data_fields.variables['air_mass_factor_total'][0, :, idx_gpx]
            trop_idx = Data_fields.variables['tm5_tropopause_layer_index'][0, :, idx_gpx]
            cloud_frac = Data_fields.groups['SUPPORT_DATA'].groups['DETAILED_RESULTS'].variables['cloud_radiance_fraction_nitrogendioxide_window'][0, :, idx_gpx]
            ps_sat = Data_fields.groups['SUPPORT_DATA'].groups['INPUT_DATA'].variables['surface_pressure'][0, :, idx_gpx]
            ak = []
            for i in range(34):
                ak.append(averaging_kernel[:, :, i][keep])
            averaging_kernel = np.array(ak)
            averaging_kernel = np.ma.masked_where(averaging_kernel == fv,
                                                  averaging_kernel)
            sat_points = np.vstack((lons, lats)).T
            qa_value = qa_value[keep]
            NO2_tropcol = np.ma.masked_where(qa_value < qa_filter, NO2_tropcol)
            amf_trop = amf_trop[keep]
            amf_total = amf_total[keep]
            trop_idx = trop_idx[keep]
            cloud_frac = cloud_frac[keep]
            ps_sat = ps_sat[keep]
            sat_tropcol = NO2_tropcol*6.02214e+19/1E16
            tropSCD_sat = sat_tropcol*amf_trop
            sorbit_wf_3d = np.tile(sorbit_wf, (scanline, 1, 1))
            sorbit_wf_3d = np.transpose(sorbit_wf_3d, (2, 0, 1))
            sorbit_wf_keep = np.array([sorbit_wf_3d[k][keep]
                                       for k in range(0, 3)])

# %% interpolate  model data on satellite grids. Apply the scaling factor to
# get a combined sorbit interpolated data
# faster way, filter the relevant lat lon before interpolation
            no2_intp_l, no2_intp_c, no2_intp_r = [], [], []
            tm1_intp = []
            for lev in range(34):
                no2_intp_ll = griddata((model_lon.ravel(), model_lat.ravel()),
                                       no2_conc_l[lev, :].ravel(),
                                       (lons, lats), method='linear')
                no2_intp_cl = griddata((model_lon.ravel(), model_lat.ravel()),
                                       no2_conc_c[lev, :].ravel(),
                                       (lons, lats), method='linear')
                no2_intp_rl = griddata((model_lon.ravel(), model_lat.ravel()),
                                       no2_conc_r[lev, :].ravel(),
                                       (lons, lats), method='linear')
                tm1_intp_l = griddata((model_lon.ravel(), model_lat.ravel()),
                                      tm[lev, :].ravel(),
                                      (lons, lats), method='linear')
                no2_intp_l.append(no2_intp_ll)
                no2_intp_c.append(no2_intp_cl)
                no2_intp_r.append(no2_intp_rl)
                tm1_intp.append(tm1_intp_l)
            no2_intp_l = np.ma.masked_where(np.array(no2_intp_l) < 0,
                                            np.array(no2_intp_l))
            no2_intp_c = np.ma.masked_where(np.array(no2_intp_c) < 0,
                                            np.array(no2_intp_c))
            no2_intp_r = np.ma.masked_where(np.array(no2_intp_r) < 0,
                                            np.array(no2_intp_r))
            tm1_intp = np.ma.masked_where(np.array(tm1_intp) < 0,
                                          np.array(tm1_intp))
            no2_intp_final = np.ma.masked_array([(no2_intp_l[lev, :]*sorbit_wf_keep[0, :]+
                                                  no2_intp_c[lev, :]*sorbit_wf_keep[1, :]+
                                                  no2_intp_r[lev, :]*sorbit_wf_keep[2, :])
                                                for lev in range(0, 34)])

# %% calculation of VCDs
            '''
            calculation of box heights
            '''
            prs_i = np.array([hyai[i] + (ps_sat[:]*hybi[i])
                              for i in range(0, 35)])
            h_interface = np.array([7.4*np.log(ps_sat[:]/prs_i[i, :])
                                    for i in range(0, 35)])*1000
            h = np.array([h_interface[i, :]-h_interface[i-1, :]
                          for i in range(1, 35)])
            h = h*tm1_intp[:]/250    # correct for assumed scale height at 250K
            h = h*100
            '''
            calculation of box vcd
            '''
            tropvcdbox = no2_intp_c*h
            tropvcdbox_l = no2_intp_l*h
            tropvcdbox_r = no2_intp_r*h
            tropvcdbox_i = no2_intp_final*h
            tropvcd_sim = np.sum(tropvcdbox[0:16, :], 0)
            # trp_idx_fill = trop_idx.filled(0)
            # for i in range(0,len(llats)):
            #     tpl = trp_idx_fill[i]
            #     tropvcd_sim[i] = np.sum(tropvcdbox[0:tpl+1])
            trop_profile_sim = np.array([tropvcdbox[i, :]/tropvcd_sim[:]
                                         for i in range(0, 34)])
            mask = np.ma.getmask(sat_tropcol)
            # tropvcd_sim = np.ma.masked_where(mask, tropvcd_sim)
            trop_profile_sim = np.ma.masked_where(trop_profile_sim == np.nan,
                                                  trop_profile_sim)
            trop_amf_sim = np.ma.divide(np.ma.sum(np.ma.multiply(trop_profile_sim,
                                                                 averaging_kernel),0)*amf_trop,np.sum(trop_profile_sim,0))
            trop_amf_scale = np.ma.sum(trop_profile_sim*averaging_kernel,0)*amf_trop/np.ma.sum(trop_profile_sim,0)
            trp_idx_fill = trop_idx.filled(0)
            for i in range(len(lats)):
                tpl = trp_idx_fill[i]
                trop_amf_scale[i] = np.ma.divide(np.ma.sum(trop_profile_sim[0:tpl+1,i]*averaging_kernel[0:tpl+1,i]),
                                                 np.ma.sum(trop_profile_sim[0:tpl+1,i]))
            trop_amf_sim = trop_amf_scale*amf_total
            amf_trop = np.ma.masked_where(np.ma.getmask(trop_amf_sim), amf_trop)
            tropVCD_satcorr = np.ma.divide(tropSCD_sat, trop_amf_sim)
            # sat_tropcol = np.ma.masked_where(np.ma.getmask(tropvcd_sim), sat_tropcol)
            tropVCD_satcorr = np.ma.masked_where(mask, tropVCD_satcorr)
            tropvcd_sim = np.ma.masked_invalid(tropvcd_sim)
            trop_amf_sim = np.ma.masked_where(mask, trop_amf_sim)
            amf_trop = np.ma.masked_where(mask, amf_trop)
            # calculate VCD for left, right and interpolated
            tropvcd_sim_l = np.sum(tropvcdbox_l[0:16, :], 0)
            tropvcd_sim_l = np.ma.masked_where(mask,
                                               tropvcd_sim_l)
            tropvcd_sim_r = np.sum(tropvcdbox_r[0:16, :], 0)
            tropvcd_sim_r = np.ma.masked_where(mask,
                                               tropvcd_sim_r)
            tropvcd_sim_i = np.sum(tropvcdbox_i[0:16, :], 0)
            tropvcd_sim_i = np.ma.masked_where(mask,
                                               tropvcd_sim_i)
            SORBIT = {}
            SORBIT['L'] = tropvcd_sim_l
            SORBIT['C'] = tropvcd_sim
            SORBIT['R'] = tropvcd_sim_r
            SORBIT['I'] = tropvcd_sim_i

            idx = np.isfinite(tropVCD_satcorr) & np.isfinite(tropvcd_sim_i)
            fit_stats = lin_fit(tropVCD_satcorr[idx], 1e-16*tropvcd_sim_i[idx])
            fit_param = fit_stats.stats_calc()
            fit_func = np.poly1d([fit_param['m'], fit_param['c']])
            fit_data.loc[ocnt] = [day, np.mean(tropVCD_satcorr[idx]),
                                  1e-16*np.mean(tropvcd_sim_i[idx]),
                                  fit_param['m'],
                                  fit_param['r'], fit_param['c'],
                                  fit_param['bias'], fit_param['rmse']]
            ocnt += 1
            fig, ax = plt.subplots()
            cs = ax.hexbin(tropVCD_satcorr, tropvcd_sim_i*1e-16,
                           gridsize=200, cmap=new_cm)
            ax.set_ylim(0, 1.4)
            ax.set_xlim(0, 1.4)
            ax.plot([0, 1], [0, 1], transform=ax.transAxes,
                    ls="--", c=".3", label='1:1')
            ax.fill_between([0, 1], [0, 1.3], [0, 1], transform=ax.transAxes,
                            ls=":", color=".3", alpha=.2, label='$\pm 30\%$')
            ax.fill_between([0, 1], [0, 0.7], [0, 1], transform=ax.transAxes,
                            ls=":", color=".3", alpha=.2, label=None)
            ax.plot(tropVCD_satcorr.ravel()[idx],
                    fit_func(tropVCD_satcorr.ravel()[idx]),
                    c="k", label='linear regression')
            fit_stats.add_fit_param(pos_x=0.6, ax=ax, showr=True)
            ax.set_ylabel('Simulated VCD (10$^{16}$ molecules cm$^2$)')
            ax.set_xlabel('TROPOMI VCD (10$^{16}$ molecules cm$^2$)')
            plt.tight_layout()
            plt.savefig(path.join(savepath, 'mecoi_tropomi_corr_regr{}.png'.format(orbit_num)),
                        format='png', dpi=100)
            plt.close()
# %% plot individual sorbit data for comparison
            fig, ax = plt.subplots(2, 2,
                                   subplot_kw=dict(projection=projection),
                                   figsize=[12, 7], sharex=True, sharey=True)
            plt.suptitle('Comparisons of SORBIT output NO$_{2}$ VCD for ' +
                         str(year) + str(month).zfill(2) + str(days).zfill(2) +
                         '_orbit ' + orbit_num)
            plot_text = ['L', 'C', 'R', 'I']
            plt.subplots_adjust(wspace=.1, hspace=.1)
            for i in range(0, 2):
                for j in range(0, 2):
                    datadict = {'lat': lats, 'lon': lons,
                                'plotable': 1E-16*SORBIT[plot_text[(2*i)+j]],
                                'vmin': -0.1, 'vmax': 2,
                                'text': 'NO$_{2}$ tropospheric column (10 $^{16}$molec cm$^{-2}$)'}
                    datadict['label'] = 'Sorbit_' + plot_text[(2*i)+j]
                    cs = map2plot.plot_map(datadict, ax[i,j], cmap='jet',
                                           mode='scatter',
                                           projection=projection, alpha=1)
            fig.subplots_adjust(right=0.88, wspace=0.02)
            cbar_ax = fig.add_axes([0.9, 0.15, 0.02, 0.7])
            cbar = fig.colorbar(cs, cax=cbar_ax, extend='max')
            cbar.set_label(datadict['text'], fontsize=10)
            # plt.savefig(path.join(savepath, 'sorbit_comp_' +str(month).zfill(2)+str(days).zfill(2)+
            #                       '_orbit_'+orbit_num+'.png'), format='png', dpi=300)
            plt.close()
            # ###############################
            fig, ax = plt.subplots(2, 3,
                                   subplot_kw=dict(projection=projection),
                                   figsize=[17, 10])
            plt.subplots_adjust(wspace=.1, hspace=.1)
            plt.suptitle('Comparisons of Tropospheric NO$_{2}$ VCD for ' +
                         str(year) + str(month).zfill(2) + str(days).zfill(2) +
                         '_orbit ' + orbit_num)
            ax[0, 0].set_title('COSMO NO2_Trop_VCD (*1E16 mcl cm-2)',
                               fontsize=10)
            temp_dict = {'plotable': tropvcd_sim_i[:]*1E-16, 'label': 'MECO(n)',
                         'text': 'NO$_{2}$ trop. VCD (10 $^{16}$molec cm$^{-2}$)'}
            datadict.update(temp_dict)
            cs = map2plot.plot_map(datadict, ax[0,0], cmap='jet',
                                   mode='scatter',
                                   projection=projection, alpha=1)
            cbar = fig.colorbar(cs, ax=ax[0, 0], orientation='horizontal',
                                fraction=0.07, pad=0.07, shrink=0.7,
                                extend='max')
            cbar.set_label(datadict['text'], fontsize=10)
            # ###############################
            ax[0, 1].set_title('TROPOMI NO2_Trop_VCD (*1E16 mcl cm$-{2}$)',
                                 fontsize=10)
            temp_dict = {'plotable': sat_tropcol[:], 'label': 'TROPOMI'}
            datadict.update(temp_dict)
            cs = map2plot.plot_map(datadict, ax[0,1], cmap='jet',
                                   mode='scatter',
                                   projection=projection, alpha=1)
            cbar = fig.colorbar(cs, ax=ax[0, 1], orientation='horizontal',
                                fraction=0.07, pad=0.07, shrink=0.7,
                                extend='max')
            cbar.set_label(datadict['text'], fontsize=10)
            ## ########### wind vectors ###############
            #model_llon, model_llat = np.meshgrid(model_lon, model_lat)
            #x_wind, y_wind = map1(model_llon, model_llat)
            #map1.quiver(x_wind[points], y_wind[points],
            #            um1[points], vm1[points], ws[points],
            #            latlon=True, cmap=plt.cm.autumn)
            ################################
            ax[0, 2].set_title('TROPOMI Corr NO2_Trop_VCD (*1E16 mcl cm-2)',
                               fontsize=10)
            temp_dict = {'plotable': tropVCD_satcorr[:],
                         'label': 'TROPOMI_corr'}
            datadict.update(temp_dict)
            cs = map2plot.plot_map(datadict, ax[0, 2], cmap='jet', alpha=1,
                                   mode='scatter', projection=projection)
            cbar = fig.colorbar(cs, ax=ax[0, 2], orientation='horizontal',
                                fraction=0.07, pad=0.07, shrink=0.7,
                                extend='max')
            cbar.set_label(datadict['text'], fontsize=10)
            ################################
            ax[1, 0].set_title( 'BIAS (MECO(n) - TROPOMI Corr (*1E16 mcl cm-2)) ', fontsize=10)
            temp_dict = {'plotable': tropvcd_sim_i[:]*1E-16-tropVCD_satcorr[:],
                         'vmin': -1, 'vmax': 1, 'label': 'Bias',}
            datadict.update(temp_dict)
            cs = map2plot.plot_map(datadict, ax[1, 0], cmap='RdBu_r',
                                   mode='scatter',
                                   projection=projection, alpha=1)
            cbar = fig.colorbar(cs, ax=ax[1, 0], orientation='horizontal',
                                fraction=0.07, pad=0.07, shrink=0.7,
                                extend='both')
            cbar.set_label(datadict['text'], fontsize=10)
            ####################################
            ax[1, 1].set_title( 'TROP. AMF ORIG ('+str(days).zfill(2)+str(month).zfill(2)+'_orbit_'+orbit_num+')', fontsize=10)
            temp_dict = {'plotable': amf_trop,
                         'vmin': 0, 'vmax': 2.5, 'label': 'AMF',
                         'text': 'trop. airmass factors'}
            datadict.update(temp_dict)
            cs = map2plot.plot_map(datadict, ax[1, 1], cmap='jet',
                                   mode='scatter',
                                   projection=projection, alpha=1)
            cbar = fig.colorbar(cs, ax=ax[1, 1], orientation='horizontal',
                                fraction=0.07, pad=0.07, shrink=0.7,
                                extend='max')
            cbar.set_label(datadict['text'], fontsize=10)

            ####################################
            ax[1, 2].set_title( 'TROP. AMF CORR ('+str(days).zfill(2)+str(month).zfill(2)+'_orbit_'+orbit_num+')', fontsize=10)
            temp_dict = {'plotable': trop_amf_sim, 'label': 'AMF'}
            datadict.update(temp_dict)
            cs = map2plot.plot_map(datadict, ax[1, 2], cmap='jet',
                                   mode='scatter',
                                   projection=projection, alpha=1)
            cbar = fig.colorbar(cs, ax=ax[1, 2], orientation='horizontal',
                                fraction=0.07, pad=0.07, shrink=0.7,
                                extend='max')
            cbar.set_label(datadict['text'], fontsize=10)
            # plt.savefig(path.join(savepath, 'summary_' +str(month).zfill(2)+str(days).zfill(2)+'_orbit_'+orbit_num+'.png'),
            #             format='png', dpi=300)
            plt.close()
            # regrid back to regular grid and create daily and monthly mean
            # grid_vcd_sim = griddata((lons,lats), tropvcd_sim.filled(np.nan)*1E-16,
            #                         (model_lon, model_lat), method='nearest')
            grid_vcd_sim_i = griddata((lons,lats), tropvcd_sim_i.filled(np.nan)*1E-16,
                                      (model_lon, model_lat), method='nearest')
            grid_sfc_conc_i = griddata((lons,lats), no2_intp_final[0, :].filled(np.nan),
                                       (model_lon, model_lat), method='nearest')
            grid_vcd_sat = griddata((lons,lats), sat_tropcol.filled(np.nan),
                                    (model_lon, model_lat), method='nearest')
            grid_vcd_sat_corr = griddata((lons,lats), tropVCD_satcorr.filled(np.nan),
                                         (model_lon, model_lat), method='nearest')
            '''
            if I do linear interpolation, I need to limit the interpolation
            range around data removed by cloud filter. It doesnot matter for
            nearest interpolation method
            '''
            THRESHOLD = 0.07
            grid = [model_lon, model_lat]
            xy = np.vstack((lons, lats)).T
            xy = xy[~mask]
            if len(xy) > 0:
                tree = cKDTree(xy)
                xi = _ndim_coords_from_arrays(tuple(grid), ndim=xy.shape[1])
                dists, indexes = tree.query(xi)
                # grid_vcd_sim[dists > THRESHOLD] = np.nan
                # grid_vcd_sim = np.ma.masked_where(np.isnan(grid_vcd_sim), grid_vcd_sim)
                grid_vcd_sim_i[dists > THRESHOLD] = np.nan
                grid_vcd_sim_i = np.ma.masked_where(np.isnan(grid_vcd_sim_i), grid_vcd_sim_i)
                grid_sfc_conc_i[dists > THRESHOLD] = np.nan
                grid_sfc_conc_i = np.ma.masked_where(np.isnan(grid_sfc_conc_i), grid_sfc_conc_i)
                grid_vcd_sat[dists > THRESHOLD] = np.nan
                grid_vcd_sat = np.ma.masked_where(np.isnan(grid_vcd_sat), grid_vcd_sat)
                grid_vcd_sat_corr[dists > THRESHOLD] = np.nan
                grid_vcd_sat_corr = np.ma.masked_where(np.isnan(grid_vcd_sat_corr), grid_vcd_sat_corr)

            if counter_imp_swath == 0:
                # mean_meco = grid_vcd_sim[:, :]
                mean_meco_i = grid_vcd_sim_i[:, :]
                mean_meco_conc_i = grid_sfc_conc_i[:, :]
                mean_tropomi = grid_vcd_sat[:, :]
                mean_tropomi_corr = grid_vcd_sat_corr[:, :]
            else:
                # mean_meco = np.ma.dstack((mean_meco, grid_vcd_sim))
                mean_meco_i = np.ma.dstack((mean_meco_i, grid_vcd_sim_i))
                mean_meco_conc_i = np.ma.dstack((mean_meco_conc_i, grid_sfc_conc_i))
                mean_tropomi = np.ma.dstack((mean_tropomi, grid_vcd_sat))
                mean_tropomi_corr = np.ma.dstack((mean_tropomi_corr, grid_vcd_sat_corr))
            counter_imp_swath += 1
            
    if counter_imp_swath == 0:
        # no orbit contain domain of interest
        # mean_meco = np.empty(model_lon.shape)*np.nan
        mean_meco_i = np.empty(model_lon.shape)*np.nan
        mean_meco_conc_i = np.empty(model_lon.shape)*np.nan
        mean_tropomi = np.empty(model_lon.shape)*np.nan
        mean_tropomi_corr = np.empty(model_lon.shape)*np.nan
    else:
        try:
            # more than one orbit contains domain of interest
            # mean_meco = np.ma.mean(mean_meco, 2)
            mean_meco_i = np.ma.mean(mean_meco_i, 2)
            mean_meco_conc_i = np.ma.mean(mean_meco_conc_i, 2)
            mean_tropomi = np.ma.mean(mean_tropomi, 2)
            mean_tropomi_corr = np.ma.mean(mean_tropomi_corr, 2)
        except:
            pass  # only one orbit contains domain of interest
    try:
        f.close()
    except RuntimeError:
        pass
    if days == st_day:
        # mean_meco_month = mean_meco[:, :, np.newaxis]
        mean_meco_i_month = mean_meco_i[:, :, np.newaxis]
        mean_meco_conc_i_month = mean_meco_conc_i[:, :, np.newaxis]
        mean_tropomi_month = mean_tropomi[:, :, np.newaxis]
        mean_tropomi_corr_month = mean_tropomi_corr[:, :, np.newaxis]
    else:
        # mean_meco_month = np.ma.dstack((mean_meco_month, mean_meco))
        mean_meco_i_month=np.ma.dstack((mean_meco_i_month, mean_meco_i))
        mean_meco_conc_i_month = np.ma.dstack((mean_meco_conc_i_month, mean_meco_conc_i))
        mean_tropomi_month=np.ma.dstack((mean_tropomi_month, mean_tropomi))
        mean_tropomi_corr_month=np.ma.dstack((mean_tropomi_corr_month, mean_tropomi_corr))
    print('Day ' + str(days))
# mean_meco_month = np.transpose(mean_meco_month, (2, 0, 1))
mean_meco_month = tropvcd_sim_orig[st_day-1:end_day+1, :]
mean_meco_conc_month = no2_c[st_day-1:end_day+1, 0:13, :]
mean_meco_i_month = np.transpose(mean_meco_i_month, (2, 0, 1))
mean_meco_conc_i_month = np.transpose(mean_meco_conc_i_month, (2, 0, 1))
mean_tropomi_month = np.transpose(mean_tropomi_month, (2, 0, 1))
mean_tropomi_corr_month = np.transpose(mean_tropomi_corr_month, (2, 0, 1))
fit_data.to_csv(path.join(savepath, 'daily_fit_stats.csv'))
# %% create merged netcdf output for corrected satellite VCD on regular grid
ff_out = netCDF4.Dataset(path.join(savepath, 'meanVCD' +
                                   str(month).zfill(2)+'_merge.nc'), 'w',
                         format='NETCDF4')
ff_out.createDimension('time', None)
ff_out.createDimension('lev', 13)
ff_out.createDimension('glon', model_lon.shape[1])
ff_out.createDimension('glat', model_lat.shape[0])
time_out = ff_out.createVariable('time', 'f4', ('time',))
lev_out = ff_out.createVariable('lev', 'f4', ('lev'))
glon_out = ff_out.createVariable('glon', 'f4', ('glon'))
glat_out = ff_out.createVariable('glat', 'f4', ('glat'))
lon_out = ff_out.createVariable('lon', 'f4', ('glat', 'glon'))
lat_out = ff_out.createVariable('lat', 'f4', ('glat', 'glon'))
hsurf_out = ff_out.createVariable('hsurf', 'f4', ('glat', 'glon'))
h_out = ff_out.createVariable('altitude', 'f4', ('lev'))
hyam_out = ff_out.createVariable('hyam', 'f4', ('lev'))
hybm_out = ff_out.createVariable('hybm', 'f4', ('lev'))
aps_out = ff_out.createVariable('aps', 'f4', ('time', 'glat', 'glon'))
pblh_out = ff_out.createVariable('pblh', 'f4', ('time', 'glat', 'glon'))
NO2_tropcol_sat_out = ff_out.createVariable('NO2_tropcol_sat', 'f4',
                                            ('time', 'glat','glon'), fill_value=fv)
NO2_tropcol_sat_corr_out = ff_out.createVariable('NO2_tropcol_sat_corr', 'f4',
                                                 ('time', 'glat','glon'), fill_value=fv)
NO2_tropcol_sim_out = ff_out.createVariable('NO2_tropcol_sim', 'f4',
                                            ('time', 'glat','glon'), fill_value=fv)
NO2_tropcol_sim_i_out = ff_out.createVariable('NO2_tropcol_sim_i', 'f4',
                                              ('time', 'glat','glon'), fill_value=fv)
NO2_conc_sim_out = ff_out.createVariable('NO2_conc_sim', 'f4',
                                         ('time', 'lev', 'glat','glon'), fill_value=fv)
NO2_conc_sim_i_out = ff_out.createVariable('NO2_conc_sim_i', 'f4',
                                           ('time', 'glat','glon'), fill_value=fv)
tm1_out = ff_out.createVariable('tm1', 'f4', ('time', 'lev', 'glat', 'glon'),
                                fill_value=fv)
um1_out = ff_out.createVariable('um1', 'f4', ('time', 'lev', 'glat', 'glon'),
                                fill_value=fv)
vm1_out = ff_out.createVariable('vm1', 'f4', ('time', 'lev', 'glat', 'glon'),
                                fill_value=fv)

time_out[:] = np.array([i for i in range(0, end_day-st_day+1)])
time_out.long_name='time'; time_out.units='days since {}-{}-{} 00:00:00'.format(year, str(month).zfill(2), str(st_day).zfill(2))
lon_out[:] = model_lon; lon_out.standard_name='longitude'; lon_out.units='degrees_east'
lat_out[:] = model_lat; lat_out.standard_name='latitude'; lat_out.units='degrees_north'
glon_out[:] = glon; glon_out.standard_name='grid_longitude'; glat_out.units='degrees'
glat_out[:] = glat; glat_out.standard_name='grid_latiitude'; glat_out.units='degrees'
hsurf_out[:] = hsurf[:]; hsurf_out.standard_name='surface_altitude'; hsurf_out.units='m'
aps_out[:] = aps[st_day-1:end_day+1, :]; aps_out.standard_name='MECO(n) surface pressure'; aps_out.units='Pa'
pblh_out[:] = pblh[st_day-1:end_day+1, :]; pblh_out.standard_name='PBL height'; pblh_out.units='m'
NO2_tropcol_sat_out[:] = mean_tropomi_month[:]; NO2_tropcol_sat_out.standard_name='TROPOMI NO2 tropospheric VCD';NO2_tropcol_sat_out.units = "1E16 molecules cm-2"
NO2_tropcol_sat_corr_out[:] = mean_tropomi_corr_month[:]; NO2_tropcol_sat_corr_out.standard_name='Modified TROPOMI NO2 tropospheric VCD';NO2_tropcol_sat_corr_out.units = "1E16 molecules cm-2"
NO2_tropcol_sim_out[:] = mean_meco_month[:]; NO2_tropcol_sim_out.standard_name='MECO(n) NO2 tropospheric VCD at TROPOMI overpass';NO2_tropcol_sim_out.units = "1E16 molecules cm-2"
NO2_tropcol_sim_i_out[:] = mean_meco_i_month[:]; NO2_tropcol_sim_i_out.standard_name='MECO(n) NO2 tropospheric VCD at TROPOMI overpass; corrected for swath width';NO2_tropcol_sim_i_out.units = "1E16 molecules cm-2"
NO2_conc_sim_out[:] = mean_meco_conc_month[:]; NO2_conc_sim_out.standard_name='MECO(n) NO2 concentration profiles at TROPOMI overpass';NO2_conc_sim_out.units = "molecules cm-3"
NO2_conc_sim_i_out[:] = mean_meco_conc_i_month[:]; NO2_conc_sim_i_out.standard_name='MECO(n) NO2 concentration profiles at TROPOMI overpass; corrected for swath width';NO2_conc_sim_i_out.units = "molecules cm-3"
tm1_out[:] = tm_c[0:end_day-st_day+1, :13, :]; tm1_out.standard_name='Temperature';tm1_out.units = "K"
um1_out[:] = um1[0:end_day-st_day+1, :13, :]; um1_out.standard_name='X component of wind'; um1_out.units = "m s-1"
vm1_out[:] = vm1[0:end_day-st_day+1, :13, :]; vm1_out.standard_name='Y component of wind'; vm1_out.units = "m s-1"
h_out[:] = [28.9, 126.8, 302.7, 572.5, 977.8, 1488.5, 2202.7,
            3097.4, 4067.1, 5154.2, 6457.5, 7806.8, 8892.5 ]
h_out.standard_name='Grid altitude at midpoint';h_out.units = "m"
hyam_out[:] = hyam[:13]; hyam_out.standard_name='hybrid A coefficient at layer midpoints'; vm1_out.units = "Pa"
hybm_out[:] = hybm[:13]; hybm_out.standard_name='hybrid B coefficient at layer midpoints'; vm1_out.units = "1"

ff_out.close()
toc = datetime.now()
print('Time taken: %.1f seconds' % (toc - tic).total_seconds())
plt.ion()
