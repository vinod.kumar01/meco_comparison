# -*- coding: utf-8 -*-
"""
Created on Fri Aug  3 18:57:08 2018

@author: Vinod
"""

import netCDF4
import os
import numpy as np
import matplotlib.pyplot as plt
#from sklearn import linear_model
import datetime
from mpl_toolkits.basemap import Basemap
dirname = r'M:\nobackup\vinod\model_work\MECO'
filename_COSMO = 'COSMO_pbl_2018_04_regrid.nc'
#filename_COSMO = 'COSMO_met_2015_04_gridded_l40.nc'
filename_ECMWF = 'ECMWF_pbl_2018_04_gridded.nc'
#filename_ECMWF = 'ECMWF_met_2015_04_gridded_6h.nc'
#from scipy import stats
path_COSMO = os.path.join(dirname, filename_COSMO)
path_ECMWF = os.path.join(dirname, filename_ECMWF)
f_COSMO=netCDF4.Dataset(path_COSMO,'r')
f_ECMWF=netCDF4.Dataset(path_ECMWF,'r')
lats=f_COSMO.variables['lat'][:]
lons=f_COSMO.variables['lon'][:]
day=5
sample_freq= 1   #6 #hour
var_cosmo='pblh_ave'
#var_cosmo='qm1'
#var_cosmo='tm1'
#var_ecmwf='blh'
#var_ecmwf='t2m'
var_ecmwf='blh'
for hour in range(1,int(24/sample_freq)+1):
    param_COSMO=f_COSMO.variables[var_cosmo][(day-1)*24/sample_freq + hour-1,:,:]
    param_ECMWF=f_ECMWF.variables[var_ecmwf][(day-1)*24/sample_freq + hour-1,:,:]
    timestamp_COSMO = f_COSMO.variables['time']
    timestamp_ECMWF = f_ECMWF.variables['time']
    date_display_COSMO=datetime.datetime.strptime(timestamp_COSMO.units[10:28],'%Y-%m-%d %H:%M:%S')+ datetime.timedelta(days=timestamp_COSMO[(day-1)*24/sample_freq+hour-1]+(1/24/60))
    date_display_ECMWF=datetime.datetime.strptime(timestamp_ECMWF.units[12:30],'%Y-%m-%d %H:%M:%S')+ datetime.timedelta(hours=timestamp_ECMWF[(day-1)*24/sample_freq+hour]-sample_freq/2+0.5)
    ##plots
    plt.ioff()
    fig, axes = plt.subplots(nrows=2, ncols=1, figsize=[8,9])
    #plt.suptitle('Comparisons of ECMWF and COSMO pblh for' + date_display_COSMO.strftime('%d-%b-%Y'))
    axes[0].set_title('COSMO ' + var_cosmo + ' for ' + date_display_COSMO.strftime('%d-%b-%Y %H:%M') + 'h UTC', fontsize=11)
    map = Basemap(projection='cyl', resolution = 'l', llcrnrlon=1, llcrnrlat=43,urcrnrlon=20, urcrnrlat=56, ax=axes[0])
    map.drawparallels(np.arange(30.,60.,4.),labels=[1,0,0,0])
    map.drawmeridians(np.arange(0.,20.,4.),labels=[0,0,0,1])
    map.drawcoastlines()
    map.drawcountries()
    map.drawmapboundary()
    llons, llats = np.meshgrid(lons, lats)
    x,y = map(llons, llats)
    cs1 = map.pcolormesh(x, y, param_COSMO, cmap='jet')
    l_limit=np.min(param_COSMO)
    u_limit=np.max(param_COSMO)
    cb1=fig.colorbar(cs1, cmap=plt.cm.get_cmap('jet'), ax=axes[0], orientation='vertical',extend = 'both', fraction = 0.1, shrink = 0.85)
    cb1.set_label(f_ECMWF.variables[var_ecmwf].long_name + ' (' + f_ECMWF.variables[var_ecmwf].units + ')' )
    ################################
    axes[1].set_title( 'ECMWF ' + var_ecmwf + ' for ' + date_display_ECMWF.strftime('%d-%b-%Y %H:%M') + 'h UTC', fontsize=11)
    map = Basemap(projection='cyl', resolution = 'l', llcrnrlon=1, llcrnrlat=43,urcrnrlon=20, urcrnrlat=56, ax=axes[1])
    map.drawparallels(np.arange(30.,60.,4.),labels=[1,0,0,0])
    map.drawmeridians(np.arange(0.,20.,4.),labels=[0,0,0,1])
    map.drawcoastlines()
    map.drawcountries()
    map.drawmapboundary()
    cs2 = map.pcolormesh(x, y, param_ECMWF, cmap='jet', vmin =  l_limit, vmax =  u_limit )
    cb2=fig.colorbar(cs2, cmap=plt.cm.get_cmap('jet'), ax=axes[1], orientation='vertical',extend = 'both', fraction = 0.1, shrink = 0.85)
    cb2.set_label(f_ECMWF.variables[var_ecmwf].long_name + ' (' + f_ECMWF.variables[var_ecmwf].units + ')' )
    plt.tight_layout()
    plt.savefig(r'M:\home\vinod\nobackup\model_work\MECO\Plots\met\ '+ var_ecmwf+str(day)+'h'+str(hour*sample_freq)+'.png', format = 'png', dpi = 200)
    plt.ion()
    plt.close()
    #plt.show()
