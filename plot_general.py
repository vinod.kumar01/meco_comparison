# -*- coding: utf-8 -*-
"""
Created on Mon Jun  3 14:28:11 2019

@author: Vinod
"""

import netCDF4
import numpy as np
import matplotlib.pyplot as plt
import os
from scipy import stats
import shapefile
from shapely.geometry import shape, Point, box
from datetime import datetime, timedelta
from plot_tools import map_prop, ccrs
from mod_colormap import add_white
import matplotlib.patches as patches
from tools import lin_fit
import skill_metrics as sm
# llon, ulon, llat, ulat = 0, 19, 43, 56
llon, ulon, llat, ulat = 0.6, 18.4, 43.6, 55.4
prop_dict = {'extent': [llon, ulon, llat, ulat],
             'name': 'MECO_DE',
             'border_res': '10m'}
projection = ccrs.PlateCarree()
map2plot = map_prop(**prop_dict)


def check(lon, lat):
    # build a shapely point from your geopoint
    point = Point(lon, lat)
    # the contains function does exactly what you want
    return polygon.contains(point)


create_de_keep = False
# %% data import and averaging
dirname_sim = r'M:\nobackup\vinod\model_work\MECO\sorbit_subsample\2018_07\UBA_fl\qa_75'
filename = 'meanVCD07_merge.nc'
with netCDF4.Dataset(os.path.join(dirname_sim, filename)) as f:
    tropcol_sim = f.variables['NO2_tropcol_sim'][:]
    tropcol_sim_i = f.variables['NO2_tropcol_sim_i'][:]
    tropcol_sat = f.variables['NO2_tropcol_sat'][:]
    tropcol_sat_corr = f.variables['NO2_tropcol_sat_corr'][:]
    lons = f.variables['lon'][:]
    lats = f.variables['lat'][:]
    timeax = f.variables['time'][:]
    timeax = np.array(timeax, dtype='f8')
    time_dsp = [datetime.strptime(f.variables['time'].units[11:30],
                                  '%Y-%m-%d %H:%M:%S') + timedelta(days=i)
                for i in timeax]

mean_meco = np.nanmean(tropcol_sim, axis=0)
mean_meco_i = np.nanmean(tropcol_sim_i, axis=0)
mean_tropomi = np.nanmean(tropcol_sat, axis=0)
mean_tropomi_corr = np.nanmean(tropcol_sat_corr, axis=0)

if create_de_keep:
    # germany shapr file and get data within
    De_shp = r'D:\python_toolbox\Igismap\Germany_Boundary.shp'
    r = shapefile.Reader(De_shp)
    # get the shapes
    shapes = r.shapes()
    # build a shapely polygon from your shape
    polygon = shape(shapes[0])
    minx, miny, maxx, maxy = polygon.bounds
    bounding_box = box(minx, miny, maxx, maxy)
    keep = np.zeros(mean_meco.shape, dtype=bool)
    for j in range(lats.shape[0]):
        if j % 10 == 0:
            print(j)
        for i in range(lons.shape[1]):
            if (miny < lats[j, i] < maxy) & (minx < lons[j, i] < maxx):
                keep[j, i] = check(lons[j, i], lats[j, i])
    keep_bin = keep.copy().astype(int)
    outname = r'M:\nobackup\vinod\model_work\MECO\De_mask_meco7.txt'
    np.savetxt(outname, keep_bin, fmt='%i')
else:
    keep = np.loadtxt(r'M:\nobackup\vinod\model_work\MECO\De_mask_meco7.txt')
    keep = keep.copy().astype(bool)
# %% plot map
datadict = {'lat': lats, 'lon': lons,
            'plotable': mean_tropomi_corr,
            'vmin': -0.1, 'vmax': 1.2,
            'text': 'NO$_2$ Trop. VCD (10$^{16}$ molecules cm$^{-2}$)',
            'label': 'tropomi_corr'}
fig, ax = plt.subplots(subplot_kw=dict(projection=projection),
                       figsize=[8, 6.5])
plt.title('TROPOMI (corr.) tropospheric NO$_{2}$ VCD for July 2018')
cs = map2plot.plot_map(datadict, ax, cmap='jet', mode='pcolormesh',
                       projection=projection, alpha=1)
cb = fig.colorbar(cs, ax=ax, orientation='horizontal',
                  extend='max', fraction=0.04, shrink=0.8, pad=0.08)
cb.set_label(datadict['text'], fontsize=11)
fig.canvas.draw()
plt.tight_layout()
# '''
# add boxes for sub areas
areas = {}
box_id = []
box_st = [[52, 7.0, 8], [50.7, 6.0, 9], [49.5, 7.0, 6], [48.1, 8.0, 6]]
# start lat, start lon, number of boxes
for boxes in box_st:
    lat_st = boxes[0]
    lon_st = boxes[1]
    for i in np.arange(1, boxes[2]):
        box_id_now = '{}_{}'.format(lat_st, lon_st)
        box_id.append(box_id_now)
        areas[box_id_now] = {'lat_bound': [lat_st, lat_st+1],
                             'lon_bound': [lon_st, lon_st+1]}
        lon_st += 1
        rect = patches.Rectangle((areas[box_id_now]['lon_bound'][0],
                                  areas[box_id_now]['lat_bound'][0]),
                                 1, 1, linewidth=2, edgecolor='r',
                                 facecolor='none', label=str(box_id_now))
        # ax.add_patch(rect)
plt.savefig(os.path.join(dirname_sim, 'mean_tropomi_corr.png'),
            format='png', dpi=300)

# %% scatter plot
new_cm = add_white('rainbow', replace_frac=0.01, start=None, transparent=True)
fig, ax = plt.subplots()
VCD_sat_flat = mean_tropomi_corr.ravel()
VCD_sim_flat = mean_meco_i.ravel()
cs = ax.hexbin(VCD_sat_flat, VCD_sim_flat, gridsize=[300, 150], cmap=new_cm,
               mincnt=1, vmax=50)
idx = np.isfinite(VCD_sat_flat) & np.isfinite(VCD_sim_flat)
fit_stats = lin_fit(VCD_sat_flat[idx], VCD_sim_flat[idx])
fit_param = fit_stats.stats_calc()
fit_func = np.poly1d([fit_param['m'], fit_param['c']])
ax.plot(VCD_sat_flat[idx], fit_func(VCD_sat_flat[idx]),
        color="k", linewidth=2, label='linear regression')
ax.plot([0, 1], [0, 1], transform=ax.transAxes, ls="--", c=".3", label='1:1')
ax.fill_between([0, 1], [0, 1.3], [0, 1], transform=ax.transAxes, ls=":",
                color=".3", alpha=.2, label='$\pm 30\%$')
ax.fill_between([0, 1], [0, 0.7], [0, 1], transform=ax.transAxes,
                ls=":", color=".3", alpha=.2, label=None)
fit_stats.add_fit_param(pos_x=0.01, pos_y=0.7, ax=ax, color='k', c_fmt='%.2f',
                        showr=True)
ax.set_ylabel('MECO(n) tropospheric NO$_{2}$ VCD (10$^{16}$ mcl cm$^{-2}$)')
ax.set_xlabel('TROPOMI (corr) tropospheric NO$_{2}$ VCD (10$^{16}$ mcl cm$^{-2}$)')
ax.set_ylim(0, 1.2)
ax.set_xlim(0, 1.2)
ax.legend(loc=[0.01, 0.8])
ax.grid(alpha=0.3)
plt.tight_layout()
cbaxes = fig.add_axes([0.6, 0.25, 0.3, 0.02])
cbaxes.minorticks_on()
cb = plt.colorbar(cs, cax=cbaxes, shrink=0.8, fraction=0.07, extend='max',
                  orientation='horizontal')
cb.set_label('Frequency', fontsize=11)
plt.savefig(os.path.join(dirname_sim, 'meco_i_tropomi_corr_regr.png'),
            format='png', dpi=300)

# %% box plot of bias in grids
bias = {}
taylor_stats = {}
rel_bias = [0]
for i, box_id_now in enumerate(areas.keys()):
    keep = (lats > areas[box_id_now]['lat_bound'][0])
    keep &= (lats < areas[box_id_now]['lat_bound'][1])
    keep &= (lons > areas[box_id_now]['lon_bound'][0])
    keep &= (lons < areas[box_id_now]['lon_bound'][1])
    ts_sim = tropcol_sim_i[:, keep]
    ts_sim = np.ma.mean(ts_sim, 1).filled(np.nan)
    ts_sim = ts_sim[~np.isnan(ts_sim)]
    ts_sat = tropcol_sat_corr[:, keep]
    ts_sat = np.ma.mean(ts_sat, 1).filled(np.nan)
    ts_sat = ts_sat[~np.isnan(ts_sat)]
    bias[box_id_now] = ts_sim - ts_sat
    rel_bias.append(100 * np.ma.mean(bias[box_id_now]) / np.ma.mean(ts_sat))
    taylor_stats[box_id_now] = sm.taylor_statistics(ts_sim, ts_sat)
sdev = [1]
sdev.extend([taylor_stats[b]['sdev'][1]/taylor_stats[b]['sdev'][0]
             for b in areas.keys()])
crmsd = [0]
crmsd.extend([taylor_stats[b]['crmsd'][1] for b in areas.keys()])

ccoef = [1]
ccoef.extend([taylor_stats[b]['ccoef'][1] for b in areas.keys()])

fig, ax = plt.subplots(figsize=[6, 6])
sm.taylor_diagram(np.array(sdev), np.array(crmsd), np.array(ccoef),
                  widthRMS=0.5, widthCOR=0.5, axismax=3,
                  # tickRMS=[0.5, 1, 1.5, 2.0], widthRMS=0.5, widthCOR=0.5,
                  styleCOR='--',
                  colormap='on', cmap='RdBu_r', cmap_vmin=-30, cmap_vmax=30,
                  cmap_marker='*', markerDisplayed='colorBar',
                  titleColorbar='Relative bias (%)', cmapzdata=rel_bias,
                  colOBS='r', markerobs='o', titleOBS='observation')
ax.set_ylabel('Standard Deviation (Normalized)')
# plt.savefig(os.path.join(dirname_sim, 'taylor_1deg.png'),
#             format='png', dpi=300)
labels, data = [i.replace('_', '\n') for i in bias.keys()], bias.values()
fig, ax = plt.subplots(figsize=[12, 4])
c = 'k'
bw = {'boxprops': {'color': c}, 'whiskerprops': {'color': c},
      'capprops': {'color': c}, 'medianprops': {'color': c},
      'meanprops': {'marker': 'o', 'markeredgecolor': c,
                    'markerfacecolor': 'none', 'markersize': 4}}
a = ax.boxplot(data, positions=np.arange(len(labels)),
               showfliers=False, showmeans=True, whis=[5, 95], widths=0.2,
               boxprops=bw['boxprops'], medianprops=bw['medianprops'],
               capprops=bw['capprops'], meanprops=bw['meanprops'],
               whiskerprops=bw['whiskerprops'])
a = plt.xticks(range(len(labels)), labels)
ax.grid(alpha=0.3)
ax.set_ylabel('Sim-Obs VCD (10$^{16}$ molecules cm$^{-2}$)')
# ax.legend(loc='upper left')
# plt.xticks(range(1, len(labels) + 1), labels)
# plt.savefig(os.path.join(dirname_sim, 'bw_bias.png'),
#             format='png', dpi=300)

# ax.boxplot(bias[box_id_now])

# %% time series at selected area
boxes = ['NW', 'NE', 'RP', 'PO']
# boxes = ['NW']
areas = {}
for sbox in boxes:
    areas[sbox] = {}
areas['NW']['lat_bound'] = [50., 53.]
areas['NW']['lon_bound'] = [3., 7.5]
areas['NW']['color'] = 'r'
areas['NE']['lat_bound'] = [50., 54.]
areas['NE']['lon_bound'] = [10.5, 14.]
areas['NE']['color'] = 'g'
areas['RP']['lat_bound'] = [48., 50.5]
areas['RP']['lon_bound'] = [7.5, 10.]
areas['RP']['color'] = 'b'
areas['PO']['lat_bound'] = [44.5, 46.5]
areas['PO']['lon_bound'] = [7., 12.]
areas['PO']['color'] = 'k'

fig, ax = plt.subplots(figsize=[10,5])
for sbox in boxes:
    keep = (lats > areas[sbox]['lat_bound'][0])
    keep &= (lats < areas[sbox]['lat_bound'][1])
    keep &= (lons > areas[sbox]['lon_bound'][0])
    keep &= (lons < areas[sbox]['lon_bound'][1])
    ts_sim = tropcol_sim_i[:, keep]
    ts_sim = np.ma.mean(ts_sim, 1)
    ts_sat = tropcol_sat_corr[:, keep]
    ts_sat = np.ma.mean(ts_sat, 1)
    ax.plot(time_dsp, ts_sat, c=areas[sbox]['color'],
            linestyle='-', label=sbox)
    ax.plot(time_dsp, ts_sim, c=areas[sbox]['color'],
            linestyle='--', label=None)
    ax.minorticks_on()
    ax.set_ylabel('NO$_{2}$ trop. VCD (10$^{16}$molecules cm$^{-2}$)')
    ax.legend(loc='upper right')
    ax.set_ylim(0.09, 1)
ax2 = ax.twinx()
ax2.set_ylim(0.09, 1)
ax2.set_yticks([])
ax2.plot(time_dsp, ts_sat, c=areas['PO']['color'],
         linestyle='-', label='TROPOMI')
ax2.plot(time_dsp, ts_sim, c=areas['PO']['color'],
         linestyle='--', label='MECO(n)')
ax2.legend(loc=[0.72, 0.85])
ax2.minorticks_on()
plt.tight_layout()
plt.savefig(os.path.join(dirname_sim, 'ts_meco_tropomi.png'),
            format='png', dpi=300)

# scatter plots in boxes   
fig, ax = plt.subplots(2, 2, sharex=True, sharey=True)
fig.add_subplot(111, frameon=False)
plt.tick_params(labelcolor='none', top=False, bottom=False, left=False, right=False)
plt.ylabel('MECO(n) tropospheric NO$_{2}$ VCD (*1E16 mcl cm$^{-2}$)')
plt.xlabel('TROPOMI_corr tropospheric NO$_{2}$ VCD (*1E16 mcl cm$^{-2}$)')
box_idx = [[0,0], [0,1], [1,0], [1,1]]
for i,box in zip(box_idx,boxes):
    keep = (lats > areas[box]['lat_bound'][0]) & (lats < areas[box]['lat_bound'][1])
    keep &= (lons > areas[box]['lon_bound'][0]) & (lons < areas[box]['lon_bound'][1])
    VCD_sim_flat = mean_meco[keep]
    VCD_sat_flat = mean_tropomi_corr[keep]
    VCD_sat_flat = VCD_sat_flat.flatten()
    ax[i[0],i[1]].scatter(VCD_sat_flat, VCD_sim_flat,s=2)
    idx = np.isfinite(VCD_sat_flat) & np.isfinite(VCD_sim_flat)
    fit_param = stats.linregress(VCD_sat_flat[idx], VCD_sim_flat[idx])[0:3]
    fit_func=np.poly1d(fit_param[0:2])
    r_sqr=fit_param[2]**2
    ax[i[0],i[1]].plot(VCD_sat_flat[idx], fit_func(VCD_sat_flat[idx]), color = "black", linewidth=2)
    ax[i[0],i[1]].plot([0, 1], [0, 1], transform=ax[i[0],i[1]].transAxes, ls="--", c=".3")
    ax[i[0],i[1]].annotate(box+"\ny = " + str('%.2f' % fit_param[0]) + "x + "+ str('%.2f' % fit_param[1]) +
                "\n r$^{2}$ = " + str('%.2f' % r_sqr), xy=(0.1, 0.7),  xycoords='axes fraction')
   
    ax[i[0],i[1]].set_ylim(0, 1.4)
    ax[i[0],i[1]].set_xlim(0, 1.4)
    ax[i[0],i[1]].minorticks_on()
plt.tight_layout()
# plt.savefig(os.path.join(dirname_sim, 'box_meco_i_tropomi_corr_regr.png'), format = 'png', dpi = 300)