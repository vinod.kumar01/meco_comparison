# -*- coding: utf-8 -*-
"""
Created on Fri Apr 27 15:33:15 2018

@author: Vinod
"""

import netCDF4
import matplotlib.pyplot as plt 
from mpl_toolkits.basemap import Basemap
import numpy as np
f_sim_big = netCDF4.Dataset(r'M:\nobackup\vinod\model_work\MECO\domain\border_02.nc', 'r')
f_sim_small = netCDF4.Dataset(r'M:\nobackup\vinod\model_work\MECO\domain\border_03.nc' , 'r')
geolats_big,geolats_small  = f_sim_big.variables['geolat_ave'][:], f_sim_small.variables['geolat_ave'][:]
geolons_big,geolons_small = f_sim_big.variables['geolon_ave'][:],f_sim_small.variables['geolon_ave'][:]
var_obs='O3_ave'
V0,V1 = f_sim_big.variables[var_obs][39,:,:],f_sim_small.variables[var_obs][39,:,:]
V0[:],V1[:]=np.nan,np.nan
V0[:,:1]=1
V0[:,-2:]=1
V0[:1,:]=1
V0[-2:,:]=1
V1[:,:6]=1
V1[:,-6:]=1
V1[:6,:]=1
V1[-6:,:]=1
#V0[V0<1]=np.nan
#data_mask = np.ma.masked_array(V0, np.isnan(V0))
fig=plt.Figure()
ax = fig.add_subplot(1, 1, 1)
map = Basemap(projection='cyl', resolution = 'l', llcrnrlon=-60, llcrnrlat=4,urcrnrlon=90, urcrnrlat=80)
#map = Basemap(projection = 'moll', lon_0 = 0,resolution='c')
map.drawcoastlines()
map.drawcountries(linewidth=1)
m = ['ESRI_Imagery_World_2D',    # 0
        'ESRI_StreetMap_World_2D',  # 1
        'NatGeo_World_Map',         # 2
        'NGS_Topo_US_2D',           # 3
        'Ocean_Basemap',            # 4
        'USA_Topo_Maps',            # 5
        'World_Imagery',            # 6
        'World_Physical_Map',       # 7
        'World_Shaded_Relief',      # 8
        'World_Street_Map',         # 9
        'World_Terrain_Base',       # 10
        'World_Topo_Map'            # 11
        ]
map.arcgisimage(service=m[7], xpixels=1000, verbose=False)
#map.etopo()
#map.fillcontinents(color = 'coral')
#map.drawmapboundary()
map.drawparallels(np.arange(5.,75.,15.),labels=[1,0,0,0])
map.drawmeridians(np.arange(-60.,90.,30.),labels=[0,0,0,1])
x,y = map(geolons_big, geolats_big)
x2,y2 = map(geolons_small, geolats_small)
#map.arcgisimage(service="ESRI_Imagery_World_2D", xpixels=1000, verbose=False)
#map.plot(x, y, '.', markersize=2)
cs = map.pcolormesh(x, y, V0, cmap = 'Greys_r')
cs2 = map.pcolormesh(x2, y2, V1, cmap = 'Reds_r')
#plt.savefig('Mecon_domain.png', format = 'png', dpi = 200)
plt.show()
plt.savefig(r'M:\nobackup\vinod\model_work\MECO\domain\border_02_03.png', format='png', dpi=300)