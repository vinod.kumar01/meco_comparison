# -*- coding: utf-8 -*-
"""
Created on Tue Jan 15 13:33:39 2019

@author: Vinod
"""
# %% definition and imports
import pandas as pd
import os
import matplotlib.pyplot as plt
from matplotlib import dates as mdates
import numpy as np
import skill_metrics as sm
from tools import tracer_prop, lin_fit
from datetime import datetime, timedelta
from scipy.interpolate import griddata
import netCDF4
from plot_tools import map_prop, plot_diurnal, ccrs
from filetools import dict2hdf5, hdf2dict
from mod_colormap import add_white


def my_to_datetime(date_str):
    if date_str[11:13] != '24':
        try:
            return pd.to_datetime(date_str, format="%Y-%m-%d'%H:%M'")
        except ValueError:
            return pd.to_datetime(date_str, format="'%Y%m%d''%H:%M'")
    else:
        date_str = date_str[0:11] + '00' + date_str[13:]
        try:
            return pd.to_datetime(date_str, format="%Y-%m-%d'%H:%M'") + timedelta(days=1)
        except ValueError:
            return pd.to_datetime(date_str, format="'%Y%m%d''%H:%M'") + timedelta(days=1)


new_cm = add_white('jet', shrink_colormap=True, replace_frac=0.02)
create_sim_dict = False
plot_diurnal_profiles = False
plot_station_ts = False
comp_sel_hours = False
plot_daily_mean = False
# %% load observation data
dirname = r'M:\nobackup\vinod\model_work\MECO\station_data\UBA'
coord_file = 'STATIONS_KOORDINATEN.xlsx'
stn_coords = pd.read_excel(os.path.join(dirname, coord_file),
                           header=1, skiprows=[2])
stn_coords = stn_coords.set_index('station code')

# tracer = tracer_prop(name="O3", label='O$_3$', mol_wt=48, conc_lim=[20, 55])
tracer = tracer_prop(name="NO2", label='NO$_2$', mol_wt=46, conc_lim=[0, 20])
# tracer = tracer_prop(name="CO", label='CO', mol_wt=28, conc_lim=[0, 500])
# tracer = tracer_prop(name="SO2", label='SO$_2$', mol_wt=64.06, conc_lim=[0, 10])

# %% old data downlaoded manually
# filename = "stations_2018-04-30-2018-05-31.csv"
# names = ["State", "Station code", "Station name", "Station setting",
#         "Station type", "Pollutant", "Time scope", "Date", "Time",
#         "Measure value", "Unit"]
# df = pd.read_csv(os.path.join(dirname, filename), dtype=None, delimiter=';',
#                 names=names, header=0)
# #names = ["st_time", "end_time", "p", "rh", "T", "NO2ppb", "NO2ugNm3",
# #         "NO2a", "NO2b", "NO2c", "NO2d", "NO2e", "NO2f", "NOppb", "NOugNm3",
# #          "NOa", "NOb", "NOc", "NOd", "NOe", "NOf", "flag"]
# #df = pd.read_csv(os.path.join(dirname,filename), dtype=None,
# #                 delimiter=r"\s+", skiprows=91, header=0 ,names=names)
# #names = ['station code','Stationsname','Netzwerk','tracer','data_type',
# #         'date_time', 'concentration']

# df['Date_time'] = df['Date'] + df['Time']
# df['Date_time'] = df['Date_time'].apply(my_to_datetime)
# # Measurement data is averaged at end of hour
# df['Date_time'] = df.apply(lambda x: x['Date_time']-timedelta(hours=1),
#                            axis=1)
# df['Measure value'] = pd.to_numeric(df['Measure value'], errors='coerce')
# df = df.set_index('Date_time')
# sitelist = df['Station code'].unique()

# new hourly data of all the stations got from mail request
filename = 'DE2018{}_inv1SMW_20200605.csv'.format(tracer.name)
df = pd.read_csv(os.path.join(dirname, filename), dtype=None, delimiter=';',
                 skiprows=[1, 2, 3], header=0)
df['Date_time'] = df['Datum'] + df['Uhrzeit']
df['Date_time'] = df['Date_time'].apply(my_to_datetime)
# shift back 1 hour because UBA data is reported at end of hour
# shift back 2 hours for UTC difference
df['Date_time'] = df.apply(lambda x: x['Date_time']-timedelta(hours=2), axis=1)
sitelist = [i for i in df.columns if i.startswith('DE')]
df = df.set_index('Date_time')
df[df == -999] = np.nan
df = df[df.index.month == 5]
if tracer.name == 'CO':
    for site in sitelist:
        df[site] *= 1000

# %% load  and process model data
model_setup = 'UBA_di'
dirname_sim = r'M:\nobackup\vinod\model_work\MECO\DOAS_COMP'
if create_sim_dict:
    f_sim = netCDF4.Dataset(os.path.join(dirname_sim,
                                         model_setup + '_18_5_tr_1hr_sfc.nc'))
    timeax = f_sim.variables['time'][:]
    try:
        time_unit = f_sim.variables['time'].units[11:30]
        time_dsp = [datetime.strptime(time_unit,
                                      '%Y-%m-%d %H:%M:%S') + timedelta(days=i)
                    for i in timeax]
    except ValueError:
        time_unit = f_sim.variables['time'].units[10:30]
        time_dsp = [datetime.strptime(time_unit,
                                      '%Y-%m-%d %H:%M:%S') + timedelta(days=i)
                    for i in timeax]
    tracer_model = f_sim.variables[tracer.name+'_ave'][:, 0, :]*1E9
    temp_model = f_sim.variables['tm1_ave'][:, 0, :]
    try:
        model_lon = f_sim.variables['geolon_ave'][:]
        model_lat = f_sim.variables['geolat_ave'][:]
    except KeyError:
        model_lon = f_sim.variables['lon'][:]
        model_lat = f_sim.variables['lat'][:]
# #---------------------# plot map----------------------------------------
#    llon, ulon, llat, ulat = 5.8, 11.2, 48.2, 52.1
    llon, ulon, llat, ulat = 5.9, 11.2, 48.3, 52.05  #MECO3
    # llon, ulon, llat, ulat = 5.8, 15.1, 47.0, 55
    prop_dict = {'extent': [llon, ulon, llat, ulat],
                 'name': ' ',
                 'border_res': '10m',
                 'bkg': None}
    datadict = {'lat': model_lat, 'lon': model_lon,
                'plotable': np.nanmean(tracer_model, 0),
                'vmin': tracer.conc_lim[0], 'vmax': tracer.conc_lim[1],
                'label': '', 'text': tracer.label+' surface VMR (ppb)'}
    map2plot = map_prop(**prop_dict)
    projection = ccrs.PlateCarree()  # ccrs.Orthographic(8, 40)
    fig, ax0 = plt.subplots(subplot_kw=dict(projection=projection))
    scat = map2plot.plot_map(datadict, ax0, cmap=new_cm, projection=projection,
                             alpha=0.8)
    cb = fig.colorbar(scat, ax=ax0, orientation='vertical',
                      extend='max', fraction=0.03, shrink=0.7, pad=0.03)
    cb.set_label(datadict['text'], fontsize=10)
    plt.title('Mean {} surface VMR (May 2018)'.format(tracer.label))
    df_loc = [{'lon': 6.7623, 'lat': 51.4344, 'site': 'Duisburg'},
              {'lon': 9.4797, 'lat': 51.3127, 'site': 'Kassel'},
              {'lon': 6.9603, 'lat': 50.9375, 'site': 'Cologne'},
              {'lon': 8.6784, 'lat': 50.5841, 'site': 'Giessen'},
              {'lon': 7.5890, 'lat': 50.3569, 'site': 'Koblenz'},
              {'lon': 8.6821, 'lat': 50.1109, 'site': 'Frankfurt'},
              {'lon': 8.2283, 'lat': 49.9909, 'site': 'Mainz'},
              {'lon': 7.9046, 'lat': 49.9667, 'site': 'Bingen'},
              {'lon': 8.4660, 'lat': 49.4875, 'site': 'Mannheim'},
              {'lon': 8.4037, 'lat': 49.0069, 'site': 'Karlsruhe'},
              {'lon': 9.1829, 'lat': 48.7758, 'site': 'Stuttgart'}]

    for i, point in enumerate(df_loc):
        lo, la = point['lon'], point['lat']
        # plt.annotate(point['site'], xy=(lo, la),  xycoords='data',
        #              xytext=(lo, la), textcoords='data', color='white',
        #              horizontalalignment='left', verticalalignment='bottom',
        #              arrowprops=dict(facecolor='black', shrink=0.05))
        plt.text(lo, la, str(i+1), color='k', fontweight='bold',
                 bbox=dict(boxstyle='round', facecolor='white', alpha=0.5))
    bbox_props = dict(boxstyle="round", fc="w", ec="0.5", alpha=0.8)
    ax0.annotate(model_setup, xy=(0.4, 0.94),
                xycoords='axes fraction', size=13, bbox=bbox_props)
    # plt.savefig(os.path.join(dirname_sim, 'plots',
    #                           'mean_{}_map_{}.png'.format(tracer.name,
    #                                                       model_setup)),
    #             format='png', dpi=300)
# #-----------------------------------------------------------------
    f_sim.close()
    sim_data = {}
    target_lat, target_lon = [], []
    # sitelist = ['DERP008', 'DERP009']
    for site in sitelist:
        target_lon.append(stn_coords['coordinate L'][site])
        target_lat.append(stn_coords['coordinate L.1'][site])
        sim_data[site] = {tracer.name: [], 'temp': []}
    for i in range(temp_model.shape[0]):
        if i % 10 == 0:
            print(i)
        tracer_sim_now = griddata((model_lon.ravel(), model_lat.ravel()),
                               tracer_model[i, :].ravel(), (target_lon, target_lat),
                               method='linear')
        temp_now = griddata((model_lon.ravel(), model_lat.ravel()),
                            temp_model[i, :].ravel(), (target_lon, target_lat),
                            method='linear')
        for i, site in enumerate(sitelist):
            sim_data[site][tracer.name].append(tracer_sim_now[i])
            sim_data[site]['temp'].append(temp_now[i])
    sim_data['time'] = {'unit': time_unit, 'data': timeax}
    dict2hdf5(os.path.join(dirname, 'plots', 'model_intp',
                           'sim_{}_{}.hdf'.format(model_setup, tracer.name)),
              sim_data)
else:
    sim_data = hdf2dict(os.path.join(dirname, 'plots', 'model_intp',
                        'sim_{}_{}.hdf'.format(model_setup, tracer.name)))
    time_unit = sim_data['time']['unit']
    time_dsp = [datetime.strptime(time_unit,
                                  '%Y-%m-%d %H:%M:%S') + timedelta(days=i)
                for i in sim_data['time']['data']]
# %% station wise plots
fit_data = pd.DataFrame(columns=['site_code', 'site_name', 'type', 'lat',
                                 'lon', 'meas_mean', 'meas_std', 'sim_mean',
                                 'sim_std', 'slope', 'r', 'c', 'bias', 'rmse'])
fit_data_day = pd.DataFrame(columns=['site_code', 'type', tracer.name+'_sim',
                                     tracer.name+'_meas'])
taylor_stats = {}
for i, site in enumerate(sitelist):
    sim_data[site][tracer.name] = np.asarray(sim_data[site][tracer.name])
    sim_data[site]['temp'] = np.asarray(sim_data[site]['temp'])
    df_sim = pd.DataFrame(data={'date_time': time_dsp,
                                tracer.name+'_sim': sim_data[site][tracer.name],
                                'temp': sim_data[site]['temp']})
    df_sim = df_sim.set_index('date_time')
    if comp_sel_hours:
        sel_hours = np.arange(7, 18)
        df_sim_sel = df_sim[df_sim.index.hour.isin(sel_hours)]
    else:
        df_sim_sel = df_sim
    if df_sim[tracer.name+'_sim'].sum() == 0:
        continue
    try:
        df_meas = df[df['Station code'] == site]
        meas_col = 'Measure value'
    except KeyError:
        df_meas = df[site].to_frame()
        meas_col = site
    df_meas = df_meas.reindex(df_sim.index)
    df_meas['temp'] = df_sim['temp']
    df_meas['vmr'] = df_meas.apply(lambda x: tracer.massconc2vmr(x[meas_col],
                                   1e5, x['temp']), axis=1)
    if comp_sel_hours:
        df_meas_sel = df_meas[df_meas.index.hour.isin(sel_hours)]
    else:
        df_meas_sel = df_meas
    if plot_daily_mean:
        df_day = df_sim_sel.resample('D').mean().drop(columns='temp')
        df_day[tracer.name+'_meas'] = df_meas.resample('D').mean()[meas_col]
        df_day['site_code'] = site
        df_day['type'] = stn_coords['station type'][meas_col]
        fit_data_day = fit_data_day.append(df_day)
    mask = df_meas_sel['vmr'].isnull() | df_sim_sel[tracer.name+'_sim'].isnull()
    if np.sum(~mask) == 0:
        continue
    taylor_stats[site] = sm.taylor_statistics(df_sim_sel[tracer.name+'_sim'][~mask],
                                              df_meas_sel['vmr'][~mask])
    fit_stats = lin_fit(df_meas_sel['vmr'][~mask],
                        df_sim_sel[tracer.name+'_sim'][~mask])
    fit_param = fit_stats.stats_calc()
    fit_func = np.poly1d([fit_param['m'], fit_param['c']])
    fit_data.loc[i] = [site, meas_col,
                       stn_coords['station type'][meas_col],
                       stn_coords['coordinate L.1'][meas_col],
                       stn_coords['coordinate L'][meas_col],
                       df_meas_sel['vmr'][~mask].mean(),
                       df_meas_sel['vmr'][~mask].std(),
                       df_sim_sel[tracer.name+'_sim'][~mask].mean(),
                       df_sim_sel[tracer.name+'_sim'][~mask].std(),
                       fit_param['m'], fit_param['r'], fit_param['c'],
                       fit_param['bias'], fit_param['rmse']]
    # if site in ['DERP007', 'DERP009', 'DERP010', 'DERP011']:
    if plot_station_ts:
        fig, ax = plt.subplots(1, 2, figsize=[14, 4],
                               gridspec_kw={'width_ratios': [2.5, 1]})
        ax[0].plot(df_sim.index, df_sim[tracer.name+'_sim'], '-x',
                   label='MECO(n)')
        ax[0].plot(df_meas.index, df_meas['vmr'], '-x', label='Measured')
        ax[0].text(0.01, 0.9, site+'-'+stn_coords['station name'][site],
                   transform=ax[0].transAxes)
        ax[0].grid(alpha=0.4, axis='y')
        ax[0].set_ylabel(tracer.label + " VMR (ppb)", fontsize=14)
        ax[0].xaxis.set_major_formatter(mdates.DateFormatter("%d-%b"))
        # for cloud_type, cloud_val in {'Broken clouds': 4,
        #                               'Cont. clouds': 5}.items():
        #     cloud_sel = cloud_dat[cloud_dat['cloud_type'] == cloud_val]
        #     ax[0].bar(cloud_sel.index, 80, color='grey',
        #               alpha=0.3*(cloud_val-3), width=0.03, label=cloud_type)
        # cloud_sel = cloud_dat[cloud_dat['Thick clouds_o4'] == 1]
        # ax[0].bar(cloud_sel.index, 80, color='r',
        #           alpha=0.4, width=0.03, label='Optically\nthick clouds')
        # ax[0].set_xlim(date(2018, 5, 1), date(2018, 6, 1))
        # ax[0].set_ylim(0, 80)
        ax[1].scatter(df_meas_sel['vmr'][~mask],
                      df_sim_sel[tracer.name+'_sim'][~mask],
                      s=4, label='hourly data')
        ax[1].set_xlim(ax[0].get_ylim())
        ax[1].set_ylim(ax[0].get_ylim())
        ax[1].set_ylabel("Simulated " + tracer.label + " VMR (ppb)",
                         fontsize=14)
        ax[1].set_xlabel("Measured " + tracer.label + " VMR (ppb)",
                         fontsize=14)
        ax[1].plot([0, 1], [0, 1],
                   transform=ax[1].transAxes, ls="--", c=".3", label='1:1')
        ax[1].plot(df_meas['vmr'][~mask], fit_func(df_meas['vmr'][~mask]),
                   c="k", label='linear regression')
        fit_stats.add_fit_param(pos_x=0.05, pos_y=0.85, ax=ax[1],
                                color='k', showr=True, c_fmt='%.2f')
        ax[0].legend(loc='upper right')
        plt.tight_layout()
        plt.savefig(os.path.join(dirname, 'plots',
                                '{}_{}.png'.format(model_setup, site)),
                    format='png', dpi=100)
        plt.close()
# # Diurnal profiles
    if site in ['DERP007', 'DERP009', 'DERP010', 'DERP011', 'DERP017',
                'DEUB029']:
    # if plot_diurnal_profiles:
        fig, ax = plt.subplots(figsize=[9, 4])
        plot_diurnal(df_meas['vmr'], df_meas.index, label='Measured',
                     color='r', ax=ax)
        plot_diurnal(df_sim[tracer.name+'_sim'], df_sim.index, label='CM02',
                     color='b', ax=ax)
        ax.text(0.65, 0.85, site+'\n'+stn_coords['station name'][site],
                transform=ax.transAxes, size=14)
        ax.legend(loc='upper left', prop={'size':14})
        bbox_props = dict(boxstyle="round", fc="w", ec="0.5", alpha=0.9)
        ax.annotate(model_setup, xy=(0.5, 0.85),
                    xycoords='axes fraction', size=14, bbox=bbox_props)
        ax.set_ylabel(tracer.label + ' VMR (ppb)', size=14)
        ax.set_xlabel('Time (UTC)', size=14)
        ax.tick_params(axis='both', which='major', labelsize=14)
        ax.grid(alpha=0.4)
        ax.set_ylim(0, 70)
        plt.axhline(59, color='k', alpha=0.5)
        plt.tight_layout()
        plt.savefig(os.path.join(dirname, 'plots',
                                 'diurnal_{}_{}.png'.format(model_setup, site)),
                    format='png', dpi=300)
        plt.close()
#fit_data.to_csv(os.path.join(dirname, 'plots',
#                             'summary_comp{}.csv'.format(model_setup)))
if create_sim_dict:
    for site_type, m_type in {'Hintergrund': 's',
                              'Verkehr': 'o', 'Industrie': 'p'}.items():
        fit_data_sel = fit_data[fit_data['type'] == site_type]
        ax0.scatter(fit_data_sel['lon'], fit_data_sel['lat'],
                    c=fit_data_sel['meas_mean'], marker=m_type, s=30,
                    edgecolor='k', vmin=tracer.conc_lim[0],
                    vmax=tracer.conc_lim[1], cmap=new_cm)
# %% scatter plot
fig, ax = plt.subplots()
pos_y = 0.83
translate = {'Hintergrund': 'Background', 'Verkehr': 'Traffic',
             'Industrie': 'Industry'}
for site_type, c_val in {'Hintergrund': ['b', 's'], 'Verkehr': ['red', 'o'],
                         'Industrie': ['k', 'p']}.items():
    fit_data_sel = fit_data[fit_data['type'] == site_type]
    ax.scatter('meas_mean', 'sim_mean', data=fit_data_sel, c=c_val[0],
               marker=c_val[1], label=translate[site_type])
    #  not enough O3 measurements available for traffic and industry locations
    if tracer.name == "O3":
        if site_type != "Hintergrund":
            continue
    mask = fit_data_sel['meas_mean'].isnull() | fit_data_sel['sim_mean'].isnull()
    fit_stats = lin_fit(fit_data_sel['meas_mean'][~mask].values,
                        fit_data_sel['sim_mean'][~mask].values,
                        sx=fit_data_sel['meas_std'][~mask].values,
                        sy=fit_data_sel['sim_std'][~mask].values)
    fit_param = fit_stats.stats_calc(method="odr")
    fit_func = np.poly1d([fit_param['m'], fit_param['c']])
    # ax.plot(fit_data['meas_mean'],
    #         fit_func(fit_data['meas_mean']),
    #         c=c_val[0], label=None)
    fit_stats.add_fit_param(pos_x=0.05, pos_y=pos_y, ax=ax,
                            color=c_val[0], showr=True, c_fmt='%.2f')
    pos_y -= 0.1
ax.legend(loc='upper right', prop={'size': 12})
ax.set_xlabel('Measured {} VMR (ppb)'.format(tracer.label), size=14)
ax.set_ylabel('Simulated {} VMR (ppb)'.format(tracer.label), size=14)
plt.tight_layout()
# ax.set_xlim(0, 35)
# ax.set_ylim(0, 35)
ax.set_ylim(20, 60)
ax.set_xlim(20, 60)
ax.plot([0, 1], [0, 1], transform=ax.transAxes, ls="--", c=".3", label='1:1')
ax.fill_between([0, 1], [0, 2], [0, 1], transform=ax.transAxes, ls=":",
                color=".3", alpha=.2, label='$\pm 50\%$')
ax.fill_between([0, 1], [0, 0.5], [0, 1], transform=ax.transAxes,
                ls=":", color=".3", alpha=.2, label=None)
ax.annotate(model_setup, xy=(0.05, 0.94), xycoords='axes fraction', size=14)
ax.grid(alpha=0.3)
plt.tight_layout()
plt.savefig(os.path.join(dirname, 'plots',
                          'scatter_obs_{}_{}.png'.format(tracer.name, model_setup)),
            format='png', dpi=300)
if plot_daily_mean:
    fig, ax = plt.subplots()
    pos_y = 0.8
    for site_type, c_val in {'Hintergrund': 'b',
                             'Verkehr': 'red', 'Industrie': 'k'}.items():
        fit_data_sel = fit_data_day[fit_data_day['type'] == site_type]
        ax.scatter(tracer.name+'_meas', tracer.name+'_sim',
                   data=fit_data_sel, c=c_val, label=site_type, s=4)
        fit_stats = lin_fit(fit_data_sel[tracer.name+'_meas'],
                            fit_data_sel[tracer.name+'_sim'])
        fit_param = fit_stats.stats_calc()
        fit_func = np.poly1d([fit_param['m'], fit_param['c']])
        ax.plot(fit_data_day[tracer.name+'_meas'],
                fit_func(fit_data_day[tracer.name+'_meas']),
                c=c_val, label=None)
        fit_stats.add_fit_param(pos_x=0.05, pos_y=pos_y, ax=ax,
                                color=c_val, showr=True, c_fmt='%.2f',
                                show_fit_eqn=False)
        pos_y -= 0.1
    ax.legend(loc='upper right', prop={'size': 12})
    ax.set_xlabel('Measured {} (ppb)'.format(tracer.label))
    ax.set_ylabel('Simulated {} (ppb)'.format(tracer.label))
    plt.tight_layout()
    # ax.set_ylim(0, 100)
    # ax.set_xlim(0, 100)
    ax.grid(alpha=0.3)
    ax.plot([0, 1], [0, 1], transform=ax.transAxes, ls="--", c=".3", label='1:1')
    ax.fill_between([0, 1], [0, 1.3], [0, 1], transform=ax.transAxes, ls=":",
                    color=".3", alpha=.2, label='$\pm 30\%$')
    ax.fill_between([0, 1], [0, 0.7], [0, 1], transform=ax.transAxes,
                    ls=":", color=".3", alpha=.2, label=None)
    ax.annotate(model_setup, xy=(0.05, 0.94),
                xycoords='axes fraction', size=14)
    plt.savefig(os.path.join(dirname, 'plots',
                             'scatter_obs_{}_{}.png'.format(tracer.name,
                                                            model_setup)),
                format='png', dpi=300)

# %%
rel_bias = [0]
rel_bias.extend((100*fit_data['bias']/fit_data['meas_mean']).tolist())
sdev = [1]
sdev.extend([vals['sdev'][1]/vals['sdev'][0]
             for key, vals in taylor_stats.items()])
crmsd = [0]
crmsd.extend([vals['crmsd'][1] for key, vals in taylor_stats.items()])
crmsd = np.asarray(crmsd)

ccoef = [1]
ccoef.extend([vals['ccoef'][1] for key, vals in taylor_stats.items()])
ccoef = np.asarray(ccoef)
if tracer.name == 'O3':
    ccoef[ccoef < 0] = np.nan

sdev = np.asarray(sdev)
sdev[sdev > 2] = np.nan
fig, ax = plt.subplots(figsize=[6, 6])

overlay = "off"
for site_type, m_val in {'Hintergrund': 's', 'Verkehr': 'o',
                         'Industrie': 'p'}.items():
    keep = [True]
    if site_type != 'Hintergrund':
        overlay = "on"
    keep.extend(fit_data['type'] == site_type)
    sm.taylor_diagram(np.array(sdev)[keep], crmsd[keep], np.array(ccoef)[keep],
                      axismax=2, widthRMS=0.5, widthCOR=0.5, styleCOR='--',
                      colormap='on', cmap='RdBu_r', cmap_vmin=-50, cmap_vmax=50,
                      cmap_marker=m_val, markerDisplayed='colorBar',
                      titleColorbar='Relative bias (%)', cmapzdata=np.array(rel_bias)[keep],
                      colOBS='r', markerobs='o', titleOBS='observation', overlay=overlay)
    ax.scatter(np.nan, np.nan, marker=m_val, color='k', 
            label=translate[site_type])
ax.set_ylabel('Standard Deviation (Normalized)')
if model_setup == 'TNO_fl':
    if tracer.name == 'O3':
        ax.legend(loc=[0.85, 0.7], frameon=False)
    else:
        ax.legend(loc=[-0.1, 0.8], frameon=False)
if tracer.name == 'O3':
    ax.annotate(model_setup, xy=(0.1, 0.87),
                 xycoords='axes fraction', size=12)
else:
    ax.annotate(model_setup, xy=(0.35, 0.87),
                     xycoords='axes fraction', size=12)
# plt.savefig(os.path.join(dirname, 'plots',
#                           'taylor_{}_{}.png'.format(tracer.name, model_setup)),
#             format='png', dpi=300)
# %% test plot
# site = 'DERP007'
# sim_data[site][tracer.name] = np.asarray(sim_data[site][tracer.name])
# sim_data[site]['temp'] = np.asarray(sim_data[site]['temp'])
# df_sim = pd.DataFrame(data={'date_time': time_dsp,
#                             tracer.name+'_sim': sim_data[site][tracer.name],
#                             'temp': sim_data[site]['temp']})
# df_sim = df_sim.set_index('date_time')
# if comp_tropomi_overpass:
#     df_sim_sel = df_sim[df_sim.index.hour.isin([12, 13])]
# else:
#     df_sim_sel = df_sim
# try:
#     df_meas = df[df['Station code'] == site]
#     meas_col = 'Measure value'
# except KeyError:
#     df_meas = df[site].to_frame()
#     meas_col = site
# df_meas = df_meas.reindex(df_sim.index)
# df_meas['temp'] = df_sim['temp']
# df_meas['vmr'] = df_meas.apply(lambda x: tracer.massconc2vmr(x[meas_col],
#                                 1e5, x['temp']), axis=1)
# fig, ax = plt.subplots(figsize=[9, 4])
# plot_diurnal(df_meas['vmr'], df_meas.index, label='Measured',
#               color='r', ax=ax)
# plot_diurnal(df_sim['NO2_sim'], df_sim.index, label='CM02',
#               color='b', ax=ax)
# ax.text(0.75, 0.9, site+'-'+stn_coords['station name'][site],
#         transform=ax.transAxes)
# bbox_props = dict(boxstyle="round", fc="w", ec="0.5", alpha=0.9)
# ax.annotate('{}'.format(model_setup[:6]), xy=(0.5, 0.9),
#               xycoords='axes fraction', size=16, bbox=bbox_props)
# ax.grid(alpha=0.3)
# ax.set_ylabel(tracer.label + ' VMR (ppb)')
# ax.set_xlabel('Time (UTC)')
# plt.tight_layout()
# plt.savefig(os.path.join(dirname, 'plots',
#                           'diurnal_{}_{}.png'.format(model_setup, site)),
#             format='png', dpi=100)