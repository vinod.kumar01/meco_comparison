# -*- coding: utf-8 -*-
"""
Created on Wed Jun  3 22:36:18 2020
Module to estimate the surface concentration from TROPOMI VCD measurements
by using the ratio of sfc concentration and VCD calculated from regional model
@author: Vinod
"""
# %% definitons and imports
from netCDF4 import Dataset
from os import path
from plot_tools import map_prop
import cartopy.crs as ccrs
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from datetime import timedelta
from scipy.interpolate import griddata


def my_to_datetime(date_str, format="%Y-%m-%d'%H:%M'"):
    if date_str[11:13] != '24':
        return pd.to_datetime(date_str, format=format)

    date_str = date_str[0:11] + '00' + date_str[13:]
    return pd.to_datetime(date_str, format=format) + timedelta(days=1)

# %% load data
dirname =  r'M:\nobackup\vinod\model_work\MECO\sorbit_subsample\2018_05\UBA_fl\qa_75'
filename = 'meanVCD05_regrid_inc_edge.nc'
f = Dataset(path.join(dirname, filename), 'r')
lats = f.variables['lat'][:]
lons = f.variables['lon'][:]
no2_conc_sim = f.variables['NO2_conc_sim'][:]
no2_conc_sim_i = f.variables['NO2_conc_sim_i'][:]
sim_VCD_i = f.variables['NO2_tropcol_sim_i'][:]
tropomi_VCD = f.variables['NO2_tropcol_sat'][:]
tropomi_VCD_corr = f.variables['NO2_tropcol_sat_corr'][:]

rat_sim_grid = no2_conc_sim_i/sim_VCD_i
tropomi_conc = tropomi_VCD*rat_sim_grid
tropomi_conc_corr = tropomi_VCD_corr*rat_sim_grid
# %% calculate model VCD in original COSMO grid
'''
This we we avoid vertical interpolation which was done previously
to being TROPOMI and MECOn on same vertical grid.
'''
f_orig = Dataset(r'M:\nobackup\vinod\model_work\MECO\sorbit_subsample\UBA_fl_05_sorbit_s5a.nc')
tm1 = f_orig.variables['COSMO_tm1'][:]
dhyam = f_orig.variables['dhyam'][:]
geolon = f_orig.variables['geolon'][:]
geolat = f_orig.variables['geolat'][:]
aps = f_orig.variables['ps'][:]
prs_m = np.array([f_orig.variables['dhyam'][i] +
                  (aps[:]*f_orig.variables['dhybm'][i])
                  for i in range(0, len(dhyam))])
prs_m = np.transpose(prs_m, (1, 0, 2, 3))
prs_m[prs_m < 0] = np.nan
tracer_mr = f_orig.variables['tracer_gp_NO2'][:]
tracer_conc = tracer_mr*6.023E23*prs_m*1E-6/8.314/tm1

target_grid = np.meshgrid(lons, lats)
target_grid = (target_grid[0], target_grid[1])
sfc_conc_sim = tracer_conc[:, -1, :]
sfc_conc_sim_regrid = []
for i in range(sfc_conc_sim.shape[0]):
    sfc_conc_sim_regrid.append(griddata((geolon.ravel(), geolat.ravel()),
                               sfc_conc_sim[i, :].ravel(), target_grid,
                               method='linear'))
sfc_conc_sim_regrid = np.array(sfc_conc_sim_regrid)
rat_sim_grid = sfc_conc_sim_regrid/sim_VCD_i
# molecules cm-3
#'''
#calculation of box height
#'''
#h_interface = np.nanmean(f_orig.variables['COSMO_ORI_HHL'][:], 0)
#h_agl = h_interface[:, :, :] - h_interface[40, :, :]
#h = h_interface[:-1, :, :] - h_interface[1:, :, :]
#tracer_sim_vcd_box = np.ma.masked_array([tracer_conc[i, :]*h*100
#                                         for i in range(tracer_conc.shape[0])])
#tracer_sim_geovcd = np.nansum(tracer_sim_vcd_box[:, 13:len(dhyam), :, :], 1)
#rat_sim = tracer_conc[:, -1, :]/(tracer_sim_geovcd*1e-16)
#target_grid = np.meshgrid(lons, lats)
#target_grid = (target_grid[0], target_grid[1])
#rat_sim_grid = []
#for i in range(rat_sim.shape[0]):
#    rat_now = griddata((geolon.ravel(), geolat.ravel()),
#                       rat_sim[i, :].ravel(), target_grid, method='linear')
#    rat_sim_grid.append(rat_now)
#rat_sim_grid = np.array(rat_sim_grid)
f_orig.close()
tropomi_conc = tropomi_VCD*rat_sim_grid
tropomi_conc_corr = tropomi_VCD_corr*rat_sim_grid
# %% diagnostic plots
llon, ulon, llat, ulat = 0, 19, 43, 56
prop_dict = {'extent': [llon, ulon, llat, ulat],
             'name': 'MECO_DE',
             'border_res': '10m'}
projection = ccrs.PlateCarree()
map2plot = map_prop(**prop_dict)
datadict = {'lat': lats, 'lon': lons,
            'plotable': np.nanmean(tropomi_conc_corr, 0),
            'vmin': 1e9, 'vmax': 1e11, 'label': '',
            'text': 'NO$_{2}$ surface concentration (molecules cm$^{-3}$)'}
fig, ax = plt.subplots(subplot_kw=dict(projection=projection))
cs = map2plot.plot_map(datadict, ax, cmap='jet', alpha=0.9)
cb = plt.colorbar(cs, ax=ax, orientation='vertical', extend='max',
                  fraction=0.05, shrink=0.7, pad=0.07)
cb.set_label(datadict['text'], fontsize=10)
# %% load measurement data
meas_dir = r'M:\nobackup\vinod\model_work\MECO\station_data\UBA'
coord_file = 'STATIONS_KOORDINATEN.xlsx'
stn_coords = pd.read_excel(path.join(meas_dir, coord_file),
                           header=1, skiprows=[2])
stn_coords = stn_coords.set_index('station code')

meas_file = "DE2018NO2_inv1SMW_20200605.csv"
df = pd.read_csv(path.join(meas_dir, meas_file), dtype=None, delimiter=';',
                 skiprows=[1, 2, 3], header=0, na_values=[-999])
df['Date_time'] = df['Datum'] + df['Uhrzeit']
df['Date_time'] = df['Date_time'].apply(my_to_datetime,
                                        format="'%Y%m%d''%H:%M'")


'''
# For data downloaded from web interface

meas_file = 'stations_2018-04-30-2018-05-31.csv'
names = ["State", "Station code", "Station name", "Station setting",
         "Station type", "Pollutant", "Time scope", "Date", "Time",
         "Measure value", "Unit"]
df = pd.read_csv(path.join(meas_dir, meas_file), dtype=None, delimiter=';',
                 names=names, header=0)
df['Date_time'] = df['Date'] + df['Time']
df['Date_time'] = df['Date_time'].apply(my_to_datetime)
df['Measure value'] = pd.to_numeric(df['Measure value'], errors='coerce')
'''
# Measurement data is averaged at end of hour
df['Date_time'] = df.apply(lambda x: x['Date_time']-timedelta(hours=1), axis=1)
df = df.set_index('Date_time')
df_sel = df[(df.index.hour.isin([12, 13, 14]) & (df.index.month == 5))]

# %% extract tropomi data at measurement location
dict_comp = {}
target_lat, target_lon = [], []
#sitelist = df['Station code'].unique()
sitelist = [i for i in df.columns if i.startswith('DE')]
for site in sitelist:
    target_lon.append(stn_coords['coordinate L'][site])
    target_lat.append(stn_coords['coordinate L.1'][site])
#    meas_data = df_sel[df_sel['Station code'] == site]['Measure value'].values
#    meas_data = df_sel[df_sel['Station code'] == site]['Measure value'].mean()
    meas_data = df_sel[site].mean()
    dict_comp[site] = {'no2_tropomi': [], 'no2_tropomi_corr': [],
                       'sim_conc': [], 'no2_meas': meas_data}

llats, llons = np.meshgrid(lats, lons)

# Montly mean data
# convert into ug/m3
data_now = np.nanmean(tropomi_conc, 0).T
data_now *= 1e6*1e6*46/6.023e23
# conversion from molecules cm-3 to ug m-3
data_corr_now = np.nanmean(tropomi_conc_corr, 0).T
data_corr_now *= 1e6*1e6*46/6.023e23

sim_conc_now = np.nanmean(tracer_conc[:, 39, :, :], 0).T
sim_conc_now *= 1e6*1e6*46/6.023e23

stn_data_now = griddata((llats.ravel(), llons.ravel()), data_now.ravel(),
                        (target_lat, target_lon), method='linear')
stn_data_corr_now = griddata((llats.ravel(), llons.ravel()),
                             data_corr_now.ravel(), (target_lat, target_lon),
                             method='linear')
stn_sim_conc_now = griddata((geolat.ravel(), geolon.ravel()),
                            sim_conc_now.T.ravel(), (target_lat, target_lon),
                            method='linear')
for i, site in enumerate(sitelist):
    dict_comp[site]['no2_tropomi'].append(stn_data_now[i])
    dict_comp[site]['no2_tropomi_corr'].append(stn_data_corr_now[i])
    dict_comp[site]['sim_conc'].append(stn_sim_conc_now[i])
for key, vals in dict_comp.items():
    dict_comp[key]['no2_tropomi'] = np.array(dict_comp[key]['no2_tropomi'])
    dict_comp[key]['no2_tropomi_corr'] = np.array(dict_comp[key]['no2_tropomi_corr'])
    dict_comp[key]['sim_conc'] = np.array(dict_comp[key]['sim_conc'])

# # Daily data
#for day in range(tropomi_conc_corr.shape[0]):
#    data_now = tropomi_conc[day, :].T
#    data_now *= 1e6*1e6*46/6.023e23
#    # conversion from molecules cm-3 to ug m-3
#    data_corr_now = tropomi_conc_corr[day, :].T
#    data_corr_now *= 1e6*1e6*46/6.023e23
#    stn_data_now = griddata((llats.ravel(), llons.ravel()), data_now.ravel(),
#                            (target_lat, target_lon), method='linear')
#    stn_data_corr_now = griddata((llats.ravel(), llons.ravel()),
#                                 data_now.ravel(), (target_lat, target_lon),
#                                 method='linear')
#    for i, site in enumerate(sitelist):
#        dict_comp[site]['no2_tropomi'].append(stn_data_now[i])
#        dict_comp[site]['no2_tropomi_corr'].append(stn_data_corr_now[i])
# %% scatter plot
data_table = pd.DataFrame(columns=['site_code', 'site_name', 'site_type',
                                   'lat', 'lon', 'meas_conc', 'sim_conc',
                                   'tropomi_conc', 'tropomi_corr_conc'])
i = 0
for key, vals in dict_comp.items():
    dict_comp[key]['sim_conc'] = np.array(dict_comp[key]['sim_conc'])
    dict_comp[key]['no2_tropomi'] = np.array(dict_comp[key]['no2_tropomi'])
    dict_comp[key]['no2_tropomi_corr'] = np.array(dict_comp[key]['no2_tropomi_corr'])
    data_table.loc[i] = [key, stn_coords['station name'].loc[key],
                         stn_coords['station type'].loc[key],
                         stn_coords['coordinate L.1'].loc[key],
                         stn_coords['coordinate L'].loc[key], vals['no2_meas'],
                         np.asscalar(vals['sim_conc']),
                         np.asscalar(vals['no2_tropomi']),
                         np.asscalar(vals['no2_tropomi_corr'])]
    i += 1
#meas_data_gridded = griddata((data_table['lon'], data_table['lat']),
#                             data_table['meas_conc'], target_grid,
#                             method='linear')
llon, ulon, llat, ulat = 5.8, 15.2, 46.5, 55.5
prop_dict = {'extent': [llon, ulon, llat, ulat],
             'name': 'MECO_DE',
             'border_res': '10m'}
map2plot = map_prop(**prop_dict)
datadict = {'lat': data_table['lat'], 'lon': data_table['lon'],
            'plotable': data_table['tropomi_corr_conc'],
            'vmin': 4, 'vmax': 59, 'label': '',
            'text': 'NO$_{2}$ surface concentration ($\mu$g m$^{-3}$)'}
fig, ax = plt.subplots(subplot_kw=dict(projection=projection))
cs = map2plot.plot_map(datadict, ax, cmap='jet', mode='scatter',
                       alpha=0.9, s=20)
cb = plt.colorbar(cs, ax=ax, orientation='vertical', extend='max',
                  fraction=0.05, shrink=0.7, pad=0.07)
cb.set_label(datadict['text'], fontsize=10)
