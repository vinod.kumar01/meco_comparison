# -*- coding: utf-8 -*-
"""
Created on Sat Sep  7 11:03:44 2019
script to sort out a given directory and move files to 
subdirectory based on type
@author: Vinod
"""

import os
import shutil
dirname = r'C:\Users\Vinod\Downloads'
names = os.listdir(dirname)
folder_name = ['Images', 'Documents', 'Compressed', 'Programs', 'Data', 'Citations']

category_list = [['jpg', 'JPG', 'jpeg', 'png', 'tiff', 'tif'],
                 ['pdf', 'doc', 'docx', 'ppt', 'pptx', 'tex', 'djvu'],
                 ['zip', 'tar', 'gz'],
                 ['exe', 'py', 'ncl', 'iso', 'msi', 'sh'],
                 ['xls', 'xlsx', 'dat', 'nc', 'pxp', 'csv', 'hdf', 'he5',
                  'kmz', 'shp', 'kml'],
                 ['bib', 'ris', 'enw', 'bibtex']]

for files in names:
    file_extension = files.split('.')[-1]
    for dest, extension in zip(folder_name, category_list):
        # Check if the destination folder exists
        if not os.path.exists(os.path.join(dirname, dest)):
            os.makedirs(os.path.join(dirname, dest))
        # Check if the file is already present in the folder
        if (file_extension in extension):
            if  not os.path.exists(os.path.join(dirname, dest, files)):
                shutil.move(os.path.join(dirname, files), os.path.join(dirname, dest, files))