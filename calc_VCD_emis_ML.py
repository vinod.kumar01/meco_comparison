# -*- coding: utf-8 -*-
"""
Created on Fri Jan  8 16:55:41 2021

@author: Vinod
"""

import netCDF4
import numpy as np
import os
from tensorflow import keras
from ML_tools import arrange_feature_flux, arrange_emis
from plot_tools import plt, map_prop, ccrs
from matplotlib.colors import LinearSegmentedColormap


def man_cm(vals, c_names):
    norm = plt.Normalize(min(vals), max(vals))
    tuples = list(zip(map(norm, vals), c_names))
    new_cm = LinearSegmentedColormap.from_list("", tuples)
    return new_cm


llon, ulon, llat, ulat = 0, 19, 43, 56
prop_dict = {'extent': [llon, ulon, llat, ulat], 'name': 'MECO_DE',
             'border_res': '10m'}
projection = ccrs.PlateCarree()
map2plot = map_prop(**prop_dict)

vals = [5e4, 1e5, 1.2e5, 1.5e5, 1.8e5, 2e5, 4e5, 5e5, 7e5]
c_names = ["azure", "deepskyblue", "aquamarine", "greenyellow", "darkorange",
           "orangered", "red", "darkred", "purple"]
new_cm = man_cm(vals, c_names)

# %%load model data and calculate VCD
dirname = r'M:\nobackup\vinod\model_work\MECO\sorbit_subsample'
exp = 'TNO_fl'
months = [5]

count_dat = 0
for month in months:
    sorbit_c = '{}_{}_sorbit_s5a.nc'.format(exp, str(month).zfill(2))
    meco_out = os.path.join(dirname, sorbit_c)
    if count_dat == 0:
        dataset2d = arrange_feature_flux(meco_out)
    else:
        dataset2d = np.append(dataset2d, arrange_feature_flux(meco_out), axis=0)
    count_dat += 1
# dataset2d = dataset2d.reshape(dataset2d.shape[0],
#                               dataset2d.shape[1]*dataset2d.shape[2])
# %% Load emission data for validation
tracer = 'NO2'
year = 2018
dirname = r'M:\nobackup\vinod\model_work\MECO\sorbit_subsample'
emis_file = 'TNO_anth_NOx_sfc_2011_remapcon.nc'
# emis_file = 'UBA_TNO_2018_syn_sfc_remapcon.nc'

count_dat = 0
for month in months:
    if count_dat == 0:
        flux_val = arrange_emis(os.path.join(dirname, 'emissions', emis_file),
                                month)
    else:
        flux_val = np.append(flux_val,
                             arrange_emis(os.path.join(dirname, 'emissions', emis_file),
                                           month), axis=0)
    count_dat += 1


# %% plotting emissions
with netCDF4.Dataset(meco_out) as sim_c:
    model_lon = sim_c.variables['geolon'][:]
    model_lat = sim_c.variables['geolat'][:]


with netCDF4.Dataset(os.path.join(dirname, 'emissions', emis_file)) as f:
    mw = f.variables['NOx_flux'].molar_mass
    emis_flux = f.variables['NOx_flux'][month-1, :]
    mean_flux = mw*emis_flux/6.023E23/1000  # Kg m-2 s-1
    mean_flux *= 1e9  # ug m-2 s-1
    # area = f.variables['area'][:]
    # emis = mean_flux*area*3600*24*365  # Kg year-1
# Plot test emissions
datadict = {'lat': model_lat, 'lon': model_lon, 'vmin': 0, 'vmax': 1,
            'text': 'NOx emission flux (${\mu}g ~m^{-2} ~s^{-1}$)',
            'label': '', 'plotable': mean_flux}
fig, ax = plt.subplots(subplot_kw=dict(projection=ccrs.PlateCarree()))
cs = map2plot.plot_map(datadict, ax, cmap=new_cm,
                       mode='pcolormesh',
                       projection=projection, alpha=1)
fig.canvas.draw()
plt.tight_layout()
fig.subplots_adjust(right=0.86, wspace=0.02)
cbar_ax = fig.add_axes([0.88, 0.2, 0.02, 0.6])
cbar = fig.colorbar(cs, cax=cbar_ax, extend='max', shrink=0.8)
cbar.set_label(datadict['text'], fontsize=10)

# %% calculate chemistry sensititvity
# base_file = 'TNO_sfc_05_sorbit_s5a.nc'
# extra_file = 'TNO_sfc_1_2_05_sorbit_s5a.nc'
# vcd_base = np.ma.mean(calc_vcd(os.path.join(dirname, base_file)), 0)
# vcd_extra = np.ma.mean(calc_vcd(os.path.join(dirname, extra_file)), 0)
# beta = 0.2*vcd_base/(vcd_extra-vcd_base)



# %% load DNN model
dnn_model = keras.models.load_model('UBA_05_06_02_model')

# %% test model and make prediction
# test model
test_results = {}
test_results['model'] = dnn_model.evaluate(dataset2d, flux_val)
print(test_results)

# make predictions
test_predictions = dnn_model.predict(dataset2d).flatten()
fig, ax = plt.subplots(1, 2, figsize=[8, 4],
                       gridspec_kw={'width_ratios': [0.6, 0.4]})
# ax[0].scatter(test_labels, test_predictions, s=2)
hbin = ax[0].hexbin(flux_val, test_predictions, gridsize=1000, mincnt=1,
                    cmap='jet', vmax=100)
ax[0].set_xlabel('Test emission')
ax[0].set_ylabel('Predicted emission')
ax[0].set_ylabel('Predicted emission ($\mu$g m$^{-2}$ s$^{-1}$)')
cb = plt.colorbar(hbin, ax=ax[0], orientation='vertical', shrink=0.9,
                  extend='max')
lims = [0, 1]
ax[0].set_xlim(lims)
ax[0].set_ylim(lims)
ax[0].plot(lims, lims, alpha=0.5, c='k')
error = test_predictions - flux_val
ax[1].hist(error, bins=np.arange(-1, 1, 0.02))
ax[1].set_xlabel('Prediction Error [Emission]')
ax[1].set_ylabel('Count')
ax[1].set_xlim(-1, 1)
plt.tight_layout()
ax[0].grid(alpha=0.3)
ax[1].grid(alpha=0.3)

# %% add lat lon information to emissions
test_predictions_grid = test_predictions.reshape(176, 156)
# Plot predicted emissions
datadict = {'lat': model_lat[2:-2, 2:-2], 'lon': model_lon[2:-2, 2:-2],
            'vmin': 0, 'vmax': 1,
            'text': 'NOx emission flux (${\mu}g ~m^{-2} ~s^{-1}$)',
            'label': '', 'plotable': test_predictions_grid}
fig, ax = plt.subplots(subplot_kw=dict(projection=ccrs.PlateCarree()))
cs = map2plot.plot_map(datadict, ax, cmap=new_cm,
                       mode='pcolormesh',
                       projection=projection, alpha=1)
fig.canvas.draw()
plt.tight_layout()
fig.subplots_adjust(right=0.86, wspace=0.02)
cbar_ax = fig.add_axes([0.88, 0.2, 0.02, 0.6])
cbar = fig.colorbar(cs, cax=cbar_ax, extend='max', shrink=0.8)
cbar.set_label(datadict['text'], fontsize=10)