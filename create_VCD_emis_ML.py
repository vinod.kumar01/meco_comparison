# -*- coding: utf-8 -*-
"""
Created on Mon Jan  4 14:01:08 2021

@author: Vinod
"""

import numpy as np
import os
from tensorflow import keras
from tensorflow.keras import layers, Input, Model
from tensorflow.keras.layers.experimental import preprocessing
from keras.utils.vis_utils import plot_model
from plot_tools import plt, map_prop, ccrs
from ML_tools import arrange_feature_flux, arrange_emis
# from tools import wind
from sklearn.model_selection import train_test_split

llon, ulon, llat, ulat = 0, 19, 43, 56
prop_dict = {'extent': [llon, ulon, llat, ulat], 'name': 'MECO_DE',
             'border_res': '10m'}
projection = ccrs.PlateCarree()
map2plot = map_prop(**prop_dict)


def plot_loss(history):
    fig, ax = plt.subplots()
    ax.plot(history.history['loss'], label='Training loss')
    ax.plot(history.history['val_loss'], label='Validation loss')
    ax.set_ylim([0, 0.1])
    ax.set_xlabel('Epoch')
    ax.set_ylabel('Error [emis]')
    ax.legend()
    plt.grid(alpha=0.3)
    return ax


# %% load emissions for training
tracer = 'NO2'
year = 2018

months = [5, 6, 7, 11, 12]
dirname = r'M:\nobackup\vinod\model_work\MECO\sorbit_subsample'
emis_file = 'UBA_TNO_anth_sum_cm07_remapcon.nc'
count_dat = 0
for month in months:
    if count_dat == 0:
        flux_val = arrange_emis(os.path.join(dirname, 'emissions', emis_file),
                                month)
    else:
        flux_val = np.append(flux_val,
                             arrange_emis(os.path.join(dirname, 'emissions', emis_file),
                                          month), axis=0)
    count_dat += 1
# Add gaussian noise
# noise = np.random.normal(0, 0.05, len(flux_val))
# flux_val += noise

# %% add additional TNO data
# months = [5]
# emis_file = 'UBA_TNO_anth_sum_cm07_remapcon.nc'
# flux_val = np.append(1.2*flux_val,
#                       arrange_label(os.path.join(dirname, 'emissions', emis_file),
#                                     month), axis=0)
# # months = [5]
# # emis_file = 'TNO_anth_NOx_sfc_2011_remapcon.nc'
# # flux_val = np.append(flux_val,
# #                       arrange_label(os.path.join(dirname, 'emissions', emis_file),
# #                                     month), axis=0)
# %%load model data and calculate VCD
exp = 'UBA_fl'
count_dat = 0
for month in months:
    sorbit_c = '{}_{}_sorbit_s5a.nc'.format(exp, str(month).zfill(2))
    meco_out = os.path.join(dirname, sorbit_c)
    if count_dat == 0:
        dataset2d = arrange_feature_flux(meco_out)
    else:
        dataset2d = np.append(dataset2d, arrange_feature_flux(meco_out),
                              axis=0)
    count_dat += 1

# %% add additional TNO data
# exp = 'UBA_fl_1_2'
# # exp = 'TNO_fl'
# month = 5
# sorbit_c = '{}_{}_sorbit_s5a.nc'.format(exp, str(month).zfill(2))
# meco_out = os.path.join(dirname, sorbit_c)
# dataset2d = np.append(dataset2d, arrange_feature(meco_out), axis=0)
# # dataset2d = dataset2d.reshape(dataset2d.shape[0],
# #                               dataset2d.shape[1]*dataset2d.shape[2])

# %% calculate chemistry sensititvity
# base_file = 'TNO_sfc_05_sorbit_s5a.nc'
# extra_file = 'TNO_sfc_1_2_05_sorbit_s5a.nc'
# vcd_base = np.ma.mean(calc_vcd(os.path.join(dirname, base_file)), 0)
# vcd_extra = np.ma.mean(calc_vcd(os.path.join(dirname, extra_file)), 0)
# beta = 0.2*vcd_base/(vcd_extra-vcd_base)

# datadict = {'lat': model_lat, 'lon': model_lon, 'vmin': 0, 'vmax': 2,
#             'text': 'beta', 'label': '', 'plotable': beta}
# fig, ax = plt.subplots(subplot_kw=dict(projection=ccrs.PlateCarree()))
# cs = map2plot.plot_map(datadict, ax, cmap='RdBu_r',
#                         mode='pcolormesh',
#                         projection=projection, alpha=1)
# fig.canvas.draw()
# plt.tight_layout()
# fig.subplots_adjust(right=0.86, wspace=0.02)
# cbar_ax = fig.add_axes([0.88, 0.2, 0.02, 0.6])
# cbar = fig.colorbar(cs, cax=cbar_ax, extend='max', shrink=0.8)
# cbar.set_label(datadict['text'], fontsize=10)
# if vcd_mean.shape[0] > 1:
#     beta = np.tile(beta, (vcd_mean.shape[0], 1, 1))
# %% normalization for 2d

normalizer = preprocessing.Normalization(axis=-1)
normalizer.adapt(dataset2d)
print(normalizer.mean.numpy())

x_train, x_test, y_train, y_test = train_test_split(dataset2d, flux_val,
                                                    test_size=0.1)

# %% DNN regression multiple parameter
def build_and_compile_model(norm):
    inputs = Input(shape=(x_test.shape[1],))
    x = norm(inputs)
    x = layers.Dropout(0.05)(x)
    x = layers.Dense(128, activation="relu")(x)
    x = layers.Dense(128, activation="relu")(x)
    x = layers.Dense(128, activation="relu")(x)
    # x = layers.Dropout(0.05)(x)
    x = layers.Dense(1, activation="linear")(x)
    model = Model(inputs, x)
    # model = keras.Sequential([norm,
    #                           layers.Dense(128, activation='selu'),
    #                           # layers.BatchNormalization(),
    #                           layers.Dense(64, activation='relu'),
    #                           layers.Dense(32, activation='relu'),
    #                           # layers.Dense(16, activation='relu'),
    #                           layers.Dropout(0.05),
    #                           layers.Dense(1, activation='linear')])
# # # loss = 'mean_squared_logarithmic_error', 'mean_absolute_error', 'mean_squared_error'
    model.compile(loss='mean_squared_logarithmic_error',
                  optimizer=keras.optimizers.Adam(learning_rate=5e-4))
    return model


dnn_model = build_and_compile_model(normalizer)
dnn_model.summary()
# plot_model(dnn_model, to_file=os.path.join(dirname, 'model_plot.png'),
#             show_shapes=True, show_layer_names=True)
# interrupt traiing when model isno longer imporving
path_checkpoint = "model_checkpoint.h5"
modelckpt_callback = keras.callbacks.ModelCheckpoint(monitor="val_loss",
                                                     filepath=path_checkpoint,
                                                     verbose=1,
                                                     save_weights_only=True,
                                                     save_best_only=True)
es_callback = keras.callbacks.EarlyStopping(monitor="val_loss",
                                            min_delta=0, patience=20)
history = dnn_model.fit(x_train, y_train, validation_split=0.2,
                        epochs=120, callbacks=[es_callback, modelckpt_callback])
ax = plot_loss(history)

# %% test model and make prediction
# test model
test_results = {}
test_results['train'] = dnn_model.evaluate(x_train, y_train)
test_results['model'] = dnn_model.evaluate(x_test, y_test)
print(test_results)

# make predictions
test_predictions = dnn_model.predict(x_test).flatten()
fig, ax = plt.subplots(1, 2, figsize=[8, 4],
                       gridspec_kw={'width_ratios': [0.6, 0.4]})
hbin = ax[0].hexbin(y_test, test_predictions, mincnt=1, cmap='jet',
                    gridsize=1000, vmin=1, vmax=20)

# ax[0].scatter(test_labels, test_predictions, s=2)
ax[0].set_xlabel('Test emission ($\mu$g m$^{-2}$ s$^{-1}$)')
ax[0].set_ylabel('Predicted emission ($\mu$g m$^{-2}$ s$^{-1}$)')
cb = plt.colorbar(hbin, ax=ax[0], orientation='vertical', shrink=0.9,
                  extend='max')
lims = [0, 1]
ax[0].set_xlim(lims)
ax[0].set_ylim(lims)
ax[0].plot(lims, lims, alpha=0.5, c='k')
error = test_predictions - y_test
ax[1].hist(error, bins=np.arange(-1, 1, 0.02))
ax[1].set_xlabel('Prediction Error [Emission]')
ax[1].set_ylabel('Count')
ax[1].set_xlim(-1, 1)
plt.tight_layout()
ax[0].grid(alpha=0.3)
ax[1].grid(alpha=0.3)

# %% save the model
dnn_model.save('UBA_05_06_02_model')
