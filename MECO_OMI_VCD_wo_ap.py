# -*- coding: utf-8 -*-
"""
Created on Thu Aug 30 17:33:37 2018

@author: Vinod
"""

import netCDF4
import os
import sys
import numpy as np
import matplotlib.pyplot as plt
import datetime
from scipy import stats
from mpl_toolkits.basemap import Basemap
data_product = "QA4ECV"   #DOMINO, QA4ECV
dirname_sim = r'M:\home\vinod\nobackup\model_work\MECO'
if data_product == "QA4ECV":
    dirname_sat = r'M:\nobackup\vinod\QA4ECV\NO2\raster'
    filename_sim = 'TNO_201504_sorbit_AURA-A_QA4ECV25s.nc'
    filename_sat = 'QA4ECV_L2G_NO2_OMI_201504_merge_025.nc'
elif data_product == "DOMINO":
    dirname_sat = r'M:\nobackup\vinod\omi_raster\datasets'
    filename_sim = 'TNO_201504_sorbit_AURA-A_DOMINO25s.nc'
    filename_sat = 'OMI-Aura_full_new-OMDOMINO_2015m04_merge_025.nc'
else:
    sys.exit("please provide satellite data product")
if "TNO_" in filename_sim:
    savepath = r'M:\home\vinod\nobackup\model_work\MECO\Plots\OMI_comp\TNO\ '
elif "EC_" in filename_sim:
    savepath = r'M:\home\vinod\nobackup\model_work\MECO\Plots\OMI_comp\EC\ '
elif "RCP_" in filename_sim:
    savepath = r'M:\home\vinod\nobackup\model_work\MECO\Plots\OMI_comp\RCP\ '
path_sim = os.path.join(dirname_sim, filename_sim)
path_sat = os.path.join(dirname_sat, filename_sat)
f_sim=netCDF4.Dataset(path_sim,'r')
counter = 0
timestamp = f_sim.variables['time']
f_sat=netCDF4.Dataset(path_sat,'r')
if "QA4ECV" in filename_sat:
    NO2_omitropcol = f_sat.variables['tropospheric_no2_vertical_column']
else:
    NO2_omitropcol = f_sat.variables['TroposphericVerticalColumn']
fv=NO2_omitropcol._FillValue
lats=f_sim.variables['lat'][:]
lons=f_sim.variables['lon'][:]
plt.ioff()
for day_num in range(1,29):
    d = day_num-1
    date_display=datetime.datetime.strptime(timestamp.units[10:30],'%Y-%m-%d %H:%M:%S')+ datetime.timedelta(days=timestamp[d]-1)
    if "QA4ECV" in filename_sat:
        TropoPauseLevel = f_sat.variables['TropoPauseLevel'][d,:,:]
        TropoPauseLevel.mask = np.ma.nomask
        ak = f_sat.variables['averaging_kernel'][d,:,:,:]   #averaging kernel
        trop_amf_sat=f_sat.variables['amf_trop'][d,:,:]
        tot_amf_sat=f_sat.variables['amf_total'][d,:,:]
        #tropSCD_sat=f_sat.variables['SlantColumnAmountNO2'][:]-f_sat.variables['AssimilatedStratosphericSlantColumn'][:]  #striped SCD
        NO2_omitropcol = f_sat.variables['tropospheric_no2_vertical_column'][d,:,:]
        NO2_omitropcol[NO2_omitropcol==fv]=np.nan
        NO2_omitropcol=NO2_omitropcol*1E-15
        cf=f_sat.variables['cloud_fraction'][d,:]
    else:    
        TropoPauseLevel = f_sat.variables['TropoPauseLevel'][d,:,:]
        ak = f_sat.variables['AveragingKernel'][d,:,:,:]*1E-3   #averaging kernel
        #ap = f_sat.variables['TM4profile'][d,:,:,:]*1E15        #for method 3, satellite a priori profile
        trop_amf_sat=f_sat.variables['AirMassFactorTropospheric'][d,:,:]
        tot_amf_sat=f_sat.variables['AirMassFactor'][d,:,:]
        #tropSCD_sat=f_sat.variables['SlantColumnAmountNO2'][:]-f_sat.variables['AssimilatedStratosphericSlantColumn'][:]  #striped SCD
        NO2_omitropcol = f_sat.variables['TroposphericVerticalColumn'][d,:,:]
        NO2_omitropcol[NO2_omitropcol==fv]=np.nan
        cf=f_sat.variables['CloudFraction'][d,:]*1E-3
    tropSCD_sat=NO2_omitropcol*trop_amf_sat
    flag=f_sat.variables['TropColumnFlag'][d,:]
    #tropSCD_sat[cf>0.3]=np.nan ##cloud fraction
    tropSCD_sat = np.ma.masked_where(cf>0.3, tropSCD_sat)
    if np.ma.is_masked(TropoPauseLevel):
        TropoPauseLevel = TropoPauseLevel.filled(0)
    #TropoPauseLevel[TropoPauseLevel>34]=0
    #tropSCD_sat[tropSCD_sat<0]=np.nan
    '''
    calculate layer heights
    '''
    aps=f_sim.variables['aps'][d-1,:,:]
    #aps.mask = np.ma.nomask
    prs_i = np.ma.MaskedArray([f_sim.variables['hyai'][i]+(aps[:]*f_sim.variables['hybi'][i]) for i in range(0,35)])
    prs_i = np.ma.masked_where(prs_i<0, prs_i)
    #prs_i[prs_i<0]=np.nan
    h_interface=np.ma.MaskedArray([7.4*np.log(aps[:]/prs_i[i,:]) for i in range(0,35)])*1000 #in meters
    h=np.ma.MaskedArray([h_interface[i,:]-h_interface[i-1,:] for i in range(1,35)])
    #h_interface=np.array([7.4*np.log(aps[:]/prs_i[i,:]) for i in range(0,35)])*1000   #in meters
    #h=np.array([h_interface[i,:]-h_interface[i-1,:] for i in range(1,35)])
    h=h*f_sim.variables['COSMO_tm1'][d-1,:]/250    #correct for assumed scale height at 250K
    #h = np.array([f_sim.variables['grvol'][d,i,:,:]/f_sim.variables['gboxarea'][d,:,:] for i in range(0,34)])
    h=h*100
    h[h<=0]=np.nan
    ############calculate subcolumn #######
    NO2_conc=f_sim.variables['tracer_gp_NO2_conc'][d-1,:,:,:]
    #NO2_conc[(NO2_conc<=1E7) | (NO2_conc>=1E12)]=np.nan
    tropvcdbox = NO2_conc[:,:,:]*h[:,:,:]  #without averaging kernels
    #tropvcdbox[(tropvcdbox>1E18) | (tropvcdbox<1E7)]=np.nan
    #################### calculate tropospheric VCD
    #tropvcd_sim = np.nansum(tropvcdbox,0)
    tropvcd_sim = np.ma.sum(tropvcdbox,0)
    for i in range(0,len(lats)):
        for j in range(0,len(lons)):
            tpl = int(round(TropoPauseLevel[i,j]))
            TropoPauseLevel[i,j] = tpl
            #tropvcd_sim[i,j] = np.nansum(tropvcdbox[0:tpl+1,i,j])
            tropvcd_sim[i,j] = np.ma.sum(tropvcdbox[0:tpl+1,i,j])
            #NO2_tropvcd_sat[i,j] = np.nansum(ap[0:tpl+1,i,j])  ####for methond 3
            #NO2_vcd=np.nansum(h*NO2_conc,0)
    #tropvcd_sim[(tropvcd_sim<=1E12) | (tropvcd_sim>=1E19)]=np.nan
    tropvcd_sim = np.ma.masked_where(cf>0.3, tropvcd_sim)   ##cloud fraction filter on model product for comparison
    '''
    calculation of profile and amf
    '''
    #trop_profile_sim = np.array([tropvcdbox[i,:,:]/tropvcd_sim[:,:] for i in range(0,34)])
    trop_profile_sim = np.ma.MaskedArray([np.ma.divide(tropvcdbox[i,:,:],tropvcd_sim[:,:]) for i in range(0,34)])
    #trop_profile_sat = np.array([ap[i,:,:]/NO2_omitropcol[:,:] for i in range(0,34)])  ####for methond 3 
    #trop_amf_sim=np.nansum(trop_profile_sim*ak,0)*trop_amf_sat/np.nansum(trop_profile_sim,0)
    #trop_amf_scale=np.nansum(trop_profile_sim*ak,0)*trop_amf_sat/np.nansum(trop_profile_sim,0)
    trop_amf_sim=np.ma.divide(np.ma.sum(np.ma.multiply(trop_profile_sim,ak),0)*trop_amf_sat,np.ma.sum(trop_profile_sim,0))
    trop_amf_scale=np.nansum(trop_profile_sim*ak,0)*trop_amf_sat/np.nansum(trop_profile_sim,0)
    #trop_amf_scale=np.nansum(trop_profile_sim*ak,0)*trop_amf_sat/np.nansum(trop_profile_sat*ak,0)  ######for method 3
    for i in range(0,len(lats)):
        for j in range(0,len(lons)):
            #tpl = int(round(TropoPauseLevel[i,j]))
            #TropoPauseLevel[i,j] = tpl
            #trop_amf_scale[i,j] = np.nansum(trop_profile_sim[0:tpl+1,i,j]*ak[0:tpl+1,i,j])/np.nansum(trop_profile_sim[0:tpl+1,i,j])
            trop_amf_scale[i,j] = np.ma.divide(np.ma.sum(trop_profile_sim[0:tpl+1,i,j]*ak[0:tpl+1,i,j]), np.ma.sum(trop_profile_sim[0:tpl+1,i,j]))
            #trop_amf_scale[i,j] = np.nansum(trop_profile_sim[0:tpl+1,i,j]*ak[0:tpl+1,i,j])/np.nansum(trop_profile_sat[0:tpl+1,i,j]*ak[0:tpl+1,i,j]) ###for method 3
    trop_amf_sim=trop_amf_scale*tot_amf_sat
    trop_amf_sat = np.ma.masked_where(np.ma.getmask(trop_amf_sim), trop_amf_sat)
    #trop_amf_sim=trop_amf_scale*trop_amf_sat   #### for method 3
    #trop_amf_sim[trop_amf_sim==0]=np.nan
    tropVCD_sat=np.ma.divide(tropSCD_sat,trop_amf_sim)
    #tropVCD_sat=tropSCD_sat/trop_amf_sim
    #tropVCD_sat[np.where(np.isnan(tropvcd_sim)==True)]=np.nan
    #tropSCD_sat[np.where(np.isnan(tropvcd_sim)==True)]=np.nan
    #tropvcd_sim[cf>0.3]=np.nan ##cloud fraction filter on model product for comparison
    if "QA4ECV" in filename_sat:
        tropvcd_sim = np.ma.masked_where(flag>=0.5, tropvcd_sim)
        tropVCD_sat = np.ma.masked_where(flag>=0.5, tropVCD_sat)
        tropSCD_sat = np.ma.masked_where(flag>=0.5, tropVCD_sat)
    else:
        tropvcd_sim = np.ma.masked_where(flag<=-0.5, tropvcd_sim)
        tropVCD_sat = np.ma.masked_where(flag<=-0.5, tropVCD_sat)
        tropSCD_sat = np.ma.masked_where(flag<=-0.5, tropVCD_sat)
    fig=plt.Figure()
    plt.title('MECO(n) Tropospheric NO$_{2}$ VCD for ' + date_display.strftime('%d-%b-%Y'))
    ax = fig.add_subplot(1, 1, 1)
    if "_050s" in filename_sat:
        map = Basemap(projection='cyl', resolution = 'l', llcrnrlon=-60, llcrnrlat=4,urcrnrlon=90, urcrnrlat=80)
        map.drawparallels(np.arange(5.,75.,15.),labels=[1,0,0,0])
        map.drawmeridians(np.arange(-60.,90.,30.),labels=[0,0,0,1])
    else:    
        map = Basemap(projection='cyl', resolution = 'l', llcrnrlon=1, llcrnrlat=43,urcrnrlon=20, urcrnrlat=56)
        map.drawparallels(np.arange(30.,60.,4.),labels=[1,0,0,0])
        map.drawmeridians(np.arange(0.,20.,4.),labels=[0,0,0,1])
    map.drawcoastlines()
    map.drawcountries()
    map.drawmapboundary()
    llons, llats = np.meshgrid(lons, lats)
    x,y = map(llons, llats)
    cs = map.pcolormesh(x, y, tropvcd_sim*1E-15, cmap='jet', vmin = 0, vmax = 12)
    cb=map.colorbar(extend = 'both')
    cb.set_label('NO2_Trop_VCD * 10$^{15}$ molecules cm$^{-2}$' , fontsize=11)
    ############ wind vectors ###############
    um1=f_sim.variables['COSMO_um1'][d-1,0,:,:]
    vm1=f_sim.variables['COSMO_vm1'][d-1,0,:,:]
    ws= np.sqrt(um1*um1 + vm1*vm1)
    xx = np.arange(0, llons.shape[0], 4)
    yy = np.arange(0, llats.shape[0], 4)
    points = np.meshgrid(yy, xx)
    map.quiver(x[points],y[points],um1[points],vm1[points],ws[points],latlon=True, cmap=plt.cm.autumn)
    #######################################
    plt.savefig(savepath + 'MECO_VCD_3_'+ date_display.strftime('%Y%m%d') +'.png', format = 'png', dpi = 200)
    plt.close()
    #plot satellite vcd
    fig=plt.Figure()
    plt.title(data_product + ' OMI Tropospheric NO$_{2}$ VCD for ' + date_display.strftime('%d-%b-%Y'))
    ax = fig.add_subplot(1, 1, 1)
    if "_050s" in filename_sat:
        map = Basemap(projection='cyl', resolution = 'l', llcrnrlon=-60, llcrnrlat=4,urcrnrlon=90, urcrnrlat=80)
        map.drawparallels(np.arange(5.,75.,15.),labels=[1,0,0,0])
        map.drawmeridians(np.arange(-60.,90.,30.),labels=[0,0,0,1])
    else:    
        map = Basemap(projection='cyl', resolution = 'l', llcrnrlon=1, llcrnrlat=43,urcrnrlon=20, urcrnrlat=56)
        map.drawparallels(np.arange(30.,60.,4.),labels=[1,0,0,0])
        map.drawmeridians(np.arange(0.,20.,4.),labels=[0,0,0,1])
    map.drawcoastlines()
    map.drawcountries()
    map.drawmapboundary()
    x,y = map(llons, llats)
    cs = map.pcolormesh(x, y, tropVCD_sat, cmap='jet', vmin = 0, vmax = 12)
    #cs = map.pcolormesh(x, y, NO2_tropvcd*1E-15-V0, cmap='jet', vmin = -7, vmax = 4 )  #for bias
    cb=map.colorbar(extend = 'both')
    cb.set_label('NO2_Trop_col * 10$^{15}$ molecules cm$^{-2}$' , fontsize=10)
    plt.savefig(savepath + 'OMI_VCD_3_'+ date_display.strftime('%Y%m%d') +'.png', format = 'png', dpi = 200)
    plt.close()
    ##############comparison plot of old and new airmass factors
    fig, axes = plt.subplots(nrows=2, ncols=1, figsize=[8,9])
    plt.suptitle('Comparisons of Tropospheric AMF for ' + date_display.strftime('%d-%b-%Y'))
    axes[0,].set_title( 'OMI data product', fontsize=10)
    map = Basemap(projection='cyl', resolution = 'l', llcrnrlon=1, llcrnrlat=43,urcrnrlon=20, urcrnrlat=56, ax=axes[0])
    map.drawparallels(np.arange(30.,60.,4.),labels=[1,0,0,0])
    map.drawmeridians(np.arange(0.,20.,4.),labels=[0,0,0,1])
    map.drawcoastlines()
    map.drawcountries()
    map.drawmapboundary()
    cs1 = map.pcolormesh(x, y, trop_amf_sat, cmap='jet', vmin = 0, vmax = 2.5)
    cb1=fig.colorbar(cs1, cmap=plt.cm.get_cmap('jet'), ax=axes[0], orientation='vertical',extend = 'both', fraction = 0.1, shrink = 0.8)
            #######################
    axes[1].set_title( 'calculated usig model profile',fontsize=10)
    map = Basemap(projection='cyl', resolution = 'l', llcrnrlon=1, llcrnrlat=43,urcrnrlon=20, urcrnrlat=56, ax=axes[1])
    map.drawparallels(np.arange(30.,60.,4.),labels=[1,0,0,0])
    map.drawmeridians(np.arange(0.,20.,4.),labels=[0,0,0,1])
    map.drawcoastlines()
    map.drawcountries()
    map.drawmapboundary()
    cs2 = map.pcolormesh(x, y, trop_amf_sim, cmap='jet', vmin = 0, vmax = 2.5)
    cb2=fig.colorbar(cs2, cmap=plt.cm.get_cmap('jet'), ax=axes[1], orientation='vertical',extend = 'both', fraction = 0.1, shrink = 0.8)
    plt.savefig(savepath + 'comp_AMF_3'+ date_display.strftime('%Y%m%d') +'.png', format = 'png', dpi = 200)
    plt.close()
    #####################################
    #########calculate and plot average over given number of days###########
    if counter == 0:
        mean_meco=tropvcd_sim[:,:,np.newaxis]
        mean_omi=tropVCD_sat[:,:,np.newaxis]
        mean_amf_old=trop_amf_sat[:,:,np.newaxis]
        mean_amf_new=trop_amf_sim[:,:,np.newaxis]
    else:
        mean_meco=np.ma.dstack((mean_meco, tropvcd_sim))
        mean_omi=np.ma.dstack((mean_omi, tropVCD_sat))
        mean_amf_old=np.ma.dstack((mean_amf_old, trop_amf_sat))
        mean_amf_new=np.ma.dstack((mean_amf_new, trop_amf_sim))
    counter=counter+1
################################Overview plot of all days with correlation##########
#mean_omi[mean_omi>1E20]=np.nan
plt.ion()
fig, axes = plt.subplots(nrows=2, ncols=2, figsize=[11,9])
plt.suptitle('Comparisons of Tropospheric NO$_{2}$ VCD for ' + date_display.strftime('%b-%Y'))
axes[0, 0].set_title( 'COSMO NO2_Trop_VCD (*1E15 molecules cm-2)', fontsize=10)
map = Basemap(projection='cyl', resolution = 'l', llcrnrlon=1, llcrnrlat=43,urcrnrlon=20, urcrnrlat=56, ax=axes[0,0])
map.drawparallels(np.arange(30.,60.,4.),labels=[1,0,0,0])
map.drawmeridians(np.arange(0.,20.,4.),labels=[0,0,0,1])
map.drawcoastlines()
map.drawcountries()
map.drawmapboundary()
llons, llats = np.meshgrid(lons, lats)
x,y = map(llons, llats)
cs1 = map.pcolormesh(x, y, np.ma.mean(mean_meco,2)*1E-15, cmap='jet', vmin = 0, vmax = 12)
cb1=fig.colorbar(cs1, cmap=plt.cm.get_cmap('jet'), ax=axes[0,0], orientation='vertical',extend = 'both', fraction = 0.1, shrink = 0.8)
################################
axes[0, 1].set_title(data_product + ' OMI NO2_Trop_VCD (*1E15 molecules cm-2)',fontsize=10)
map = Basemap(projection='cyl', resolution = 'l', llcrnrlon=1, llcrnrlat=43,urcrnrlon=20, urcrnrlat=56, ax=axes[0,1])
map.drawparallels(np.arange(30.,60.,4.),labels=[1,0,0,0])
map.drawmeridians(np.arange(0.,20.,4.),labels=[0,0,0,1])
map.drawcoastlines()
map.drawcountries()
map.drawmapboundary()
cs2 = map.pcolormesh(x, y, np.ma.mean(mean_omi,2), cmap='jet', vmin = 0, vmax = 12)
cb2=fig.colorbar(cs2, cmap=plt.cm.get_cmap('jet'), ax=axes[0,1], orientation='vertical',extend = 'both', fraction = 0.1, shrink = 0.8)
################################
axes[1,0].set_title( '(COSMO-OMI) NO2_Trop_VCD(*1E15 molecules cm-2)', fontsize=10)
map = Basemap(projection='cyl', resolution = 'l', llcrnrlon=1, llcrnrlat=43,urcrnrlon=20, urcrnrlat=56, ax=axes[1,0])
map.drawparallels(np.arange(30.,60.,4.),labels=[1,0,0,0])
map.drawmeridians(np.arange(0.,20.,4.),labels=[0,0,0,1])
map.drawcoastlines()
map.drawcountries()
map.drawmapboundary()
cs3 = map.pcolormesh(x, y, np.ma.mean(mean_meco,2)*1E-15-np.nanmean(mean_omi,2), cmap='jet', vmin = -3, vmax = 3)
cb3=fig.colorbar(cs3, cmap=plt.cm.get_cmap('jet'), ax=axes[1,0], orientation='vertical',extend = 'both', fraction = 0.1, shrink = 0.8)
################################
axes[1,1].set_title( 'Correlation')
VCD_sim_flat=np.ma.mean(mean_meco,2).flatten()*1E-15
VCD_sat_flat=np.ma.mean(mean_omi,2).flatten()
idx = np.isfinite(VCD_sat_flat) & np.isfinite(VCD_sim_flat)
#fit_param=np.polyfit(VCD_sat_flat[idx], VCD_sim_flat[idx],1)
fit_param = stats.linregress(VCD_sat_flat[idx], VCD_sim_flat[idx])[0:3]
fit_func=np.poly1d(fit_param[0:2])
r_sqr=fit_param[2]**2
axes[1,1].scatter(VCD_sat_flat,VCD_sim_flat, s=2)
axes[1,1].plot(VCD_sat_flat[idx], fit_func(VCD_sat_flat[idx]), color = "black", linewidth=2)
#axes[1,1].plot(SCD_sat_flat[idx], np.poly1d([1,0])(SCD_sat_flat[idx]), linewidth=1)
axes[1,1].plot(VCD_sat_flat[idx], np.poly1d([1,0])(VCD_sat_flat[idx]), color = "red", linewidth=1, linestyle = ":")
#axes[1,1].fill_between(VCD_sat_flat[idx], np.poly1d([0.8,0])(VCD_sat_flat[idx]), np.poly1d([1.2,0])(VCD_sat_flat[idx]), color = 'red', alpha = 0.5 )
axes[1,1].plot(VCD_sat_flat[idx], np.poly1d([1.2,0])(VCD_sat_flat[idx]), color = "red", linewidth=1, linestyle = ":", alpha=0.7)
axes[1,1].plot(VCD_sat_flat[idx], np.poly1d([0.8,0])(VCD_sat_flat[idx]), color = "red", linewidth=1, linestyle = ":", alpha=0.7)
axes[1,1].annotate("y = " + str('%.1f' % fit_param[0]) + "x + "+ str('%.1f' % fit_param[1]) + "\n r$^{2}$ = " + str('%.2f' % r_sqr), xy=(0.1, 0.8),  xycoords='axes fraction')
axes[1,1].set_xlabel("OMI NO2 VCD (*10$^{15}$ molec. cm$^{-2}$)", fontsize=10)
axes[1,1].set_ylabel("COSMO NO2 VCD (*10$^{15}$ molec. cm$^{-2}$)", fontsize=10)
plt.tight_layout()
plt.show()
plt.savefig(savepath +'summary_comp'+'.png', format = 'png', dpi = 200)