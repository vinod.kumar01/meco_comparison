# -*- coding: utf-8 -*-
"""
Created on Thu Apr 26 12:37:31 2018
script to calculate VCD usinng MECO(n) output. It uses the raw MECO(n) output
and any gridding is not needed, as it ignores averaging kernels.
works with both normal output and sorbit output
@author: Vinod
"""

import netCDF4
import os
import numpy as np
from plot_tools import plt, map_prop, ccrs
from datetime import datetime, timedelta
from mod_colormap import add_white


def calc_vcd(meco_out):
    f_sim = netCDF4.Dataset(meco_out, 'r')
    tracer = 'NO2'
    try:
        aps = f_sim.variables['ps'][:]
        dhyam = f_sim.variables['dhyam'][:]
        dhybm = f_sim.variables['dhybm'][:]
        tm1 = f_sim.variables['COSMO_tm1'][:]
    except KeyError:
        aps = f_sim.variables['aps'][:]
        tm1 = f_sim.variables['tm1_ave'][:]
        dhyam = f_sim.variables['dhyam_ave'][:]
        dhybm = f_sim.variables['dhybm_ave'][:]

    l4km = 29 if tm1.shape[1] == 50 else 21
    prs_m = np.array([dhyam[i] + (aps[:]*dhybm[i])
                      for i in range(0, len(dhyam))])
    prs_m = np.transpose(prs_m, (1, 0, 2, 3))
    prs_m[prs_m < 0] = np.nan
    try:
        tracer_mr = f_sim.variables[tracer+'_ave'][:]
        h_interface = f_sim.variables['hhl'][:]
    except KeyError:
        tracer_mr = f_sim.variables['tracer_gp_'+tracer][:]
        h_interface = np.nanmean(f_sim.variables['COSMO_ORI_HHL'][:], 0)
    tracer_conc = tracer_mr*6.023E23*prs_m*1E-6/8.314/tm1
    '''
    calculation of box height
    '''
    h = np.ma.masked_array([h_interface[i, :] - h_interface[i+1, :]
                            for i in range(0, len(dhyam))])
    tracer_sim_vcd_box = np.ma.masked_array([tracer_conc[i, :]*h*100
                                             for i in range(tracer_conc.shape[0])])
    tracer_sim_geovcd = np.nansum(tracer_sim_vcd_box[:, l4km:len(dhyam), :, :], 1)
    f_sim.close()
    return tracer_sim_geovcd

# %% Save netcdf file of VCD only
if __name__ == "__main__":
    exp = 'UBA_di'
    dirname = r'M:\nobackup\vinod\model_work\MECO\DOAS_COMP'
    filename_sim = exp + '_18_5_no2_1hr.nc'
    meco_out = os.path.join(dirname, filename_sim)
    tracer_sim_geovcd = calc_vcd(meco_out)
    with netCDF4.Dataset(meco_out, 'r') as f_sim:
        lat = f_sim.variables['lat'][:]
        lon = f_sim.variables['lon'][:]
        timeax = f_sim.variables['time'][:]
        time_units = f_sim.variables['time'].units
        tracer_mr = f_sim.variables['NO2_ave'][:]
        try:
            time_dsp = [datetime.strptime(f_sim.variables['time'].units[11:30],
                                          '%Y-%m-%d %H:%M:%S') + timedelta(days=i)
                        for i in timeax]
        except ValueError:
            time_dsp = [datetime.strptime(f_sim.variables['time'].units[10:30],
                                          '%Y-%m-%d %H:%M:%S') + timedelta(days=i)
                        for i in timeax]
    f_out = netCDF4.Dataset(os.path.join(dirname, 'vcd_'+filename_sim), 'w',
                            format='NETCDF4')
    f_out.createDimension('time', None)
    f_out.createDimension('glon', lon.shape[1])
    f_out.createDimension('glat', lat.shape[0])
    time_out = f_out.createVariable('time', 'f4', ('time',))
    lon_out = f_out.createVariable('lon', 'f4', ('glat', 'glon'))
    lat_out = f_out.createVariable('lat', 'f4', ('glat', 'glon'))
    NO2_tropcol_sim_out = f_out.createVariable('NO2_tropcol_sim', 'f4',
                                               ('time', 'glat', 'glon'),
                                               fill_value=-999.9)
    NO2_vmr_sim_out = f_out.createVariable('NO2_vmr_sim', 'f4',
                                               ('time', 'glat', 'glon'),
                                               fill_value=-999.9)
    time_out[:] = timeax
    time_out.units = time_units
    lon_out[:] = lon
    lon_out.standard_name = 'longitude'
    lon_out.units = 'degrees_east'
    lat_out[:] = lat
    lat_out.standard_name = 'latitude'
    lat_out.units = 'degrees_north'
    NO2_tropcol_sim_out[:] = tracer_sim_geovcd
    NO2_tropcol_sim_out.standard_name = 'MECO(n) NO2 tropospheric VCD'
    NO2_tropcol_sim_out.units = "1E16 molecules cm-2"
    NO2_vmr_sim_out[:] = 1e9*tracer_mr[:, -1, :]
    NO2_vmr_sim_out.standard_name = 'MECO(n) NO2 surface VMR'
    NO2_vmr_sim_out.units = "ppb"
    f_out.close()

# %% plotting
# if __name__ == "__main__":
#     new_cm = add_white("rainbow", shrink_colormap=False, replace_frac=0.1,
#                        start="min")
#     llon, ulon, llat, ulat = 0, 19, 43, 56
#     prop_dict = {'extent': [llon, ulon, llat, ulat], 'name': 'MECO_DE',
#                  'border_res': '10m'}
#     projection = ccrs.PlateCarree()
#     map2plot = map_prop(**prop_dict)
#     dirname = r'M:\nobackup\vinod\model_work\MECO\sorbit_subsample'
#     filename_sim = 'TNO_sfc_05_sorbit_s5a.nc'
#     meco_out = os.path.join(dirname, filename_sim)
#     tracer_sim_geovcd = calc_vcd(meco_out)
#     with netCDF4.Dataset(meco_out, 'r') as f_sim:
#         lat = f_sim.variables['geolat'][:]
#         lon = f_sim.variables['geolon'][:]
#         timeax = f_sim.variables['time'][:]
#         try:
#             time_dsp = [datetime.strptime(f_sim.variables['time'].units[11:30],
#                                           '%Y-%m-%d %H:%M:%S') + timedelta(days=i)
#                         for i in timeax]
#         except ValueError:
#             time_dsp = [datetime.strptime(f_sim.variables['time'].units[10:30],
#                                           '%Y-%m-%d %H:%M:%S') + timedelta(days=i)
#                         for i in timeax]
#     datadict = {'lat': lat, 'lon': lon, 'vmin': -0.1, 'vmax': 1,
#                 'text': 'NO$_{2}$ VCD molecules cm$^{-2}$ (upto 4km)',
#                 'label': ''}
#     df = [{'lon': 8.2283, 'lat': 49.9909, 'site': 'MPIC'}]
#     for timestep in np.arange(len(time_dsp)):
#         fig, ax = plt.subplots(subplot_kw=dict(projection=ccrs.PlateCarree()))
#         timetext = datetime.strftime(time_dsp[timestep], '%d.%m.%Y')
#         plt.title('NO$_{2}$ VCD for ' + timetext + ' (S5P overpass)')
#         datadict['plotable'] = tracer_sim_geovcd[timestep, :]/1e16
#         cs = map2plot.plot_map(datadict, ax, cmap=new_cm,
#                                mode='pcolormesh',
#                                projection=projection, alpha=1)
#         fig.canvas.draw()
#         plt.tight_layout()
#         fig.subplots_adjust(right=0.86, wspace=0.02)
#         cbar_ax = fig.add_axes([0.88, 0.2, 0.02, 0.6])
#         cbar = fig.colorbar(cs, cax=cbar_ax, extend='max', shrink=0.8)
#         cbar.set_label(datadict['text'], fontsize=10)
#         for point in df:
#             lo, la = point['lon'], point['lat']
#             plt.annotate(point['site'], xy=(lo, la),  xycoords='data',
#                          xytext=(lo, la), textcoords='data', color='k',
#                          horizontalalignment='left', verticalalignment='bottom',
#                          arrowprops=dict(facecolor='black', shrink=0.05))
#             plt.plot(lo, la, 'ko')
#         break