# -*- coding: utf-8 -*-
"""
Created on Sun Nov  8 15:47:09 2020

@author: Vinod
"""

import netCDF4
import numpy as np
from scipy.interpolate import interp2d


def era2dict(era_file, params):
    data_out = {}
    with netCDF4.Dataset(era_file) as f:
        lat = f.variables['latitude']
        lon = f.variables['longitude']
        time = f.variables['time']
        data_out['lat'] = {'unit': lat.units,
                           'data': lat[:]}
        data_out['lon'] = {'unit': lon.units,
                           'data': lon[:]}
        data_out['time'] = {'unit': time.units,
                            'data': time[:]}
        for key, param in params.items():
            if param not in f.variables.keys():
                continue
            param_var = f.variables[param]
# =============================================================================
# By default, scale factors and offset are already applied in netCDF4.variables
# Similary missing values and fill values are masked by default.
# Howevevr, masked values arenot considered later for interpolation
# and might result unrealistic values. Hence, I'll convert them to nan
# http://unidata.github.io/netcdf4-python/netCDF4/index.html
# if not applied, uncomment 8 limes below
# =============================================================================
            fv = param_var._FillValue
            mv = param_var.missing_value
#            sf = param_var.scale_factor
#            offset = param_var.add_offset
#            param_data *= sf
#            param_data += offset
            unit = param_var.units
            param_data = np.array(param_var[:])
            param_data[param_data == fv] = np.nan
            param_data[param_data == mv] = np.nan
            data_out[key] = {'unit': unit,
                             'data': param_data}
        return data_out


def grid2point_ts(era_file, params, loc, lev=None):
    '''
    @level: at which to extract the data  # only for data at multiple levels
    '''
    griddata = era2dict(era_file, params)
    ts = {}
    ts['time'] = griddata['time']
    ts_len = len(ts['time']['data'])
    for point in loc:
        ts[point['site']] = {}
        for param in params.keys():
            if param not in griddata.keys():
                continue
            ts[point['site']][param] = {'data': np.full(ts_len, np.nan),
                                        'unit': griddata[param]['unit']}
            ts[point['site']][param]['data'][:] = np.nan

    for param in params.keys():
        print(param)
        if param not in griddata.keys():
            continue
        for i in range(ts_len):
            if lev is None:
                fx = interp2d(griddata['lat']['data'], griddata['lon']['data'],
                              griddata[param]['data'][i, :].T)
            else:
                fx = interp2d(griddata['lat']['data'], griddata['lon']['data'],
                              griddata[param]['data'][i, lev, :].T)

            for point in loc:
                data_now = fx(point['lat'], point['lon'])
                ts[point['site']][param]['data'][i] = data_now
    return ts
