# -*- coding: utf-8 -*-
"""
Created on Sun Nov  8 15:41:50 2020

@author: Vinod
"""

import cdsapi
import os

c = cdsapi.Client()
savedir = r'M:\nobackup\vinod\model_work\MECO\DOAS_COMP'
savename = 'May_MEDO_DE.nc'
savefile = os.path.join(savedir, savename)


c.retrieve(
    'reanalysis-era5-single-levels',
#    'reanalysis-era5-land',
# pblh not avaiable for reanalysis-era5-land
    {
        'product_type': 'reanalysis',
        'format': 'netcdf',
        'variable': [
            '10m_u_component_of_wind', '10m_v_component_of_wind',
            '2m_temperature', 'boundary_layer_height', 'surface_pressure',
        ],
        'month': '05',
        'day': [
            '01', '02', '03',
            '04', '05', '06',
            '07', '08', '09',
            '10', '11', '12',
            '13', '14', '15',
            '16', '17', '18',
            '19', '20', '21',
            '22', '23', '24',
            '25', '26', '27',
            '28', '29', '30',
            '31'
        ],
        'time': [
            '00:00', '01:00', '02:00',
            '03:00', '04:00', '05:00',
            '06:00', '07:00', '08:00',
            '09:00', '10:00', '11:00',
            '12:00', '13:00', '14:00',
            '15:00', '16:00', '17:00',
            '18:00', '19:00', '20:00',
            '21:00', '22:00', '23:00',
        ],
        # 'area': [25, 67, 21, 73],
        'area': [55, 1, 41, 20],
        'year': '2018',
    },
    savefile)
