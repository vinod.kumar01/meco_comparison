# -*- coding: utf-8 -*-
"""
Created on Mon Jan 25 15:11:35 2021

@author: Vinod
"""

from os import path
from datetime import date
import matplotlib.dates as mdates
from ERA5.ERA5_tools import grid2point_ts, netCDF4
from datetime import datetime, timedelta
from scipy.interpolate import griddata
import numpy as np
import matplotlib.pyplot as plt

tracer = "ws"
sitelist = {'Lahore': [31.52, 74.36],
            'Sheikhupura': [31.72, 73.98]}
loc = [{'lon': 74.36, 'lat': 31.52, 'site': 'Lahore'},
       {'lon': 73.98, 'lat': 31.72, 'site': 'Sheikhupura'}]


def calc_ws(u, v):
    return(np.sqrt(u*u + v*v))


target_lat, target_lon = [], []
for key, vals in sitelist.items():
    target_lon.append(vals[1])
    target_lat.append(vals[0])
# %% ERA data
era_file_sfc = r'M:\nobackup\vinod\model_work\MECO\pak\ERA5\summer_2018.nc'
era_file_lvl = r'M:\nobackup\vinod\model_work\MECO\pak\ERA5\summer_2018_vlevs.nc'
params_sfc = {'wind_u10': 'u10',
          'wind_v10': 'v10',
          'sfc_temp': 't2m'}
params_lvl = {'wind_u': 'u', 'wind_v': 'v', 'sfc_temp': 't'}
ts = grid2point_ts(era_file_sfc, params_sfc, loc)
# ts_1000 = grid2point_ts(era_file_lvl, params_lvl, loc, lev=-1)
# ts_975 = grid2point_ts(era_file_lvl, params_lvl, loc, lev=-2)
# ts_950 = grid2point_ts(era_file_lvl, params_lvl, loc, lev=-3)
# ts_925 = grid2point_ts(era_file_lvl, params_lvl, loc, lev=-4)
ts_900 = grid2point_ts(era_file_lvl, params_lvl, loc, lev=-5)

date_display_lvl = datetime.strptime(ts_900['time']['unit'][12:31],
                                     '%Y-%m-%d %H:%M:%S')
date_time_lvl = [date_display_lvl+timedelta(hours=int(dt)+5.5)
                 for dt in ts_900['time']['data']]
date_display_sfc = datetime.strptime(ts['time']['unit'][12:31],
                                     '%Y-%m-%d %H:%M:%S')
date_time_sfc = [date_display_sfc+timedelta(hours=int(dt)+5.5)
                 for dt in ts['time']['data']]


for point in loc:
    site = point['site']
    ts_900[site]['ws'] = {'data': calc_ws(ts_900[site]['wind_u']['data'],
                                          ts_900[site]['wind_v']['data']),
                          'unit': 'ms**-1'}
    ts[site]['ws'] = {'data': calc_ws(ts[site]['wind_u10']['data'],
                                      ts[site]['wind_v10']['data']),
                      'unit': 'ms**-1'}

# %% MECO(n) data
sim_dir = r'M:\nobackup\vinod\model_work\MECO\pak'
f = netCDF4.Dataset(path.join(sim_dir, 'met_merge_1hr_small.nc'))
aps = f.variables['aps'][:]
dhyam = f.variables['dhyam_ave'][:]
prs = np.array([f.variables['dhyam_ave'][i] + (aps[:]*f.variables['dhybm_ave'][i])
                for i in range(0, len(dhyam))])
prs = np.mean(prs, 1)
meco_plvl = {900: 33, 950: 39, 1000: -1}
timeax = f.variables['time'][:]
try:
    time_unit = f.variables['time'].units[11:30]
    time_dsp = [datetime.strptime(time_unit,
                                  '%Y-%m-%d %H:%M:%S') + timedelta(days=i)
                for i in timeax]
except ValueError:
    time_unit = f.variables['time'].units[10:30]
    time_dsp = [datetime.strptime(time_unit,
                                  '%Y-%m-%d %H:%M:%S') + timedelta(days=i)
                for i in timeax]
geolon = f.variables['lon'][:]
geolat = f.variables['lat'][:]
MECO_dict = {s: {} for s in sitelist.keys()}
MECO_dict['Date_time'] = []
for p_val, p_lev in meco_plvl.items():
    if tracer == 'ws':
        sim_tracer = calc_ws(f.variables['um1_ave'][:, p_lev, :],
                             f.variables['um1_ave'][:, p_lev, :])
    elif tracer == 'sfc_temp':
        sim_tracer = f.variables['tm1_ave'][:, p_lev, :]
    else:
        sim_tracer = f.variables[tracer+'_ave'][:, p_lev, :]
    for s in loc:
        MECO_dict[s['site']][p_val] = []
        for t in range(sim_tracer.shape[0]):
            val_now = griddata((geolon.ravel(), geolat.ravel()),
                               sim_tracer[t, :].ravel(), (s['lon'], s['lat']),
                               method='linear')
            MECO_dict[s['site']][p_val].append(val_now)
for t in range(sim_tracer.shape[0]):
    MECO_dict['Date_time'].append(time_dsp[t])

for s in loc:
    for key in MECO_dict[s['site']].keys():
        MECO_dict[s['site']][key] = np.asarray(MECO_dict[s['site']][key])


# %% plotting
for site in sitelist.keys():
    fig1, ax1 = plt.subplots(figsize=[10, 3])
    ax1.plot(date_time_lvl, ts_900[site][tracer]['data'], label='ERA5')
    ax1.plot(MECO_dict['Date_time'], MECO_dict[s['site']][900], label='MECO(n)')
    # ax1.set_ylabel('900 hpa temperature (K)')
    ax1.set_ylabel('Wind speed (900 hpa) (m s$^{-1}$)')
    fig2, ax2 = plt.subplots(figsize=[10, 3])
    ax2.plot(date_time_sfc, ts[site][tracer]['data'], label='ERA5')
    ax2.plot(MECO_dict['Date_time'], MECO_dict[s['site']][1000], label='MECO(n)')
    # ax2.set_ylabel('Surface temperature (K)')
    ax2.set_ylabel('Wind speed (surface) (m s$^{-1}$)')
    for ax in [ax1, ax2]:
        ax.set_xlim(date(2018, 6, 16), date(2018, 7, 11))
        ax.xaxis.set_major_formatter(mdates.DateFormatter("%d-%b"))
        ax.legend(loc='upper right')
        ax.grid(alpha=0.5)
        ax.annotate(site, xy=[0.01, 0.9], size=16, xycoords='axes fraction')
    fig1.tight_layout()
    fig2.tight_layout()