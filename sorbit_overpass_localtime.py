# -*- coding: utf-8 -*-
"""
Created on Wed Jul  3 12:33:11 2019

@author: Vinod
"""
# %% import and function definition
# python 3 version of http://stackoverflow.com/a/13424528/65387
# UTC should be provided in "YYYY/MM/DD hh:mm:ss"
# latitutde should be provided as integer
from datetime import datetime, time, timedelta
from math import pi, cos, sin
import numpy as np
import netCDF4
import os
import matplotlib.pyplot as plt
from matplotlib.ticker import (MultipleLocator, FormatStrFormatter)


def solar_time(dt, longit):
    dt = datetime.strptime(dt, '%Y-%m-%dT%H:%M:%S.%fz')
    gamma = 2*pi/365*(dt.timetuple().tm_yday - 1 + float(dt.hour - 12)/24)
    eqtime = 229.18*(0.000075 + 0.001868 * cos(gamma) - 0.032077 * sin(gamma) \
                     - 0.014615 * cos(2 * gamma) - 0.040849 * sin(2 * gamma))
    time_offset = eqtime + 4 * longit
    tst = dt.hour * 60 + dt.minute + dt.second / 60 + time_offset
    solar_time = datetime.combine(dt.date(), time(0)) + timedelta(minutes=tst)
    return solar_time

# %% load satellite orbit
dirname_sat = r'M:\nobackup\cborger\TROPOMI\v1\L2_RPRO\NO2\2018\05\10'
f_sat = os.path.join(dirname_sat, 'S5P_RPRO_L2__NO2____20180510T104122_20180510T122441_02966_01_010202_20190203T061433.nc')
f = netCDF4.Dataset(f_sat,'r')
Data_fields=f.groups['PRODUCT']
sat_time_utc = Data_fields.variables['time_utc'][0,:]
lon = Data_fields.variables['longitude'][0,:]
lat = Data_fields.variables['latitude'][0,:]
lst_frac = np.zeros(lon.shape)
lst_frac.fill(np.nan)
for scanline in np.arange(0,3245,1):
    for ground_pixel in np.arange(0,450,1):
        lst_o = solar_time(sat_time_utc[scanline], lon[scanline, ground_pixel])
        lst_frac[scanline, ground_pixel] = lst_o.hour + lst_o.minute/60

# %% get local solar time
# ## sorbit time
flag = 1  #ascending 1, descending -1
lst_eq_c = 13.5
lst_eq_l = 12.75
lst_eq_r = 14.35
incl = 98.7
lst_cc=[]
lst_cl=[]
lst_cr=[]
lats = np.arange(-75,75,1)
for lat_sim in lats:
    arg = np.tan(np.deg2rad(lat_sim))/np.tan(np.deg2rad(incl))
    if (arg>=-1)& (arg<=1):
        lst = flag*np.arcsin(arg)*12/np.pi
        lst_c = lst_eq_c + lst
        lst_l = lst_eq_l + lst
        lst_r = lst_eq_r + lst
        
        if lst_c < 0:
            lst_c +=24
        elif lst_c > 24:
            lst_c -=24
            
        if lst_l < 0:
            lst_l +=24
        elif lst_l > 24:
            lst_l -=24
            
        if lst_r < 0:
            lst_r +=24
        elif lst_r > 24:
            lst_r -=24
    else:
        lst = -1
    lst_cc.append(lst_c)
    lst_cl.append(lst_l)
    lst_cr.append(lst_r)
lst_cc = np.array(lst_cc)
lst_cl = np.array(lst_cl)
lst_cr = np.array(lst_cr)

# %% plotting
fig,ax = plt.subplots()
ax.scatter(lst_frac.flatten(),lat.flatten(),s=1,label='s5p_footprint')
ax.set_xlim(0,24)
ax.scatter(lst_cc, lats,label='sorbit_13.5',s=2, c='orange')
#ax.scatter(lst_cl, lats,label='sorbit_12.75',s=2, c='r')
#ax.scatter(lst_cr, lats,label='sorbit_14.35',s=2, c='g')
ax.legend(loc='upper right')
ax.set_ylabel('latitude')
ax.set_xlabel('Local solar time')
ax.xaxis.set_major_locator(MultipleLocator(4))
ax.xaxis.set_major_formatter(FormatStrFormatter('%d'))
ax.xaxis.set_minor_locator(MultipleLocator(1))
plt.axvline(x=13.5, c='orange', alpha=0.5, ls=':')
plt.axhline(y=0, c='k', alpha=0.5, ls=':')
#plt.axvline(x=12.75, c='r', alpha=0.5, ls=':')
#plt.axvline(x=14.35, c='g', alpha=0.5, ls=':')
plt.tight_layout()
ax.minorticks_on()
