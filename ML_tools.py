# -*- coding: utf-8 -*-
"""
Created on Sun Feb  7 12:56:56 2021

@author: Vinod
"""
import netCDF4
import numpy as np
from MECOn_plot_vcd import calc_vcd


def arrange_emis(emis_file, month_num):
    with netCDF4.Dataset(emis_file) as f:
        mw = f.variables['NOx_flux'].molar_mass
        emis_flux = f.variables['NOx_flux'][month_num-1, :]
        mean_flux_now = mw*emis_flux/6.023E23/1000  # Kg m-2 s-1
        mean_flux_now *= 1e9  # ug m-2 s-1
    emis_list = []
    for i in range(2, mean_flux_now.shape[-2]-2):
        print(i)
        for j in range(2, mean_flux_now.shape[-1]-2):
            emis_list.append(mean_flux_now[i, j])
    return(np.asarray(emis_list))


def arrange_feature_flux(meco_out):
    with netCDF4.Dataset(meco_out) as sim_c:
        # model_lon = sim_c.variables['geolon'][:]
        # model_lat = sim_c.variables['geolat'][:]
        tm1 = sim_c.variables['COSMO_tm1'][:]-273.15
        ps_mean = np.ma.mean(sim_c.variables['ps'][:], 0)*1e-5
        hsurf = sim_c.variables['HSURF_ave'][:]
        pblh = np.ma.mean(sim_c.variables['tropop_pblh'][:], 0)*1e-3
        um1 = sim_c.variables['COSMO_um1'][:, -8:, :]
        vm1 = sim_c.variables['COSMO_vm1'][:, -8:, :]
        # ws, wd = wind(u=um1, v=vm1).merge_uv()
        # wd *= np.pi/180
        # wd_sin = np.sin(wd)
        # wd_cos = np.cos(wd)
        t2m_mean = np.ma.mean(tm1[:, -1, :], 0)
    tracer_sim_geovcd = calc_vcd(meco_out)
    vcd = tracer_sim_geovcd*1e-16
    flux_u_sfc = np.ma.mean(um1[:, -1, :] * vcd, 0)
    flux_v_sfc = np.ma.mean(vm1[:, -1, :] * vcd, 0)
    flux_u_500 = np.ma.mean(um1[:, 0, :] * vcd, 0)
    flux_v_500 = np.ma.mean(vm1[:, 0, :] * vcd, 0)
    vcd_mean = np.ma.mean(vcd, 0)

    dataset2d = []
    for i in range(2, vcd_mean.shape[-2]-2):
        print(i)
        for j in range(2, vcd_mean.shape[-1]-2):
            data_row = []
            # data_row.extend(vcd_mean[i, j].ravel())
            # data_row.extend(flux_u_sfc[i-2: i+3, j-2: j+3].ravel())
            # data_row.extend(flux_v_sfc[i-2: i+3, j-2: j+3].ravel())
            # data_row.extend(flux_u_500[i-2: i+3, j-2: j+3].ravel())
            # data_row.extend(flux_v_500[i-2: i+3, j-2: j+3].ravel())
            data_row.extend(vcd_mean[i-2: i+3, j-2: j+3].ravel())
            data_row.extend(flux_u_sfc[i, j].ravel())
            data_row.extend(flux_v_sfc[i, j].ravel())
            data_row.extend(flux_u_500[i, j].ravel())
            data_row.extend(flux_v_500[i, j].ravel())
            data_row.extend([t2m_mean[i, j]])
            data_row.extend([ps_mean[i, j]])
            data_row.extend(hsurf[i-2: i+3, j-2: j+3].ravel())
            data_row.extend([pblh[i, j]])
            dataset2d.append(data_row)
    return np.asarray(dataset2d)


def arrange_feature_vcd(meco_out):
    with netCDF4.Dataset(meco_out) as sim_c:
        model_lon = sim_c.variables['geolon'][:]
        model_lat = sim_c.variables['geolat'][:]
        tm1 = sim_c.variables['COSMO_tm1'][:]-273.15
        ps_mean = np.ma.mean(sim_c.variables['ps'][:], 0)*1e-5
        hsurf = sim_c.variables['HSURF_ave'][:]
        pblh = np.ma.mean(sim_c.variables['tropop_pblh'][:], 0)*1e-3
        um1 = np.ma.mean(sim_c.variables['COSMO_um1'][:, -8:, :], 0)
        vm1 = np.ma.mean(sim_c.variables['COSMO_vm1'][:, -8:, :], 0)
        # ws, wd = wind(u=um1, v=vm1).merge_uv()
        # wd *= np.pi/180
        # wd_sin = np.sin(wd)
        # wd_cos = np.cos(wd)
        t2m_mean = np.ma.mean(tm1[:, -1, :], 0)
    tracer_sim_geovcd = calc_vcd(meco_out)
    vcd_mean = np.ma.mean(tracer_sim_geovcd, 0)*1e-16
    flux_u = vcd_mean*um1
    flux_v = vcd_mean*vm1

    dataset2d = []
    for i in range(2, vcd_mean.shape[-2]-2):
        print(i)
        for j in range(2, vcd_mean.shape[-1]-2):
            data_row = []
            data_row.extend(vcd_mean[i-2: i+3, j-2: j+3].ravel())
            data_row.extend([t2m_mean[i, j]])
            data_row.extend([ps_mean[i, j]])
            data_row.extend(hsurf[i-2: i+3, j-2: j+3].ravel())
            data_row.extend([pblh[i, j]])
            data_row.extend([um1[-1, i, j]])
            data_row.extend([um1[0, i, j]])
            data_row.extend([vm1[-1, i, j]])
            data_row.extend([vm1[0, i, j]])
            # #####
            dataset2d.append(data_row)
    return np.asarray(dataset2d)
