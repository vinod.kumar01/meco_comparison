# -*- coding: utf-8 -*-
"""
Created on Tue Feb 23 11:07:12 2021

@author: Vinod
"""
import os
import numpy as np
from plot_tools import plt, map_prop, ccrs
from tools import gmxe
llon, ulon, llat, ulat = 0, 19, 43, 56
prop_dict = {'extent': [llon, ulon, llat, ulat], 'name': 'MECO_DE',
             'border_res': '10m'}
projection = ccrs.PlateCarree()
map2plot = map_prop(**prop_dict)

# %% load data and calculate PM2.5
dirname = r'M:\nobackup\vinod\model_work\MECO\GMXe'
filename = 'MECO_GMXe______20180501_060000_aer_physc.nc'
gmxe_data = gmxe(os.path.join(dirname, filename))

sigmas = np.asarray([1.59, 1.59, 1.59, 2.20, 1.59, 1.59, 2.0])
modes = ['M1', 'M2', 'M3', 'M4', 'M5', 'M6', 'M7']
pm25 = gmxe_data.calc_pm(sigmas=sigmas, modes=modes, upper_limit=2.5e-6)

# %% plotting
lat = gmxe_data.lat
lon = gmxe_data.lon

fig, ax = plt.subplots(subplot_kw=dict(projection=projection))
datadict = {'lat': lat, 'lon': lon,
            # 'plotable': pm25[1, :],
            'plotable': np.ma.mean(pm25[4:8, :], 0),
            'vmin': 0, 'vmax': 5,
            'text': 'PM 2.5', 'label': ''}
cs = map2plot.plot_map(datadict, ax, cmap='jet',
                       mode='pcolormesh',
                       projection=projection, alpha=1)
cb = plt.colorbar(cs, extend='max', shrink=0.6)
fig.canvas.draw()
plt.tight_layout()
cb.set_label('PM$_{2.5} (~\mu g ~m^{-3})$')
