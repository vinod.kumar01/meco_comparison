# -*- coding: utf-8 -*-
"""
Created on Mon Jul 20 11:10:13 2020
adapted from https://gist.github.com/twpayne/4409500
@author: Vinod
"""

import pyproj
import shapefile as shp
import math


_projections = {}


def zone(coordinates):
    if 56 <= coordinates[1] < 64 and 3 <= coordinates[0] < 12:
        return 32
    if 72 <= coordinates[1] < 84 and 0 <= coordinates[0] < 42:
        if coordinates[0] < 9:
            return 31
        elif coordinates[0] < 21:
            return 33
        elif coordinates[0] < 33:
            return 35
        return 37
    return int((coordinates[0] + 180) / 6) + 1


def letter(coordinates):
    return 'CDEFGHJKLMNPQRSTUVWXX'[int((coordinates[1] + 80) / 8)]


def project(coordinates):
    z = zone(coordinates)
    l = letter(coordinates)
    if z not in _projections:
        _projections[z] = pyproj.Proj(proj='utm', zone=z, ellps='WGS84')
    x, y = _projections[z](coordinates[0], coordinates[1])
    if y < 0:
        y += 10000000
    return z, l, x, y


def unproject(z, l, x, y):
    if z not in _projections:
        _projections[z] = pyproj.Proj(proj='utm', zone=z, ellps='WGS84')
    if l < 'N':
        y -= 10000000
    lng, lat = _projections[z](x, y, inverse=True)
    return (lng, lat)




#minx,maxx,miny,maxy = 5.86, 15.05, 47.27, 55.06
#dx = 0.01
#dy = 0.01
#
#nx = int(math.ceil(abs(maxx - minx)/dx))
#ny = int(math.ceil(abs(maxy - miny)/dy))
#
#w = shp.Writer(shp.POLYGON)
#w.autoBalance = 1
#w.field("ID")
#id=0
#
#for i in range(ny):
#    for j in range(nx):
#        id+=1
#        vertices = []
#        parts = []
#        vertices.append([min(minx+dx*j,maxx),max(maxy-dy*i,miny)])
#        vertices.append([min(minx+dx*(j+1),maxx),max(maxy-dy*i,miny)])
#        vertices.append([min(minx+dx*(j+1),maxx),max(maxy-dy*(i+1),miny)])
#        vertices.append([min(minx+dx*j,maxx),max(maxy-dy*(i+1),miny)])
#        parts.append(vertices)
#        w.poly(parts)
#        w.record(id)
#
#w.save('D:\python_toolbox\Igismap\DE_1km_1km_grid.shp')