# -*- coding: utf-8 -*-
"""
Created on Thu May  3 23:08:13 2018

@author: Vinod
"""

import netCDF4
#import os
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap, shiftgrid
import datetime
import os
#from matplotlib import colors as c
path = r'M:\nobackup\vinod\Manish\model_data'
filename = "REFERENCE_PM25_50RH_full.nc"
f=netCDF4.Dataset(os.path.join(path,filename),'r')
lats=f.variables['lat'][:]
lons=f.variables['lon'][:]
#levels = f.variables['lev'][:]
timestamp = f.variables['time']
d=1
date_display=datetime.datetime.strptime(timestamp.units[10:30],'%Y-%m-%d %H:%M:%S')+ datetime.timedelta(days=timestamp[d])
#h = f.variables['geopot_ave'][5,:,84,401]/9.8
#aer_profile = f.variables['aot_opt_TOT_550_total_ave'][5,:,84,401]
#aer = f.variables['aot_opt_TOT_550_total_ave'][0,30,:,:]
#tg_profile = f.variables['HCHO_ave'][5,:,84,401]*1E9
V0 = f.variables['NO2_ave'][d,30,:,:]*1E9
fig=plt.Figure()
plt.suptitle('Surface NO$_{2}$ mixing ratio for ' + date_display.strftime('%d-%b-%Y'))
ax = fig.add_subplot(1, 1, 1)
#map = Basemap(projection = 'moll', lon_0 = 0,resolution='c')
#map = Basemap(projection='robin',lon_0=0,resolution='c')
map = Basemap(projection='cyl', resolution = 'l', llcrnrlon=1, llcrnrlat=43,urcrnrlon=20, urcrnrlat=56)
map.drawcoastlines()
map.drawcountries()
#map.fillcontinents(color = 'coral')
map.drawmapboundary()
#map.drawparallels(np.arange(-90.,120.,30.),labels=[1,0,0,0])
#map.drawmeridians(np.arange(-180.,180.,60.),labels=[0,0,0,1])
map.drawparallels(np.arange(30.,60.,4.),labels=[1,0,0,0])
map.drawmeridians(np.arange(0.,20.,4.),labels=[0,0,0,1])
V0,lons = shiftgrid(180.,V0,lons,start=False)
llons, llats = np.meshgrid(lons, lats)
x,y = map(llons, llats)
#map.plot(x, y, '.', markersize=2)
cs = map.pcolormesh(x, y, V0, cmap='jet' , vmin = 0, vmax=12)
cb=map.colorbar(extend = 'both')
cb.set_label('NO$_{2}$ mixing ratio (ppb)' , fontsize=10)
plt.savefig(r'M:\home\vinod\nobackup\model_work\MECO\Plots' + '\Conc_1_normal'+'.png', format = 'png', dpi = 200)
plt.show()