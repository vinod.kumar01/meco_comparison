# -*- coding: utf-8 -*-
"""
Created on Thu Jun 21 22:31:45 2018

@author: Vinod
"""
import netCDF4
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from mpl_toolkits.basemap import Basemap
f=netCDF4.Dataset(r'M:\nobackup\vinod\NOX_land_2010_con.nc','r')
lats=f.variables['lat'][:]
lons=f.variables['lon'][:]
var = 'NOX_flux'
l=0
V0 = np.nanmean(f.variables[var][:,l,:,:],0)
V0[V0==0]=np.nan
fig=plt.Figure()
plt.title('NO surface emission flux for 2010')
#ax = fig.add_subplot(1, 1, 1)
#map = Basemap(projection = 'moll', lon_0 = 0,resolution='c')
#map = Basemap(projection='robin',lon_0=0,resolution='c')
#map = Basemap(projection='cyl', resolution = 'i', llcrnrlon=-31, llcrnrlat=30,urcrnrlon=61, urcrnrlat=72)
map = Basemap(projection='cyl', resolution = 'l', llcrnrlon=1, llcrnrlat=43,urcrnrlon=20, urcrnrlat=56)
map.drawcoastlines()
map.drawcountries(linewidth=2)
map.drawrivers(linewidth=0.2)
#map.fillcontinents(color = 'coral')
map.drawmapboundary()
map.drawparallels(np.arange(-90.,120.,10.),labels=[1,0,0,0])
map.drawmeridians(np.arange(-180.,180.,20.),labels=[0,0,0,1])
llons, llats = np.meshgrid(lons, lats)
x,y = map(llons, llats)
#map.plot(x, y, '.', markersize=2)
#cs = map.pcolormesh(x, y, V0, norm=colors.LogNorm(vmin=np.nanmin(V0), vmax=np.nanmax(V0)), cmap='jet')
cs = map.pcolormesh(x, y, V0, cmap='jet', vmin=1E14, vmax = 5E15)
cb=map.colorbar(extend = 'both')
cb.set_label(var + ' (molecules m$^{-2}$ s$^{-1}$)')



#fig=plt.Figure()
#ax = fig.add_subplot(1, 1, 1)
#map = Basemap(projection='cyl', resolution = 'l', llcrnrlon=1, llcrnrlat=43,urcrnrlon=20, urcrnrlat=56)
#map.drawparallels(np.arange(30.,60.,4.),labels=[1,0,0,0])
#map.drawmeridians(np.arange(0.,20.,4.),labels=[0,0,0,1])
#map.drawcoastlines()
#map.drawcountries()
#llons, llats = np.meshgrid(lons, lats)
#x,y = map(llons, llats)
#cs = map.pcolormesh(x, y, trop_amf_sim, cmap='jet', vmin=0, vmax=1.5)
#cb=map.colorbar(extend = 'both')
#cb.set_label("Tropospheric airmass factors", fontsize=10)