#!/bin/bash
in_path="/mnt/lustre01/work/mm0062/b302069/SAT_UBA_di5"
filename="UBA_di_7km_18_5_tr_1hr.nc"
target_grid="/mnt/lustre01/work/mm0062/b302069/meco_omi_conversion/grid_02_small.txt"
vars_list="NO2_ave,HCHO_ave,O3_ave,CO_ave,SO2_ave,tm1_ave,um1_ave,vm1_ave"  #pblh_ave,NO2_ave ##!!!mention all the variable which can be regridded separated by comma
IFS=,
vars=($vars_list)
for key in "${!vars[@]}"
 do echo "${vars[$key]}"
done
cd ${in_path}
cp -f ${filename} temp1.nc
ncrename -v .geolon,lon -v .geolat,lat -v .ps,aps -v .geolon_ave,lon -v .geolat_ave,lat temp1.nc
ncrename -v .ps_ave,aps temp1.nc
ncks -C -A -v rotated_pole /mnt/lustre01/work/mm0062/b302069/meco_omi_conversion/cosmo_orig_output.nc temp1.nc
#for DE domain
ncatted -a grid_north_pole_latitude,rotated_pole,m,f,40.0 -a grid_north_pole_longitude,rotated_pole,m,f,-170.0 temp1.nc
ncatted -a grid_mapping,aps,c,c,rotated_pole temp1.nc
ncatted -a coordinates,aps,c,c,'lon lat' temp1.nc
for key in "${!vars[@]}"
        do ncatted -a grid_mapping,${vars[$key]},c,c,'rotated_pole' temp1.nc
           ncatted -a coordinates,${vars[$key]},c,c,'lon lat' temp1.nc
done
cdo setgridtype,curvilinear temp1.nc temp2.nc
#outname=$(echo $filename | rev | cut -d"." -f2-  | rev)
#cdo remapbil,${target_grid} temp2.nc ${outname}_regrid.nc
#rm -f temp2.nc
#cdo setzaxis,/mnt/lustre01/work/mm0062/b302069/meco_omi_conversion/zaxis_l40.txt test.nc test2.nc
#cdo -b F64 remapeta,/mnt/lustre01/work/mm0062/b302069/meco_omi_conversion/remapeta_omi34.txt test2.nc test3.nc
#out_name="${filename%.*}"
#out_name=${out_name}_omi25s.nc 
#cdo remapbil,/mnt/lustre01/work/mm0062/b302069/meco_omi_conversion/grid_025.txt  test3.nc ${out_name}
##cdo sellonlatbox,0,20,40,60 test4.nc ${out_name}
#rm -rf temp1.nc test*.nc
