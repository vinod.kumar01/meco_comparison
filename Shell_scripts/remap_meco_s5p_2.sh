!/bin/bash
in_path="/mnt/lustre01/work/mm0062/b302069/SAT_UBA_di5"
filename="UBA_di_05_sorbit_s5ar.nc"
cd ${in_path}
cp -f ${filename} temp1.nc
# ignore 10 pixels from the border
#ncks -O -d glat,-5.25,4.4375 -d glon,-4.5,3.8125 temp1.nc temp1.nc
ncrename -v geolon,lon -v geolat,lat -v ps,aps temp1.nc
ncks -C -A -v rotated_pole /mnt/lustre01/work/mm0062/b302069/meco_omi_conversion/cosmo_orig_output.nc temp1.nc
ncatted -a grid_north_pole_latitude,rotated_pole,m,f,40.0 -a grid_north_pole_longitude,rotated_pole,m,f,-170.0 temp1.nc
ncap2 -O -s 'tracer_gp_NO2_conc[time,lev,glat,glon]=tracer_gp_NO2*6.023E23*(dhyam+dhybm*aps)*1E-6/8.314/COSMO_tm1' temp1.nc
ncap2 -O -s 'tracer_gp_HCHO_conc[time,lev,glat,glon]=tracer_gp_HCHO*6.023E23*(dhyam+dhybm*aps)*1E-6/8.314/COSMO_tm1' temp1.nc
ncatted -a long_name,'tracer_gp_HCHO_conc',m,c,'HCHO concentration' -a units,'tracer_gp_HCHO_conc',m,c,'molecules cm-3' temp1.nc
ncatted -a long_name,'tracer_gp_NO2_conc',m,c,'NO2 concentration' -a units,'tracer_gp_NO2_conc',m,c,'molecules cm-3' temp1.nc
ncatted -a grid_mapping,'lsthr',c,c,'rotated_pole' -a grid_mapping,'aps',c,c,'rotated_pole' -a grid_mapping,'^tracer',c,c,'rotated_pole'  -a grid_mapping,'^COSMO',c,c,'rotated_pole' temp1.nc
ncatted -a coordinates,'lsthr',c,c,'lon lat' -a coordinates,'aps',c,c,'lon lat' -a coordinates,'^tracer',c,c,'lon lat' -a coordinates,'^COSMO',c,c,'lon lat' temp1.nc
cdo -O setgridtype,curvilinear temp1.nc test.nc
ncks -O -x -v dhyam,dhybm,dt,nstep test.nc test.nc
cdo -O setzaxis,/mnt/lustre01/work/mm0062/b302069/meco_omi_conversion/zaxis_l40.txt test.nc test2.nc
out_name="${filename%.*}"
out_name=${out_name}_inc_border.nc 
out_name_regrid=${out_name}_s5p_0625.nc 
cdo -O -b F64 remapeta,/mnt/lustre01/work/mm0062/b302069/meco_omi_conversion/remapeta_s5p_34.txt test2.nc ${out_name}
#out_name=${out_name}_s5p_0625.nc 
#cdo -O remapbil,/mnt/lustre01/work/mm0062/b302069/meco_omi_conversion/grid_0625.txt ${out_name} ${out_name_regrid}
#cdo sellonlatbox,0,20,40,60 test4.nc ${out_name}
rm -f temp1.nc test*.nc
